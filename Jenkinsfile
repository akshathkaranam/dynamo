def buildComponent = "Dynamo-frontend"
def buildFileName = "webapp.zip"
def buildFolderName="webapp"
//def buildDirectory = "gsnotificationgateway"
def ecrName = "076544065911.dkr.ecr.us-east-1.amazonaws.com"
def ecrProject = "dynamo-frontend"
//def oldImage = "076544065911.dkr.ecr.us-east-1.amazonaws.com/notificationgw-stage:v1.34"
def newImage = ""
//def testAutomationPath = "notificationgatewaytestautomation"

pipeline {

    agent{ 
                label 'build01-node'
            }

    options {
	    //Only keep the 30 most recent builds
	    buildDiscarder(logRotator(numToKeepStr:'30'))
	    disableConcurrentBuilds()
	 }

     stages {

         stage ('Choose your pipeline stages'){


			steps {
				script {

					env.DEPLOY_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'DEPLOY_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Deploy stage?')]
					env.TESTING_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'TESTING_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Testing stage?')]
				}
				echo "Deploy stage: ${env.DEPLOY_STAGE_CHOICE}"
				echo "Testing stage: ${env.TESTING_STAGE_CHOICE}"
				
			}
		}

         stage ('Initialization & Preparation') {
            
            steps {          	 
					
                    sh """
                        echo "Starting CI for notification gateway"
                        
                    """
                 }
        }

        stage ('Building Dynammo app') {

            steps{
                
                sh"""
                ./build.sh ${env.BUILD_ID}
                pwd
                """
                script{
                nexusArtifactUploader artifacts: [[artifactId: 'dynamo', classifier: '', file: "${buildFileName}", type: 'zip']], credentialsId: 'Nexus-Stage', groupId: 'com.gigsky', nexusUrl: 'nexus.gskystaging.com', nexusVersion: 'nexus3', protocol: 'http', repository: 'frontend-releases', version: "1.0.0-${env.BUILD_ID}"
               
                }
            }
            post {

                success {
			      //archive "target/**/*"			      
			      archiveArtifacts artifacts:"${buildFileName}"
                  stash includes: "${buildFileName}", name: "${buildComponent}"
                  echo "Build successfully completed. Artifacts have been archived"			      
			    }

			    failure {
			    	echo "Build failed. Please check logs"
			    }
            }
        }

        /*stage ('Unit Tests') {
                //Placeholder for unit tests stage
        }*/

         /*stage (SonarQube Analysis) {
                //Placeholder for SonarQube stage
        }*/

        stage ('Dockerize Dynamo and push to ECR') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }

            steps{
            unstash "${buildComponent}"
            script{
                newImage = "${ecrName}/${ecrProject}:1.0.0-${env.BUILD_ID}"
            }
            sh """
            pwd
            mkdir ${buildFolderName}
            unzip -o ${buildFileName} -d ${buildFolderName}
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}
            docker build -t ${newImage} .
            docker push ${newImage}
            pwd
            rm -r ${buildFolderName}
            ls -lhrt
            """
            }

        }


        /*
        stage ('Deploy using Docker Compose') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }
            agent{ 
                label 'testnode_2a'
             }

            steps{
            unstash "${buildComponent}"    
            sh """
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}/${ecrProject}
            mkdir ngw-db-data
            IMAGE_ID=${newImage} docker-compose up -d
            sleep 60
            """
            }
        }*/

        /*
        stage ('QA Automation') {
                when {
				expression {
						return env.TESTING_STAGE_CHOICE=='execute';
					}
            }

            agent{ 
                label 'testnode_2a'
             }
                //Placeholder for QA Automation stage


            steps{
                		        
		    	echo "..............................\nExecuting Test NG based testing\n.............................."
		    	unstash "${buildComponent}" 
	   			configFileProvider([configFile(fileId: 'nexus-maven-settings', variable: 'NexusSettings')]) {
		   		
		    		catchError {
		    			sh "mvn -f ${testAutomationPath}/pom.xml -DSUITE_XML_FILES='suites/AllSuites.xml' -DCONFIG_FILE='local_mac/config_keyvalue.txt' clean test"
		    		}
		
				}
					   
										
		
		    	}		    	 
 
		    
			post {
				always {
                    
                     publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/html/', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                     step([$class: 'Publisher', reportFilenamePattern: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/*.xml'])
                     }

                success {		
                	echo "TestNG Execution Successful"
			    }

			    failure {
			    	echo "Ops! Maven build failed for Test automation"
			        }
			    }	
            }*/
        }
        


     post {


		cleanup {
  
            echo "cleaning the workspace for this branch to avoid leaving unnecessary directories. It is understood that it will impact the build performance negatively in the next run on this branch"
            cleanWs deleteDirs: true
            
    }

	} 
}
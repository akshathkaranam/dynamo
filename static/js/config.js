/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
//var v=window.location.search;if(v.indexOf("sl=ja")!=-1){document.body.setAttribute("data-cur-lang","ja");}
var IS_CAPTIVE_MODE = false;

if(window.location.hostname.indexOf("captive.gigsky.com")!=-1)
{
	IS_CAPTIVE_MODE = true;
}

var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.Config = {};
var lh = window.location.hostname;

var   APPLE_SIM_URL = '/gigsky-apple-sim/';
var   GIGSKY_SIM_URL = '/gigsky-data-sim';
var   BUSINESS_URL = '/business';
var   WEBAPP_URL = "/";
var   GIGSKY_STATIC_URL = '/';

ensuant.gigsky.Config.gsAppleSIMURL = APPLE_SIM_URL;
ensuant.gigsky.Config.gsGigSkySIMURL = GIGSKY_SIM_URL;
ensuant.gigsky.Config.gsStaticURL = '/';

if (lh.indexOf('app.gigsky.com') != -1) {
	ensuant.gigsky.Config.gsAppleSIMURL = '//www.gigsky.com/index.php' + APPLE_SIM_URL;
	ensuant.gigsky.Config.gsGigSkySIMURL = '//www.gigsky.com/index.php' + GIGSKY_SIM_URL;
	ensuant.gigsky.Config.gsStaticURL = '//www.gigsky.com'
}else if(lh.indexOf('app-stage.gigsky.com') != -1){
	ensuant.gigsky.Config.gsAppleSIMURL = '//www-stage.gigsky.com/index.php' + APPLE_SIM_URL;
	ensuant.gigsky.Config.gsGigSkySIMURL = '//www-stage.gigsky.com/index.php' + GIGSKY_SIM_URL;
	ensuant.gigsky.Config.gsStaticURL = '//www-stage.gigsky.com'
}
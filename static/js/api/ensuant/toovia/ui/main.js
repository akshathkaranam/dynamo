/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.toovia = ensuant.toovia ? ensuant.toovia : {};
ensuant.toovia.ui = ensuant.toovia.ui ? ensuant.toovia.ui : {};

$(document).ready(function(){

    ensuant.toovia.ui.Main = function() {

        var _VERSION = '0.0.4';

        var _init = function(){
            _checkMobile();
            _initProcessors();
            _assignEvents();
        };

        var _initProcessors = function(){
            //process all page processors on main scope
            ensuant.toovia.ui.Utils.initProcessors({
                scope: $('body'),
                scopeType: 'main',
                type: 'page',
                options: {}
            });

            ensuant.toovia.ui.Utils.initProcessors({
                scope: $('body'),
                scopeType: 'main',
                type: 'widget',
                options: {}
            });
        };

        var _assignEvents = function(){
            $(window).resize(function(e){
                _checkMobile();
            });
        };

        var _checkMobile = function(){
            if ($(window).width() < 960 || JSUtils.isMobile.any()) {
                $('body').addClass('mobile-ui');

                if(JSUtils.isTablet.any()){
                    $('body').addClass('wide');
                }
                return false;
            }else if(JSUtils.isTablet.any()){
                if(JSUtils.isTablet.any()){
                    $('body').addClass('tablet-ui');
                }
                return false;
            }

            $('body').removeClass('mobile-ui tablet-ui wide');
        };

        _init();

        return{
            VERSION: _VERSION,
            parse: _initProcessors
        };
    }();
});
/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.toovia = ensuant.toovia ? ensuant.toovia : {};
ensuant.toovia.ui = ensuant.toovia.ui ? ensuant.toovia.ui : {};
ensuant.toovia.ui.Utils = function() {
	
	var _VERSION = '0.0.15';
	var _overlay = null;
	var _pageLoadingSpinner = null;

    var getSpinnerOptions = function(options){

        return {
            lines: (options.lines)?options.lines:14, // The number of lines to draw
            length: (options.length)?options.length:12, // The length of each line
            width: (options.width)?options.width:3, // The line thickness
            radius: (options.radius)?options.radius:15, // The radius of the inner circle
            corners: (options.corners)?options.corners:1, // Corner roundness (0..1)
            rotate: (options.rotate)?options.rotate:0, // The rotation offset
            direction: (options.direction)?options.direction:1, // 1: clockwise, -1: counterclockwise
            color: (options.color)?options.color:'#000', // #rgb or #rrggbb or array of colors
            speed: (options.speed)?options.speed:1, // Rounds per second
            trail: (options.trail)?options.trail:60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent in px
            left: '50%' // Left position relative to parent in px
        };
    };
	
	var loadClass = function(options){
		if(!options.noConversion){
			var clz = pathToClass(options.path);
			if(clz){
				if(options.success){
					options.success()
				}
				
				return;
			}
		}
		
		var path;
		if(options.noConversion){
			path = options.path;
		}else{
			path = _pathToActualPath(options.path);
		}
		
		var ajaxOptions = {
			url: path,
			dataType: "script",
			cache: true,
			success: function(response){
				if(options.success){
					options.success(response);
				}
			}, 
			error : function(jqXHR, textStatus, errorThrown) {
				if(options.error){
					options.error(jqXHR, textStatus, errorThrown);
				}
			}
		};
		
		if(typeof options.async === 'boolean'){
			ajaxOptions.async = options.async; 
		}
		
		$.ajax(ajaxOptions);
	};
	
	//hiphenated string is converted to camelcase
	var pathToClass = function(path, fetchIfNotExists){
		var index = path.lastIndexOf('.');
		var pkg = path.substring(0, index + 1);
		var clzName = path.substring(index + 1);
		var clz = pkg + JSUtils.String.hiphenatedToCamelCase(clzName, true);
		
		var parts = clz.split('.');
		var curr = window;
		var nxt;
		while(nxt = parts.shift()) {
			if(!curr) break;
			curr = curr[nxt];
		}
		
		if(!curr && fetchIfNotExists){
			var curr = null;
			loadClass({
				path: path,
				async: false,
				success: function(response){
					curr = pathToClass(path);
					return curr;
				},
				error : function(jqXHR, textStatus, errorThrown) {
					//TODO what to do?
					log({
						msg: textStatus
					});
				}
			});
		}
		
		return curr;
	};
	
	var _pathToActualPath = function(path){
		var appendStr = '';
		//enable when ready
		
		return 'static/js/api/' + path.replace(/\./g, '/') + appendStr + '.js';
	};
	
	//only works with classes that implement call function
	var process = function(options){
		loadClass({
			path: options.path,
			success: function(response){
				var f = pathToClass(options.path);
				var callOptions = options.options ? options.options : {};
				callOptions.container = options.element;
				f.call(callOptions);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				//TODO what to do?
				log({
					msg: textStatus
				});
			}
		});
	};
	
	var initProcessors = function(options){
		var selector = '[data-' + options.type + '-processor]';
		if(options.scopeType){
			selector += '[data-scope="' + options.scopeType + '"]';
		}
		
		$.each(options.scope.find(selector), function(k, card){
			var $card = $(card);
			var processor = $card.attr('data-' + options.type + '-processor');
			ensuant.toovia.ui.Utils.process({
				element: $card,
				path: processor,
				options: $.extend(true, {}, options.options)
			});
		});
	};
	
	var reload = function(withoutKey){
		window.location = window.location.href.split('#')[0];
	};
	
	var redirect = function(url, withoutKey){
		var key = JSUtils.getUrlParams()['key'];
		var newKey = JSUtils.getUrlStringParams(url)['key'];
		
		//only append if there is no new key specified
		if(!newKey && key){
			if(url.indexOf('?') !== -1){
				url += '&';
			}else{
				url += '?';
			}
			
			url += 'key=' + key;
		}
		
		window.location = url;
	};
    
    var getTemplate = function(options){
    	var response = null;
    	
        ensuant.toovia.framework.Ajax.Consumer.call({
            dataType: 'html',
            url: options.url,
            async: (options.callback) ? true : false,
            type: options.type ? options.type : 'GET',
            success:function (html) {
                response = $.trim(html);
                var error = true;
                if(response && response.length > 0){
                    error = false;
                }
                if(options.callback){
                    var callbackOptions = (options.callbackOptions) ? options.callbackOptions : {};
                    if(error){
                    	callbackOptions.status = 'Error';
                    } else {
                    	callbackOptions.status = 'Success';
                    	callbackOptions.html = response;
                    }
                    options.callback(callbackOptions);
                }
            }
        });
        if(!options.callback) return response;
    };
    
    var displayProgress = function(options){
    	var target = null;
    	if(!options) options = {};
    	var spinerOptions = getSpinnerOptions(options);
    	
    	if(options.$container){
            target = options.$container.get(0);
    	}else{
    		if(_overlay) return;
        	
        	var $overlay = $('<div style="position: fixed; width: 100%; height: 100%; z-index: 99999999; background-color: transparent; left: 0; top: 0;"></div>');
        	
        	if(options.message){
        		var $message = $('<div style="position: fixed; width: 100%; text-align: center; top: 50%;"></div>');
        		$message.text(options.message);
        		$overlay.append($message);
        		
        		$message.css({
        			'margin-top': (spinerOptions.radius * 3) + 'px',
        			'font-size': (spinerOptions.radius * 2) + 'px',
        			color: spinerOptions.color
        		});
        	}
        	
        	$('body').append($overlay);
        	_overlay = $overlay[0];
        	target = _overlay;
    	}
		
		_pageLoadingSpinner = new Spinner(spinerOptions).spin(target);
    };
    
    var hideProgress = function(options){
    	if(_pageLoadingSpinner) _pageLoadingSpinner.stop();
    	if(!_overlay || !_overlay.parentElement) return;
    	
		_overlay.parentElement.removeChild(_overlay);
		_overlay = null;
    };

	var log = function (options) {
	    if(typeof bakpak === 'undefined' || bakpak.env !== 'PROD'){
	    	console.log(options.msg);
	    }
	};
	
	return{
		VERSION: _VERSION,
		loadClass: loadClass,
		process: process,
		pathToClass: pathToClass,
		reload: reload,
		redirect: redirect,
		getTemplate: getTemplate,
		initProcessors: initProcessors,
		displayProgress: displayProgress,
		hideProgress: hideProgress,
		log: log
	};
	
}();
/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.ui = ensuant.ui ? ensuant.ui : {};
ensuant.ui.History = function () {

    var _VERSION = '0.0.12';
    var _history = null;
    var _urlMap = {};
    var _lock = false;
    var _state1 = null;
    
    var _init = function(options){
        _history = window.History;
        if ( !_history.enabled ) {
            return false;
        }
        
        _state1 = {
        	url: decodeURI(window.location.href)	
        };
        
        _assignEvents();
        _initHistoryElements();
    };
    
    var _assignEvents = function(){
    	_history.Adapter.bind(window, 'statechange', function(){
            var state = _history.getState();
            
            if(!_lock){
            	var options = _urlMap[decodeURI(window.location.href)];
                if(options){
                	_execute(options, state.data);
                }
                
                if(state.data && state.data.topic){
                	EJM.Topic(state.data.topic).publish({
                    	data: state.data,
                    	url: state.url
                    });
                }
            }
            
            _lock = false;
        });
    	
    	$('body').on('click', '[history-tab]', function(e){
    		var $this = $(this);
    		var url = $this.attr('history-tab');
    		var title = $this.attr('history-title');
    		if(!title){
    			title = $('head title').html();
    		}
    		var targetURL = _formTabURL(url);
    		_replaceState(null, title, targetURL);
    	});
    	
    	$('body').on('click', '[history-page],[history-navpage]', function(e){
    		var $this = $(this);
            var url = null;
            if($this.attr('history-page')){
                url = $this.attr('history-page');
            } else {
                url = $this.attr('history-navpage');
            }
    		var title = $this.attr('history-title');
    		if(!title){
    			title = $('head title').html();
    		}
    		var targetURL = _formPageURL(url);
    		_pushState(null, title, targetURL);
    	});
    };
    
    var _formTabURL = function(uri){
    	var adjusted = window.location.href;
		if(window.location.href.indexOf('h-tab') !== -1){
			adjusted = window.location.href.substring(0, window.location.href.indexOf('h-tab') - 1);
		}
		
    	if(!uri || $.trim(uri).length === 0){
    		return adjusted;
    	}
		
    	if(adjusted.indexOf('?') === -1){
    		return adjusted + '?h-tab=' + uri;
    	}else{
    		return adjusted + '&h-tab=' + uri;
    	}
    };
    
    var _formPageURL = function(uri){
    	var adjusted = window.location.href;
		if(window.location.href.indexOf('h-page') !== -1){
			adjusted = window.location.href.substring(0, window.location.href.indexOf('h-page') - 1);
		}
		
    	if(!uri || $.trim(uri).length === 0){
    		return adjusted;
    	}
		
    	if(adjusted.indexOf('?') === -1){
    		return adjusted + '?h-page=' + uri;
    	}else{
    		return adjusted + '&h-page=' + uri;
    	}
    };
    
    var _initHistoryElements = function(){

    	var $historyTabs = $('[history-tab]');
    	$.each($historyTabs, function(k, tab){
    		var $this = $(tab);
    		var url = $this.attr('history-tab');
    		var selector = '[history-tab="' + $this.attr('history-tab') + '"]';
    		
    		var targetURL = _formTabURL(url);

//			_register(targetURL, {
//    			type: 'trigger',
//    			event: 'click',
//    			targetSelector: selector
//    		});
            _register(targetURL, {
                type: 'callback',
                callback: function(data){
                    $(selector).tab('show');
                },
                targetSelector: selector
            });
			$this.data('history-registered', true);
    	});

        var nav = false;
    	var $historyPages = $('[history-page]');
        if($historyPages && $historyPages.length == 0){
            $historyPages = $('[history-navpage]');
            if($historyPages && $historyPages.length > 0){
                nav = true;
            }
        }
    	$.each($historyPages, function(k, page){
    		var $this = $(page);
    		var url = (nav)?$this.attr('history-navpage'):$this.attr('history-page');
    		var selector = (nav)?'[history-navpage="' + $this.attr('history-navpage') + '"]':'[history-page="' + $this.attr('history-page') + '"]';
    		
    		var targetURL = _formPageURL(url);

            if(nav){
                _register(targetURL, {
                    type: 'trigger',
                    event: 'click',
                    targetSelector: selector
                });
            } else {
                _register(targetURL, {
                    type: 'callback',
                    callback: function(data){
                        $(selector).tab('show');
                    },
                    targetSelector: selector
                });
            }
			$this.data('history-registered', true);
    	});
    };
    
    var _pushState = function(data, title, url){
    	var state = _history.getState();
    	
    	if(window.location.href === url){
    		return;
    	}
    	
    	_lock = true;
    	_history.pushState(data, title, decodeURI(url));
    };
    
    var _replaceState = function(data, title, url){
    	var state = _history.getState();
    	
    	if(window.location.href === url && !data){
    		return;
    	}
    	
    	if(window.location.href !== url){
    		_lock = true;
    	}
    	_history.replaceState(data, title, decodeURI(url));
    };
    
    var _execute = function(options, data){
    	if(options.type === 'trigger'){
    		var $target = $(options.targetSelector);
    		
    		if($target.length > 0){
    			$target.trigger(options.event);
    		}
    	}else if(options.type === 'callback'){
    		if(options.callback){
    			options.callback(data);
    		}
    	}
    };
    
    var _getState = function(){
    	return _history.getState();
    };
    
    var _back = function(){
    	return _history.back();
    };
    
    var _forward = function(){
    	return _history.forward();
    };
    
    var _register = function(url, options, registerOnly){
    	_urlMap[decodeURI(url)] = options;
    	
    	if(registerOnly){
    		return;
    	}
    	
    	if(_history.enabled && //_history.savedStates.length === 1 && 
    			_compareURL(_state1.url, url)){
    		_execute(options);
    	}
    };
    
    var _compareURL = function(stateURL, url){
    	return stateURL === url || encodeURI(stateURL) === url;
    };
    
    _init();

    return {
    	VERSION: _VERSION,
        register: _register,
        getState: _getState,
        pushState: _pushState,
        replaceState: _replaceState,
        back: _back,
        forward: _forward,
        initHistoryElements: _initHistoryElements
    };

}();
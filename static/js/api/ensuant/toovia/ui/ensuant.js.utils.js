/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var JSUtils = function(){

	var _VERSION = '0.5.68';
	var getUrlParams = function(allowMultipleParamSameName){
		return getUrlStringParams(window.location, allowMultipleParamSameName);
	};

    var getUrlStringParams = function(urlString, allowMultipleParamSameName){
		var urlParams = {};
		var aTag = document.createElement('a');
		aTag.href = urlString;
		
		var e,
		a = /\+/g,  // Regex for replacing addition symbol with a space
		r = /([^&=]+)=?([^&]*)/g,
		d = function (s) { return decodeURIComponent(s.replace(a, ' ')); },
		q = aTag.search.substring(1);

		if(allowMultipleParamSameName){
			while (e = r.exec(q)){
				if(urlParams[d(e[1])]){
					if(urlParams[d(e[1])] instanceof Array){
						urlParams[d(e[1])].push(d(e[2]));
					}else{
						var array = [urlParams[d(e[1])]];
						urlParams[d(e[1])] = array;
						array.push(d(e[2]));
					}
				}else{
					urlParams[d(e[1])] = d(e[2]);
				}
			}
		}else{
			while (e = r.exec(q)){
				urlParams[d(e[1])] = d(e[2]);
			}
		}
		
		return urlParams;
	};


    var copyAttributes = function($a, $b){
		for(var i = 0, a = $a.get(0).attributes, d = $b.eq(0); i < a.length; i++){
			d.attr(a[i].name, a[i].value);
		}
	};
	
	var String = function(){
		var startsWith = function(str, prefix){
			return str.indexOf(prefix) === 0;
		};
		
		var endsWith = function(str, suffix){
			return str.indexOf(suffix, str.length - suffix.length) !== -1;
		};
		
		var containSpecialCharacters = function(str){
			return str.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/);
		};
		
		var replaceNonAlphaNumeric = function(str){
			return str.replace(/[^a-z0-9]/gi, '');
		};
		
		var isEmpty = function(str){
			return !str || $.trim(str).length === 0; 
		};
		
		var getRequiredHeight = function(text, width, padding, font) {
			var f = font || '12px arial',
			$o = $('<div>' + text + '</div>')
		    	.css({width: width, padding: padding, position: 'absolute', 'float': 'left', 'white-space': 'pre-wrap', visibility: 'hidden', font: f})
		    	.appendTo($('body')),
		    	h = $o.outerHeight(true);

			$o.remove();

			return h;
		};
		
		var charactersInOneLine = function($el) {
			var font = $el.css('font-family');
			var width = $el.innerWidth();
			var text = $el.text();
			var f = font || '12px arial';
			$o = $('<span></span>')
		    	.css({'white-space': 'nowrap', 'visibility': 'hidden', font: f})
		    	.appendTo($('body'));
			
			var fit = text.length;
			for (var i = 0; i < fit; ++i) {
				$o.append(text[i]);
				if ($o.innerWidth() > width) {
					fit = i - 1;
					break;
				}
			}

			$o.remove();

			return fit;
		};
		
		var charactersWidth = function($el, length) {
			var font = $el.css('font-family');
			var text = $el.text();
			text.substring(0, length < text.length ? length: text.length);
			var f = font || '12px arial';
			
			var $c = $('<canvas style="visibility: hidden;"></canvas>');
			$('body').append($c);
			var c = $c[0];
			
			var ctx= c.getContext('2d');
			ctx.font = f;
			var calculated = ctx.measureText(text).width;
			$c.remove();
			return calculated;
		};
		
		var fitText = function($textarea){
			var text = $textarea.val();
			var width = $textarea.width();
			var fontCSS = $textarea.css('font');
			var paddingCSS = $textarea.css('padding');
			var h = getRequiredHeight(text, width, paddingCSS, fontCSS);
			
			var minHeightCSS = $textarea.css('min-height');
			var minHeight = 0;
			if(minHeightCSS){
				minHeight = parseFloat(minHeightCSS.split('p')[0]);
			}
			
			if(h > minHeight){
				$textarea.css('height', h + 'px');
			}
		};
		
		var hiphenatedToCamelCase = function(str, includeFirst){
			var newStr = str.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
			if(includeFirst){
				return newStr[0].toUpperCase() + newStr.substring(1); 
			}else{
				return newStr;
			}
		};
		
		var blindTruncate = function($t){
			var limit = parseInt($t.attr('data-char-limit-truncate'));
			var text = $t.text();
			if(text.length > limit){
				$t.text(text.substring(0, limit) + '...');
			}
		};
		
		var textDiff = function(first, second) {
	        var start = 0;
	        while (start < first.length && first[start] == second[start]) {
	            ++start;
	        }
	        var end = 0;
	        while (first.length - end > start && first[first.length - end - 1] == second[second.length - end - 1]) {
	            ++end;
	        }
	        end = second.length - end;
	        return second.substr(start, end - start);
		};
		
		return {
			isEmpty: isEmpty,
			startsWith: startsWith,
			endsWith: endsWith,
			containSpecialCharacters: containSpecialCharacters,
			replaceNonAlphaNumeric: replaceNonAlphaNumeric,
			fitText: fitText,
			hiphenatedToCamelCase: hiphenatedToCamelCase,
			blindTruncate: blindTruncate,
			charactersInOneLine: charactersInOneLine,
			charactersWidth: charactersWidth,
			textDiff: textDiff
		};
	}();
	
	var Web = function(){
		var replaceParam = function(url, name, value){
			var curParams = getUrlStringParams(url, true);
			var path = url.split('?')[0];
			
			var count = 0;
			var found = false;
			$.each(curParams, function(k, v){
				if(k === name){
					if(count++ === 0){
						path += '?';
					}else{
						path += '&';
					}
					
					if(value instanceof Array){
						var innerCount = 0;
						var checkDup = {};
						$.each(value, function(kk, vv){
							if(checkDup[vv]) return true;
							
							if(innerCount++ > 0){
								path += '&';
							}
							path += k + '=' + vv;
							
							checkDup[vv] = true;
						});
					}else{
						path += k + '=' + value;
					}
					
					found = true;
				}else{
					if(count++ === 0){
						path += '?';
					}else{
						path += '&';
					}
					
					if(v instanceof Array){
						$.each(v, function(kk, vv){
							if(kk > 0){
								path += '&';
							}
							
							path += k + '=' + vv;
						});
					}else{
						path += k + '=' + v;
					}
				}
			});
			
			if(!found){
				if(count++ === 0){
					path += '?';
				}else{
					path += '&';
				}
				
				if(value instanceof Array){
					var innerCount = 0;
					var checkDup = {};
					$.each(value, function(k, v){
						if(checkDup[v]) return true;
						if(innerCount++ > 0){
							path += '&';
						}
						path += name + '=' + v;
						
						checkDup[v] = true;
					});
				}else{
					path += name + '=' + value;
				}
			}
			
			return path;
		};
		
		var removeParam = function(url, name){
			var curParams = getUrlStringParams(url, true);
			var path = url.split('?')[0];
			
			var count = 0;
			$.each(curParams, function(k, v){
				if(k !== name){
					if(count++ === 0){
						path += '?';
					}else{
						path += '&';
					}
					
					if(v instanceof Array){
						$.each(v, function(kk, vv){
							if(kk > 0){
								path += '&';
							}
							
							path += k + '=' + vv;
						});
					}else{
						path += k + '=' + v;
					}
				}
			});
			
			return path;
		};
		
		var partialUrlEncode = function(s){
	    	return s.replace(/&/g,'%26').replace(/#/g,'%23').replace(/'+'/g,'%2B');
	    };
		
		return {
			replaceParam: replaceParam,
			removeParam: removeParam,
			partialUrlEncode: partialUrlEncode,
		};
	}();
	
	var Type = function(){
		var validateEmail = function(email){
			var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return regex.test(email);
		};
	
		var validateURL = function(url){
			return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
		};
	
		return{
			validateEmail: validateEmail,
			validateURL: validateURL
		};
	}();
	
	var Form = function(){
		var displayErrorForField = function($scope, field, message){
			var $field = $scope.find('[data-input="' + field + '"]');
			if($field.length === 0) return;
			
			var $help = $field.siblings('.help-inline, .help-block');
			if($help.length === 0 && ($field.attr('type') === 'radio' || $field.attr('type') === 'checkbox')){
				$help = $field.parent().siblings('.help-inline, .help-block');
			}
			
			if(message){
				$help.text(message);
			}
			
			$help.removeClass('hide');
			$field.closest('.control-group, .form-group').addClass('error has-error');
		};
		
		var hideErrorForField = function($scope, field){
			var $field = $scope.find('[data-input="' + field + '"]');
			if($field.length === 0) return;
			
			var $help = $field.siblings('.help-inline, .help-block');
			if(!$help.hasClass('hide')){
				$help.addClass('hide');
			}
			
			$field.closest('.control-group, .form-group').removeClass('error has-error');
		};
		
		var clear = function($scope, field){
			var $field = $scope.find('[data-input="' + field + '"]');
			if($field.length === 0){
				$field = $scope.find('[data-field="' + field + '"]');
			}
			
			if($field.length === 0) return;
			
			if($field.attr('type') === 'text' || $field[0].tagName === 'TEXTAREA'){
				$field.val('');
			}else if($field.attr('type') === 'checkbox'){
				$field.removeAttr('checked');
			}
		};
		
		var clearForm = function($scope){
			$.each($scope.find('[data-input]'), function(k, field){
				var $field = $(field);
				var name = $field.attr('data-input');
				clear($scope, name);
			});
			
			$.each($scope.find('[data-field]'), function(k, field){
				var $field = $(field);
				var name = $field.attr('data-field');
				clear($scope, name);
			});
		};
		
		var get = function($scope, field){
			var $field = $scope.find('[data-input="' + field + '"]');
			if($field.length === 0){
				$field = $scope.find('[data-field="' + field + '"]');
			}
			
			if($field.length === 0) return;
			
			if($field.attr('data-type') === 'color'){
				var value = $field.attr('data-color');
				if(!value){
					value = $.trim($field.val());
				}
				
				if(!value) return null;
				
				if(value.indexOf('#') === -1){
					value = '#' + value;
				}
				return Color.hexToRgb(value);
			}else if($field.attr('data-type') === 'node'){
				return $field.attr('data-node-uri');
			}else if($field.attr('data-type') === 'node-or-text'){
				var value = $field.attr('data-node-uri');
				if(!value){
					value = $.trim($field.val());
				}
				return value;
			}else if($field.attr('data-type') === 'switch-toggle'){
				return $field.hasClass('active');
			}else if($field.attr('data-type') === 'date-range'){
				return $field.data('data');
			}else if($field.attr('data-type') === 'internal-url'){
				return $field.attr('data-node-uri');
			}else if($field.attr('data-type') === 'date'){
				return $field.data('data');
			}else if($field.attr('data-type') === 'time-select'){
				var data = $field.val()
				if(data){
					return DateTime.parseTime(data);
				}else{
					return null;
				}
			}else if($field.attr('data-type') === 'time'){
				var data = $.timePicker($field).getTime()
				if(data){
					return {
						hour: data.getHours(),
						minute: data.getMinutes()
					};
				}else{
					return null;
				}
			}else if($field.attr('data-type') === 'address'){
				return $field.data('data');
			}else if($field.attr('data-type') === 'google-address'){
				return $field.data('data');
			}else if($field.attr('data-type') === 'emails'){
				var emails = $field.val();
				var parts = emails.split(',');
				var hasError = false;
				var validEmails = [];
				var invalidEmails = [];
				$.each(parts, function(k, part){
					var trimmed = $.trim(part);
					if(trimmed.length === 0) return true;
					
					if(Type.validateEmail(trimmed)){
						validEmails.push(trimmed);
					}else{
						invalidEmails.push(trimmed);
						hasError = true;
					}
				});
				
				return {
					hasError: hasError,
					valid: validEmails,
					invalid: invalidEmails
				};
			}else if($field.attr('data-type') === 'html'){
				var instanceName = $field.attr('id');
				var content = CKEDITOR.instances[instanceName].getData();
				content = _.unescape(content);
				
				//just in case
				content = $.htmlClean(content, {
					removeTags: ['script'],
					allowedAttributes: [['style']],
					allowEmpty: ['div', 'p', 'i']
				});
				return content;
			}else if($field.attr('data-type') === 'media' || $field.attr('data-type') === 'photo'){
				var array = [];
				$.each($field.children(), function(k, entry){
					var $entry = $(entry);
					var uri = $entry.attr('data-node-uri');
					if(!uri) return true;
					
					array.push(uri);
				});
				
				return array;
			}else if($field.attr('data-type') === 'slider'){
				return $field.slider('value');
			}else if($field.attr('type') === 'time'){
				var value = $field.val();
				if(!value) return null;
				var parts = value.split(':');
				return {
					hour: parts[0],
					minute: parts[1],
					formatted: value
				};
			}else if($field.attr('type') === 'password'){
				return $field.val();
			}else if($field.attr('type') === 'hidden'){
				return $field.val();
			}else if($field.attr('type') === 'text'){
				return $.trim($field.val());
			}else if($field.attr('data-type') === 'timezone'){
				var id = $.trim($field.val());
				var offset = parseFloat($field.find('option:selected').attr('data-offset'))
				return {
					id: id,
					offset: offset
				};
			}else if($field[0].tagName === 'SELECT'){
				return $.trim($field.val());
			}else if($field[0].tagName === 'TEXTAREA'){
				return $.trim($field.val());
			}else if($field.attr('type') === 'number'){
				return $.trim($field.val());
			}else if($field.attr('type') === 'checkbox'){
				return $field.is(':checked');
			}else if($field.attr('type') === 'radio'){
				$field = $scope.find('[data-input="' + field + '"]:checked');
				if($field.length === 0){
					$field = $scope.find('[data-field="' + field + '"]:checked');
				}
				return $field.val();
			}else if($field.attr('data-network')){
				var array = [];
				$.each($field, function(k, entry){
					var $entry = $(entry);
					if(!$entry.hasClass('active')) return true;
					
					array.push($entry.attr('data-network'));
				});
				
				return array;
			}
		};
		
		return {
			get: get,
			clear: clear,
			displayErrorForField: displayErrorForField,
			hideErrorForField: hideErrorForField,
			clearForm: clearForm
		};
	}();
	
	var _topics = {};
	var Topic = function( id ) {
	    var callbacks,
	        method,
	        topic = id && _topics[ id ];
	    if ( !topic ) {
	        callbacks = jQuery.Callbacks();
	        topic = {
	            publish: function(parameters){
	            	setTimeout(function(){
	            		callbacks.fire(parameters);
	            	}, 1);
	            },
	            subscribe: callbacks.add,
	            unsubscribe: callbacks.remove
	        };
	        if ( id ) {
	            _topics[ id ] = topic;
	        }
	    }
	    return topic;
	};
	
	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    webOS: function() {
	        return navigator.userAgent.match(/webOS/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.webOS());
	    }
	};
	
	var isTablet = {
		iOS: function() {
	        return navigator.userAgent.match(/iPad/i);
	    },
	    Android: function() {
	        return navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/Mobile/i);
	    },
	    any: function() {
	        return isTablet.iOS() || isTablet.Android();
	    }
	};
	
    var browserDetect = function(){
        var constants = {
            IE: "Explorer",
            FIREFOX: "Firefox",
            CHROME: "Chrome",
            OPERA: "Opera",
            SAFARI: "Safari"
    	};
		var getBrowser = function(){
			return (searchString(dataBrowser) || "An unknown browser");
		};
		var getVersion = function(){
			return (searchVersion(navigator.userAgent) || searchVersion(navigator.appVersion) || "an unknown version");
		};
		var getOS = function(){
			return (searchString(dataOS) || "an unknown OS");
		};
		var searchString = function (data) {
			for (var i=0;i<data.length;i++)	{
				var dataString = data[i].string;
				var dataProp = data[i].prop;
				this.versionSearchString = data[i].versionSearch || data[i].identity;
				if (dataString) {
					if (dataString.indexOf(data[i].subString) != -1)
						return data[i].identity;
				}
				else if (dataProp)
					return data[i].identity;
			}
		};
		var searchVersion = function (dataString) {
			var index = dataString.indexOf(this.versionSearchString);
			if (index == -1) return;
			return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
		};

		var dataBrowser = [
			{
				string: navigator.userAgent,
				subString: "Chrome",
				identity: "Chrome"
			},
			{ 	string: navigator.userAgent,
				subString: "OmniWeb",
				versionSearch: "OmniWeb/",
				identity: "OmniWeb"
			},
			{
				string: navigator.vendor,
				subString: "Apple",
				identity: "Safari",
				versionSearch: "Version"
			},
			{
				prop: window.opera,
				identity: "Opera",
				versionSearch: "Version"
			},
			{
				string: navigator.vendor,
				subString: "iCab",
				identity: "iCab"
			},
			{
				string: navigator.vendor,
				subString: "KDE",
				identity: "Konqueror"
			},
			{
				string: navigator.userAgent,
				subString: "Firefox",
				identity: "Firefox"
			},
			{
				string: navigator.vendor,
				subString: "Camino",
				identity: "Camino"
			},
			{		// for newer Netscapes (6+)
				string: navigator.userAgent,
				subString: "Netscape",
				identity: "Netscape"
			},
			{
				string: navigator.userAgent,
				subString: "MSIE",
				identity: "Explorer",
				versionSearch: "MSIE"
			},
			{
				string: navigator.userAgent,
				subString: "Gecko",
				identity: "Mozilla",
				versionSearch: "rv"
			},
			{ 		// for older Netscapes (4-)
				string: navigator.userAgent,
				subString: "Mozilla",
				identity: "Netscape",
				versionSearch: "Mozilla"
			}
		];
		var dataOS = [
			{
				string: navigator.platform,
				subString: "Win",
				identity: "Windows"
			},
			{
				string: navigator.platform,
				subString: "Mac",
				identity: "Mac"
			},
			{
				string: navigator.userAgent,
				subString: "iPhone",
				identity: "iPhone/iPod"
		    },
			{
				string: navigator.platform,
				subString: "Linux",
				identity: "Linux"
			}
		];

		return {
		    getBrowser : getBrowser,
			getVersion : getVersion,
			getOS : getOS,
		    constants: constants
		};

    }();

	return{
		VERSION: _VERSION,
		getUrlParams: getUrlParams,
		getUrlStringParams: getUrlStringParams,
		copyAttributes: copyAttributes,
		String: String,
		Web: Web,
		Form: Form,
		Type: Type,
		Topic: Topic,
		browserDetect: browserDetect,
		isMobile: isMobile,
		isTablet: isTablet
	};
}();
gigsky.config(['$translateProvider', function ($translateProvider) {
	$translateProvider.translations('en', {
		'page-title': 'Terms and Conditions',
		'page-description': 'These Terms and Conditions ("T&amp;C") shall govern the relationship between the Customer and GigSky, Inc. (GigSky);',
		's1-header': 'Definitions',
		's1-p1': '"<strong>GigSky SIM</strong>" or "<strong>SIM</strong>" means the prepaid roaming SIM offered by GigSky;',
		's1-p2': '"<strong>GigSky Application</strong>" or "<strong>GigSky App</strong>" means the proprietary software application offered by GigSky;',
		's1-p3': '"<strong>Mobile Router</strong>" means the portable mobile router offered by GigSky;',
		's1-p4': '"<strong>Customer</strong>" means the person who purchases and/or uses the SIM, GigSky App and/or Mobile Router;',
		's1-p5': '"<strong>Participating Member Operator</strong>" means each of the operator partners of GigSky in whose networks the GigSky SIM may be used by the Customer;',
		's1-p6': '"<strong>Top-Up</strong>" of the SIM means the purchase by the Customer of additional data usage of the SIM.',
		's2-header': 'Service Description',
		'l1': 'These T&amp;C govern the sale and use of the SIM.',
		'l2': 'The SIM and any Top-Up of the SIM once sold cannot be returned to GigSky and the cost of the SIM and any unused usage will not be refunded by GigSky, regardless of whether the card and/or Top-Up plan have been activated. GigSky shall not, in any circumstance, be required to refund any balance remaining in the SIM or in any Top-Up purchased by the Customer.',
		'l3': 'The Customer shall be fully responsible for the use of the SIM and under no circumstances shall GigSky be liable for any loss, expenses or damages incurred or suffered by a Customer resulting from the Customer&rsquo;s use of the SIM.',
		'l4': 'A Customer&rsquo;s SIM and any Top Up of the SIM is non-transferable.',
		'l5': 'The SIM only allows for data usage as defined by the GSM (Global System for Mobile). All other services, including voice calls, SMS and MMS cannot be made with the SIM.',
		'l6': 'The SIM only works on unlocked devices and devices, which can use the &lsquo;gigsky&rsquo; APN. For device configuration, please read FAQ.',
		'l7': 'The SIM is not pre-loaded with data and a service must be purchased in order to activate the SIM.',
		'l8': 'GigSky reserves the right to expire the SIM after a period of 365 days from the last successful Top-Up expiry date or the date of activation, whichever is later.',
		'l9': 'The SIM may only be sold to persons 18 years old and above.',
		'l10': 'Upon activation, the SIM will be valid for 365 days unless Topped-Up, in which case the SIM will be valid until the expiration of the Top-Up period.',
		's3-header': 'Online Purchase or Top-Up of SIM',
		'l11': 'The Customer, by making his first online purchase or Top-Up transaction for a GigSky SIM, consents and authorizes GigSky to automatically debit the credit card used for the first purchase or Top-Up by the Customer for any future purchases or Top-Up of the same SIM.',
		'l12': 'The default transaction currency for the SIM shall be US Dollars or its converted local currency equivalent at the point of sale.',
		's4-header': 'No Warranties',
		'l13': 'THE SIM SERVICES ARE PROVIDED ON AN "AS IS" BASIS AND THE CUSTOMER&rsquo;S USE OF THE SIM SERVICES IS AT HIS OWN RISK. GIGSKY DOES NOT MAKE AND HEREBY DISCLAIMS ANY AND ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, ANY WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABILITY, TITLE OR NON-INFRINGEMENT, OR ANY WARRANTY ARISING FROM ANY COURSE OF DEALING, USAGE OR TRADE PRACTICE. GIGSKY DOES NOT WARRANT THAT THE SIM SERVICE WILL BE UNINTERRUPTED, ERROR-FREE OR COMPLETELY SECURE.',
		's5-header': 'Limitations of Warranty and Liability',
		'l14': 'UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY (WHETHER CONTRACT, TORT [INCLUDING NEGLIGENCE], STRICT LIABILITY OR ANY OTHER THEORY WHATSOVER) SHALL GIGSKY BE LIABLE FOR ANY DAMAGES THE CUSTOMER MAY SUFFER FROM OR IN CONNECTION WITH THE USE OR INABILITY TO USE THE GIGSKY SIM OR GIGSKY APP, INCLUDING THE INABILITY TO MAKE EMERGENCY SERVICE CALLS. THIS LIMITATION INCLUDES, BUT IS NOT LIMITED TO, DAMAGES RESULTING FROM LOSS OR THEFT OF DATA, TRANSMISSION DELAYS OR FAILURES, SERVICE INTERRUPTIONS, UNAUTHORIZED ACCESS OR DAMAGE TO RECORDS, SOFTWARE PROGRAMS OR OTHER INFORMATION OR PROPERTY; LOSS OF PROFITS; COST OF COVER; OR ANY OTHER SPECIAL, INCIDENTAL, CONSEQUENTIAL, DIRECT, INDIRECT OR PUNITIVE DAMAGES, HOWEVER CAUSED. THIS LIMITATION WILL APPLY EVEN IF GIGSKY HAS BEEN ADVISED OF, OR IS AWARE OF, THE POSSIBILITIES OF SUCH DAMAGES.',
		'l15': 'GIGSKY DOES NOT AND CANNOT CONTROL THE QUALITY OF THE PARTICIPATING MEMBER OPERATOR&rsquo;S NETWORKS IN WHICH THE SIM MAY BE USED OR INTERCONNECT WITH. THEREFORE, GIGSKY DISCLAIMS ANY AND ALL LIABILITY THAT MAY ARISE FROM THE PERFORMANCE, INCLUDING FAILURE, OF THE PARTICIPATING MEMBER OPERATOR&rsquo;S NETWORKS.',
		'l16': 'GIGSKY&rsquo;S LIABILITY TO A CUSTOMER WHETHER IN CONTRACT, TORT, OR OTHERWISE, IN RELATION TO SIM SHALL BE LIMITED TO THE FACE VALUE OF THE SIM AS THE CASE MAY BE.',
		's6-header': 'Changes in Regulation',
		'l17': 'Customer and GigSky understand and agree that regulators of the Participating Member Operator&rsquo;s networks, or other bodies of competent legal jurisdiction, may impose regulations on telecommunication services. If new regulations are imposed upon GigSky, then Customer hereby acknowledges that GigSky is authorized to make changes to this T&amp;C that reflect the new regulations, and will post such changes on its corporate website.',
		's7-header': 'Right to Disclose Customer Information',
		'l18': 'Save with the Customer&rsquo;s consent or authority (including consent and authority granted pursuant to clause 17 below), GigSky shall not use any information obtained from the Customer ("Customer Registration Form Information") for any purpose other than the purposes permitted by regulatory bodies or authorities where the SIM is sold or used.',
		'l19': 'The Customer hereby affirmatively consents to and authorizes GigSky to use and/or disclose any information or data relating to the Customer (including Customer User Account Information) for the purposes of sending commercial messages to the Customer.',
		's8-header': 'Termination',
		'l20': 'GigSky shall be entitled to terminate the SIM if',
		'l20-l1': 'The Customer uses the SIM improperly, or for illegal or immoral purposes;',
		'l20-l2': 'Any of the information provided by the Customer upon purchasing the SIM is found to be false or unauthorized;',
		'l20-l3': 'For any reason, the Customer fails to provide his registration information;',
		'l20-l4': 'GigSky deems it in the best interests of the public to terminate the SIM; or',
		'l20-l5': 'The Customer breaches any of the Customer&rsquo;s obligations under these T&amp;C.',
		'l21': 'Upon termination, GigSky shall have the right to disconnect the Customer&rsquo;s SIM.',
		's9-header': 'Force Majeure',
		'l22': 'GigSky shall not be liable for any breach of these T&amp;C where the breach was caused by insurrection or civil disorder, war or military operations, national or local emergency, acts or omissions of government, highway authority, regulatory authority or other competent authority, GigSky&rsquo;s compliance with any statutory obligation or an obligation under a statute, international treaties and other international agreements, industrial disputes or any kind (whether or not involving GigSky&rsquo;s employees), fire, lightning, explosion, flood, subsidence, or any other cause whether similar or dissimilar outside GigSky&rsquo;s reasonable control.',
		's10-header': 'Fair Use Policy',
		'l23': 'The use of the SIM by a Customer is expected to be a fair data usage.',
		'l24': 'Excessive volume or duration of data use, determined in GigSky&rsquo;s sole discretion, acting reasonably, shall constitute abuse of data usage privileges by the Customer. GigSky reserves the right to monitor data usage and to withdraw the services supplied to the Customer at any time in case of overuse or abuse of the service by the Customer. GigSky will not be liable for reimbursement, compensation or any subsequent loss of any SIM that is suspended, withdrawn or terminated under this Fair Use Policy.',
		's11-header': 'Law and Arbitration',
		'l25': 'These T&amp;C shall be governed by the laws of the State of California and the Customer hereby submits to the jurisdiction of the Courts of California.',
		's12-header': 'Ownership',
		'l26': 'All intellectual property rights in the SIM belong to GigSky. The purchase or use of the SIM by Customer does not imply any transfer of intellectual property rights from GigSky to the Customer.',
		's13-header': 'Privacy Policy',
		'l27': 'GigSky takes the Customer&rsquo;s privacy seriously. Please refer to GigSky&rsquo;s Privacy Policy for full details at <a href="/privacy-policy">http://www.gigsky.com</a>.',
		's14-header': 'Collection Information',
		'l28': 'Please collect your GigSky SIM card within 30 days from date of purchase. <strong>In the event a Customer fails to collect his GigSky SIM card within 30 days from the date of purchase, the Customer agrees that the Customer shall be deemed to have abandoned his GigSky SIM card, and to have waived collection thereof, and the Customer&rsquo;s payment for the GigSky SIM card shall not be refunded but shall be applied, by way of liquidated damages and not by way of penalty, in respect of GigSky&rsquo;s administrative and transaction charges.</strong>',
		's15-header': 'General',
		'l29': 'These T&amp;C may be amended by GigSky from time to time in its discretion, without the need to inform the Customer.',
		'l30': 'Unless expressly stated herein, GigSky&rsquo;s Terms and Conditions appearing at <a href="/">www.gigsky.com</a> shall apply in addition to the T&amp;Cs.'
	});
	$translateProvider.translations('ja', {
		'page-title': 'サービス規約',
		'page-description': 'このサービス規約（以下「本規約」という）はお客様とGigSky,Inc.（以下「GigSky」という）との関係を規定したものです。',
		's1-header': '定義',
		's1-p1': '「GigSky SIM」「SIM」は、GigSkyによるプリペイドのローミングSIMをいいます。',
		's1-p2': '「GigSkyアプリケーション」「GigSky App」は、GigSkyが所有し提供するソフトウェア・アプリケーションをいう。',
		's1-p3': '「モバイルルーター」は、GigSkyが提供するポータブル・モバイルルーターをいう。',
		's1-p4': '「お客様」は、SIM、GigSkyアプリケーション、モバイルルーターを購入、あるいは使用する人をいう。',
		's1-p5': '「メンバー通信事業社」は、お客様がGigSky SIMを通じて使うことのできるネットワークを提供する通信事業社をいう。',
		's1-p6': '「SIMへのチャージ」は、SIMへの追加データ容量を購入することをいう。',
		's2-header': 'サービスディスクリプション',
		'l1': 'この本規約はSIMの販売と使用について規定しています。',
		'l2': 'SIMあるいはデータプランがアクティベートされたかどうかにかかわらず、一度販売されたSIMとSIMへのチャージはいかなる事情があろうとGigSkyへ返品することはできません、またSIMのコストやSIMに残存する未使用のデータ残高について現金化することはできません。',
		'l3': 'お客様はSIMの利用に関して全責任を負うものとし、いかなる事情があろうともSIMの使用に際しお客様が被った損失、費用、損害についてGigSkyはその責任を負うことはありません。',
		'l4': 'お客様のSIM及びSIMへのチャージは、その権利を他者に移転することはできません。',
		'l5': 'SIMはGSM（Global System for Mobile）方式によるデータ通信にのみ利用できます。音声通話・SMS・MMSを含むその他の全てのサービスは利用できません。',
		'l6': 'SIMはgigsky APNを使用することのできるSIMロック解除された機器でのみ利用できます。機器の設定方法についてはFAQをご確認ください。',
		'l7': 'SIMをお買い上げの時点ではデータは入っておりません。SIMをアクティベートするためにはデータの購入が必要です。',
		'l8': 'SIMは18歳以上の方にのみ販売いたします。',
		'l9': '',
		'l10': '',
		's3-header': 'オンラインでの購入あるいはSIMへのチャージ',
		'l11': 'お客様は、初めてのオンラインでのGigSky SIMの購入あるいはSIMへのチャージをすることにより、ご利用されたクレジットカードを次回以降の購入あるいはチャージでも利用するよう自動的に登録されることに同意します。',
		'l12': '決済通貨の初期設定はUSドルあるいは購入時の現地通貨相当額です。',
		's4-header': '免責条項',
		'l13': 'SIMは現状のままで提供されるものであり、SIMのご利用に際してはお客様の責任において行って下さい。GigSKyはいかなる理由においても特定と目的への適切性・商品性・権利の非侵害を含むがそれに限定されない明示的・黙示的な一切の保証をいたしません。GigSkyは途中で停止しないか、不具合がないか、安全であるかどうか等について一切の保証をいたしません。',
		's5-header': '保証及び責任の限定',
		'l14': 'GigSkyはいかなる理由であれ、お客様がSIMおよびGigSkyアプリの利用あるいは利用できないこと（緊急電話がかけられないことを含む）により生じた損失について一切の責任を負いません。この限定にはデータの消失、通信の遅れや失敗、不正アクセス、記録・ソフトウェア・情報・資産の損失、利益損失、負担するコスト、その他の特殊な事象・事故・直接的・間接的・懲罰的な損害を含みますが、それらに限定されません。この限定はGigSkyがそれらの損害の可能性をあらかじめ認識していた場合においても適用されます。',
		'l15': 'GigSkyはSIMをご利用になるメンバー通信事業社のネットワーク品質についてコントロールする立場にありません。従いまして、GigSkyはメンバー通信事業社のネットワークに関して接続できない等のパフォーマンスについて一切の責任を負いません。',
		'l16': 'GigSkyのSIMに関するお客様への責任は、契約・不法行為・その他いかなる理由であれ、SIMの額面価格を上回りません。',
		's6-header': '規制の変更',
		'l17': 'お客様及びGigSkyは、メンバー通信事業社、司法当局等が通信サービスに関する規制を将来変更する可能性があることを理解し同意します。新しい規制がGigSkyに適用される場合、お客様はGigSkyが本規約を新しい規制に沿う形に変更し、その変更をGigSkyのウェブサイトにて告知することに同意します。',
		's7-header': 'お客様の情報開示権限',
		'l18': 'お客様との合意に基づき、GigSkyはSIMが販売もしくは使用される国の規制団体や政府等の公的機関から認められる場合を除き、いかなる理由においてもお客様の情報を開示しません。',
		'l19': '但し、お客様はGigSkyがユーザーアカウント情報を含むお客様の情報を広告メッセージを送るために利用することに同意します。',
		's8-header': '解除',
		'l20': 'GigSkyは下記のいずれかに該当した場合、SIMを解除することができます。',
		'l20-l1': 'お客様がSIMを不適切に、違法に、あるいは道徳に反する用途に使用した場合。',
		'l20-l2': 'お客様がSIMを購入する際に提供された情報が虚偽あるいは不正である場合。',
		'l20-l3': 'いかなる理由であれ、お客様から登録に必要な情報が提供されない場合。',
		'l20-l4': 'SIMを解除することが公共の利益に照らして最善であると判断される場合。',
		'l20-l5': 'お客様が本規約のいずれかの条文に違反した場合。',
		'l21': '解除された場合、GigSkyはお客様のSIMをネットワークに接続できないよう手続する権利を有します。',
		's9-header': '不可抗力',
		'l22': 'GigSkyは、暴動・内政の混乱・戦争・軍事作戦・国家あるいは地域の緊急事態、政府・規制当局・その他政府機関による行動あるいは怠慢、GigSkyが法令を遵守することにより生じる結果、国際条約や国際合意、労働争議（GigSkyの従業員が関与しているか否かにかかわららない）、火災、落雷、爆発、洪水、地盤沈下、その他GigSkyが合理的にコントロールすることのできないあらゆる事象により生じた本規約違反については一切責任を負いません。',
		's10-header': '公正使用ポリシー',
		'l23': 'SIMの利用にあたり、お客様には適正にデータを利用することが求められます。',
		'l24': 'GigSky独自の判断によりデータの使用量や使用時間が異常と判断される場合においては、お客様によるデータの不正利用とみなされることがあります。GigSkyはこのような異常利用や不正利用に関して、いかなる時でもお客様のデータ利用状況を監視し、サービスを停止する権利を有します。GigSkyはこのような場合でも、返金・保障・SIMが利用できないことに対するいかなる損失についても一切責任を負いません。',
		's11-header': '準拠法及び訴訟',
		'l25': '本規約は、米国カリフォルニア州法に準拠します。またお客様は、本規約に起因する紛争または請求を解決するに当たって米国カリフォルニア州の裁判所を専属管轄裁判所とすることに同意します。',
		's12-header': '所有権',
		'l26': 'SIMに関する全ての知的所有権はGigSkyに帰属します。お客様のSIMの購入や利用は、知的所有権のGigSkyからお客様への移転を意味するものではありません。',
		's13-header': 'プライバシーポリシー',
		'l27': 'お客様とのプライバシーはGigSkyにとってとても重要です。http://www.gigsky.jpにアクセスして、GigSkyのプライバシーポリシーを是非ご一読ください。',
		's14-header': '',
		'l28': '',
		's15-header': 'その他',
		'l29': '本規約は不定期に事前の予告なく変更されることがあります。',
		'l30': 'GigSkyのサービス規約はGigSkyのウェブサイトに表示されます。'
	});
	$translateProvider.preferredLanguage('en');
}]);

gigsky.controller('TCController', ['$controller', '$translate', '$scope', function ($controller, $translate, $scope) {
	$controller('GSController', {$scope: $scope});
	
	$scope.changeLanguage = function (langKey) {
		$translate.use(langKey);
		
		//only required when number of points are not the same between languages
		setTimeout(function(){
			fixNumbering();
		}, 500);
	};
	
	//fix numbering since Japanese version has less points
	var fixNumbering = function(){
		var $container = $('[data-points]');
		var $points = $container.find('li');
		$points.show();
		$container.find('[data-top-level]').parent().show();
		
		$.each($points, function(k, p){
			var $p = $(p);
			if($p.children().length === 0) return true;
			if($p.children().text().length === 0){
				$p.hide();
			}
		});
		
		var count = 0;
		$.each($container.find('[data-top-level]'), function(k, l){
			var $l = $(l);
			
			if(count > 0){
				$l.attr('start', count + 1);
			}
			
			var pCount = $l.children(':visible').length;
			count += pCount;
			
			if(pCount === 0){
				$l.parent().hide();
			}
		});
	};
	
	var params = JSUtils.getUrlParams();
	
	if(params.sl){
		var lang = params.sl;
		$scope.changeLanguage(lang);
		
		//only required when number of points are not the same between languages
		setTimeout(function(){
			fixNumbering();
		}, 500);
	}else{
		Localize.detectLanguage(function(err, languages){
			if (err) return console.log(err);
			try{
				var lang = 'en';
				for(var i = 0; i < languages.length; i++){
					if(languages[i].length === 2){
						lang = languages[i];
						break;
					}
				}

				$scope.changeLanguage(lang);
			}catch(e){}
		});
	}
}]);
gigsky.config(['$translateProvider', function ($translateProvider) {
	$translateProvider.translations('en', {
		'page-title': 'Privacy Policy',

		'page-description': 'Your privacy is important to GigSky. So we have developed a Privacy Policy that covers how we collect, use, disclose, transfer, and store your information. Please take a moment to familiarize yourself with our privacy practices and let us know if you have any questions.',
		's1-header': 'Collection and use of personal information',
		's1-p1': 'Personal information is data that can be used to uniquely identify or contact a single person.',
		's1-p2': 'You may be asked to provide your personal information anytime you are in contact with GigSky or a GigSky affiliated company. GigSky and its affiliates may share this personal information with each other and use it consistent with this Privacy Policy. They may also combine it with other information to provide and improve our products, services, content, and advertising.',
		's1-p3': 'Here are some examples of the types of personal information GigSky may collect and how we may use it.',
		's2-header': 'What personal information we collect',
		's2-l1': 'When you create a GigSky ID, register your products, apply for commercial credit, purchase a product, download a software update, or participate in an online survey, we may collect a variety of information, including your name, mailing address, phone number, email address, contact preferences, and credit card information.',
		's2-l2': 'When you share your content with family and friends using GigSky products, or invite others to join you on GigSky forums, GigSky may collect the information you provide about those people such as name, mailing address, email address, and phone number.',
		's2-l3': 'In the U.S., we may ask for your Social Security number (SSN) but only in limited circumstances such as when setting up a wireless account and activating your GigSky SIM or when determining whether to extend commercial credit.',
		's3-header': 'How we use your personal information',
		's3-l1': 'The personal information we collect allows us to keep you posted on GigSky&rsquo;s latest product announcements, software updates, and upcoming events. It also helps us to improve our services, content, and advertising.',
		's3-l2': 'We also use personal information to help us develop, deliver, and improve our products, services, content, and advertising.',
		's3-l3': 'From time to time, we may use your personal information to send important notices, such as communications about purchases and changes to our terms, conditions, and policies. Because this information is important to your interaction with GigSky, you may not opt out of receiving these communications.',
		's3-l4': 'We may also use personal information for internal purposes such as auditing, data analysis, and research to improve GigSky&rsquo;s products, services, and customer communications.',
		's3-l5': 'If you enter into a sweepstake, contest, or similar promotion we may use the information you provide to administer those programs.',
		's4-header': 'Collection and use of non-personal information',
		's4-p1': 'We also collect non-personal information–data in a form that does not permit direct association with any specific individual. We may collect, use, transfer, and disclose non-personal information for any purpose. The following are some examples of non-personal information that we collect and how we may use it:',
		's4-p1-l1': 'We may collect information such as occupation, language, zip code, area code, unique device identifier, location, and the time zone where a GigSky product is used so that we can better understand customer behavior and improve our products, services, and advertising.',
		's4-p2': 'If we do combine non-personal information with personal information the combined information will be treated as personal information for as long as it remains combined.',
		's5-header': 'Cookies and other technologies',
		's5-p1': 'GigSky&rsquo;s website, online services, interactive applications, email messages, and advertisements may use "cookies" and other technologies such as pixel tags and web beacons. These technologies help us better understand user behavior, tell us which parts of our website people have visited, and facilitate and measure the effectiveness of advertisements and web searches. We treat information collected by cookies and other technologies as non-personal information. However, to the extent that Internet Protocol (IP) addresses or similar identifiers are considered personal information by local law, we also treat these identifiers as personal information. Similarly, to the extent that non-personal information is combined with personal information, we treat the combined information as personal information for the purposes of this Privacy Policy.',
		's5-p2': 'GigSky and its partners use cookies and other technologies in mobile advertising services to control the number of times you see a given ad, deliver ads that relate to your interests, and measure the effectiveness of ad campaigns. If you do not want to receive ads with this level of relevance on your iPhone mobile device, you can opt out by accessing the following link on your device: <a href="http://oo.apple.com" target="_blank">http://oo.apple.com</a>. If you opt out, you will continue to receive the same number of mobile ads, but they may be less relevant because they will not be based on your interests. You may still see ads related to the content on a web page or in an application or based on other non-personal information. This opt-out applies only to Apple advertising services and does not affect interest-based advertising from other advertising networks.',
		's5-p3': 'GigSky and our partners also use cookies and other technologies to remember personal information when you use our website, online services, and applications. Our goal in these cases is to make your experience with GigSky more convenient and personal. For example, knowing your first name lets us welcome you the next time you visit the GigSky Online Store. Knowing your country and language–and if you are an educator, your school–helps us provide a customized and more useful shopping experience. Knowing someone using your computer or device has shopped for a certain product or used a particular service helps us make our advertising and email communications more relevant to your interests. And knowing your contact information, product serial numbers, and information about your computer or device helps us register your products, personalize your operating system, set up your GigSky service, and provide you with better customer service.',
		's5-p4': 'If you want to disable cookies and you&rsquo;re using the Safari web browser, go to Safari preferences and then to the privacy pane to disable cookies. On an Apple mobile device, go to Settings, then Safari, and then to the Cookies section.',
		's5-p5': 'You can manage cookies by using features and functions available on most Internet browsers. For example, most browsers will allow you to choose what cookies can be placed on your computer and to delete or disable cookies. You can find instructions for managing cookie controls on websites for particular browsers. For example:',
		's5-p5-l1': 'Microsoft Internet Explorer browsers',
		's5-p5-l2': 'Apple Safari browsers',
		's5-p5-l3': 'Mozilla Firefox browsers',
		's5-p6': 'Please note that disabling cookies may prevent you from using specific features on our sites and other websites, such as ordering products or services and maintaining an online account.',
		's5-p7': 'As is true of most websites, we gather some information automatically and store it in log files. This information includes Internet Protocol (IP) addresses, browser type and language, Internet service provider (ISP), referring and exit pages, operating system, date/time stamp, and clickstream data.',
		's5-p8': 'We use this information to understand and analyze trends, to administer the site, to learn about user behavior on the site, and to gather demographic information about our user base as a whole. GigSky may use this information in our marketing and advertising services.',
		's5-p9': 'In some of our email messages, we use a "click-through URL" linked to content on the GigSky website. When customers click one of these URLs, they pass through a separate web server before arriving at the destination page on our website. We track this click-through data to help us determine interest in particular topics and measure the effectiveness of our customer communications. If you prefer not to be tracked in this way, you should not click text or graphic links in the email messages.',
		's5-p10': 'Pixel tags enable us to send email messages in a format customers can read, and they tell us whether mail has been opened. We may use this information to reduce or eliminate messages sent to customers.',
		's6-header': 'Disclosure to third parties',
		's6-p1': 'At times GigSky may make certain personal information available to strategic partners that work with GigSky to provide products and services, or that help GigSky market to customers. For example, when you purchase and activate your iPhone, you authorize Apple and its carrier to exchange the information you provide during the activation process to carry out service. If you are approved for service, your account will be governed by Apple and its carrier&rsquo;s respective privacy policies. Personal information will only be shared by GigSky to provide or improve our products, services and advertising; it will not be shared with third parties for their marketing purposes.',
		's7-header': 'Service providers',
		's7-p1': 'GigSky shares personal information with companies who provide services such as information processing, extending credit, fulfilling customer orders, delivering products to you, managing and enhancing customer data, providing customer service, assessing your interest in our products and services, and conducting customer research or satisfaction surveys. These companies are obligated to protect your information and may be located wherever GigSky operates.',
		's8-header': 'Others',
		's8-p1': 'It may be necessary–by law, legal process, litigation, and/or requests from public and governmental authorities within or outside your country of residence–for GigSky to disclose your personal information. We may also disclose information about you if we determine that for purposes of national security, law enforcement, or other issues of public importance, disclosure is necessary or appropriate.',
		's8-p2': 'We may also disclose information about you if we determine that disclosure is reasonably necessary to enforce our terms and conditions or protect our operations or users. Additionally, in the event of a reorganization, merger, or sale we may transfer any and all personal information we collect to the relevant third party.',
		's9-header': 'Protection of personal information',
		's9-p1': 'GigSky takes precautions–including administrative, technical, and physical measures–to safeguard your personal information against loss, theft, and misuse, as well as against unauthorized access, disclosure, alteration, and destruction.',
		's9-p2': 'GigSky online services such as the GigSky Online Store, Apple Online Store and iTunes Store use Secure Sockets Layer (SSL) encryption on all web pages where personal information is collected. To make purchases from these services, you must use an SSL-enabled browser such as Safari, Firefox, or Internet Explorer. Doing so protects the confidentiality of your personal information while it&rsquo;s transmitted over the Internet.',
		's9-p3': 'When you use some GigSky products, services, or applications or post on a GigSky forum, chat room, or social networking service, the personal information you share is visible to other users and can be read, collected, or used by them. You are responsible for the personal information you choose to submit in these instances. For example, if you list your name and email address in a forum posting, that information is public. Please take care when using these features.',
		's10-header': 'Integrity and retention of personal information',
		's10-p1': 'GigSky makes it easy for you to keep your personal information accurate, complete, and up to date. We will retain your personal information for the period necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law.',
		's11-header': 'Access to personal information',
		's11-p1': 'You can help ensure that your contact information and preferences are accurate, complete, and up to date by logging in to your account at www.gigsky.com. For other personal information, we make good faith efforts to provide you with access so you can request that we correct the data if it is inaccurate or delete the data if GigSky is not required to retain it by law or for legitimate business purposes. We may decline to process requests that are unreasonably repetitive, require disproportionate technical effort, jeopardize the privacy of others, are extremely impractical, or for which access is not otherwise required by local law. Access, correction, or deletion requests can be made through <a href="/contact">http://www.gigsky.com/contact</a>.',
		's12-header': 'Children',
		's12-p1': 'We do not knowingly collect personal information from children under 13. If we learn that we have collected the personal information of a child under 13 we will take steps to delete the information as soon as possible.',
		's13-header': 'Location-based services',
		's13-p1': 'To provide location-based services on GigSky products, GigSky and our partners and licensees may collect, use, and share precise location data, including the real-time geographic location of your computer or device. This location data is collected anonymously in a form that does not personally identify you and is used by GigSky and our partners and licensees to provide and improve location-based products and services. For example, we may share geographic location with application providers when you opt in to their location services.',
		's14-header': 'Third-party sites and services accommodate',
		's14-p1': 'GigSky websites, products, applications, and services may contain links to third-party websites, products, and services. Our products and services may also use or offer products or services from third parties– for example, a third-party iPhone app. Information collected by third parties, which may include such things as location data or contact details, is governed by their privacy practices. We encourage you to learn about the privacy practices of those third parties.',
		's15-header': 'International users',
		's15-p1': 'Information you provide may be transferred or accessed by entities around the world as described in this Privacy Policy. GigSky abides by the "safe harbor" frameworks set forth by the U.S. Department of Commerce regarding the collection, use, and retention of personal information collected by organizations in the European Economic Area and Switzerland. Learn more about the U.S. Department of Commerce Safe Harbor Program.',
		's16-header': 'Our companywide commitment to your privacy',
		's16-p1': 'To make sure your personal information is secure, we communicate our privacy and security guidelines to GigSky employees and strictly enforce privacy safeguards within the company.',
		's17-header': 'Privacy questions',
		's17-p1': 'If you have any questions or concerns about GigSky&rsquo;s Privacy Policy or data processing, please <a href="/contact">contact us</a>.',
		's17-p2': 'GigSky may update its Privacy Policy from time to time. When we change the policy in a material way, a notice will be posted on our website along with the updated Privacy Policy.',
		's17-p3': 'GigSky Inc. 2390 El Camino Real, Suite 250<br/>Palo Alto, CA 94306<br/>USA'
	});
	$translateProvider.translations('ja', {
		'page-title': 'プライバシーポリシー',
		'page-description': 'お客様のプライバシーはGisSkyにとってとても重要です。そのためGigSkyではお客様の情報をどのように収集・利用・開示・移転・保存するのかについてプライバシーポリシーを定めております。下記のプライバシーポリシーをご一読いただき、ご不明な点がある場合にはGigSkyまでお問い合わせください。',
		's1-header': '個人情報の収集と利用について',
		's1-p1': '個人情報とは、一個人を特定あるいはコンタクトすることのできる情報のことを言います。',
		's1-p2': 'お客様がGigSkyの製品をご利用いただくに際し、GigSkyあるいはその関連会社から個人情報の提示を求められることがあります。またGigSkyおよびその関連会社はそれらの個人情報を共有し、本プライバシーポリシーに従い運用して参ります。また製品、サービス、コンテンツや広告の品質を向上させるために、個人情報とその他の情報とを組み合わせて利用されることがあります。',
		's1-p3': '下記はGigSkyが収集する可能性のある個人情報の属性と利用方法の事例です。',
		's2-header': '収集する個人情報について',
		's2-l1': 'お客様がGigSkyIDを作成、製品を登録、決済情報の登録、製品の購入、ソフトェアアップデートのダウンロード、オンラインでのアンケートの回答、などの様々な行動の中で、GigSkyは氏名、住所、電話番号、メールアドレス、ご希望されるご連絡方法、クレジットカード情報等を含む様々な情報を収集することがあります。',
		's2-l2': 'お客様がGigSky製品を通じてお客様のコンテンツを家族や友人と共有したり、GigSkyフォーラムへの加入に招待したりする場合、GigSkyはお客様により提供されたそれらの方々の氏名、住所、メールアドレス、電話番号等の情報を収集することがあります。',
		's2-l3': '米国内においては、GigSkyはワイアレスアカウントの設定、SIMのアクティベーション、決済情報の更新などの限定された状況においてソーシャルセキュリティナンバーをお伺いすることがあります。',
		's3-header': '個人情報の利用方法について',
		's3-l1': 'GigSkyが収集する個人情報は、お客様がGigSkyの新製品のアナウンス、ソフトウェアのアップデート、その他のイベント情報を受け取るために利用されます。またGigSkyの製品、サービス、コンテンツ、広告の品質を向上させるために利用されます。',
		's3-l2': 'GigSkyは個人情報を製品の開発及びデリバリー、製品、サービス、コンテンツ、広告の品質を向上させるために利用いたします。',
		's3-l3': 'GigSkyは個人情報を不定期に配信される重要なお知らせをお届けするために利用します。重要なお知らせには、購入、規約・条件・ポリシーの変更等が含まれます。これらのお知らせはお客様がGigSkyの製品をご利用になる上で大変重要ですので、受信拒否をすることはできません。',
		's3-l4': 'GigSkyは個人情報を社内の監査、データ解析、製品・サービス・お客様とのコミュニケーションを改善するために利用いたします。',
		's3-l5': 'GigSkyは個人情報をお客様が抽選、コンテスト、その他類似のプロモーションに参加される場合に、これらを運営する目的で利用いたします。',
		's4-header': '非個人情報の収集について',
		's4-p1': 'GigSkyは非個人情報についても、個人と直接結びつかない形で収集しております。またGigSkyはそれらの情報についていかなる目的のためにも収集、利用、移転、開示されることがあります。下記はGigSkyが収集する可能性のある非個人情報と利用方法の事例です。',
		's4-p1-l1': 'GigSkyはお客様の属性や行動を理解し、製品・サービス・広告の品質を向上させるために、職業・言語・郵便番号・機器の識別・場所・タイムゾーン等の情報を収集することがあります。',
		's4-p2': 'GigSkyが収集した非個人情報が個人情報と合わせて使われる場合、それらは個人情報として扱われます。',
		's5-header': 'Cookieとその他のテクノロジーについて',
		's5-p1': 'GigSkyのウェブサイト、オンラインサービス、アプリケーション、Eメール、広告にはCookieとピクセルタグやウェブビーコン等のその他のテクノロジーが使われることがあります。これらのテクノロジーはお客様の行動を理解することに利用され、ウェブサイトのどこに訪問者が多いのか、広告やウェブ検索の実施と効果測定をするのに利用されます。またそれらによって収集された情報は非個人情報として取り扱われます。しかしながら、IPアドレスやその他の個人を特定できる同様の情報については法律によって個人情報として扱われる場合があり、その場合はこれらの情報を個人情報として取り扱います。同様に、非個人情報が個人情報と合わせて利用される場合、それらの情報は個人情報として扱われます。',
		's5-p2': 'GigSkyとそのパートナー企業は、Cookieとその他のテクノロジーをモバイル広告でも利用し、出現回数、配信、お客様の関心に基づく広告、またキャンペーンの効果測定等を管理する目的で利用いたします。お客様がiPhone上において個人の属性に適した広告の受信を望まない場合には、次のリンクにアクセスすることによって設定を変更することができます: <a href="http://oo.apple.com" target="_blank">http://oo.apple.com</a>。設定変更した場合でも広告を受信いたしますが、それらはお客様の属性とは関係なく配信されることとなります。またウェブページのコンテンツに関連した広告や、非個人情報に基づく広告が配信されることがあります。これらの設定変更はAppleの広告サービスにのみ適用され、インターネット上でのその他の広告ネットワークには反映されません。',
		's5-p3': 'GigSkyとそのパートナー企業は、Cookieとその他のテクノロジーをお客様がウェブサイト、オンラインサービス、アプリケーションを利用する際の個人情報を保存する目的で利用いたします。それらはGigskyのサービスをより便利に、よりパーソナルにする目的で利用されます。例えばお客様の名前を記憶することによって、お客様がGigSkyオンラインストアに次回訪問される際に、よりパーソナルな形でお迎えができます。お住まいの国と言語を記憶することにより、よりカスタマイズされた形でショッピングをお楽しみいただくことができます。ご利用のコンピュータやその他の機器と、購入された製品を理解することにより広告やEメールでのコミュニケーションがより適切にお客様の関心に近い物となります。連絡先、製品のシリアル番号、コンピュータや機器の情報を記憶することにより製品登録、OSのパーソナライズを可能にし、GigSkyのサービスの設定を容易にし、より良い顧客サービスを提供することができます。',
		's5-p4': 'Cookieの利用を望まないお客様でウェブブラウザはSafariをご利用の方は、Safariの設定画面のプライバシーの項目よりCookieをブロックすることができます。Appleの通信端末をご利用の場合、設定画面からSafariを選択し、プライバシーとセキュリティの項目よりCookieをブロックすることができます。',
		's5-p5': 'Cookieに関してはほとんどのインターネットブラウザで設定することができます。例えば、ほとんどのブラブザはコンピュータで記憶するCookieの種類について選択することができ、またそれらを消去あるいは利用停止することができます。Cookieの管理についてはそれぞれのブラウザのウェブサイトをご参照ください。以下はブラウザの一例です：',
		's5-p5-l1': 'Microsoft Internet Explorer browsers',
		's5-p5-l2': 'Apple Safari browsers',
		's5-p5-l3': 'Mozilla Firefox browsers',
		's5-p6': 'Cookieを利用しない場合、GigSkyやその他のウェブサイトにおいて、製品・サービスの注文やアカウントの管理等を含む特定の機能が利用できない可能性があることにご注意ください。',
		's5-p7': 'GigSkyでは他のほとんどのウェブサイトと同様に、IPアドレス・ブラウザーの種類・言語・インターネットサービスプロバイダー・参照ページ・閲覧終了ページ・OS・日時・ページ遷移等に関して自動的に収集・保存しています。',
		's5-p8': 'これらの情報はトレンドを理解・解析、ウェブサイトの運営、お客様の行動の学習、お客様の属性に関する総合的な情報を取得するために利用され、マーケティングと広告を目的として活用されます。',
		's5-p9': 'GigSkyではEメールにおいてGigSkyのウェブサイトへ誘導するURLリンクを利用することがあります。お客様がこれらのURLをクリックすると、目的のウェブサイトに到達する前に別個のウェブサーバーを経由します。これらのクリックデータを利用することにより、特定のトピックに対する関心やお客様とのコミュニケーションの効果を測定することができます。このような形でトラックされることを希望されないお客様は、Eメールにある文字やグラフィックによるリンクをクリックしないようお願いいたします。',
		's5-p10': 'ピクセルタグはお客様が読みやすい形でEメールを送ることを可能とし、更にはお客様がお読みになっているかどうかを測定できます。これらの情報を利用してお客様へ配信される不要なメッセージを削減することが可能になります。',
		's6-header': 'サードパーティへの情報開示について',
		's6-p1': 'GigSkyは特定の個人情報をGigSkyが製品・サービスの提供やマーケティングをする上で連携している戦略的パートナーに対し開示する可能性があります。例えばお客様がiPhoneをお買い求めになられアクティベーションする場合、Appleと通信事業社はサービスを開始するためにお客様から提供された情報を共有することがあります。お客様が認めた場合、アカウントはAppleと通信事業社との別々のプライバシーポリシーによって管理されます。GigSkyが保持する個人情報はGigSkyの製品・サービス・広告を提供あるいは改善する目的にのみ利用され、サードパーティの営業目的等のために利用されることはありません。',
		's7-header': 'サービスプロバイダーについて',
		's7-p1': 'GigSkyが個人情報を共有するのは、情報処理、クレジットカード、ご注文された製品の倉庫内処理、配送、顧客情報の管理・メンテナンス、顧客サービス、お客様の製品・サービスに対する関心の評価、顧客調査の実施、満足度調査の実施をする事業者に限られます。これらの事業者はお客様の個人情報に対する守秘義務があります。',
		's8-header': 'その他',
		's8-p1': 'お客様の個人情報は法律、法的手段、訴訟、内国・外国を問わず政府の要求に応じて開示されることがあります。またその他、国家の安全保障・法的処置・公共的に重要な事例・その他社会通念上開示が必要あるいは適当と思われる事例においては個人情報を開示することがあります。',
		's8-p2': 'またGigSkyのサービス規約を遵守するために合理的に必要と判断される事例、サービスやお客様を保護する目的において個人情報を開示することがあります。更に、組織再編、合併、買収等によりGigSkyが収集した情報の一部あるいは全部が第三者に移転することがあります。',
		's9-header': '個人情報の保護について',
		's9-p1': 'GigSkyはお客様の個人情報の消失、盗難、悪用、不正アクセス、開示、改変、破壊に対し、管理的・技術的・物理的手段を持って予防措置を講じます。',
		's9-p2': 'GigSkyオンラインストア等のGigSkyの提供するオンラインサービス、Appleオンラインストア、iTunesストアは個人情報が収集される全てのページでSSL暗号を利用しています。これらのページから購入するに際し、お客様はSSLの利用が可能なSafari、Firefox、Internet Explorer等のブラウザを利用する必要があります。それによりお客様の個人情報がインターネット上を通信している間も保護されます。',
		's9-p3': 'いくつかのGigSkyの製品・サービス・アプリケーション・フォーラムへの投稿・チャット・SNSの利用中において、お客様の個人情報が他のお客様に判読・収集あるいは使用されることがあります。そのような手段においての個人情報のご入力については、お客様自身の責任において行って下さい。例えばフォーラムにおいてお客様の名前やメールアドレスを開示すること等が含まれますので、これらの機能のご利用にはくれぐれもご用心ください。',
		's10-header': '個人情報の整合性と保持について',
		's10-p1': 'GigSkyはお客様が個人情報を正確、完全、かつ最新の状態で維持できるようにいたします。個人情報は本プライバシーポリシーに記載されている目的を満たすのに必要な期間は保持され続けます。但し、それ以上の期間保持することを法律で定められている場合、あるいは認められている場合はそれに従います。',
		's11-header': '個人情報へのアクセスについて',
		's11-p1': 'お客様はwww.gigsky.comからアカウントにログインするに際し、ご連絡先や好みが正確、完全、かつ最新の状態で維持できるようご協力をお願いいたします。その他の個人情報につきましては、不正確なデータの修正や、法律やビジネス上の目的により保持を求められないものについて消去することができるよう、できる限りの誠意をもって対処いたします。但し、合理的な理由なく繰り返されるリクエスト、技術的に難易度の高い場合、他のお客様のプライバシーを侵す場合、著しく実現困難な場合、あるいは法律により義務付けられていない場合については処理をお断りする場合があります。個人情報へのアクセス・修正・消去については <a href="/contact">http://www.gigsky.com/contact</a> にて受け付けております。',
		's12-header': 'お子様について',
		's12-p1': 'GigSkyは意図的に13歳以下のお子様の個人情報を収集することはありません。万が一、13歳以下のお子様の個人情報を収集したことが発覚した場合には、可及的速やかにそれらの情報を消去いたします。',
		's13-header': '位置情報に基づくサービスについて',
		's13-p1': 'GigSkyの製品上の位置情報に基づくサービスを提供するために、GigSkyとそのパートナー及びライセンスを付与されたものは、コンピュータやその他の機器のリアルタイムの地理的位置情報を含む正確な位置情報を収集・利用・共有することがあります。これらの位置情報は個人が特定できない形で自動的に収集され、GigSkyとそのパートナー及びライセンスを付与されたものが位置情報に基づく製品やサービスの提供と品質向上を目的として利用されます。例えば、お客様が位置情報サービスの利用を許諾したアプリケーションプロバイダーに対し、許諾した時点で地理的位置情報を共有いたします。',
		's14-header': 'サードパーティのウェブサイト及びサービス',
		's14-p1': 'GigSkyのウェブサイト・製品・アプリケーション及びサービスはサードパーティのウェブサイト・製品・アプリケーション及びサービスへのリンクを含んでいることがあります。またGigSkyの製品及びサービスはiPhoneのアプリ等のサードパーティの製品及びサービスを利用あるいは提供することがあります。その場合はお客様ご自身でサードパーティの個人情報の取扱についてお調べになることをお勧めします。',
		's15-header': '海外のお客様について',
		's15-p1': 'お客様の情報は、本プライバシーポリシーに記載のとおり世界中の関連事業者によって移転あるいはアクセスされることがあります。GigSkyは欧州経済地域及びスイス連邦に存在する組織を通じて収集・利用・保持された個人情報に関し、米国商務省の定める免責条項を遵守いたします。米国商務省の免責条項につきましては、別途ご確認ください。',
		's16-header': 'お客様のプライバシーに対する当社の責務について',
		's16-p1': 'お客様の個人情報をお守りするために、私たちはプライバシー及びセキュリティガイドラインをGigSkyの全従業員と共有し、厳密にそれらを実施していくことお約束します。',
		's17-header': 'プライバシーに関するご質問について',
		's17-p1': 'GigSkyのプライバシーポリシーやデータ処理についてご質問やご心配がある場合には、どうぞ<a href="/contact">お問い合わせ</a>ください。',
		's17-p2': 'GigSkyはプライバシーポリシーを不定期に更新いたします。プライバシーポリシーが大幅に変更される場合には、その旨が新しいプライバシーポリシーとともにウェブサイト上で告知されます。',
		's17-p3': 'GigSky Inc. 2390 El Camino Real, Suite 250<br/>Palo Alto, CA 94306<br/>USA'
	});
	$translateProvider.preferredLanguage('en');
}]);

gigsky.controller('PPController', ['$controller', '$translate', '$scope', function ($controller, $translate, $scope) {
	$controller('GSController', {$scope: $scope});
	
	$scope.changeLanguage = function (langKey) {
        if(langKey=="en")
		    $translate.use(langKey);
	};
	
	var params = JSUtils.getUrlParams();
	
	if(params.sl){
		var lang = params.sl;
		$scope.changeLanguage(lang);
	}/*else{
		Localize.detectLanguage(function(err, languages){
			if (err) return console.log(err);
			try{
				var lang = 'en';
				for(var i = 0; i < languages.length; i++){
					if(languages[i].length === 2){
						lang = languages[i];
						break;
					}
				}

				$scope.changeLanguage(lang);
			}catch(e){}
		});
	}*/
}]);
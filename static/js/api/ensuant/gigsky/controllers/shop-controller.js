//gigsky.config(['$translateProvider', function ($translateProvider) {
//    $translateProvider.translations('en', {
//        'nano-sim-link': (JSUtils.isMobile.any() && !JSUtils.isTablet.any()) ? 'http://www.amazon.com/gp/aw/d/B00CTUCTZS/177-7785689-7274427' : 'http://www.amazon.com/GigSky-Global-Data-Nano-Card/dp/B00CTUCTZS/',
//        'nano-sim-price': '$9.95',
//        'micro-sim-link': (JSUtils.isMobile.any() && !JSUtils.isTablet.any()) ? 'http://www.amazon.com/gp/aw/d/B00FBMR72Q/177-7785689-7274427' : 'http://www.amazon.com/GigSky-Global-Data-Mini-Micro/dp/B00FBMR72Q/',
//        'micro-sim-price': '$9.95',
//        'hotspot-link': 'http://shop.gigsky.com/Huawei-E5776s-32-Hotspot-Unlocked-Including/dp/B00V7ITV4A?field_availability=-1&field_browse=6905840011&id=Huawei+E5776s-32+Hotspot+Unlocked+Including&ie=UTF8&searchNodeID=6905840011&searchPage=1&searchRank=salesrank&searchSize=12',
//        'hotspot-price': '$199.95',
//        'adapter-link': 'http://shop.gigsky.com/R-SIM-SIM-card-adapter-set/dp/B00LH78B4C?field_availability=-1&field_browse=6905840011&id=R-SIM+SIM+card+adapter+set&ie=UTF8&searchNodeID=6905840011&searchPage=1&searchRank=salesrank&searchSize=12',
//        'adapter-price': '$2.99'
//    });
//    $translateProvider.translations('ja', {
//        'nano-sim-link': 'http://www.gigsky.jp',
//        'nano-sim-price': '¥1,250',
//        'micro-sim-link': 'http://www.gigsky.jp',
//        'micro-sim-price': '¥1,250',
//        'hotspot-link': '',
//        'hotspot-price': '',
//        'adapter-link': '',
//        'adapter-price': ''
//    });
//    $translateProvider.preferredLanguage('en');
//}]);

gigsky.controller('ShopController', ['$controller', '$translate', '$scope', function ($controller, $translate, $scope) {

    $controller('GSController', {$scope: $scope});
    $scope.amazonLink = (JSUtils.isMobile.any() && !JSUtils.isTablet.any()) ? 'http://www.amazon.com/gp/aw/d/B00CTUCTZS/177-7785689-7274427' : 'http://www.amazon.com/GigSky-Global-Data-Nano-Card/dp/B00CTUCTZS/';

    $scope.currentLang = 'en';

    $scope.enableSim1ToSim2Transition = ensuant.gigsky.Config.enableSim1ToSim2Transition;

    $scope.selectAStore = function(){
        var offset = -40; //Offset of 20px

        $('html, body').animate({
            scrollTop: $("#global-distributors-blk").offset().top + offset
        }, 500);
    }

//    $scope.changeLanguage = function (langKey) {
//        $scope.currentLang = langKey;
//        try{
//            $scope.$apply(function(){
//                //$translate.use(langKey);
//                _toggleProducts(langKey);
//            });
//        }
//        catch(e){
//            $translate.use(langKey);
//            _toggleProducts(langKey);
//        }
//    };
//
//    var _toggleProducts = function(lang){
//        if(JSUtils.isMobile.any() && !JSUtils.isTablet.any()){
//            $('[data-extra-products]').addClass('hide');
//        }else if(lang === 'en'){
//            $('[data-extra-products]').removeClass('hide');
//        }else{
//            $('[data-extra-products]').addClass('hide');
//        }
//    };

    $scope.openDistributorLnk = function(distributor,link){
        try{
            if(ga){
                ga('send','event',distributor,'SIM Distributor');
            }
        }
        catch(e){}
        var win = window.open(link, '_blank');
    };

    $scope.sendBuySIMEvent = function()
    {
        var lang = $scope.currentLang;
        if(lang!='ja')
            lang = 'en';
        if(ga){
            ga('send','event','behavior','buy SIM step2',lang);
        }

        return true;
    };

//    var params = JSUtils.getUrlParams();
//
//    if(params.sl){
//        var lang = params.sl;
//        $scope.currentLang  = lang;
//        //$scope.changeLanguage(lang);
//        //_toggleProducts(lang);
//    }else{
//        Localize.detectLanguage(function(err, languages){
//            if (err) return console.log(err);
//            try{
//                var lang = 'en';
//                for(var i = 0; i < languages.length; i++){
//                    if(languages[i].length === 2){
//                        $scope.currentLang = lang = languages[i];
//                        break;
//                    }
//                }
//
//                //$scope.changeLanguage(lang);
//                //_toggleProducts(lang);
//            }catch(e){}
//        });
//    }
}]);
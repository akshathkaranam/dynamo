var gigsky = angular.module("gigsky", ['pascalprecht.translate']);
gigsky.config( ['$provide', function ($provide){
    $provide.decorator('$sniffer', ['$delegate', function ($delegate) {
        $delegate.history = false;
        return $delegate;
    }]);
}]);
gigsky.controller('GSController', ['$scope','$rootScope', function($scope,$rootScope) {
    $rootScope.isCaptive = window.IS_CAPTIVE_MODE;
    $scope.config = {
        gsAppleSIMURL:ensuant.gigsky.Config.gsAppleSIMURL,
        gsGigSkySIMURL:ensuant.gigsky.Config.gsGigSkySIMURL,
        gsStaticURL:ensuant.gigsky.Config.gsStaticURL
    };
	/*$scope.config = {
		gsWebAppURL: ensuant.gigsky.Config.gsWebAppURL,
		gsAPIURL: ensuant.gigsky.Config.gsAPIURL
	};*/
	
	$scope.parse = function(){
		ensuant.toovia.ui.Main.parse();
	};

    var params = JSUtils.getUrlParams();
    if(window.location.href.indexOf('concur')!=-1 )
    {
        $rootScope.linkingToConcur = true;
    }
    //.Mobile apps accessing page through webview
    if(params.wv)
    {
        $scope.webview = true;
        $rootScope.linkingToConcur = true;
    }
    var params = window.JSUtils.getUrlParams();
    if(params.ls == "false"){
        $scope.myAccountURL = "/#/login";
    }else{
        $scope.myAccountURL = "/#/dashboard";
    }

    $scope.navigateToUrl = function(url){
        window.location.href = url;
    };

}]);
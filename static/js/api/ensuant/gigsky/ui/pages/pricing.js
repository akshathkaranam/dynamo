/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.pages = ensuant.gigsky.ui.pages ? ensuant.gigsky.ui.pages : {};
ensuant.gigsky.ui.pages.Pricing = function() {
	
	var _VERSION = '0.0.2';
	var _namespace = 'pricing';
	
	var Page = function(options){
		var _$container = null;
		var _$rateTableTemplate = null;
		var _$rateEntryTemplate = null;
		var _$rateNotAvailableMsg = null;
		var _countryWithPlansIndex = {};
		var _countryIndex = {};
		var _apiBaseURL = 'https://cy-services-stage.gigsky.com';
		
		var _init = function(options){
			_$container = options.container;
			var apiBaseURL = _$container.attr('data-gigsky-api-url');
			if(apiBaseURL){
				_apiBaseURL = apiBaseURL;
			}
			
			_$rateTableTemplate = _$container.find('[data-rate-table-template]').detach();
			_$rateTableTemplate.removeClass('hide');
			_$rateTableTemplate.removeAttr('data-rate-table-template');
			_$rateEntryTemplate = _$rateTableTemplate.find('[data-rate-entry-template]').detach();
			_$rateEntryTemplate.removeAttr('data-rate-entry-template');
			_$rateNotAvailableMsg = _$rateTableTemplate.find('[data-rate-not-avaiable]').detach();
			
			_setup();
			_assignEvents();
		};
		
		var _assignEvents = function(){
			_$container.on('change', '[data-input="destination-country"], [data-input="country-of-resident"]', function(e){
				var dest = JSUtils.Form.get(_$container, 'destination-country');
				var resi = JSUtils.Form.get(_$container, 'country-of-resident');
				if(dest === 'default' || resi === 'default') return;
				
				_renderRateTable({
					destination: dest,
					resident: resi
				});
			});
			
			JSUtils.Topic('LANGUAGE.CHANGED').subscribe(function(data){
				ensuant.toovia.ui.Utils.displayProgress();
				_fetchCountriesWithPlans(data.language);
				_fetchCountries(data.language);
			});
		};
		
		var _setup = function(){
			ensuant.toovia.ui.Utils.displayProgress();
			_fetchCountriesWithPlans();
			_fetchCountries();
		};
		
		var _renderMap = function(dataRates){
			var $map = _$container.find('[data-map]');
			$map.empty();
			$map.vectorMap({
                map: 'world_mill_en',
                backgroundColor: '#383f47',
                series: {
                    regions: [{
                        values: dataRates,
                        min: 1.0,
                        max: 100.0,
                        scale: ['#0B9BD7', '#0071A4'],
                        normalizeFunction: 'linear'
                    }]
                },
				onRegionTipShow: function(e, el, code){
				    el.html(_countryIndex[code].name);
				},
                onMarkerLabelShow: function(event, label, index){
                },
                onRegionLabelShow: function(event, label, code){
                },
                onRegionClick:function(event,dest){
                    if(dataRates[dest]){
        				var resi = JSUtils.Form.get(_$container, 'country-of-resident');
        				if(resi === 'default') return;
        				
        				_$container.find('[data-input="destination-country"]').val(dest);
        				_renderRateTable({
        					destination: dest,
        					resident: resi
        				});
                    }
                }
            });
		};
		
		var _fetchCountriesWithPlans = function(language){
			var lang = language;
			if(!lang){
				lang = Localize.getLanguage();
			}

			$.ajax({
				type: 'GET',
				url: _apiBaseURL + '/api/v4/countries?count=200',
				beforeSend: function (request){
	                request.setRequestHeader("Accept-Language", lang);
	            },
				success: function(response, textStatus, jqXHR){
					var $dest = _$container.find('[data-input="destination-country"]');
					var selected = $dest.val();
					$dest.empty();
					$dest.append('<option value="default">-- Select A Country --</option>');
					
					var dataRates = [];
					$.each(response.list, function(k, country){
						$dest.append('<option value="' + country.code + '">' + country.name + '</option>');
						_countryWithPlansIndex[country.code] = country;
						
						dataRates[country.code] = 1.0;
					});
					
					if(selected){
						$dest.val(selected);
					}
					
					if (!(JSUtils.isMobile.any() && !JSUtils.isTablet.any())) {
						_renderMap(dataRates);
					}
					
					ensuant.toovia.ui.Utils.hideProgress();
				},
				error: function(response, textStatus, jqXHR){
					ensuant.toovia.ui.Utils.log({msg:'failed to fetch countries with plans'});
				}
			})
		};
		
		var _fetchCountries = function(language){
			var lang = language;
			if(!lang){
				lang = Localize.getLanguage();
			}
			
			$.ajax({
				type: 'GET',
				url: _apiBaseURL + '/api/v4/countries/all?count=1000',
				beforeSend: function (request){
	                request.setRequestHeader("Accept-Language", lang);
	            },
				success: function(response, textStatus, jqXHR){
					var $dest = _$container.find('[data-input="country-of-resident"]');
					var selected = $dest.val();
					$dest.empty();
					$dest.append('<option value="default">-- Select A Country --</option>');
					
					$.each(response.list, function(k, country){
						_countryIndex[country.code] = country;
						var $entry = $('<option value="' + country.code + '">' + country.name + '</option>');
						$dest.append($entry);
						if(country.code === 'US'){
							$entry.attr('selected', 'selected');
						}
					});
					
					if(selected && selected !== 'default'){
						$dest.val(selected);
					}
				},
				error: function(response, textStatus, jqXHR){
					ensuant.toovia.ui.Utils.log({msg:'failed to fetch countries'});
				}
			})
		};
		
		var _renderRateTable = function(options){
			ensuant.toovia.ui.Utils.displayProgress();
			_fetchNetworkGroup({
				destination: options.destination,
				success: function(response){
					var $space = _$container.find('[data-rate-table-space]');
					$space.empty();
					$.each(response.list, function(k, group){
						var networkId = group.networkGroupId;
						_fetchPlans({
							networkGroupId: networkId,
							resident: options.resident,
							currency: options.currency,
							success: function(planResponse){
								var $rateTable = _$rateTableTemplate.clone();
								var $rates = $rateTable.find('[data-rates]');
								$rateTable.find('[data-country-name]').text(group.networkGroupName);
								$rateTable.find('[data-country-flag]').attr('src', _apiBaseURL + '/' + group.logoUrl);
								
								if(!planResponse.list || planResponse.list.length === 0){
									$rates.empty();
									$rates.append(_$rateNotAvailableMsg.clone());
								}else{
									$.each(planResponse.list, function(k, entry){
										var $entry = _$rateEntryTemplate.clone();
										
										var dataLimit;
										if(entry.dataUnlimited){
											dataLimit = 'Unlimited'
										}else{
											if(entry.dataLimitInKB >= 1024 * 1024){
												dataLimit = Math.round(entry.dataLimitInKB/(1024 * 1024)) + " GB";
											}else{
												dataLimit = Math.round(entry.dataLimitInKB/(1024)) + " MB";
											}
										}
										
										$entry.find('[data-rate]').text(getLocalCurrencySign(entry.currency) + entry.price);
										$entry.find('[data-data-allowed]').text(dataLimit);
										$entry.find('[data-expiration]').text(entry.validityPeriodInDays);
										$rates.append($entry);
									});
									
									$rateTable.find('[data-suggestion]').removeClass('hide');
								}

								$space.html($rateTable);
								
								var offset = $rateTable.offset();
								$("html, body").animate({ scrollTop: offset.top - 100}, 400);
								ensuant.gigsky.ui.widgets.Header.updateAccountLink();
							}
						});
					});
					ensuant.toovia.ui.Utils.hideProgress();
				}
			});
		};
		
		var _fetchNetworkGroup = function(options){
			var lang = options.language;
			if(!lang){
				lang = Localize.getLanguage();
			}
			
			$.ajax({
				type: 'GET',
				url: _apiBaseURL + '/api/v4/countries/' + options.destination,
				beforeSend: function (request){
	                request.setRequestHeader("Accept-Language", lang);
	            },
				success: function(response, textStatus, jqXHR){
					options.success(response);
				},
				error: function(response, textStatus, jqXHR){
					options.error(response);
				}
			})
		};
		
		var _fetchPlans = function(options){
			var lang = options.language;
			if(!lang){
				lang = Localize.getLanguage();
			}
			
			var url = _apiBaseURL;
			var currency = ensuant.toovia.constants.CountryCurrencyMap[options.resident];
			if(!currency) currency = 'USD';
			if('USD,EUR,GBP,JPY'.indexOf(currency) === -1) currency = 'USD';
			if(options.resident){
				url += '/api/v4/networkGroups/' + options.networkGroupId + '/plansExt?countryCode=' + options.resident;
			}else{
				url += '/api/v4/networkGroups/' + options.networkGroupId + '/plans?currency=' + currency;
    		}
			
			$.ajax({
				type: 'GET',
				url: url,
				beforeSend: function (request){
	                request.setRequestHeader("Accept-Language", lang);
	            },
				success: function(response, textStatus, jqXHR){
					options.success(response);
				},
				error: function(response, textStatus, jqXHR){
					options.error(response);
				}
			})
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Page(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.pages.pricing.js

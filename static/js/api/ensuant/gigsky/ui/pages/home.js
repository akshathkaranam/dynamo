/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.pages = ensuant.gigsky.ui.pages ? ensuant.gigsky.ui.pages : {};
ensuant.gigsky.ui.pages.Home = function() {
	
	var _VERSION = '0.0.1';
	var _namespace = 'home';
	
	var Page = function(options){
		var _$container = null;
		var _windowHeight = 0;
		
		var _init = function(options){
			_$container = options.container;
			
			if (JSUtils.isMobile.any()) {
				var navHeight = _$container.find('nav[role="gs-main"]').outerHeight(true);
				_windowHeight = $(window).height();
				$('#bgc-1').css('height', (0.8 * (_windowHeight - navHeight)) + 'px');
			}
			
			_assignEvents();
			
			// fade in
			$('.fade-in').fadeTo('slow', 1);
			
			if (!JSUtils.isMobile.any()) {
				// show parallax images after they've loaded
			    _$container.find('[data-bg]').waitForImages({
			        waitForAll: true,
			        each: function() {
			            $(this).removeClass('is-hidden');
			        }
			    });
			}
		};
		
		var _assignEvents = function(){
			// scroll to id
			_$container.on('click', '[data-scroll]', function(e) {
				e.preventDefault();
				var $el = $('#' + $(this).attr('data-scroll'));
				$('html,body').animate({
					scrollTop: $el.offset().top - 90
				}, 'slow');
				//(_windowHeight * 0.125)
			});
			
			if (!JSUtils.isMobile.any()) {
				$(window).resize(function() {
					_windowHeight = $(window).height();
					$('.vh').height( _windowHeight + 'px' );
					
					// parallax: section heights
					$('#parallax-front .section').height((_windowHeight * 0.70) + 'px');
					$('#parallax-front .section-spacer').height((_windowHeight * 0.75) + 'px');
					
					// parallax: cover offsets (from top)
					$('#parallax-back .bgc').each(function(i) {
						var top = parseInt($(this).data('top'));
						$(this).css({ top: (_windowHeight * (top/100)) + 'px' });
					});
					
					$(window).trigger('scroll');
					
				}).trigger('resize');
				
				var parallaxFront = $('#parallax-front').offset().top;
				if($('body').hasClass('with-android-banner')){
					parallaxFront -= $('#smartbanner').outerHeight(true);
				}
				
				$(window).scroll(function() {
					var winScrollTop = $(window).scrollTop();
				
					// scroll speed
					var scrollPos = (parallaxFront - winScrollTop) / 3;
					$('#parallax-back').css({ 'margin-top': scrollPos + 'px' });
					
					// manage z-indexes
					var bgcId = $('.bgc').first();
					$('#parallax-front .section').each(function(i) {
						var offsetTop = $(this).offset().top - winScrollTop;
						if (offsetTop < _windowHeight / 5) {
							bgcId = '#' + $(this).data('bgc');
						}
					});
					$('#parallax-back .bgc').css({ 'z-index': -3 });
					$(bgcId).css({ 'z-index': -1 });
					$(bgcId).next().css({ 'z-index': -2 });
				});
			}
			
			$('body').removeClass('hide');
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Page(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.pages.home.js

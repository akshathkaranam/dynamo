/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.pages = ensuant.gigsky.ui.pages ? ensuant.gigsky.ui.pages : {};
ensuant.gigsky.ui.pages.Login = function() {
	
	var _VERSION = '0.0.1';
	var _namespace = 'login';
	
	var Page = function(options){
		var _$container = null;
		var windowHeight = 0;
		
		var _init = function(options){
			_$container = options.container;
			_assignEvents();
		};
		
		var _assignEvents = function(){
			_$container.on('click', '[data-login]', function(e){
		    	var email = JSUtils.Form.get(_$container, 'email');
		    	var pw = JSUtils.Form.get(_$container, 'password');
		    	
		    	if(!email || !pw){
		    		alert('Missing info');
		    		return;
		    	}
		    	
		    	var keepAlive = true;
		    	var redirectURL = '/';//'/manage/gs-account';
			    	
		    	bakpak.security.Auth.emailLogin({
		    		username: email, 
		    		password: pw, 
		    		keepAlive: keepAlive, 
		    		redirectTo: redirectURL,
		    		callback: function(response){
		    			if(response.Node){
                    		var status = response.Node.Status;
                    		if(status === 'Success'){
                    			ensuant.toovia.ui.Utils.redirect(redirectURL);
                    		}else if(status === 'LinkedToAnotherAccount'){
                    			ensuant.travel.components.StatusModal.call({
                                    title: 'Link Account Failed',
                                    message: 'This account has been linked to another account that is not yours.',
                                    timeout: -1
                                });
                    		}else{
                    			var errorMsg = response.Node.Error;
                    			if(JSUtils.String.isEmpty(errorMsg)){
                        			errorMsg = 'There was an unexpected error. Please try again later.';
                        		}
                    			ensuant.travel.components.StatusModal.call({
                                    title: 'Login Failed',
                                    message: errorMsg,
                                    timeout: -1
                                });
                    		}
                    	}else{
                    		var errorMsg = response.ErrorDescription;
                    		if(JSUtils.String.isEmpty(errorMsg)){
                    			errorMsg = 'There was an unexpected error. Please try again later.';
                    		}
                    		
                			ensuant.travel.components.StatusModal.call({
                                title: 'Login Failed',
                                message: errorMsg,
                                timeout: -1
                            });
                    	}
		    		}
		    	});
			});
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Page(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.pages.login.js

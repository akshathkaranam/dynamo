/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.pages = ensuant.gigsky.ui.pages ? ensuant.gigsky.ui.pages : {};
ensuant.gigsky.ui.pages.CreatePage = function() {
	
	var _VERSION = '0.0.1';
	var _namespace = 'create-page';
	
	var Page = function(options){
		var _$container = null;
		var windowHeight = 0;
		
		var _init = function(options){
			_$container = options.container;
			_assignEvents();
		};
		
		var _assignEvents = function(){
			_$container.on('click', '[data-create-page-trigger]', function(e){
				e.preventDefault();
				
				var $this = $(this);
				var pageType = $this.attr('data-create-page-trigger');
				var title = $this.attr('data-title');
				
				var meta = ensuant.ui.Core.getCoreMeta();
				if(meta.sessionStatus === 'PUBLIC'){
		        	var encoded = JSUtils.Web.createRedirectURL(window.location.href, 'create-page-widget', {
		        		pageType: pageType
		        	});
		        	window.location = '/login?redirectURL=' + encoded;
		        	return;
		        }
				
				ensuant.toovia.ui.Utils.displayProgress();
				ensuant.toovia.ui.Utils.loadClass({
					path: 'ensuant.toovia.services.create-page-editor',
					success: function(response){
						ensuant.toovia.services.CreatePageEditor.createPage({
							pageType: pageType,
							title: title,
							isOwner: true,
							isPrivate: true,
							postAs: meta.postingAs,
							success: function(response){
								if(response && response.status === 'Success'){
									var createdPageURI = response.node.Outputs[0].$element;
									var url = '/node/profile?nodeURI=' + createdPageURI;
									url += '&edit=true';
									ensuant.toovia.ui.Utils.redirect(url);
								}else{
									ensuant.toovia.ui.Utils.hideProgress();
									ensuant.travel.components.StatusModal.call({
										title: 'Failed to Create Page',
										message: 'There was an unexpected error. Please try again later.'
									});
								}
							}, 
							error : function(jqXHR, textStatus, errorThrown){
								ensuant.toovia.ui.Utils.hideProgress();
								ensuant.toovia.ui.Utils.log({
									msg : 'Failed to post: ' + JSON.stringify(jqXHR)
									+ " | " + textStatus + " | " + errorThrown
								});
								ensuant.travel.components.StatusModal.call({
									title: 'Failed to Create Page',
									message: 'There was an unexpected error. Please try again later.'
								});
							}
						});
					}
				});
			});
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Page(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.pages.create-page.js

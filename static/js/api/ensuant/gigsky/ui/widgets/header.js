/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.widgets = ensuant.gigsky.ui.widgets ? ensuant.gigsky.ui.widgets : {};
ensuant.gigsky.ui.widgets.Header = function() {

    var _VERSION = '0.0.1';
    var _namespace = 'header';
    var _appURL = null;

    var Widget = function(options){
        var _$container = null;

        var _init = function(options){
            _$container = options.container;
            _appURL = _$container.attr('data-gigsky-app-url');

            _checkState();
            _assignEvents();
        };

        var _assignEvents = function(){
            $('#nav-mobile a').click(function(e) {
                e.preventDefault();
                $('body').toggleClass('show-mobile-menu');
                $(window).trigger('scroll');
            });

            /*
             All pages:

             - Close mobile navigation menu when a link is the same as the current page

             */

            var addMobileMenuClick = function(){
                $('div[ng-include="\'templates/header.html\'"]').on('click','.mobile-menu .nav-links a',
                    function(){
                        var text = this.href;
                        var path = window.location.pathname;

                        var textPath = (text.substring(text.lastIndexOf('/')+1)).toLowerCase();
                        var page = (path.substring(path.lastIndexOf('/')+1)).toLowerCase();

                        if(page == textPath){
                            $('#nav-mobile .close-menu').trigger('click');
                        }
                    });
            };

            addMobileMenuClick();

            $('div[ng-include="\'templates/footer.html\'"]').on('click', '[data-switch-language]', function(e){
                var $this = $(this);
                var lang = $this.attr('data-switch-language');
                try{
                    Localize.setLanguage(lang);
                }catch(e){}

                var url = window.location.pathname;
                var currentLangCode = 'en';

                var i = url.indexOf('/de');
                if(i!=-1){
                    currentLangCode = 'de';
                }
                i = url.indexOf('/ja');
                if(i!=-1){
                    currentLangCode = 'ja';
                }


                if(lang == 'en'){
                    url = url.replace(currentLangCode+'/','');
                }else{
                    if(currentLangCode == 'en'){
                        url = '/' + lang + url;
                    }else{
                        url = url.replace(currentLangCode+'/',lang+'/');
                    }
                }

                window.location = url;

                //ensuant.ui.History.replaceState(null, $('title').text(), newURL);
                _updateAccountLink(lang);
                _updateCurrentLanguageDisplay(lang);
                JSUtils.Topic('LANGUAGE.CHANGED').publish({
                    language: lang
                });
            });

            _$container.on('click', '[data-scroll]', function(e) {
                e.preventDefault();
                var $el = $('#' + $(this).attr('data-scroll'));
                $('html,body').animate({
                    scrollTop: $el.offset().top - 90
                }, 'slow');
                //(_windowHeight * 0.125)
            });
        }

        var _checkState = function(){
            var params = JSUtils.getUrlParams();
            if(params.ls === 'true'){
                _$container.find('[data-enter-account-btn]').remove();
                _$container.find('[data-account-btn]').removeClass('hide');

                $.each($('a[href^="/"]'), function(k, entry){
                    var $entry = $(entry);
                    var url = $entry.attr('href');
                    $entry.attr('href', JSUtils.Web.replaceParam(url, 'ls', true));
                });
            }else{
                _$container.find('[data-enter-account-btn]').removeClass('is-hidden');
            }

            if(params.sl){
                try{
                    var lang = params.sl;
                    Localize.setLanguage(lang);
                    _updateAccountLink(lang);
                    _updateCurrentLanguageDisplay(lang);
                }catch(e){}
            }
        };

        var _updateCurrentLanguageDisplay = function(lang){
            var $display = _$container.find('[data-current-language]');
            var $selected = _$container.find('[data-switch-language="' + lang + '"]');
            $display.text($($selected[0]).text());

            $('body').attr('data-cur-lang', lang);
        };

        _init(options);
    };

    var _updateAccountLink = function(lang){
        if(!lang){
            lang = Localize.getLanguage();
        }

        if(lang != 'en'){
            var $link = $('[data-account-btn-link]');
            $link.attr('href', _appURL + '/' + lang + '/index.html#/dashboard');

            var $link = $('[data-login-link]');
            $link.attr('href', _appURL + '/' + lang + '/index.html#/login');

            var $link = $('[data-signup-link]');
            $link.attr('href', _appURL + '/' + lang + '/index.html#/signUp');
        }

    };

    var _call = function(options){
        var obj = options.container.data(_namespace);
        if(obj){
            if(options.action){
                if(options.action === 'get'){
                    return obj;
                }else{
                    return obj[options.action](options.actionOptions);
                }
            }else{
                return obj;
            }
        }

        if(!options.action){
            obj = new Widget(options);
            options.container.data(_namespace, obj);
            return obj;
        }
    };

    return{
        VERSION: _VERSION,
        call: _call,
        updateAccountLink: _updateAccountLink
    };

}();
//@ sourceURL=ensuant.gigsky.ui.widgets.header.js

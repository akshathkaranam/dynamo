/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.widgets = ensuant.gigsky.ui.widgets ? ensuant.gigsky.ui.widgets : {};
ensuant.gigsky.ui.widgets.ContactForm = function() {
	
	var _VERSION = '0.0.2';
	var _namespace = 'contact-form';
	
	var Widget = function(options){
		var _$container = null;
		var _appURL = null;
		
		var _init = function(options){
			_$container = options.container;
			_appURL = _$container.attr('data-gigsky-app-url');
			_assignEvents();
		};
		
		var _assignEvents = function(){
			_$container.on('click', '[data-submit-contact-form]', function(e){
				_submit();
			});
			
			_$container.on('input', '[data-input]', function(e){
				var field = $(this).attr('data-input');
				var value = JSUtils.Form.get(_$container, field);
				
				if(field === 'email'){
					if(value && JSUtils.Type.validateEmail(value)){
						JSUtils.Form.hideErrorForField(_$container, field);
					}
				}else if(field === 'gs-email'){
					if(!value || JSUtils.Type.validateEmail(value)){
						JSUtils.Form.hideErrorForField(_$container, field);
					}
				}else{
					JSUtils.Form.hideErrorForField(_$container, field);
				}
			});
		};
		
		var _submit = function(){
			var name = JSUtils.Form.get(_$container, 'name');
			var email = JSUtils.Form.get(_$container, 'email');
			var gsEmail = JSUtils.Form.get(_$container, 'gs-email');
			var device = JSUtils.Form.get(_$container, 'device');
			var location = JSUtils.Form.get(_$container, 'location');
			var subject = JSUtils.Form.get(_$container, 'subject');
			var message = JSUtils.Form.get(_$container, 'message');
			
			JSUtils.Form.hideErrorForField(_$container, 'name');
			JSUtils.Form.hideErrorForField(_$container, 'email');
			JSUtils.Form.hideErrorForField(_$container, 'gs-email');
			JSUtils.Form.hideErrorForField(_$container, 'location');
			JSUtils.Form.hideErrorForField(_$container, 'subject');
			JSUtils.Form.hideErrorForField(_$container, 'message');
			
			var valid = true;
			if(!name){
				JSUtils.Form.displayErrorForField(_$container, 'name');
				valid = false;
			}
			
			if(!email || !JSUtils.Type.validateEmail(email)){
				JSUtils.Form.displayErrorForField(_$container, 'email');
				valid = false;
			}
			
			if(gsEmail && !JSUtils.Type.validateEmail(gsEmail)){
				JSUtils.Form.displayErrorForField(_$container, 'gs-email');
				valid = false;
			}
			
			if(!location){
				JSUtils.Form.displayErrorForField(_$container, 'location');
				valid = false;
			}
			
			if(!subject){
				JSUtils.Form.displayErrorForField(_$container, 'subject');
				valid = false;
			}
			
			if(!message){
				JSUtils.Form.displayErrorForField(_$container, 'message');
				valid = false;
			}
			
			if(!valid) return;
			
			_$container.find('[data-form-error]').addClass('hide');
			
			var req = {
			    type : 'SupportEmail',
			    category : 'CONTACT_HELP', 
			    name : name,
			    contactEmail: email,
			    location: location,
			    subject: subject,
			    message: message 
			};
			
			if(gsEmail){
				req.gigskyEmail = gsEmail;
			}
			
			if(device){
				req.mobileDevice = device;
			}
			
			ensuant.toovia.ui.Utils.displayProgress();
			$.ajax({
				type: 'POST',
				url:ensuant.gigsky.Config.gsAPIURL+'/api/v4/supportEmail',
				contentType: 'application/json',
				data: JSON.stringify(req),
				dataType: 'json',
				success: function(response){
					ensuant.toovia.ui.Utils.hideProgress();
					_$container.find('[data-form-success]').removeClass('hide');
					_$container.find('[data-form]').remove();
				},
				error : function(jqXHR, textStatus, errorThrown) {
					ensuant.toovia.ui.Utils.hideProgress();
					_$container.find('[data-form-error]').removeClass('hide');
				}
			});
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Widget(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.widgets.contact-form.js

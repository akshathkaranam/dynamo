/**
 * Author: Jack Vinijtrongjit
 * Version: $Revision$
 * Last Revision: $Date$
 * Modified By: $LastChangedBy$
 * Last Modified: $LastChangedDate$
 * Filesource: $URL$
 * <p/>
 * Copyright (c) 2010-2012 Ensuant Inc. All Rights Reserved.
 * Confidential and proprietary information of Ensuant Inc.
 */
var ensuant = ensuant ? ensuant : {};
ensuant.gigsky = ensuant.gigsky ? ensuant.gigsky : {};
ensuant.gigsky.ui = ensuant.gigsky.ui ? ensuant.gigsky.ui : {};
ensuant.gigsky.ui.widgets = ensuant.gigsky.ui.widgets ? ensuant.gigsky.ui.widgets : {};
ensuant.gigsky.ui.widgets.LogoDisplay = function() {
	
	var _VERSION = '0.0.1';
	var _namespace = 'logo-display';
	
	var Widget = function(options){
		var _$container = null;
		var _$cells = [];
		var _$logos = [];
		var _lastCell = -1;
		
		var _init = function(options){
			_$container = options.container;
			$.each(_$container.find('[data-extras] img'), function(k, logo){
				_$logos.push($(logo));
			});
			
			$.each(_$container.find('[data-cell]'), function(k, cell){
				_$cells.push($(cell));
			});
			
			_nextLogo = _$cells.length;
			
			setInterval(function(){
				if(_$container.is(':visible')){
					_rotate();
				}
			}, 3000);
			
			_assignEvents();
		};
		
		var _assignEvents = function(){
			
		};
		
		var _nextCell = function(){
			var nextCell = Math.ceil(Math.random() * (_$cells.length + 1)) - 1;
			if(nextCell < 0){
				nextCell = 0;
			}else if(nextCell >= _$cells.length){
				nextCell = _$cells.length - 1;
			}
			
			return nextCell;
		};
		
		var _rotate = function(){
			var nextCell = _nextCell();
			while(nextCell === _lastCell){
				nextCell = _nextCell();
			}
			
			_lastCell = nextCell;
			
			var $targetCell = _$cells[nextCell];
			var $nextLogo = _$logos.shift();
			
			var $cur = $targetCell.children();
			$.each($cur, function(k, entry){
				_$logos.push($(entry).detach());
			});
			
			$nextLogo.css('display', 'none');
			$targetCell.append($nextLogo);
			$nextLogo.fadeIn();
		};
		
		_init(options);
	};
	
	var _call = function(options){
		var obj = options.container.data(_namespace);
		if(obj){
			if(options.action){
				if(options.action === 'get'){
					return obj;
				}else{
					return obj[options.action](options.actionOptions);
				}
			}else{
				return obj;
			}
		}
		
		if(!options.action){
			obj = new Widget(options);
			options.container.data(_namespace, obj);
			return obj;
		}
	};
	
	return{
		VERSION: _VERSION,
		call: _call
	};
	
}();
//@ sourceURL=ensuant.gigsky.ui.widgets.logo-display.js

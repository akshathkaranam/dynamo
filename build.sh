#!/bin/bash
# Check if arguments are supplied
if [ $# -eq  0 ] ; then
  echo -e "ERROR: Missing arguments"; exit 1;
fi

rm -fr webapp.zip

export cdnpath="/"
export GEN_BUILD_NUMBER=$1
export GIT_REVISION=$(git rev-parse HEAD)

echo "{\"buildno\":\"$GEN_BUILD_NUMBER\",\"date\":\"$(date +"%Y-%m-%d_%H-%M-%S")\",\"git_revision\":\"$GIT_REVISION\"}" > versiondetails.json

npm install
grunt --cdnPath=$cdnpath --buildNumber=$GEN_BUILD_NUMBER

cp -rf dist wc-prod-cdn$GEN_BUILD_NUMBER/
cp -rf .ebextensions wc-prod-cdn$GEN_BUILD_NUMBER/
cp -rf .well-known wc-prod-cdn$GEN_BUILD_NUMBER/
rm -fr wc-prod-cdn$GEN_BUILD_NUMBER/node_modules
cp versiondetails.json wc-prod-cdn$GEN_BUILD_NUMBER/

chmod 0744 wc-prod-cdn$GEN_BUILD_NUMBER/prepare.sh

cd wc-prod-cdn$GEN_BUILD_NUMBER

zip -r ../webapp.zip .
cd ../
rm -fr wc-prod-cdn$GEN_BUILD_NUMBER
echo "build successful"

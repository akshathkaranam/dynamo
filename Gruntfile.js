"use strict";

module.exports = function(grunt) {

    require('time-grunt')(grunt);

    grunt.file.defaultEncoding = 'utf8';

    var cdnURL = grunt.option('cdnPath');
    var buildNum  = grunt.option('buildNumber');

    if((cdnURL == undefined) || (buildNum == undefined)){
        grunt.fail.fatal('Invalid arguments!! \n run grunt as: grunt --cdnPath=cdnPath --buildNumber=buildNumber');
    }

    cdnURL = cdnURL+'dynamo-assets/'+buildNum;

    var stringReplace = {};

    var language = ['ja','de','it','fr','es','zh','zh-TW','zh-HK'];

    //logic to add localize for corresponding language index files
    // & replacing verifyCaptiveMode.js path to not be part of cdn
    //adding meta tag
    language.forEach(function (lang) {
        stringReplace['localize_'+lang] = {
            src:['dist/index.html'],
            dest:'dist/'+lang+'/index.html',
            options:{
                replacements:[
                    {
                        pattern:/<!--[ \t]*Localizejs[ \t]*Bootstrap[ \t]*-->/,
                        replacement: function (match) {
                            return grunt.file.read('dist/scripts/data/'+lang+'.js') + '\n<script>Localize.setLanguage("'+lang+'") </script>';
                        }
                    },
                    {
                        pattern:/<!--[ \t]*meta[ \t]*for[ \t]*bing[ \t]*index[ \t]*-->/,
                        replacement:'<meta name="msvalidate.01" content="ABC0FBB9C96CEC562221A2DD5D70D1A6"\/>'
                    }
                ]
            }
        };
    });

    stringReplace['meta_index'] = {
        src:['dist/index.html'],
        dest:'dist/index.html',
        options:{
            replacements:[
                {
                    pattern:/<!--[ \t]*meta[ \t]*for[ \t]*bing[ \t]*index[ \t]*-->/,
                    replacement:'<meta name="msvalidate.01" content="ABC0FBB9C96CEC562221A2DD5D70D1A6"\/>'
                }
            ]
        }
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            build: {
                cwd: '.',
                src: ['**']  ,
                dest: 'dist',
                expand: true
            },
            build_cdnassets: {
                cwd:'dist',
                src:['bower_components/**','html/**','styles/**' ,'scripts/config.js','scripts/verifyCaptiveMode.js','static/**'],
                dest:'dist/dynamo-assets/'+buildNum,
                expand: true
            },
            build_index:{
                files:[
                    { src:['dist/index.html'], dest: 'dist/app/index.html'},
                    { src:['dist/index.html'], dest: 'dist/en/index.html'}
                ]
            },
            build_shopify:{
                files:[
                    { src:['dist/shop.html'], dest: 'dist/shop'},
                    { src:['dist/terms-and-conditions.html'], dest: 'dist/terms-and-conditions'},
                    { src:['dist/terms-of-use.html'], dest: 'dist/terms-of-use'},
                    { src:['dist/privacy-policy.html'], dest: 'dist/privacy-policy'},
                ]
            },
            build_shopify_en:{
                files:(function () {
                    return [
                        {
                            cwd:'dist',
                            src:['shop', 'terms-and-conditions', 'terms-of-use', 'privacy-policy'],
                            dest:'dist/app',
                            expand:true
                        },
                        {
                            cwd:'dist',
                            src:['shop', 'terms-and-conditions', 'terms-of-use', 'privacy-policy'],
                            dest:'dist/en',
                            expand:true
                        }
                    ];
                })()
            },
            build_static_localize:{
              files:(function () {
                  var langFolder = ['app','en','ja','de','it','fr','es','zh','zh-TW','zh-HK'];
                  var fileOutput = [];
                  langFolder.forEach(function (lang) {
                      fileOutput.push({
                          cwd:'dist',
                          src:['**.html','**.php', 'static/**', 'styles/**', 'bower_components/**', 'third_party/**',
                              'shop', 'terms-and-conditions', 'terms-of-use', 'privacy-policy'],
                          dest:'dist/'+lang,
                          expand:true
                      });
                  });
                  return fileOutput;
              })()
            }
        },
        clean: {
            build: {
                src: ['dist','.tmp']
            }
        },
        uglify: {
            options : {
                beautify : {
                    ascii_only : true
                }
            },
            minify_app_individual: {
                files:[{
                    src:['scripts/**/*.js', '!scripts/data/**','!scripts/verifyCaptiveMode.js','!scripts/config.js'],
                    dest:'dist/dynamo-assets/'+buildNum,
                    expand:true
                }]
            }
        },
        cdn:{
            'build_index_files':{
                options:{
                    cdn: cdnURL,
                    ignorePath:'scripts/verifyCaptiveMode.js',
                    flatten: true
                },
                cwd:'dist',
                src:['index.html','shop.html', 'terms-and-conditions.html', 'terms-of-use.html', 'privacy-policy.html'],
                dest:'dist'
            }
        },
        'string-replace':Object.assign(stringReplace),
        useminPrepare:{
            html:'dist/index.html',
            options:{
                dest:'dist/dynamo-assets/'+buildNum,
                flow:{
                    steps:{
                        js:['concat','uglify'],
                        css:['concat','cssmin']
                    },
                    post:{}
                }
            }
        },
        usemin:{
            html:['dist/index.html']
        }

    });


    // load the tasks
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-cdn');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-usemin');

    // define the tasks
   grunt.registerTask('default', ['clean','copy:build','copy:build_cdnassets','useminPrepare','concat','uglify','cssmin','usemin','cdn:build_index_files','copy:build_shopify','copy:build_shopify_en','copy:build_static_localize','string-replace','copy:build_index']);

};

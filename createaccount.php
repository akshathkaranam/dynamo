<?php
    require('third_party/autoload.php');

    parse_str($_SERVER['QUERY_STRING']);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $post_body = file_get_contents('php://input');
        $reqObject = json_decode($post_body);
    }
    else
    {
        http_response_code(405);
        return;
    }


    $recaptcha = new \ReCaptcha\ReCaptcha("6LdL3AgTAAAAAD8gGJrdLznY00pwIEz26_aK2olD");
    $resp = $recaptcha->verify($reqObject->{'code'}, NULL);
    if ($resp->isSuccess()) {
        //CAPTCHA verification successful
    } else {

        header('Content-Type: application/json');
        http_response_code(400);
        echo '{ "errorInt" : 20001, "httpStatus" : "Bad Request", "type" : "error", "userDisplayErrorStr" : "CAPTCHA verification failed. Please try again.", "errorStr" : "CAPTCHA verification failed. Please try again."}';
        return;
    }

    $url = urldecode($des)."account";

    if(isset($reqObject->{'referralCode'})){
        $body = '{"type":"AccountDetails", "actCreationType": "GIGSKY", "emailId":"'. $reqObject->{'emailId'} .'", "password":"'. $reqObject->{'password'} .'", "country":"'. $reqObject->{'country'} .'", "referralCode":"'. $reqObject->{'referralCode'} .'", "tncAndPPAcceptanceStatus":"'. $reqObject->{'tncAndPPAcceptanceStatus'} .'"}';
    }else{
        $body = '{"type":"AccountDetails", "actCreationType": "GIGSKY", "emailId":"'. $reqObject->{'emailId'} .'", "password":"'. $reqObject->{'password'} .'", "country":"'. $reqObject->{'country'} .'", "tncAndPPAcceptanceStatus":"'. $reqObject->{'tncAndPPAcceptanceStatus'} .'"}';
    }
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        $acceptlang = "Accept-Language: " . $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    else
        $acceptlang = "Accept-Language: en";

    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json",$acceptlang,));

    $response = curl_exec($ch);

    http_response_code(curl_getinfo($ch,CURLINFO_HTTP_CODE));
    header('Content-Type: application/json');
    echo $response;
    curl_close($ch);
    return;
?>
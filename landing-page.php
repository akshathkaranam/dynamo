<?php

    include('commonutils.php');

    parse_str($_SERVER['QUERY_STRING']);

    //echo $csn;
    $srcip = get_ip();

    $langs = array();

    setcookie("srcip",$srcip,0,"/");

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $post_body = file_get_contents('php://input');
        parse_str($post_body);
    }
    else if($_SERVER['REQUEST_METHOD'] === 'GET')
    {

    }
    else
    {
        http_response_code(405);
        return;
    }
    if (strpos($_SERVER[HTTP_HOST],'gigsky') !== false) {
        $url = "Location:https://$_SERVER[HTTP_HOST]";
    }
    else
    {
        $url = "Location:http://$_SERVER[HTTP_HOST]";
    }

     //if($eid == '89033023422210000000000252967388' || $eid == '89033023421700000000000218670510' || $eid == '89033023421700000000000215333807'){
       //  $url = "Location:https://msapp-stage.gigsky.com";
     //}

    $params_begin = 0;

    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        // break up string into pieces (languages and q factors)
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

        if (count($lang_parse[1])) {
            // create a list like "en" => 0.8
            $langs = array_combine($lang_parse[1], $lang_parse[4]);

            // set default to 1 for any without q factor
            foreach ($langs as $lang => $val) {
                if ($val === '') $langs[$lang] = 1;
            }

            // sort list based on value
            arsort($langs, SORT_NUMERIC);
        }
    }
    else {
    // set english as default language if no accept language header is fond
    $langs['en'] = 1;
    }

    // look through sorted list and use first one that matches our languages
    $acceptable_lang_found = 0;
    foreach ($langs as $lang => $val) {
        if (stripos($lang, 'de') === 0 || stripos($lang, 'ja') === 0 || stripos($lang, 'en') === 0 || stripos($lang, 'es') === 0 || stripos($lang, 'fr') === 0 || stripos($lang, 'it') === 0 || stripos($lang, 'zh-HK') === 0 || stripos($lang, 'zh-CN') === 0 || stripos($lang, 'zh-TW') === 0)
        {
            $curlang = substr($lang,0,2);
            if($curlang === 'zh')
            {
               $region = substr($lang,3,2);
                if($region != 'cn')
                {
                    $curlang = $curlang . '-' . strtoupper($region);
                }
            }
            if($curlang === 'en')
                $url = $url . '/index.html#/';
            else
                $url = $url . '/' . $curlang . '/index.html#/';
            $acceptable_lang_found = 1;
            break;
        }
    }

   if($acceptable_lang_found == 0)
   {
     $url = $url . '/index.html#/';
   }


//    $url = 'Location:index.html#/noservice';

    $eidPresent = 0;
    $queryParam = '';
    if($eid != NULL)
    {
        $queryParam = $queryParam . '?eid=' . $eid;
        if($iccids != NULL)
            $queryParam = $queryParam . '&iccid=' . $iccids;
        if($transactionId != NULL)
            $queryParam = $queryParam . '&moTransactionId=' . $transactionId;
        $params_begin = 1;
        $eidPresent = 1;


    }




    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
       $userAgent = $_SERVER['HTTP_USER_AGENT'];
       $msg = "[path: ". $_SERVER['PHP_SELF']. "] [body: " .$post_body . "][ua: " . $userAgent . "]";
       error_log($msg,0);
    }

    $locPresent = 0;
    if($location != NULL)
    {
        $locPresent = 1;
        if($eid != NULL){
               //if($eid == '89033023422210000000000252967388' || $eid == '89033023421700000000000218670510' || $eid == '89033023421700000000000215333807')
               //{
                //    $queryParam = $queryParam . '&loc=JP';
               //}
               //else
               //{
                $queryParam = $queryParam . '&loc=' . $location;
               //}
        }
    }

    if($eidPresent == 0 || $locPresent == 0)
    {
        $url = $url . 'moerror?eidPresent=' . $eidPresent . '&locPresent=' . $locPresent;
    }
    else
        {
            //if(preg_match('/^\d{5}024\d+$/', $eid) && $eid != '89033024301007060049464100006669' && $eid != '89033024341008060050879400043778' && $eid != '89033024301007060049464100030822' && $eid != '89033023424200000000000255018659' && $eid != '89033023424200000000000255017592' && $eid != '89033024301007060049464100001043' && $eid != '89033024341008061684500100001591'){
            //    $url = $url . 'moerror?eidError=1';
            //}else
            //{
                $url = $url . 'marketing' . $queryParam;
            //}
        }

    //echo $url;
     header($url);
?>

#!/bin/bash

green=`tput setaf 2`
reset=`tput sgr0`
logging=0
command=""


# Log based on a flag
function log()
{
  if [ 1 -eq "$logging" ] ; then
  echo $1: ${green} $2 ${reset}
  fi
}

# Prepare sed replace command for the specified key value pair
function prepare()
{
  local KEY=$1
  local VAL=$2
  log "Preparing with Key, Value" "$1, $2"

  if [ ! -z "$VAL" ] ; then
  find="$KEY =\([^\,|\}]*\)"
  replace="$KEY = \"$VAL\";"
    if [ ! -z "$command" ] ; then
      command="${command} ; s~$find~$replace~"
    else
      command="s~$find~$replace~"
    fi
    log "Command" "$command"
  fi
}

# Check if logging is enabled
if [ "-l" == "$3" ] ; then
  logging=1
fi

# Check if argument is supplied
log "No. of arguments:" "$#"
if [ $# -eq 0 ] ; then
  echo -e "ERROR: No arguments supplied\nScript usage: $(basename $0) <properties-file>\n" >&2; exit 1;
fi

# Read file from path
log "Properties file path" "$1"
. $1

export buildno=`cat versiondetails.json | sed -n 's/.*buildno":"\([0-9]*\)\".*$/\1/p'`

prepare "GS_SERVER_BASE_URL" $GS_SERVER_BASE_URL
prepare "GS_REFERRAL_SERVER_BASE_URL" $GS_REFERRAL_SERVER_BASE_URL
prepare "APPLE_PAY_MODE" $APPLE_PAY_MODE
prepare "LAZY_LOAD_BASE" $LAZY_LOAD_BASE$buildno"/"

# Find main bundle path
if [ "./prepare.sh" == "$0" ] ; then
  path='dynamo-assets/'$buildno'/scripts/'
else
  path="${0//assets\/prepare.sh/}"
fi

log "Main bundle path" "$path"
sed -i.backup "$command" ${path}config.js

/usr/sbin/httpd -D FOREGROUND
#/usr/local/apache2/bin/httpd -D FOREGROUND
echo "Successful"
# Usage ./prepare.sh application.<env>.properties

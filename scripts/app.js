'use strict';
  angular.module('gigSkyWebApp', ['ui.bootstrap','pascalprecht.translate','ngRoute','ngResource','infinite-scroll','angulartics', 'angulartics.google.analytics','ngclipboard'])
    .config( ["$routeProvider","$locationProvider","$httpProvider",'$sceDelegateProvider',function ($routeProvider,$locationProvider,$httpProvider,$sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from CDN.
            '*://cdn.gigsky.com**',
            '*://cdn-test.gigsky.com**',
            '*://cdn-prod.gigsky.com**',
            '*://cdn-stage.gigsky.com**'
        ]);
    $httpProvider.interceptors.push('GSHttpInterceptors');

    $routeProvider
        .when('/signUp', {
          templateUrl: LAZY_LOAD_BASE+'html/views/sign-up.html'
        })
        .when('/signUp-concur', {
            templateUrl: LAZY_LOAD_BASE+'html/views/signUp-concur.html'
        })
        .when('/login', {
            templateUrl: LAZY_LOAD_BASE+'html/views/log-in.html'
        }).when('/login/:options', {
            templateUrl: LAZY_LOAD_BASE+'html/views/log-in.html'
        })
        .when('/login-link-concur', {
            templateUrl: LAZY_LOAD_BASE+'html/views/login-link-concur.html'
        })
        .when('/login-link-concur/:options', {
            templateUrl: LAZY_LOAD_BASE+'html/views/login-link-concur.html'
        })
        .when('/concur-connected',{
            templateUrl: LAZY_LOAD_BASE+'html/views/concur_link_confirmation.html'
        })
        .when('/concur-connected-on-signup',{
            templateUrl: LAZY_LOAD_BASE+'html/views/concur_link_confirmation.html'
        })
        .when('/concur-on-added-sim',{
            templateUrl: LAZY_LOAD_BASE+'html/views/concur_link_confirmation.html'
        })
        .when('/forceChangePassword', {
            templateUrl: LAZY_LOAD_BASE+'html/views/change-password.html'
        })
        .when('/forceChangePassword/:options', {
            templateUrl: LAZY_LOAD_BASE+'html/views/change-password.html'
        })
        .when('/editAccount', {
          templateUrl: LAZY_LOAD_BASE+'html/views/editAccount.html'
        })
        .when('/logout', {
          templateUrl: LAZY_LOAD_BASE+'html/views/log-in.html'
         })
        .when('/forgotPasswd-concur', {
            templateUrl: LAZY_LOAD_BASE+'html/views/forgot-password.html'
         })
         .when('/forgotPasswd', {
           templateUrl: LAZY_LOAD_BASE+'html/views/forgot-password.html'
          })
        .when('/forgotpwd', {
            templateUrl: LAZY_LOAD_BASE+'html/views/forgot-password.html'
        })
         .when('/paymentMethod', {
           templateUrl: LAZY_LOAD_BASE+'html/views/payment-method.html'
          })
        .when('/paymentProcessed', {
            controller:'paymentProcessedController',
            templateUrl: LAZY_LOAD_BASE+'html/views/payment-proceed.html'
        })
          .when('/addCC', {
        	 templateUrl: LAZY_LOAD_BASE+'html/views/creditCard.html'
          })
          .when('/editCC/:billingId', {
             templateUrl: LAZY_LOAD_BASE+'html/views/creditCard.html'
          })
          .when('/activate', {
              templateUrl: LAZY_LOAD_BASE+'html/views/userConfirmations.html'
          })
          .when('/reset', {
            templateUrl: LAZY_LOAD_BASE+'html/views/userConfirmations.html'
          })
          .when('/unblock', {
            templateUrl: LAZY_LOAD_BASE+'html/views/userConfirmations.html'
          })
          .when('/confirmemail', {
              templateUrl: LAZY_LOAD_BASE+'html/views/userConfirmations.html'
          })
          .when('/sendactlink', {
            templateUrl: LAZY_LOAD_BASE+'html/views/send-activationlink.html'
          })
          .when('/addPP', {
        	  templateUrl: LAZY_LOAD_BASE+'html/views/payPal.html'
          })
          .when('/editPP/:billingId', {
        	   templateUrl: LAZY_LOAD_BASE+'html/views/payPal.html'
          })
          .when('/gigskyCredit', {
             templateUrl: LAZY_LOAD_BASE+'html/views/gigsky-credit.html'
           })
           .when('/paymentHistory', {
        	   templateUrl: LAZY_LOAD_BASE+'html/views/payment-history.html'
           })
        .when('/creditHistory', {
                templateUrl: LAZY_LOAD_BASE+'html/views/credit-history.html'
            })
	      .when('/buy-plan1', {
              controller:'browsePlansController',
              templateUrl: LAZY_LOAD_BASE+'html/views/buy-plan1.html'
          })  
          .when('/buy-plan2', {
              templateUrl: LAZY_LOAD_BASE+'html/views/buy-plan2.html'
          })                       
          .when('/buy-plan3', {
              controller:'makePaymentController',
              templateUrl: LAZY_LOAD_BASE+'html/views/buy-plan3.html',
              reloadOnSearch:false
          })    
          .when('/dashboard-selectplan', {
              templateUrl: LAZY_LOAD_BASE+'html/views/dashboard-selectplan.html'
          })   
	      .when('/welcome', {
	    	  templateUrl: LAZY_LOAD_BASE+'html/views/welcome.html'
          })
          .when('/signUpConfirm', {
              controller: 'EmailConfirmationController',
              templateUrl: LAZY_LOAD_BASE+'html/views/accountConfirmation.html'
          })
          .when('/dashboard', {
        	  controller: 'dashBoardController',
              templateUrl: LAZY_LOAD_BASE+'html/views/dashboard.html'
          })
          .when('/payment-method_1', {
              templateUrl: LAZY_LOAD_BASE+'html/views/payment-method_1.html'
          })  
	      .when('/usage', {
	    	  
	    	  templateUrl: LAZY_LOAD_BASE+'html/views/usage.html'
          })  
        .when('/sims', {
         
              templateUrl: LAZY_LOAD_BASE+'html/views/sims.html'
          })
        .when('/my-sims', {

            templateUrl: LAZY_LOAD_BASE+'html/views/my-simList.html'
        })
        .when('/selectsim', {
            templateUrl: LAZY_LOAD_BASE+'html/views/my-simList.html'
        })
          .when('/activesim', {
              templateUrl: LAZY_LOAD_BASE+'html/views/activateSim.html'
          })
        .when('/activesimfromdashboard', {
            templateUrl: LAZY_LOAD_BASE+'html/views/activateSim.html'
        })
            .when('/simactivatedashboard', {
                templateUrl: LAZY_LOAD_BASE+'html/views/activateSim.html'
            })
          .when('/replacesim', {
            templateUrl: LAZY_LOAD_BASE+'html/views/replace-sim.html'
          })
          .when('/linked-accounts', {
              controller:'LinkedAccountsController',
              templateUrl: LAZY_LOAD_BASE+'html/views/linked-accounts.html',
              reloadOnSearch:false
          }) 
            .when('/changeEmail', {
                templateUrl: LAZY_LOAD_BASE+'html/views/edit-email.html'
            })
            .when('/changePassword', {
                templateUrl: LAZY_LOAD_BASE+'html/views/change-password.html'
            })
          .when('/editProfile', {
            
              templateUrl: LAZY_LOAD_BASE+'html/views/edit-profile.html'
          })
          .when('/simReplace', {
            templateUrl: LAZY_LOAD_BASE+'html/views/simReplace.html'
          })
          .when('/confirmDoNotReplace', {
            templateUrl: LAZY_LOAD_BASE+'html/views/confirmDoNotReplace.html'
          })
          .when('/simReplaceAddress', {
            templateUrl: LAZY_LOAD_BASE+'html/views/simReplaceAddress.html'
          })
          .when('/unblocksim', {
           
              templateUrl: LAZY_LOAD_BASE+'html/views/unblocksim.html'
          })    
          .when('/editSIM', {
              templateUrl: LAZY_LOAD_BASE+'html/views/editSIM.html'
          })
          .when('/help', {
           
              templateUrl: LAZY_LOAD_BASE+'html/views/help.html'
          })
          .when('/about', {
              templateUrl: LAZY_LOAD_BASE+'html/views/about-app.html'
          })
          .when('/locUn', {
           
              templateUrl: LAZY_LOAD_BASE+'html/views/location-unavailable.html'
          }).when('/support', {
            controller:'supportController',
            templateUrl: LAZY_LOAD_BASE+'html/views/support.html'
        })
        .when('/creditBalance', {
            templateUrl: LAZY_LOAD_BASE+'html/views/credit-balances.html'
        })
        .when('/referFriend', {
            templateUrl: LAZY_LOAD_BASE+'html/views/referFriend.html'
        })
        .when('/moerror', {
            controller:'MODirectErrorCtrl',
            templateUrl: LAZY_LOAD_BASE+'html/views/moerror.html'
        })
        .when('/acceptTerms', {
            controller: 'AcceptTermsController',
            templateUrl: LAZY_LOAD_BASE+'html/views/accept-terms-of-service.html'
        })
        .when('/marketing', {
            controller: 'MarketingController',
            templateUrl: LAZY_LOAD_BASE+'html/views/marketing.html'
        })
        .when('/cancel-service', {
            controller: 'CancelServiceController',
            templateUrl: LAZY_LOAD_BASE+'html/views/cancel-service.html'
        })
        .when('/cancel-service-confirmation', {
            controller: 'CancelServiceController',
            templateUrl: LAZY_LOAD_BASE+'html/views/cancel-service-confirmation.html'
        })
        .when('/network/:networkId', {

            controller: 'networkViewController',
            templateUrl: LAZY_LOAD_BASE+'html/views/network.html'

        })
        .when('/plan-coverage/:networkId', {

            controller: 'planCoverageController',
            templateUrl: LAZY_LOAD_BASE+'html/views/plan-coverage.html'

        })
        .otherwise({ redirectTo: '/login' });        
          //  $locationProvider.html5Mode(true);
    }]
    ).run(['$rootScope','$window','$location','$route','$http','GSSessionStorage','$analytics','referralService',function($rootScope,$window,$location,$route,$http,gsStorage,$analytics,referralService){

          if($location.path().indexOf('concur')!=-1 )
          {
              $rootScope.linkingToConcur = true;
          }

          var userAgent = $window.navigator.userAgent;
          if(userAgent.indexOf("Windows NT 10")!=-1 || userAgent.indexOf("WM 10")!=-1)
          {
              $rootScope.isWindowsDevice = true;
          }

          $rootScope.getCurrentYearForCopyright = function() {
              return new Date().getFullYear();
          };

          $rootScope.config = {
              gsWebAppURL: WEBAPP_URL,
              gsAppleSIMURL:APPLE_SIM_URL,
              gsGigSkySIMURL:GIGSKY_SIM_URL,
              gsBusinessURL:BUSINESS_URL,
              gsStaticURL:GIGSKY_STATIC_URL
          };
          $rootScope.lazyLoadURL = LAZY_LOAD_BASE;

          $rootScope.enableLocalization = ENABLE_LOCALIZATION;
          $rootScope.enableSim1ToSim2Transition = ENABLE_SIM1_TO_SIM2_TRANSITION;
          $rootScope.enableAutoDetectLanguage = ENABLE_AUTODETECT_LANGUAGE;
          $rootScope.enableCurrencyEdit = ENABLE_CURRENCY_EDIT;
          $rootScope.staticbaseurl = STATIC_BASE_URL;

          $rootScope.app_version = APP_VERSION_NO;

          referralService.getReferralServiceStatus().success(function (response) {
              $rootScope.isReferralEnabled = response.referralEnable;
          }).error(function (err) {
              $rootScope.isReferralEnabled = false;
             console.log(err);
          });

          gsStorage.overrideStorageInPrivateBrowse();
          var token = sessionStorage.getItem('token');
          $rootScope.isCaptive = IS_CAPTIVE_MODE;
          var networkMode = $rootScope.isCaptive?'Captive':'Cellular';
          $analytics.setUserProperties({'dimension8': 'Cellular'});
          $rootScope.enableConcur = ENABLE_CONCUR;
          $rootScope.getGSLogoTargetLink = function(){
              if($rootScope.linkingToConcur)
              {
                  return '';
              }

              if($rootScope.isCaptive){
                  return '#/dashboard';
              }
              else{
                  if($rootScope.enableAutoDetectLanguage){
                      return $rootScope.staticbaseurl;
                  }else{
                      if($rootScope.currentLangCode != 'en'){
                          return $rootScope.staticbaseurl+'/'+ $rootScope.currentLangCode+"/";
                      }else{
                          return $rootScope.staticbaseurl;
                      }
                  }
              }

          }

          if(!token){
              token = sessionStorage.getItem('temptoken');
          }

          if(token && token != undefined)
          {
              $http.defaults.headers.common.Authorization = "Basic "+token;
              var customerId = sessionStorage.getItem("customerId");
              if(customerId)
                  GS_API_BASE = GS_SERVER_BASE + GS_API_VERSION + "account/"+customerId+"/";
          }

          $rootScope.$on('$routeChangeStart', function() {
            $rootScope.pageLoading = true;
          });

          $rootScope.$on('$routeChangeSuccess', function() {
              $rootScope.pageLoading = false;
          });

          $rootScope.$on('$routeChangeError', function() {
              $rootScope.pageLoading = false;
              //TBD: Show appropriate error message not able to load page: And show retry option similar to Cayman.
          });

          $rootScope.sessionInfo = "";
          var supportedLanguages = SUPPORTED_LANGUAGES;
          var langSupported = LANG_SUPPORTED;
          var curPath = $location.absUrl();

          $rootScope.hasStorage = (function() {
              try {
                  localStorage.setItem(mod, mod);
                  localStorage.removeItem(mod);
                  return true;
              } catch (exception) {
                  return false;
              }
          }());

          if($rootScope.hasStorage == false)
          {
              $("#errorMsg").html("Sorry, our web application does not support current mode of browsing. Please switch to normal browse mode and retry.");
              return;
          }

          function setLang(lang){
              Localize.setLanguage(lang);
              $rootScope.currentLangCode = lang;
              setLangUrl(lang);
              sessionStorage.setItem("currentLanguageCode",lang);
              $rootScope.currentLanguage = langSupported[lang];
              sessionStorage.setItem("currentLanguage",$rootScope.currentLanguage);

              $('body').attr('data-cur-lang', lang);
          }

          function setLangUrl(lang){
              if(lang != 'en'){
                  $rootScope.currentLangUrl = '/'+lang;
              }else{
                  $rootScope.currentLangUrl = '';
              }
          }

          if($rootScope.enableAutoDetectLanguage){
              setLang(CURRENT_LANG);
          }else{
              for(var i=0;i<supportedLanguages.length;i++){
                  var l = supportedLanguages[i];
                  if(curPath.indexOf(l+"/")!=-1)
                  {
                      setLang(l);
                      break;
                  }
              }
          }


          $rootScope.changeLanguage = function (langKey,langValue,force) {

              var url = $location.absUrl();
              //Localize.setLanguage(langKey);
              //$('body').attr('data-cur-lang', langKey);

              if(langKey == 'en'){
                  url = url.replace($rootScope.currentLangCode+'/','');
              }else{
                  if($rootScope.currentLangCode == 'en'){
                      var i = url.indexOf('/app/');
                      var remPathPos =i;
                      if(url.indexOf('/en/app/')!=-1)
                      {
                         i = url.indexOf('/en/app/');
                          remPathPos  = i + 3;
                      }
                      var baseUrl =url.substr(0,i);
                      var urlLength = url.length;

                      url = baseUrl +'/'+ langKey + url.substr(remPathPos,urlLength-1)
                  }else{
                      url = url.replace($rootScope.currentLangCode+'/',langKey+'/');
                  }
              }

              $('.custPanel').hide();
              if(!force && $rootScope.currentLanguage==langValue)
              {
                  return;
              }

              if(force && langKey!= 'en')
              {
                  url = url.replace("app/",langKey+"/app/");
              }

              $rootScope.currentLanguage = langValue;
              $rootScope.currentLangCode = langKey;
              setLangUrl(langKey);
              sessionStorage.setItem("currentLanguage",langValue);
              sessionStorage.setItem("currentLanguageCode",langKey);
              $rootScope.$broadcast("onLanguageChanged");


              if(url != $location.absUrl() || force){
                  window.location = url;
              }
              else
              {
                  $location.path($location.path());
                  $route.reload();
              }
          };

          var curPath = $location.absUrl();
          $rootScope.currentLanguage = sessionStorage.getItem("currentLanguage");
          $rootScope.currentLangCode = sessionStorage.getItem("currentLanguageCode");
          setLangUrl($rootScope.currentLangCode);

          $analytics.setUserProperties({dimension6: $rootScope.currentLangCode});


      //check from browser settings current language..
          var language = window.navigator.userLanguage || window.navigator.language;
          if(language)
          {
              language = language.substr(0,2);
          }

          if(!$rootScope.enableAutoDetectLanguage){
              if ($rootScope.enableLocalization) {

                  var isChangeLanguageCalled = false;
                  var isUrlHasLanguageCode = false;
                  for (var i = 0; i < supportedLanguages.length; i++) {
                      var langCode = supportedLanguages[i];
                      var langCodeUrl = langCode + '/';

                      if (curPath.indexOf(langCodeUrl) != -1 && $rootScope.currentLangCode != langCode) {
                          $rootScope.changeLanguage(langCode, langSupported[langCode]);
                          isChangeLanguageCalled = true;
                          break;
                      } else if (curPath.indexOf(langCodeUrl) != -1) {
                          isUrlHasLanguageCode = true;
                      }
                  }

                  if (!isChangeLanguageCalled && !isUrlHasLanguageCode) {

                      for (var i = 0; i < supportedLanguages.length; i++) {
                          var langCode = supportedLanguages[i];
                          if (language == langCode) {
                              $rootScope.changeLanguage(langCode, langSupported[langCode], true);
                              break;
                          }
                      }
                  } else {

                      if (!$rootScope.currentLangCode) {
                          $rootScope.currentLanguage = "English";
                          $rootScope.currentLangCode = 'en';
                          setLangUrl($rootScope.currentLangCode);
                          sessionStorage.setItem("currentLanguage", $rootScope.currentLanguage);
                          sessionStorage.setItem("currentLanguageCode", $rootScope.currentLangCode);
                      }

                  }
              } else {
                  $rootScope.changeLanguage('en', "English");
              }
          }

          /* //Disabling facebook sdk
          if(!$rootScope.isCaptive){
              $window.fbAsyncInit = function() {
                FB.init({
                    appId      : GS_APP_ID, // App ID
                    channelUrl : GS_FB_BASE + "channel.html", // Channel File
                    status     : true, // check login status
                    cookie     : true, // enable cookies to allow the server to access the session
                    xfbml      : true  // parse XFBML
                });

                $window.fbInitDone = true;

    //            if(fbInitCbk != null)
    //            {
    //                fbInitCbk();
    //            }

            };
            (function(d){
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
          }
          */

          var isMobile = {
              Android: function() {
                  return navigator.userAgent.match(/Android/i);
              },
              BlackBerry: function() {
                  return navigator.userAgent.match(/BlackBerry/i);
              },
              iOS: function() {
                  return navigator.userAgent.match(/iPhone|iPad|iPod/i);
              },
              Opera: function() {
                  return navigator.userAgent.match(/Opera Mini/i);
              },
              Windows: function() {
                  return navigator.userAgent.match(/IEMobile/i);
              },
              webOS: function() {
                  return navigator.userAgent.match(/webOS/i);
              },
              any: function() {
                  return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.webOS());
              }
          };

          var isTablet = {
              iOS: function() {
                  return navigator.userAgent.match(/iPad/i);
              },
              Android: function() {
                  return navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/Mobile/i);
              },
              any: function() {
                  return isTablet.iOS() || isTablet.Android();
              }
          };

          var _assignEvents = function(){
              $(window).resize(function(e){
                  _checkMobile();
              });
          };

          var _checkMobile = function(){
              if ($(window).width() < 960 || isMobile.any()) {
                  $('body').addClass('mobile-ui');

                  if(isTablet.any()){
                      $('body').addClass('wide');
                  }
                  return false;
              }else if(isTablet.any()){
                  if(isTablet.any()){
                      $('body').addClass('tablet-ui');
                  }
                  return false;
              }

              $('body').removeClass('mobile-ui tablet-ui wide');
          };

          _checkMobile();
          _assignEvents();
    }]);


Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}
Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
}

//ratingcontroller
//var RatingDemoCtrl = function ($scope) {
//  $scope.rate = 2;
//  $scope.max = 5;
//  $scope.isReadonly = false;
//
//  $scope.hoveringOver = function(value) {
//    $scope.overStar = value;
//    $scope.percent = 100 * (value / $scope.max);
//  };
//
//};

//Typeheadcontroller
function TypeaheadCtrl($scope, $http) {

  $scope.selected = undefined;
  $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
  // Any function returning a promise object can be used to load values asynchronously
/*  $scope.getLocations = function(val) {
    return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: val,
        sensor: false
      }
    }).then(function(res){
      var addresses = [];
      angular.forEach(res.data.results, function(item){
        addresses.push(item.formatted_address);
      });
      return addresses;
    });
  };*/

 // $scope.statesWithFlags = [{'name':'Alabama','flag':'5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png'},{'name':'Alaska','flag':'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png'},{'name':'Arizona','flag':'9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png'},{'name':'Arkansas','flag':'9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png'},{'name':'California','flag':'0/01/Flag_of_California.svg/45px-Flag_of_California.svg.png'},{'name':'Colorado','flag':'4/46/Flag_of_Colorado.svg/45px-Flag_of_Colorado.svg.png'},{'name':'Connecticut','flag':'9/96/Flag_of_Connecticut.svg/39px-Flag_of_Connecticut.svg.png'},{'name':'Delaware','flag':'c/c6/Flag_of_Delaware.svg/45px-Flag_of_Delaware.svg.png'},{'name':'Florida','flag':'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png'},{'name':'Georgia','flag':'5/54/Flag_of_Georgia_%28U.S._state%29.svg/46px-Flag_of_Georgia_%28U.S._state%29.svg.png'},{'name':'Hawaii','flag':'e/ef/Flag_of_Hawaii.svg/46px-Flag_of_Hawaii.svg.png'},{'name':'Idaho','flag':'a/a4/Flag_of_Idaho.svg/38px-Flag_of_Idaho.svg.png'},{'name':'Illinois','flag':'0/01/Flag_of_Illinois.svg/46px-Flag_of_Illinois.svg.png'},{'name':'Indiana','flag':'a/ac/Flag_of_Indiana.svg/45px-Flag_of_Indiana.svg.png'},{'name':'Iowa','flag':'a/aa/Flag_of_Iowa.svg/44px-Flag_of_Iowa.svg.png'},{'name':'Kansas','flag':'d/da/Flag_of_Kansas.svg/46px-Flag_of_Kansas.svg.png'},{'name':'Kentucky','flag':'8/8d/Flag_of_Kentucky.svg/46px-Flag_of_Kentucky.svg.png'},{'name':'Louisiana','flag':'e/e0/Flag_of_Louisiana.svg/46px-Flag_of_Louisiana.svg.png'},{'name':'Maine','flag':'3/35/Flag_of_Maine.svg/45px-Flag_of_Maine.svg.png'},{'name':'Maryland','flag':'a/a0/Flag_of_Maryland.svg/45px-Flag_of_Maryland.svg.png'},{'name':'Massachusetts','flag':'f/f2/Flag_of_Massachusetts.svg/46px-Flag_of_Massachusetts.svg.png'},{'name':'Michigan','flag':'b/b5/Flag_of_Michigan.svg/45px-Flag_of_Michigan.svg.png'},{'name':'Minnesota','flag':'b/b9/Flag_of_Minnesota.svg/46px-Flag_of_Minnesota.svg.png'},{'name':'Mississippi','flag':'4/42/Flag_of_Mississippi.svg/45px-Flag_of_Mississippi.svg.png'},{'name':'Missouri','flag':'5/5a/Flag_of_Missouri.svg/46px-Flag_of_Missouri.svg.png'},{'name':'Montana','flag':'c/cb/Flag_of_Montana.svg/45px-Flag_of_Montana.svg.png'},{'name':'Nebraska','flag':'4/4d/Flag_of_Nebraska.svg/46px-Flag_of_Nebraska.svg.png'},{'name':'Nevada','flag':'f/f1/Flag_of_Nevada.svg/45px-Flag_of_Nevada.svg.png'},{'name':'New Hampshire','flag':'2/28/Flag_of_New_Hampshire.svg/45px-Flag_of_New_Hampshire.svg.png'},{'name':'New Jersey','flag':'9/92/Flag_of_New_Jersey.svg/45px-Flag_of_New_Jersey.svg.png'},{'name':'New Mexico','flag':'c/c3/Flag_of_New_Mexico.svg/45px-Flag_of_New_Mexico.svg.png'},{'name':'New York','flag':'1/1a/Flag_of_New_York.svg/46px-Flag_of_New_York.svg.png'},{'name':'North Carolina','flag':'b/bb/Flag_of_North_Carolina.svg/45px-Flag_of_North_Carolina.svg.png'},{'name':'North Dakota','flag':'e/ee/Flag_of_North_Dakota.svg/38px-Flag_of_North_Dakota.svg.png'},{'name':'Ohio','flag':'4/4c/Flag_of_Ohio.svg/46px-Flag_of_Ohio.svg.png'},{'name':'Oklahoma','flag':'6/6e/Flag_of_Oklahoma.svg/45px-Flag_of_Oklahoma.svg.png'},{'name':'Oregon','flag':'b/b9/Flag_of_Oregon.svg/46px-Flag_of_Oregon.svg.png'},{'name':'Pennsylvania','flag':'f/f7/Flag_of_Pennsylvania.svg/45px-Flag_of_Pennsylvania.svg.png'},{'name':'Rhode Island','flag':'f/f3/Flag_of_Rhode_Island.svg/32px-Flag_of_Rhode_Island.svg.png'},{'name':'South Carolina','flag':'6/69/Flag_of_South_Carolina.svg/45px-Flag_of_South_Carolina.svg.png'},{'name':'South Dakota','flag':'1/1a/Flag_of_South_Dakota.svg/46px-Flag_of_South_Dakota.svg.png'},{'name':'Tennessee','flag':'9/9e/Flag_of_Tennessee.svg/46px-Flag_of_Tennessee.svg.png'},{'name':'Texas','flag':'f/f7/Flag_of_Texas.svg/45px-Flag_of_Texas.svg.png'},{'name':'Utah','flag':'f/f6/Flag_of_Utah.svg/45px-Flag_of_Utah.svg.png'},{'name':'Vermont','flag':'4/49/Flag_of_Vermont.svg/46px-Flag_of_Vermont.svg.png'},{'name':'Virginia','flag':'4/47/Flag_of_Virginia.svg/44px-Flag_of_Virginia.svg.png'},{'name':'Washington','flag':'5/54/Flag_of_Washington.svg/46px-Flag_of_Washington.svg.png'},{'name':'West Virginia','flag':'2/22/Flag_of_West_Virginia.svg/46px-Flag_of_West_Virginia.svg.png'},{'name':'Wisconsin','flag':'2/22/Flag_of_Wisconsin.svg/45px-Flag_of_Wisconsin.svg.png'},{'name':'Wyoming','flag':'b/bc/Flag_of_Wyoming.svg/43px-Flag_of_Wyoming.svg.png'}];
}

angular.module('gigSkyWebApp').controller('RatingModalControl',['$scope', '$modal',function($scope,$modal){
    var modalInstance = null;
    $scope.open = function (size) {

        modalInstance = $modal.open({
            templateUrl: 'myRatingModal.html',
            size: size,
            controller:['$scope','$modalInstance',function($scope,$modalInstance)
            {
                $scope.ok = function()
                {
                    $modalInstance.close();
                }
            }]

        });

        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });

    };
}
]);
//var RatingModalControl = function ($scope, $modal, $log) {
//
//  var modalInstance = null;
//  $scope.open = function (size) {
//
//    modalInstance = $modal.open({
//      templateUrl: 'myRatingModal.html',
//      size: size,
//      controller:ratingModalInstanceCtrl
//
//    });
//
//    modalInstance.result.then(function (selectedItem) {
//    }, function () {
//    });
//
//  };
//};
//
//var ratingModalInstanceCtrl = function($scope,$modalInstance)
//{
//    $scope.ok = function()
//    {
//        $modalInstance.close();
//    }
//}

//var SIMHelpModalCtrl = function ($scope, $modal, $log) {
//
//    $scope.open = function (size) {
//        var modalInstance = $modal.open({
//            templateUrl: 'SIMHelpModal.html',
//            size: size
//
//        });
//
//        modalInstance.result.then(function (selectedItem) {
//            //$scope.selected = selectedItem;
//        }, function () {
//
//        });
//
//    };
//};

function confirmAlert(scope,modal,template,title,message,buttonoktitle,canceltitle,confirmedCbk,cancelCbk)
{
    this.open = function (size) {

        var modalInstance = modal.open({
            templateUrl: template,
            controller: 'ModalConfirmInstanceCtrl',
            size: size,
            resolve: {
                title:function()
                {
                    return title;
                },
                message: function () {
                    return message;
                },
                buttonok:function(){
                    return buttonoktitle;
                },
                buttoncancel:function(){
                    return canceltitle;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            //$scope.selected = selectedItem;
            if(confirmedCbk)
            confirmedCbk();
        }, function () {
            if( typeof cancelCbk == "function" )
                cancelCbk();
        });

    };
}
angular.module('gigSkyWebApp').controller('ModalConfirmInstanceCtrl',['$scope','$modalInstance','title','message','buttonok','buttoncancel',function ($scope,$modalInstance,title,message,buttonok,buttoncancel){

    $scope.confirmMessage = message;
    $scope.confirmOKTitle = buttonok;
    $scope.confirmTitle = title;
    $scope.confirmCancelTitle = buttoncancel;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

/**
 * converting string to date where the format is (dd-mm-yy) will throw error in few browsers,
 * To solve that replacing '-' with '/'
 * @param date
 * @returns {Date}
 */
var getDateObj = function(date){

    var ua = navigator.userAgent.toLowerCase();

    // for Safari & IE
    if ((ua.indexOf('safari') != -1 || ua.indexOf('firefox') != -1) && ua.indexOf('chrome') <= -1 || document.documentMode ) {
        return new Date(date.replace(/-/g, "/"));
    }
    return new Date(date);
};


function TransactionInfoPopUp(scope, modal, template, transInfo, buttonoktitle, canceltitle, confirmedCbk, cancelCbk)
{
    this.open = function (size) {

        var modalInstance = modal.open({
            templateUrl: template,
            controller: 'TransactionInfoPopUpCtrl',
            size: size,
            resolve: {
                transInfo:function()
                {
                    return transInfo;
                },
                buttonok:function(){
                    return buttonoktitle;
                },
                buttoncancel:function(){
                    return canceltitle;
                }
            }
        });

        modalInstance.result.then(function () {
            if(confirmedCbk)
                confirmedCbk();
        }, function () {
            if( typeof cancelCbk == "function" )
                cancelCbk();
        });

    };
}
angular.module('gigSkyWebApp').controller('TransactionInfoPopUpCtrl',['$scope','$modalInstance','transInfo','buttonok','buttoncancel',function ($scope,$modalInstance,transInfo,buttonok,buttoncancel){

    $scope.transaction = transInfo;
    $scope.confirmOKTitle = buttonok;
    $scope.confirmCancelTitle = buttoncancel;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

function ReferralInfoPopUp(scope, modal, template,refAmt, advAmt, buttonoktitle, canceltitle, confirmedCbk, cancelCbk)
{
    this.open = function (size) {

        var modalInstance = modal.open({
            templateUrl: template,
            controller: 'ReferralInfoPopUpCtrl',
            size: size,
            resolve: {
                refAmt:function()
                {
                    return refAmt;
                },
                advAmt:function()
                {
                    return advAmt;
                },
                buttonok:function(){
                    return buttonoktitle;
                },
                buttoncancel:function(){
                    return canceltitle;
                }
            }
        });

        modalInstance.result.then(function () {
            if(confirmedCbk)
                confirmedCbk();
        }, function () {
            if( typeof cancelCbk == "function" )
                cancelCbk();
        });

    };
}
angular.module('gigSkyWebApp').controller('ReferralInfoPopUpCtrl',['$scope','$modalInstance','refAmt','advAmt','buttonok','buttoncancel',function ($scope,$modalInstance,refAmt,advAmt,buttonok,buttoncancel){

    $scope.refAmt = refAmt;
    $scope.advAmt = advAmt;
    $scope.confirmOKTitle = buttonok;
    $scope.confirmCancelTitle = buttoncancel;

    $scope.step2Msg = Localize.translate('STEP-2: Each friend gets %{refamt} on signup.',{refamt:$scope.refAmt});

    $scope.step3Msg = Localize.translate('STEP-3: You get %{advamt} the first time each friend buys a GigSky Data Plan.',{advamt:$scope.advAmt});

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

    
 

'use strict';

angular.module('gigSkyWebApp').directive('activatesim',function(){
    return {
        transclude:true,
        restrict:'E',
        templateUrl:'activeSIMTmpl.html',
        link:function(scope,element,attr){
            $(element).find("#act-number").payment('formatActivationCode');

        }
    };
});

angular.module('gigSkyWebApp').directive('activationCode',function(){
   return {
       template:'',
       scope:{acode:'='},
       link:function(scope,element,attrs){
           element.bind("keydown",function(event){
               if(event.which==8 && element.val().length==0)
               {
                   if(scope.acode!=1 && element.val().length == 0){
                       var prevField = scope.acode - 1;
                       $("[acode="+prevField+"]").focus();
                   }
               }
               if((event.which>=48 && event.which<58) || (event.which>=65 && event.which<91))
               {
                   var nextfiled = scope.acode + 1;
                   if(scope.acode<4)
                   {
                       if(element.val().length == 4){
                           $("[acode="+nextfiled+"]").focus();
                       }
                   }
               }
               //Delete & tab. & left & right
               else if(event.which!=8 && event.which!=9 && event.which!=37 && event.which!=39)
               {
                   event.preventDefault();
               }
           });
       }
   };
});

angular.module('gigSkyWebApp').controller('activateSIMController',['$analytics','$translate','$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$filter','$q','$modal',function ($analytics,$translate,$scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$filter,$q,$modal){

    $scope.viewName = 'Activate SIM';

    var p = $location.path();

    $scope.viewLoading = false;

    $scope.showHelpScreen = false;
    $scope.disableBack = false;
    var s = $location.search();
    if(!s.sa){
        $scope.showHelpScreen = true;
        $scope.viewName = 'SIM Device Settings'
        $scope.disableBack = true;
        if($location.path().indexOf("activesimfromdashboard")!=-1)
        {
            $scope.disableBack = false;
        }
    }

    $scope.onBack  = function(){
        var curPath = $location.path();
        var s = $location.search();
        if(curPath=="/activesimfromdashboard" && !s.sa)
        {
            $location.path("/dashboard");
        }
        else {
            $scope.showHelpScreen = true;
            $scope.viewName = 'SIM Device Settings'
            $scope.disableBack = true;
            delete s.sa;
            $location.search(s);
        }
    };

    $scope.langOther = false;
    $scope.langJA = false;
    if($translate.use() == 'ja')
    {
        $scope.langJA = true;
    }
    else
    {
        $scope.langOther = true;
    }

    if($location.path() == '/dashboard')
    {
        $scope.cancellable = false;
    }
    else
    {
        $scope.cancellable = true;
    }
    $scope.addSIM = function(form){

    if(!$scope.isFormValid(form))
        return;
    //var activationCode =  $scope.code1+"-"+$scope.code2+"-"+$scope.code3+"-"+$scope.code4;
    var activationCode = $scope.activationCode;
    if(activationCode.length > 19)
    {
        activationCode = activationCode.substring(0,19);
    }
    $rootScope.formSubmitErrorStr = null;
    var nickname =  commonInfoFactory.capitalize($scope.nickname);
    $scope.viewLoading  = true;
    simMgrFactory.addSIM(activationCode.toUpperCase(),nickname).success(function(response){
        $analytics.eventTrack('sim-activation', {  category: 'conversion' });
        simMgrFactory.setActiveSIM(response);
        delete s.sa;
        $location.search(s);
        var curPath = $location.path();
        if(curPath=="/activesim")
            $location.path("/my-sims");
        else
            $location.path("/dashboard");
      //Need to trigger sim manager initializaiton if there were no sims or less than 3 sims in menu sim list.
    }).error(function(data,status,headers,config,statusText){
         $scope.formSubmitErrorStr =   commonInfoFactory.getLocalizedErrorMsg(data,status,statusText);
            $scope.viewLoading = false;
        });
};
$scope.onContinue = function(){
    var curPath = $location.path();
    s= $location.search();
    s.sa = 1;
    $scope.showHelpScreen = false;
    $scope.disableBack = false;
    $scope.viewName = 'Activate SIM';
    // delete s.sa;
    // if(curPath=="/activesim"){
    //     $location.path("/my-sims");
    // }
    // else
    //     $location.path("/dashboard");
    $location.search(s);

};
$scope.open = function (size) {
    var modalInstance = $modal.open({
        templateUrl: 'SIMHelpModal.html',
        size: size,
        controller:['$scope',function($scope){
            $scope.ok = function(){
            modalInstance.close();
            }
        }]
    });
    };
    $scope.navigateBack = function () {
        /*if(p.indexOf('activesimfromdashboard')!=-1)
        {
            $location.path('/dashboard');
        }
        else*/
        $location.path("/dashboard");
    }
}]);

angular.module('gigSkyWebApp').controller('editSIMController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$filter','$q','$modal',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$filter,$q,$modal){

    $scope.viewTitle = 'SIM Details';
    $rootScope.topupNetworkInfo = null;
    var searchObj = $location.search();
    $scope.simId = searchObj.id;


    $scope.isCollapsed11 = true;
    $scope.updateSIM = {};
    $scope.updateSIM.simId = $scope.simId;
    $rootScope.purchaseForSIM = null;

    $scope.ICCIDLabel = Localize.translate('ICCID');
    $scope.CSNLabel = Localize.translate('CSN');
    $scope.activeLabel = Localize.translate('Active');
    $scope.blockedLabel = Localize.translate('Blocked');
    $scope.blockSimLabel = Localize.translate('BLOCK SIM');
    $scope.unblockSimLabel = Localize.translate('UNBLOCK SIM');

    $scope.hideSimLabel = Localize.translate('HIDE SIM');
    $scope.unhideSimLabel = Localize.translate('UNHIDE SIM');

    if($scope.sim)
    {
        $scope.updateSIM.description = $scope.sim.description;
    }

    $scope.onTopup = function()
    {
        var sub = simMgrFactory.getConnectedSubscription($scope.sim);
        if(sub)
        {
            $location.path("/buy-plan2");
            $rootScope.purchaseForSIM = $scope.sim;
            $location.search({nw:sub.networkGroupInfo.networkGroupId,si:2,sim:$scope.simId,st:'g'});
            $rootScope.topupNetworkInfo = sub.networkGroupInfo;
        }
    }

    $rootScope.$watch('editSIM',function(){
        $scope.sim = $rootScope.editSIM;
        $scope.clearErrorMessage();
        if($scope.sim)
        {
            $scope.simId = $scope.sim.simId;
            $scope.updateSIM.description = $scope.sim.description;
            $scope.updateSIM.simId = $scope.simId;
        }
        updateSIMActivePlanView($scope.sim);
    });


    function updateSIMActivePlanView(sim){
        if(sim){
            $scope.networkGroupName = simMgrFactory.connectedNetworkGroupName(sim);
            $scope.balanceData = simMgrFactory.subscriptionBalance(sim);
            $scope.balanceTime = simMgrFactory.subscriptionRemainingTime(sim);
        }
    }

    if(!$scope.sim && $scope.simId)
    {
        //Fetch sim details..
        $scope.viewLoading = true;
        simMgrFactory.getSIMDetails($scope.simId,true).success(function(result){
            $scope.viewLoading = false;
            $scope.sim = result;
            $rootScope.editSIM = result;
            $scope.updateSIM.description = $scope.sim.description;
            updateSIMActivePlanView($scope.sim);
        }).error(function(data,status,headers,config,statusText){
                $scope.viewLoading = false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
    }
    else
    {
        updateSIMActivePlanView($scope.sim);
    }

    $scope.editSIMName = function(form){
        if(!$scope.isFormValid(form))
          return;
        $scope.viewLoading = true;
        var updateSIM = $scope.updateSIM;
        updateSIM.description = commonInfoFactory.capitalize($scope.updateSIM.description);
        simMgrFactory.editSIM(updateSIM).success(function(result){
            $scope.viewLoading = false;
            $scope.sim.description = result.description;
            $rootScope.editSIM.description = $scope.sim.description;
            $scope.editableSIMName = ! $scope.editableSIMName;
        }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });
    };

    $scope.cancelSIMNameEdit = function(form){
        $scope.editableSIMName=!$scope.editableSIMName;
        $scope.updateSIM.description = $scope.copyOfSIMName;
    };
    $scope.makeFieldEditable = function(form) {
        $scope.copyOfSIMName = angular.copy($scope.updateSIM.description);
        $scope.editableSIMName = true;
    };
    $scope.blockSIM = function()
    {
        $scope.viewLoading = true;
        simMgrFactory.blockSIM($scope.sim).success(function(result){
            $scope.sim.simStatus = result.simStatus;
            $rootScope.editSIM = result;
            $scope.viewLoading = false;
        }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });
    }

    $scope.unBlockSIM = function()
    {
        $scope.viewLoading = true;
        simMgrFactory.unBlockSIM($scope.sim).success(function(result){
            $scope.sim = result;
            $rootScope.editSIM = result;
            $scope.viewLoading = false;
        }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });
    }

    $scope.hideSIM = function( ){
        $scope.viewLoading = true;
        simMgrFactory.hideSIM($scope.sim).success( function(response){

            $scope.sim = response;

            $rootScope.editSIM = response;

            $scope.viewLoading = false;
       }).error( function( data, status ) {
           $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
           $scope.viewLoading = false;
       } );

    };


    $scope.unhideSIM = function(){
        $scope.viewLoading = true;

        simMgrFactory.unhideSIM($scope.sim).success( function(response){


            $scope.sim = response;

            $rootScope.editSIM = response;
            $scope.viewLoading = false;
           
        }).error( function( data, status ) {
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            $scope.viewLoading = false;
        } );
    };



    $scope.blockSimConfirm = function(size){

        if($scope.sim.simStatus == "ACTIVE")
        {
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('CONFIRM SIM BLOCK'),Localize.translate('Blocking your SIM will prevent others from using it. You can unblock your SIM at any time from the SIM Details screen.'),Localize.translate("OK"),Localize.translate("Cancel"),function(){$scope.blockSIM();
            }).open(size);
        }
        else
        {
            $scope.unBlockSIM();
        }
    }
    $scope.navigateBack = function () {
        $location.path("/sims");
    }

    // hide SIM
    $scope.hideSimConfirm = function(size){

       if( $scope.sim.visibilityStatus == "visible" ){

            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('Confirm Hide SIM'),Localize.translate('Hiding a SIM will remove it from your "My SIMs" list. You can unhide your SIM at any time from the SIM Details screen.'),Localize.translate("OK"),Localize.translate("Cancel"),function(){$scope.hideSIM();
            }, "hideSim").open(size);
        }
        else{
            $scope.unhideSIM();

        }
    }

}]);

angular.module('gigSkyWebApp').controller('simController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','$filter','paymentFactory','$analytics',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,$filter,paymentFactory,$analytics){
	$rootScope.simList = [];
    $scope.totalSims = 0;
    $rootScope.editSIM = null;
    $scope.includeHiddenSIMs = false;
    var search = $location.search();
    var path = $location.path();

    $scope.viewTitle = 'SIMs';
    $scope.desktopModeViewTitle = 'SIMs';
    //$scope.simlistTitle = $filter('translate')('lt_mysims');
    $scope.selectSIMForPayHistory = false;
    $scope.simSerivce = simMgrFactory;
    if(search && search.select)
    {
        $scope.simListTypeSelect = true;
        $scope.viewTitle = 'Select SIM';
        $scope.desktopModeViewTitle = 'Select SIM';
    }
    else if(search.fr && search.fr == 'ph')
    {
        $scope.simListTypeSelect = true;
        $scope.viewTitle = 'Select SIM';
        $scope.desktopModeViewTitle = 'Select SIM';
        $scope.selectSIMForPayHistory = true;
        $scope.includeHiddenSIMs = true;
    }else if(path == '/my-sims'){
        $scope.viewTitle = 'My SIMs';
        $scope.desktopModeViewTitle = 'My SIMs';
        $scope.simListTypeSelect = false;
        $scope.currentSim = simMgrFactory.getActiveSIM();
    }
    else
    {
        $scope.simListTypeSelect = false;
        $scope.currentSim = simMgrFactory.getActiveSIM();

    }

    $scope.onNextSIMList = function() {

        if($rootScope.simList.length < $scope.totalSims)
            $scope.getSIMListInPage($rootScope.simList.length,NUMBER_OF_ITEMS_PER_PAGE,$scope.includeHiddenSIMs);
    }

    $scope.showActiveSim = function(){
        $location.path('/activesim');
    }

    $scope.showMySIM = function(){
        $location.path('/my-sims');
    }

    $scope.onSIMSelect = function(sim, $event )
    {
        if($scope.selectSIMForPayHistory)
        {
            $location.path('/paymentHistory');
            if(!sim)
                sim = {simId:null,description:Localize.translate('All SIMs')};
            paymentFactory.setPayHistoryOfSIM(sim);
        }
        else
        if($scope.simListTypeSelect)
        {
            simMgrFactory.setActiveSIM(sim);
            $location.path("/dashboard");
        }
        else
        {
            $rootScope.editSIM = sim;

            // show unhide button
            $location.path('/editSIM');
            $location.search({id: sim.simId});
        }
    }

    function getSelectSIMIndexInList(sim){
        for(var i= 0;i < $rootScope.simList.length;i++){
            var s = $rootScope.simList[i];
            if(s.simId == sim.simId)
            {
                return i;
            }
        }

        console.error("Selected SIM not avialbel in the SIM list on hide/unhide SIM");
    }

    $scope.updateSIMListOnSIMHide =function(sim){

            $rootScope.totalVisibleSIMs--;
            $scope.totalSims = $scope.includeHiddenSIMs === true ? $scope.totalSims :  $rootScope.totalVisibleSIMs ;
            $rootScope.hiddenSimsCount++;

            if(!$scope.includeHiddenSIMs)
            {
                if($rootScope.simList.length-1 == $rootScope.totalVisibleSIMs)
                {
                    //Remove the SIM.
                    var i = getSelectSIMIndexInList(sim);
                    $rootScope.simList.splice(i,1);
                    $rootScope.editSIM = $rootScope.simList[0];
                    var simDt = $("#simdetails");
                    $(simDt).css({top: 0});

                }
                else {
                    $scope.getSIMListInPage($rootScope.simList.length-1,NUMBER_OF_ITEMS_PER_PAGE,$scope.includeHiddenSIMs);
                }
            }
            else {
                var i = getSelectSIMIndexInList(sim);
                $rootScope.simList[i].visibilityStatus = "hidden";
                sim.visibilityStatus = "hidden";
            }


    };


    $scope.updateSIMListOnUnSIMHide =function(sim){

        $rootScope.totalVisibleSIMs++;
        $scope.totalSims = $scope.includeHiddenSIMs === true ? $scope.totalSims :  $rootScope.totalVisibleSIMs ;
        $rootScope.hiddenSimsCount--;
        if($rootScope.hiddenSimsCount==0){
            $("#sim-toggle").attr('checked',false);
            $scope.includeHiddenSIMs = false;
        }
        var i = getSelectSIMIndexInList(sim);
        var s = $rootScope.simList[i];
        s.visibilityStatus = "visible";
        $rootScope.editSIM = s;
    };

    $scope.getSIMListInPage = function(stIndex,count, includeHidden, reset ){
        if( reset !== undefined && reset === true  ){
            $rootScope.simList = [];
        }

         if($scope.busy)
            return;
        $scope.busy = true;

        var incSIMStatus = true;
        if($scope.simListTypeSelect)
         incSIMStatus = false;

        if(!stIndex)
        $scope.viewLoading = true;

        simMgrFactory.getSIMList(stIndex,count,incSIMStatus, includeHidden).success( function(response){

            $scope.busy = false;
            $scope.viewLoading = false;
            var data = response.list;
            var len = data.length;
            var activeSIMs = 0;
            if(len > 0 ){
                for(var i= 0;i < len;i++){
                    $rootScope.simList.push(data[i]);
                    if(data[i].status =='Active'){
                        activeSIMs+=1;
                    }
                }

                $analytics.setUserProperties({dimension1: activeSIMs > 1?'multiple':activeSIMs==1?'yes':'no'});

                if(stIndex==0) {

                    $rootScope.editSIM = $rootScope.simList[0];
                }
            }
        $rootScope.totalVisibleSIMs  = response.totalCount - response.totalHiddenCount;
        $scope.totalSims = includeHidden === true ? response.totalCount :  $rootScope.totalVisibleSIMs ;
        $rootScope.hiddenSimsCount =   response.totalHiddenCount;

        }).error(function(){
            $scope.busy = false;
            $scope.viewLoading = false;
        });
    };

    $scope.getSIMListInPage(0,NUMBER_OF_ITEMS_PER_PAGE,$scope.includeHiddenSIMs);
    $scope.toggleHiddenSims = function( ){
        $scope.includeHiddenSIMs = $("#sim-toggle").is( ":checked");
        $rootScope.simList = [];
        var simDt = $("#simdetails");
        $(simDt).css({top: 0});
        $scope.getSIMListInPage( 0,NUMBER_OF_ITEMS_PER_PAGE, $scope.includeHiddenSIMs);
    }

}]);

angular.module('gigSkyWebApp').controller('replaceSIMController',['$analytics','$translate','$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$filter','$q','$modal',function ($analytics, $translate,$scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$filter,$q,$modal){
    $scope.replacesim = true;
    $scope.cancellable = true; //Indicates that cancel button will be shown.
    $scope.viewName = 'Replace SIM';
    $scope.viewLoading = false;
    var searchObj = $location.search();
    $scope.simId = searchObj.id;
    $scope.sim = $rootScope.editSIM;
    $scope.updateSIM = {};
    $scope.updateSIM.simId = $scope.simId;

    $scope.langOther = false;
    $scope.langJA = false;
    if($translate.use() == 'ja')
    {
        $scope.langJA = true;
    }
    else
    {
        $scope.langOther = true;
    }


    if(!$scope.sim)
    {
        //Fetch sim details..
        $scope.viewLoading = true;
        simMgrFactory.getSIMDetails($scope.simId,true).success(function(result){
            $scope.viewLoading = false;
            $scope.sim = result;
            $rootScope.editSIM = result;
            $scope.nickname = $scope.sim.description;
        }).error(function(data,status){
                $scope.formSubmitErrorStr =   commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });
    }
    else
    {
        $scope.nickname = $scope.sim.description;
    }

$scope.addSIM = function(form){

    if(!$scope.isFormValid(form))
        return;
    //var activationCode =  $scope.code1+"-"+$scope.code2+"-"+$scope.code3+"-"+$scope.code4;
    var activationCode = $scope.activationCode;
    if(activationCode.length > 19)
    {
        activationCode = activationCode.substring(0,19);
    }
    //activationCode = activationCode.replace(/\s/g, "-");
    $rootScope.formSubmitErrorStr = null;
    var nickname = $scope.nickname;
    $scope.viewLoading = true;
    simMgrFactory.replaceSIM($scope.sim.simId,activationCode.toUpperCase(),nickname).success(function(response){
        $scope.viewLoading = false;
        $analytics.eventTrack('sim replace', {  category:'behavior' , label : response.simId });
        $scope.sim = response;
        $rootScope.editSIM = response;
        $scope.navigateBack();

    }).error(function(data,status){
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr =   commonInfoFactory.getLocalizedErrorMsg(data,status);
        });

}

$scope.navigateBack = function () {

    if($(window).width()<960)
    {
        $location.path('/editSIM');
        $location.search({id:response.simId});
    }
    else{
        $location.path('/sims');
        $location.search({});
    }
    $location.replace();

}
}]);

/**
 * Controller for sim1 to sim2 transition
 *
 */
angular.module('gigSkyWebApp').controller('simUpgradeController',['$scope','$location','SimManagerFactory','commonInfoFactory','$modal',function($scope,$location,simManagerFactory,commonInfoFactory,$modal){

    $scope.selectCountryStr = Localize.translate('Select country');

    var replacementBeginDate = getDateObj(sessionStorage.getItem("replacementBeginDate"));

    var planPurchaseEndDate = getDateObj(sessionStorage.getItem("planPurchaseEndDate"));
    $scope.purchaseEndDate = commonInfoFactory.convertDateFormat(planPurchaseEndDate);

    var serviceDate = getDateObj(sessionStorage.getItem("serviceEndDate"));
    $scope.serviceEndDate = commonInfoFactory.convertDateFormat(serviceDate);

    var today = new Date();

    if(today > serviceDate){
        $scope.simReplaceMsg = Localize.translate("This SIM will no longer work on GigSky's mobile data service. Select your replacement SIM to continue to use GigSky’s data service. Please contact help@gigsky.com if you have any questions, or need a replacement SIM.");
    }else if(today > planPurchaseEndDate){
        $scope.simReplaceMsg = Localize.translate("Data Plans for this SIM are no longer available for purchase. Select your replacement SIM to continue to use GigSky’s data service. Please contact help@gigsky.com if you have any questions, or need a replacement SIM.");
    }else if(today >= replacementBeginDate){
        $scope.simReplaceMsg = Localize.translate('You will be unable to add data plans for this GigSky SIM after %{val1}, and SIM will not work for the GigSky mobile data service after %{val2}.',{val1 : $scope.purchaseEndDate,val2 : $scope.serviceEndDate});
    }

    $scope.confirmDoNotReplaceMsg = Localize.translate('I do NOT want to replace my GigSky SIM. I understand by not replacing this SIM that I will be unable to add data plans using this SIM after %{val1}, and the SIM will not work on GigSky mobile data service after %{val2}.',{val1 : $scope.purchaseEndDate,val2 : $scope.serviceEndDate});

    //Common control specific apis definied in main.js//main controller.
    $scope.updateCountryList($scope,'formSubmitErrorStr');
    $scope.country=sessionStorage.getItem("userCountryCode");

    $scope.confirmDoNotReplaceSim = function(){

        $location.path( "/confirmDoNotReplace" );
    };

    $scope.goToSimReplacement = function(){
        $location.path( "/simReplace" );
    }

    $scope.enterMailAddress = function(){
        $location.path( "/simReplaceAddress" );
    }

    $scope.confirmNotToReplace = function(){
        var sim1List = JSON.parse(sessionStorage.getItem("sim1ListToReplace")) || 0;
        if(!sim1List){
            simManagerFactory.getSim1TransitionDetails().success(function(result){
                sim1List = result.transitionSimList;
                sendRequestForNotToReplace(sim1List);
            }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
        }else{
            sendRequestForNotToReplace(sim1List);
        }
    }

    $scope.requestFormSim2 = function(){

        if($scope.isFormValid($scope.simUpgradeForm)){

            var sim1List = JSON.parse(sessionStorage.getItem("sim1ListToReplace")) || 0;
            if(!sim1List){
                simManagerFactory.getSim1TransitionDetails().success(function(result){
                    sim1List = result.transitionSimList;
                    sendRequestForSim2(sim1List);
                }).error(function(data,status){
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                });
            }else{
                sendRequestForSim2(sim1List);
            }

        }
    }


    function sendRequestForSim2(sim1List) {
        var sim1ListRequest = [];
        var address = {};
        address.address1 = $scope.address1;
        address.address2 = $scope.address2;
        address.firstName = $scope.fName;
        address.lastName = $scope.lName;
        address.zipcode = $scope.zipCode;
        address.state = $scope.state;
        address.city = $scope.city;
        address.country = $scope.country;

        for (var i in sim1List) {
            var simObj = {};
            simObj.iccId = sim1List[i].iccId;
            simObj.replacementChoice = "ACCEPTED";
            simObj.shippingAddress = address;

            sim1ListRequest.push(simObj);
        }
        sendRequest(sim1ListRequest);
    }

    function sendRequestForNotToReplace(sim1List){
        var sim1ListRequest = [];
        for (var i in sim1List) {
            var simObj = {};
            simObj.iccId = sim1List[i].iccId;
            simObj.replacementChoice = "REJECTED";
            sim1ListRequest.push(simObj);
        }
        sendRequest(sim1ListRequest);
    }

    function sendRequest(sim1ListRequest) {
        var request = {
            "type": "NewSim1ReplacementRequest",
            "simList": sim1ListRequest
        };

        $scope.viewLoading = true;
        simManagerFactory.requestForSim2(request)
            .success(function (response) {
                $scope.viewLoading = false;
                sessionStorage.removeItem("sim1ListToReplace");
                $location.path("/dashboard");
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Your request has been submitted successfully."),Localize.translate("OK")).open('sm');
            })
            .error(function (data, status) {
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                $scope.viewLoading = false;
                sessionStorage.removeItem("sim1ListToReplace");

                if(data && data.errorInt && data.errorInt ==  6506 || data.errorInt ==  1501 || data.errorInt ==  1006 || data.errorInt ==  1508){
                    $location.path("/dashboard");
                    new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,$scope.formSubmitErrorStr,Localize.translate("OK")).open('sm');
                }
            });
    }


}]);
'use strict';

angular.module('gigSkyWebApp').controller ('paymentMethodController',['$scope','$rootScope','paymentFactory','commonInfoFactory','$location','$modal','$q','applePayService',function ($scope,$rootScope,paymentFactory,commonInfoFactory,$location,$modal,$q,applePayService){
	// $('.custPanel').css({display:"none"});

	$scope.payMethodList = [];
    $scope.viewTitle = 'Payment Methods';
    $scope.viewLoading = true;
    $rootScope.paymethod = null;
    $scope.isPurchaseFlow = false;
    var s = $location.search();
    if(s.pp == 'd' || s.pp == 'p' || s.pp == 'a'){
        $scope.isPurchaseFlow = true;
    }
    var bid = sessionStorage.getItem('billingId');
    $scope.defaultBillingId = (bid) ? bid : null;

	paymentFactory.getPaymentMethods()
		.success(function (result) {
            $scope.viewLoading = false;
            $scope.maxCCSupported = 0;
            $scope.maxPPSupported = 0;
            $scope.enableApplePay = false;

			for (var i=0;i<result.list.length;i++){
				if((result.list[i].paymentMethod == "CreditCard")&& (result.list[i].enabled == true)){
					$scope.showCC=true;
					$scope.maxCCSupported = result.list[i].maximumNumberOfAccountsSupported;
				}
				if((result.list[i].paymentMethod == "Paypal")&& (result.list[i].enabled == true)){
					$scope.showPaypal=!$rootScope.isCaptive;
                    $scope.maxPPSupported = result.list[i].maximumNumberOfAccountsSupported;
				}
                if ((result.list[i].paymentMethod == "ApplePay") && (result.list[i].enabled == true)) {
                    $scope.enableApplePay = true;
                }
			}

            existingPayMethods();
		})
		.error(function () {
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
	});

    $scope.numberOfCC = 0;
    $scope.numberOfPP = 0;

    $scope.payMethodDisplayStr = function(payMethod)
    {
        if(payMethod.paymentMethod == "CreditCard")
        {
            return Localize.translate('Credit card ending %{val}',{val: payMethod.displayCC});
        }
        else if(payMethod.paymentMethod == "Paypal")
        {
            return Localize.translate('PayPal account %{val}',{val: payMethod.userEmailId});
        }
        else if(payMethod.paymentMethod == 'ApplePay'){
            return Localize.translate('Apple Pay');
        }
        return null;
    };
//
//    $scope.payMethodMainString = function(payMethod)
//    {
//        if(payMethod.paymentMethod == "CreditCard")
//        {
//            return $filter('translate')('lt_CreditCard');
//        }
//        else if(payMethod.paymentMethod == "Paypal")
//        {
//            return $filter('translate')('lt_PayPal');
//        }
//        return null;
//    };
//
//    $scope.payMethodSubString = function(payMethod)
//    {
//        if(payMethod.paymentMethod == "CreditCard")
//        {
//            return "xxxx-xxxx-xxxx-" + payMethod.displayCC;
//        }
//        else if(payMethod.paymentMethod == "Paypal")
//        {
//            return payMethod.userEmailId;
//        }
//        return null;
//    };

    $scope.addCC = function()
    {
//        if(($.payment.cardType($scope.cardNum) != 'amex' && $scope.ccv && $scope.ccv.length !==3) || ($.payment.cardType($scope.cardNum) == 'amex' && $scope.ccv && $scope.ccv.length !==4)){
//            var label = "Card security code";
//            $scope.ccForm.ccv.errorStr = Localize.translate('%{val} invalid',{val:label});
//            return;
//        }
        if($scope.numberOfCC<$scope.maxCCSupported) {
            $location.path("/addCC");
            if($scope.isPurchaseFlow){
                $location.replace();
            }
        }
        else
        {
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('A maximum of %{val} credit cards is permitted.',{val:$scope.maxCCSupported}),Localize.translate("OK")).open('sm');

        }
    }

    $scope.addPP = function()
    {
        if($scope.numberOfPP<$scope.maxPPSupported){
            $location.path("/addPP");
            if($scope.isPurchaseFlow){
                $location.replace();
            }
        }
        else
        {
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Only one PayPal account is permitted.'),Localize.translate("OK")).open('sm');
        }
    }

    $scope.paymentMethodsTitle = null;
    $scope.noPaymentMethods = null;

    $scope.isEditAllowed = function(billing){
        return !(billing.paymentMethod == 'Paypal' && $rootScope.isCaptive);
    };

    $scope.onEditPayMethod = function(billing,setDefaultPMOnEdit)
    {
        $rootScope.paymethod = billing;
        var s = $location.search();
        (billing.paymentMethod=='CreditCard')?($location.path('/editCC/'+billing.billingId)):($location.path('/editPP/'+billing.billingId));
        if(setDefaultPMOnEdit)
        {
            s.setdefault=1;
            $location.search(s);
        }
        if($scope.isPurchaseFlow){
            $location.replace();
        }
    }
    function existingPayMethods(){
        $scope.viewLoading = true;

        if($scope.enableApplePay){

            $q.all([paymentFactory.getAllBillingInfo(),applePayService.isApplePayAvailable()]).then(function (results) {
                $scope.viewLoading = false;
                $scope.applePayAvailable = results[1].data;
                var billingInfo = results[0].data;
                if($scope.isPurchaseFlow && $scope.applePayAvailable){
                    $scope.noPaymentMethods = null;
                    var bInfo = {type:'ApplePayBillingInfo',defaultBilling:'true', paymentMethod:'ApplePay'};
                    if(billingInfo.list){
                        billingInfo.list.unshift(bInfo);
                    }
                    else if(billingInfo){
                        billingInfo.list = [bInfo];
                    }
                }

                updateExistingPayMethods(billingInfo);

            }, function (error) {
                $scope.viewLoading = false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
            });
        }
        else{
            paymentFactory.getAllBillingInfo()
                .success(function (billingInfo) {
                    $scope.viewLoading = false;
                    updateExistingPayMethods(billingInfo);
                })
                .error(function (data,status) {
                    $scope.viewLoading = false;
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                })
        }

    }

    function updateExistingPayMethods(billingInfo){
        $scope.numberOfCC = 0;
        $scope.numberOfPP = 0;
        $scope.allPaymentMethods = billingInfo.list;

        if(billingInfo.list && billingInfo.list.length)
        {
            $scope.noPaymentMethods  = null;

            if($scope.isPurchaseFlow){
                $scope.paymentMethodsTitle = null;
            }else{
                $scope.paymentMethodsTitle = Localize.translate('Select default payment method')+':';
            }

            var noOfPaymentMethods = billingInfo.list.length;

            //Check whether the default billing id exists or not.
            if($scope.isPurchaseFlow && $scope.defaultBillingId)
            {
                var found = false;
                for (var i = 0; i < noOfPaymentMethods; i++) {
                    var bInfo = billingInfo.list[i];
                    if(bInfo.billingId == $scope.defaultBillingId)
                    {
                        found = true;
                        break;
                    }
                }
                $scope.defaultBillingId = found?$scope.defaultBillingId:null;
            }

            for(var i= 0;i<noOfPaymentMethods;i++)
            {
                var bInfo = billingInfo.list[i];

                if ($scope.isPurchaseFlow  ) {

                    if(bInfo.billingId == $scope.defaultBillingId)
                    {    bInfo.defaultBilling = "true";
                        $scope.defaultBillingInfo = bInfo;
                    }
                    else
                    {
                        bInfo.defaultBilling = "false";
                    }
                }
                else {
                    if (bInfo.defaultBilling == "true") {
                        $scope.defaultBillingInfo = bInfo;
                        $scope.defaultBillingId = bInfo.billingId;
                    }
                }

                if(bInfo.paymentMethod == "CreditCard")
                    $scope.numberOfCC++;
                else if(bInfo.paymentMethod == 'Paypal')
                    $scope.numberOfPP++;
            }
        }
        else
        {
            $scope.paymentMethodsTitle = Localize.translate('My payment methods');
            $scope.noPaymentMethods = Localize.translate('There is no payment method associated with this GigSky account.');
        }

        $scope.showAddCC=false;
        $scope.showAddPaypal=false;

        if($scope.numberOfCC < $scope.maxCCSupported){
            $scope.showAddCC=true;
        }
        if($scope.numberOfPP < $scope.maxPPSupported){
            $scope.showAddPaypal=!$rootScope.isCaptive;
        }

    }

    function deletePayMethods(billing){
        var bid = sessionStorage.getItem('billingId');
        if(bid == billing.billingId){
            sessionStorage.removeItem('billingId');
        }
        $scope.viewLoading = true;
        paymentFactory.deletePayMethod(billing.billingId)
            .success(function () {
                existingPayMethods();
            })
            .error(function(data,status){
                $scope.viewLoading = false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
    }



    $scope.setDefault = function (billing){
        var billingId = billing.billingId;
        if(billing.defaultBilling=="true" && !$scope.isPurchaseFlow)
            return;
        $scope.viewLoading = true;

        if(billing.forceUserEdit)
        {
            if($scope.isPurchaseFlow){
                $scope.onEditPayMethod(billing,false);
            }else{
                $scope.onEditPayMethod(billing,true);
            }

        }
        else if($scope.isPurchaseFlow){
            $scope.navigateBack(billing)
        }
        else {

            paymentFactory.setDef(billingId)
                .success(function () {
                        existingPayMethods();
                })
                .error(function (data, status) {
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                    $scope.viewLoading = false;
                });
        }
    };

    $scope.navigateBack = function(response)
    {
        var s = $location.search();
        if(s.pd!=null){
            $location.path('/buy-plan3');
            if($scope.applePayAvailable && response && response.billingId == null){
                sessionStorage.removeItem('billingId');
            }else if(response && response.billingId){
                //s.bid = response.billingId;
                sessionStorage.setItem('billingId',response.billingId);
            }
            $location.search(s);
            $location.replace();
        }
        else
        {
            $location.path("/paymentMethod");
            $location.search({});
            $location.replace();
        }
    }

    $scope.open = function (size,billing) {
        var warning2 = Localize.translate('This payment method will be deleted.');
        new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('Are you sure?'),warning2,Localize.translate("OK"),Localize.translate("Cancel"),function(){deletePayMethods(billing);
        }).open(size);
    };

    $scope.displayLogo = function (ccType) {
        if(ccType == 'VISA'){
            return 'styles/images/common/visa.png';
        }else if(ccType == 'AMERICAN_EXPRESS'){
            return 'styles/images/common/amex.png';
        }else if(ccType == 'MASTERCARD'){
            return 'styles/images/common/mastercard.png';
        }else if(ccType == 'PAYPAL'){
            return 'styles/images/common/paypal.png';
        }
    };
}]);
angular.module('gigSkyWebApp').controller ('addCCController',['$analytics','$scope','$rootScope','$routeParams','authFactory','paymentFactory','$location','commonInfoFactory','$timeout',function ($analytics,$scope,$rootScope,$routeParams,authFactory,paymentFactory,$location,commonInfoFactory,$timeout){

    commonInfoFactory.maskTelField();

    $scope.viewLoading = false;
    $scope.selectCountryStr=Localize.translate('Select country');
    $scope.updateCountryList($scope,'formSubmitErrorStr');
    var billingId = ($routeParams.billingId);
    var title = 'Add Credit Card';
    $scope.billingInfo = $rootScope.paymethod;
    $scope.cardPlaceHolderText = "";
    $scope.noneselected = true;

    $('.cc-number').payment('formatCardNumber');

    $scope.onCardNumberEdited = function(cardNum){

        var selectedCard = commonInfoFactory.validateCreditCardType(cardNum);
        $scope.cardismaster = (selectedCard == 'mc');
        $scope.cardisvisa = (selectedCard == 'vi');
        $scope.cardisamex = (selectedCard == 'ax');
        $scope.noneselected = !selectedCard;

    };

    $('#mm').on('blur',function(e){

        var $target, value;
        $target = $(e.currentTarget);
        value = $target.val();
        if(value=="1" && value.length==1)
        {
            $target.val('0'+value);
        }
    });
    if(billingId)
    {
        title = 'Edit Credit Card';
        if(!$scope.billingInfo){
        $scope.viewLoading = true;
        paymentFactory.getPaymethodDetails(billingId)
            .success(function (billingInfo) {
                $scope.viewLoading = false;
                fillBillingInfoFields(billingInfo);
            })
            .error(function (data,status) {
                $scope.viewLoading = false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
        }
        else
        {
            fillBillingInfoFields($scope.billingInfo);
        }
    }
    else
    {
        $scope.viewLoading = true;
        authFactory.getAccount()
            .success(function (userInfo) {
                $scope.selectCountry=userInfo.country;
                $scope.viewLoading = false;
            }).error(function(data,status){
                $scope.selectCountry=userInfo.country;
                $scope.viewLoading = false;
            });
    }


    function fillBillingInfoFields(billingInfo)
    {
        $scope.cardPlaceHolderText = "xxxx xxxx xxxx " + billingInfo.displayCC;
        $scope.name=billingInfo.nameOnCard;
        $scope.address1=billingInfo.address1;
        if (billingInfo.address2 == "undefined")
            billingInfo.address2="";
        $scope.address2=billingInfo.address2;
        $scope.phone=billingInfo.phone;
        $scope.city=billingInfo.city;
        $scope.state=billingInfo.state;
        $scope.zipCode=billingInfo.zipCode;
        $scope.selectCountry=billingInfo.country;
        $scope.billingInfo = billingInfo;
        $scope.forceUserEdit = billingInfo.forceUserEdit;
    }
    $scope.currentNavLinks = [title];
    $scope.viewTitle = title;
    $scope.location = $location;

	$scope.navigateBack = function(response)
    {
        var s = $location.search();
        if(s.pd!=null){

            if($scope.billingInfo && $scope.billingInfo.forceUserEdit && !response)
            {
                if(s.pp=='d')
                {
                    $location.path('/dashboard');
                }
                else if(s.pp == 'p')
                {
                    $location.path("/buy-plan2");
                }
                else {
                    $location.path('/buy-plan3');
                }

            }
            else
            {
                $location.path('/buy-plan3');
            }
            if(response && response.billingId){
               // s.bid = response.billingId;
                sessionStorage.setItem('billingId',response.billingId);
            }
            $location.search(s);
            $location.replace();
        }
        else
        {
            $location.path("/paymentMethod");
            $location.search({});
            $location.replace();
        }
    }
	$scope.selectCountry=sessionStorage.getItem("userCountryCode");
	/****** Add Credit Card **********/
	function addCCInfo(onCreditCardAddedCbk,onCreditCardAddFailureCbk){
        $scope.viewLoading = true;
        var cardNumber = removeSeparator(2,$scope.cardNum);
        if(!$scope.isReqInProgress){
            $scope.isReqInProgress = true;
            paymentFactory.addCC($scope.name,$scope.selectCountry, $scope.address1, $scope.address2, $scope.zipCode, $scope.phone, $scope.state, $scope.city,cardNumber,$scope.ccv,$scope.expMonth,$scope.expYear,$scope.firstName,$scope.lastName,$scope.defaultStatus)
                    .success(onCreditCardAddedCbk)
                    .error(onCreditCardAddFailureCbk);
        }


	}

    function updateCCInfo(onCreditCardAddedCbk,onCreditCardAddFailureCbk){
            $scope.viewLoading = true;
            var cardNumber = removeSeparator(2,$scope.cardNum);
            var s = $location.search();
            var setDefault = undefined;
            if(s.setdefault)
                setDefault = true;

            function onCreditCardUpdatedSetDef(response){
                paymentFactory.setDef(response.billingId).success(function(){onCreditCardAddedCbk(response)}).error(onCreditCardAddFailureCbk);
            }

            if(!$scope.isReqInProgress){
                $scope.isReqInProgress = true;
                paymentFactory.updateCC(billingId,$scope.name,$scope.selectCountry, $scope.address1, $scope.address2, $scope.zipCode, $scope.phone, $scope.state, $scope.city,cardNumber,$scope.ccv,$scope.expMonth,$scope.expYear)
                        .success(setDefault?onCreditCardUpdatedSetDef:onCreditCardAddedCbk)
                        .error(onCreditCardAddFailureCbk);
            }
    }

    function removeSeparator(type, string)
    {
        if(type == 1)
        {
            // use type 1 for activation code
            return string.replace(/\-/g,'');
        }
        else if(type == 2)
        {
            // use type 2 for credit card number
            return string.replace(/\s/g,'');
        }
    }


        $scope.addCC= function (){

            $scope.formSubmitErrorMsg = null;
            var ccNotValid  = $scope.isCCNotValid($scope.ccForm.CCN, Localize.translate('Credit card number'));
            if(ccNotValid)
                return;
//            $scope.ccForm.ccv.errorStr = null;
//            if(($.payment.cardType($scope.cardNum) != 'amex' && $scope.ccv && $scope.ccv.length !==3) || ($.payment.cardType($scope.cardNum) == 'amex' && $scope.ccv && $scope.ccv.length !==4)){
//                var label = "Card security code";
//                $scope.ccForm.ccv.errorStr = Localize.translate('%{val} invalid', {val : label});
//                $scope.setFormFieldInvalid($scope.ccForm.ccv);
//                return;
//            }
            var valid = $scope.isFormValid($scope.ccForm);
//            var d = new Date();
//
//            var expYear = "20"+$scope.expYear;

//            if(valid && !$scope.isExpDateNotValid($scope.ccForm.expMonth,$scope.ccForm.expYear,$scope.ccForm.expDate,Localize.translate('Expiration date'),expYear) )
            if(valid)
            {

                $scope.formSubmitErrorMsg = null;
                if(!billingId)
                    addCCInfo(onCreditCardAdded,onCreditCardAddErrorCbk);
                else
                    updateCCInfo(onCreditCardAdded,onCreditCardAddErrorCbk);
			}
        }

        function onCreditCardAddErrorCbk(data,status)
        {
            $scope.isReqInProgress = false;
            $scope.viewLoading=false;
            $scope.formSubmitErrorMsg = commonInfoFactory.getLocalizedErrorMsg(data,status);
            $(document).scrollTop(0);
        }
        function onCreditCardAdded(response)
        {
            $scope.isReqInProgress = false;
            if(!billingId)
            {
                $analytics.eventTrack('add payment method', {  category: 'behavior', label : 'CreditCard'});
            }
            $scope.viewLoading = false;
            $scope.navigateBack(response);
        }
}]);


angular.module('gigSkyWebApp').controller ('transactionHistoryController',['$scope','$rootScope','$filter','paymentFactory','commonInfoFactory','SimManagerFactory', '$location','$modal',function ($scope,$rootScope,$filter,paymentFactory,commonInfoFactory,SimManagerFactory, $location,$modal){

    $scope.transHistory=[];
    $scope.totalItems = 0;

    $scope.onNextTransHistory = function()
    {
        if($scope.totalItems == 0 || $scope.totalItems > $scope.transHistory.length)
        {
            getTransHistory();
        }
    };

    function getTransHistory()
    {
        if($scope.busy)
            return;
        var startIndex = $scope.transHistory.length;
        var count  = NUMBER_OF_ITEMS_PER_PAGE;

        $scope.busy = true;
        if(!startIndex)
            $scope.viewLoading = true;
        paymentFactory.getConsolidatedTransactionHistory(startIndex,count)
            .success(function (transHistoryResponse) {
                $scope.viewLoading = false;
                $scope.totalItems = transHistoryResponse.totalCount;
                $scope.busy = false;

                var transHistoryList = transHistoryResponse.list;
                $scope.noTransHis = (transHistoryList.length == 0);
                $scope.pagination = !$scope.noTransHis;

                for(var i = 0; i< transHistoryList.length; i++){
                    var transHisInfo = transHistoryList[i];

                    var txnType = transHisInfo.type;

                    (txnType == 'GigskyCreditTransaction')?formatCreditTransaction(transHisInfo):formatPurchaseTransaction(transHisInfo);
                }
            })
            .error(function(){
                $scope.busy = false;
                $scope.viewLoading = false;
            });
    }

    function formatCreditTransaction(transHisInfo){
        var transDescriptionMap = {'REFERRAL_SIGNUP_CREDIT_ADD':Localize.translate('Signup credit'),'REFERRAL_ADVOCATE_CREDIT_ADD':Localize.translate('Referral credit'),'ADVOCATE_CREDIT_EXPIRED':Localize.translate('Referral credit'),'SIGNUP_CREDIT_EXPIRED':Localize.translate('Signup credit'),
            'CURRENCY_CONVERSION':Localize.translate('Currency converted'),'CREDIT_ADD':Localize.translate('General credit'),'GENERAL_CREDIT_ADD':Localize.translate('General credit'),'CREDIT_DEDUCT':Localize.translate('Credit deduct')};

        var transInfoFuncMap = {REFERRAL_SIGNUP_CREDIT_ADD: prepareSignUpTransInfo, REFERRAL_ADVOCATE_CREDIT_ADD:prepareReferralTransInfo,'CREDIT_ADD':prepareCreditTransInfo,
            'GENERAL_CREDIT_ADD':prepareCreditTransInfo,'CREDIT_DEDUCT':prepareCreditTransInfo,CURRENCY_CONVERSION :prepareCurConvTransInfo};

        var transDescription = (transHisInfo.txnType == 'CREDIT_EXPIRY')?((transHisInfo.creditDetail && transHisInfo.creditDetail.creditType == 'REFERRAL_ADVOCATE')?transDescriptionMap.ADVOCATE_CREDIT_EXPIRED:transDescriptionMap.SIGNUP_CREDIT_EXPIRED)
            :((transDescriptionMap[transHisInfo.txnType])?transDescriptionMap[transHisInfo.txnType]:transHisInfo.txnType);

        var transAmt = (transHisInfo.txnType == 'CURRENCY_CONVERSION')?commonInfoFactory.formatPrice(transHisInfo.balanceAfterTxn,transHisInfo.currency):commonInfoFactory.formatPrice(transHisInfo.txnAmount,transHisInfo.currency);

        var isTransInfoAvailable = (transHisInfo.txnType != 'CREDIT_EXPIRY');

        var transInfoFunc = transInfoFuncMap[transHisInfo.txnType];
        isTransInfoAvailable = (transInfoFunc)?isTransInfoAvailable:false;
        var transInfo = (isTransInfoAvailable && transInfoFunc )?transInfoFunc(transHisInfo):{};

        $scope.transHistory.push(
            {
                "transactionId":transHisInfo.creditTxnId,
                "date":commonInfoFactory.setDate(transHisInfo.date),
                "transDescription":transDescription,
                "transAmt":transAmt,
                "transStatus":  (transHisInfo.txnType == 'CREDIT_EXPIRY')?Localize.translate('Expired'):Localize.translate('Complete'),
                "isTranFailed":false,
                "isTransInfoAvailable":isTransInfoAvailable,
                "transInfo":transInfo
            }
        );

    }


    /**
     * @func prepareTransInfo
     * @desc prepare transaction Info required to display in the transaction info popup
     * @param transHisInfo
     * @param concurReceipt
     */
    function prepareTransInfo(transHisInfo,concurReceipt,isFreePlan){
        var transInfo = {};

        if(transHisInfo.paymentMethod && !transHisInfo.paymentDescription)
        {
            transHisInfo.paymentDescription = "****";
        }
        var paymentDescriptionMap = {
            'CreditCard':Localize.translate('Credit card ending %{val}',{val : transHisInfo.paymentDescription}),
            'Paypal':Localize.translate('PayPal account %{val}',{val : transHisInfo.paymentDescription}),
            'ApplePay':Localize.translate('Apple Pay %{val}',{val : transHisInfo.paymentDescription})
        };

        transInfo.nwGroupName = transHisInfo.networkGroupInfo.networkGroupName;
        var plan = transHisInfo.networkGroupPlan;
        transInfo.planSizeDetails = commonInfoFactory.formatPlanDataSize(plan) + " | " + plan.validityPeriodInDays + " "+commonInfoFactory.planDurationGranularity(plan);
        transInfo.simId = transHisInfo.simId;
        transInfo.concurReceipt = (concurReceipt)?concurReceipt:null;
        transInfo.paymentDetailsExists = !isFreePlan;
        if(transInfo.paymentDetailsExists)
        {
            transInfo.paymentDescription = (!isFreePlan && paymentDescriptionMap[transHisInfo.paymentMethod])? paymentDescriptionMap[transHisInfo.paymentMethod] : null;

            if(transInfo.paymentDescription){
                if(transHisInfo.gigskyCreditAmt)
                {
                    transInfo.multiplePaymentAmount = transHisInfo.cost - transHisInfo.gigskyCreditAmt;
                    transInfo.multiplePaymentAmount = commonInfoFactory.formatPrice(transInfo.multiplePaymentAmount,transHisInfo.currency);
                }
                else
                {
                    transInfo.multiplePaymentAmount = commonInfoFactory.formatPrice(transHisInfo.cost,transHisInfo.currency);
                }
            }

            if(transHisInfo.gigskyCreditAmt){
                transInfo.gigskyCreditAmt = commonInfoFactory.formatPrice(transHisInfo.gigskyCreditAmt,transHisInfo.currency);
            }
        }
        transInfo.failureReason = transHisInfo.userDisplayErrorStr;

        return transInfo;

    }

    function formatPurchaseTransaction(transHisInfo){

        $scope.transStatusMap = {
            'PENDING-PAYMENT':Localize.translate('Pending'),
            'PENDING-NETWORKUPDATE':Localize.translate('Pending'),
            'PENDING':Localize.translate('Pending'),
            'COMPLETE':Localize.translate('Complete'),
            'FAILED':Localize.translate('Failed')
        };

        var transDescription = (transHisInfo.transactionType == 'REFUND')? Localize.translate('Refund for ID# %{val}',{val : transHisInfo.refundTransactionId}): Localize.translate('Data plan purchase');
        var isFreePlan = (transHisInfo.networkGroupPlan.freePlan == "true") || (transHisInfo.networkGroupPlan.freePlan == true);

        var transAmt = (isFreePlan)? Localize.translate('Free') : commonInfoFactory.formatPrice(transHisInfo.cost,transHisInfo.currency);

        var transStatus = ($scope.transStatusMap[transHisInfo.status])?$scope.transStatusMap[transHisInfo.status]:transHisInfo.status;

        var concurReceipt = null;
        var receipts = transHisInfo.thirdPartyReceipts;
        // if(receipts)
        // {
        //     for(var m=0;m<receipts.length;m++)
        //     {
        //         var o=receipts[m];
        //         if(o.appName=="CONCUR")
        //         {
        //             concurReceipt = o;
        //             break;
        //         }
        //     }
        // }

        var transInfo = prepareTransInfo(transHisInfo,concurReceipt,isFreePlan);

        var isTranFailed = (transHisInfo.status == 'FAILED');

        $scope.transHistory.push(
            {
                "transactionId":transHisInfo.transactionId,
                "date":commonInfoFactory.setDate(transHisInfo.date),
                "transDescription":transDescription,
                "transAmt":transAmt,
                "transStatus": transStatus,
                isTranFailed:isTranFailed,
                "isTransInfoAvailable":true,
                "transInfo":transInfo
            }
        );
    }

    $scope.displayTransInfo = function (event,transInfo){
        event.stopPropagation();
        new TransactionInfoPopUp($scope,$modal,'transactionInfoModal.html',transInfo).open('sm');
    };

    function prepareSignUpTransInfo(transHisInfo){
        var expDate = (transHisInfo.creditDetail && transHisInfo.creditDetail.expiry)?transHisInfo.creditDetail.expiry:null;
        var expiryDate = commonInfoFactory.formatExpiryDate(expDate);
        return (expiryDate)?{creditExpiry:Localize.translate('Expires on %{val}',{val:expiryDate})}:{};
    }

    function prepareReferralTransInfo(transHisInfo){
        var res = {};
        var expDate = (transHisInfo.creditDetail && transHisInfo.creditDetail.expiry)?transHisInfo.creditDetail.expiry:null;
        var expiryDate = commonInfoFactory.formatExpiryDate(expDate);
        res.creditExpiry = (expiryDate)?Localize.translate('Expires on %{val}',{val:expiryDate}):'';
        var descripiton = transHisInfo.description;
        var referrer = descripiton.match(/For referring.(.*)/);
        res.creditDescription = (referrer && referrer[1])?Localize.translate('For referring %{val}',{val:referrer[1]}):descripiton;
        return res;
    }

    function prepareCreditTransInfo(transHisInfo){
        return {creditDescription:transHisInfo.description};
    }

    function prepareCurConvTransInfo(transHisInfo){
        var res = {};
        var balBefore = commonInfoFactory.formatPrice(transHisInfo.balanceBeforeTxn,transHisInfo.currencyBeforeTxn);
        var balAfter = commonInfoFactory.formatPrice(transHisInfo.balanceAfterTxn,transHisInfo.currency);

        res.creditDescription = Localize.translate('From %{costbefore} to %{costafter}',{costbefore:balBefore,costafter:balAfter});
        return res;
    }

    getTransHistory();
}]);

angular.module('gigSkyWebApp').controller ('creditHistoryController',['$scope','$rootScope','paymentFactory','commonInfoFactory','SimManagerFactory', '$location',function ($scope,$rootScope,paymentFactory,commonInfoFactory,SimManagerFactory, $location){

    $scope.creditHistory=[];
    $scope.totalItems = 0;

    $scope.creditBalance = "";

    $scope.onNextCreditHistory = function()
    {
        if($scope.totalItems == 0 || $scope.totalItems > $scope.creditHistory.length)
        {
            getCreditHistory();
        }
    }

    function getCreditHistory()
    {
        if($scope.busy)
            return;
        var startIndex = $scope.creditHistory.length;
        var count  = NUMBER_OF_ITEMS_PER_PAGE;
        $scope.busy = true;
        if(!startIndex)
            $scope.viewLoading = true;
        paymentFactory.getGigSkyCreditTrans(startIndex,count)
            .success(function (creditHisInfo) {
                $scope.viewLoading = false;
                $scope.totalItems = creditHisInfo.totalCount;
                $scope.busy = false;
                $scope.creditBalance  =  commonInfoFactory.formatPrice(creditHisInfo.gigskyBalance, creditHisInfo.currency);
                //console.log ("data is " + paymentHisInfo.list.length);
                if (creditHisInfo.list.length == 0){
                    $scope.noCreditHis= true;
                    $scope.pagination=false;
                }
                else{
                    $scope.noCreditHis=false;
                    $scope.pagination=true;
                }

                for(var i = 0; i< creditHisInfo.list.length; i++){

                    var newCreditDes,newCost,currencySymbol,newDate,creditBefore,creditAfter,runningBalance;
                    var idString;
                    currencySymbol = creditHisInfo.list[i].currency;

                    newDate=commonInfoFactory.setDate(creditHisInfo.list[i].date);
                    creditBefore = creditHisInfo.list[i].balanceBeforeTxn;
                    creditAfter = creditHisInfo.list[i].balanceAfterTxn;
                    runningBalance = commonInfoFactory.formatPrice(creditAfter, currencySymbol);

                    if(creditHisInfo.list[i].txnAmount)
                    {
                        var diff = parseFloat(creditAfter) - parseFloat(creditBefore);
                        var amt = creditHisInfo.list[i].txnAmount;
                        newCost = commonInfoFactory.formatPrice(amt, currencySymbol,(diff<0)?true:false);
                    }


//                    var diff = parseFloat(creditAfter) - parseFloat(creditBefore);
//
//                    if(diff<0)
//                    {
//                        newCost = "- "+newCost;
//                    }

                    var txType = creditHisInfo.list[i].txnType;

                    if (txType == "PLAN_PURCHASE"){
                        newCreditDes=Localize.translate('Purchase ID (#%{val})',{val : creditHisInfo.list[i].transactionId});
                    }
                    else if(txType == "TRANSACTION_REFUND")
                    {
                        newCreditDes=Localize.translate('Refund ID (#%{val})',{val : creditHisInfo.list[i].transactionId});
                    }
                    else if(txType == "CURRENCY_CONVERSION")
                    {
                        newCreditDes=Localize.translate('Currency change');
                        newCost = "0";
                    }
                    else if(txType == "PENDING")
                    {
                        newCreditDes = Localize.translate('Pending');
                    }
                    else if(txType =="PLAN_PURCHASE_FAILURE")
                    {
                        newCreditDes = Localize.translate('Failed purchase ID (#%{val})',{val : creditHisInfo.list[i].transactionId});
                    }
                    else
                    {
                        if(!creditHisInfo.list[i].description || creditHisInfo.list[i].description.length==0)
                            newCreditDes = Localize.translate('Miscellaneous');
                        else
                            newCreditDes = creditHisInfo.list[i].description;
                    }
                    idString = "#" + creditHisInfo.list[i].creditTxnId;
                    $scope.creditHistory.push({"date":newDate,
                        "paymentDescription": newCreditDes,
                        "transactionId":idString,
                        "currencySymbol":currencySymbol,
                        "cost": newCost,"runningBalance":runningBalance});


                }
            })
            .error(function(){
                $scope.busy = false;
                $scope.viewLoading = false;
            });
    }
    getCreditHistory();
}]);



angular.module('gigSkyWebApp').controller ('addPPController',['$analytics','$scope','$rootScope','$filter','paymentFactory','$location', '$http', '$routeParams','commonInfoFactory',function ($analytics,$scope,$rootScope,$filter,paymentFactory,$location, $http, $routeParams,commonInfoFactory){
	$scope.viewLoading = false;
    var successURL =null;
	var cancelURL = null;
    $scope.progress = true;
    $scope.viewTitle = 'Add PayPal account';
    var billingId = $routeParams.billingId;
    if(billingId){
        $scope.viewTitle = 'Edit PayPal';
    }
    var spath = $location.search();
	if(spath && spath.m && spath.m=='pc'){
        $scope.formSubmitErrorMsg = Localize.translate('Your preapproval was cancelled.');
         displayPayPalDetails();
	}
	else if(spath && spath.m && spath.m=='pa'){
		var PPkey= sessionStorage.getItem("PPKey");
        addOrUpdatePayPal(PPkey);
	}
	else{
        displayPayPalDetails();
	}
    function displayPayPalDetails()
    {
        $scope.payPalAccountDetails = Localize.translate('This action will take you to PayPal website, where you will be able to authorize GigSky payments.');
        if(billingId){
            var curPaymethod = $rootScope.paymethod;
            if(curPaymethod)
            {
                $scope.payPalAccountDetails = Localize.translate('This action will take you to PayPal website, where you will be able to edit your account information.');
                $scope.progress = false;
            }
            else
            {
                $scope.viewLoading = true;
                paymentFactory.getPaymethodDetails(billingId).success(function(response){
                    $scope.payPalAccountDetails = Localize.translate('This action will take you to PayPal website, where you will be able to edit your account information.');
                    $rootScope.paymethod = response;
                    $scope.progress = false;
                    $scope.viewLoading = false;
                }).error(function(data,status){
                        $scope.viewLoading = false;
                        $scope.formSubmitErrorMsg = commonInfoFactory.getLocalizedErrorMsg(daga,status);
                    });
            }
        }
        else
        {
            $scope.progress = false;
        }
        successURL =null;
        cancelURL = null;
    }

	function createURL(){

      	var url = window.location.toString();
        cancelURL = successURL = url;

        var pos = url.indexOf('m=pc');
        if(url.indexOf('m=pc')>=0)
        {
            successURL = successURL.replace('m=pc','m=pa');
            cancelURL = url;
        }
        else if(url.indexOf('m=pa')>=0)
        {
            cancelURL = cancelURL.replace('m=pa','m=pc');
        }
        else
        {
            if(url.indexOf('?')>=0)
            {
                cancelURL+='&m=pc';
                successURL+='&m=pa';
            }
            else
            {
                cancelURL +='?m=pc';
                successURL +='?m=pa';
            }
        }

    }

    $scope.navigateBack = function(response)
    {
        var s = $location.search();
        if(s.pd!=null){
            $location.path('/buy-plan3');
            if(s.m)
              delete s.m;
            if(response && response.billingId){
                sessionStorage.setItem('billingId',response.billingId);
                //s.bid = response.billingId;
            }
            $location.search(s);
            $location.replace();
        }
        else
        {
            $location.path("/paymentMethod");
            $location.search({});
            $location.replace();
        }
    };

    function addOrUpdatePayPal(key)
    {
        function onPayPalAddFailureCbk(data,status)
        {
            $scope.viewLoading=false;
            $scope.formSubmitErrorMsg = commonInfoFactory.getLocalizedErrorMsg(data,status);
            $scope.progress = false;
        }
        function onPayPalAccountAdded(response)
        {
            $scope.viewLoading=false;
            if(!billingId)
            {
                $analytics.eventTrack('add payment method', { category: 'behavior', label : 'PayPal'});
            }
            $scope.navigateBack(response);
        }
        if(billingId){
            var s = $location.search();
            delete s.m;
            $scope.viewLoading = true;
            paymentFactory.updatePP(key,billingId)
                .success(onPayPalAccountAdded)
                .error(onPayPalAddFailureCbk);
        }
        else{
            $scope.viewLoading = true;
            paymentFactory.addPP(key)
                .success(onPayPalAccountAdded)
                .error(onPayPalAddFailureCbk);
        }
    }

	$scope.openPayPal = function( ){
        $scope.formSubmitErrorMsg = null;
        $scope.progress = true;
		createURL();
        $scope.formSubmitResponseMsg = Localize.translate('Redirecting to PayPal site.');
        $scope.viewLoading = true;
		paymentFactory.genPPKey(successURL,cancelURL)
			.success(function (ppKey) {
				$scope.genPPKey=ppKey.preApprovalKey;
		 		sessionStorage.setItem("PPKey",ppKey.preApprovalKey);
		 		var ppURL=PayPAL_URL+ppKey.preApprovalKey;
                window.location=ppURL;
		 	})
		 	.error(function(data,status){
                $scope.formSubmitErrorMsg = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.progress = false;
                $scope.viewLoading = false;
            });
	 }
}]);

angular.module('gigSkyWebApp').controller('creditBalanceInfoController', ['$scope', '$rootScope', '$filter', 'paymentFactory', 'commonInfoFactory','$q',function ($scope, $rootScope, $filter, paymentFactory, commonInfoFactory,$q) {

    $scope.creditGroups = [];
    $scope.totalItems = 0;
    $scope.creditBalanceInfoAvailable = false;
    $scope.totalCreditBalance = "";

    /**
     * @func formatCreditInfo
     * @desc formats credit details
     * @param creditInfo
     * @returns {*}- object
     */
    function formatCreditInfo(creditInfo){
        var expiryDate = commonInfoFactory.formatExpiryDate(creditInfo.expiry);
        creditInfo.expiryLabel = expiryDate ? Localize.translate('Expires on %{val}',{val :expiryDate}) : Localize.translate('No expiry');
        creditInfo.creditAvailBalance = commonInfoFactory.formatPrice(creditInfo.creditBalance, $scope.creditCurrency);
        return creditInfo;
    }

    /**
     * @func getCreditGroups
     * @desc groups credit details by expiry date (todo: this function to be removed once api returns grouped data)
     * @param creditList
     */
    function getCreditGroups(creditList){
        var datePool ={};
        var poolIndex=0;

        for(var i=0;i<creditList.length;i++){
            var creditInfo = formatCreditInfo(creditList[i]);

            if(datePool[creditInfo.expiry]){
                var creditInfoWithSameExpiry = $scope.creditGroups[datePool[creditInfo.expiry]-1];
                creditInfoWithSameExpiry.creditBalance =  creditInfoWithSameExpiry.creditBalance + creditInfo.creditBalance;
                creditInfoWithSameExpiry.creditAvailBalance = commonInfoFactory.formatPrice(creditInfoWithSameExpiry.creditBalance, $scope.creditCurrency);
            }else{
                $scope.creditGroups.push(creditInfo);
                datePool[creditInfo.expiry] = ++poolIndex;
            }
        }
    }

    /**
     * @func getCreditBalanceInfo
     * @desc gets GigSky credit balance info
     */
    function getCreditBalanceInfo(){

        $scope.viewLoading = true;

        paymentFactory.getGigskyCreditGroups()
            .success(function (response) {
                $scope.viewLoading=false;

                var totalCreditBalance = response.totalCreditAmount;
                $scope.creditCurrency = response.creditCurrency;
                $scope.totalCreditBalance = commonInfoFactory.formatPrice(totalCreditBalance, $scope.creditCurrency);

                $scope.creditList = response.list;
                $scope.creditInfoAvailable = ($scope.creditList && $scope.creditList.length>0);
                getCreditGroups($scope.creditList);
            })
            .error(function(data,status){
                $scope.viewLoading=false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
            });

    }

    getCreditBalanceInfo();

}]);

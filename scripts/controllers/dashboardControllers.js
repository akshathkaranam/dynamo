/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 07/08/14
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
angular.module('gigSkyWebApp').controller('dashBoardController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$q','$interval','$compile','$modal',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$q,$interval,$compile,$modal){

    $scope.viewLoading = false;
    $scope.resourcePath = GS_SERVER_BASE;
    $rootScope.runningSubscription = null;
    $rootScope.topupNetworkInfo = null;
    $scope.commonInfoFactory = commonInfoFactory;
    $scope.sim=null;
    $scope.disableBack = true;
    var curLocation = authFactory.getCurrentLocation();

    var s = $location.search();
    s.loc= curLocation;
    $location.search(s);

    $scope.location = curLocation;


    var activeSIM = simMgrFactory.getActiveSIM();

    if(activeSIM) $scope.dashboardtitle = activeSIM.description;

//     $scope.$on('onActiveSIMAvailable',function(){
//      //   console.log("On Active SIM Available");
//         $scope.noSIMAvailable = false;
//         $scope.simAvailable = true;
//         var sim = simMgrFactory.getActiveSIM();
//         $scope.sim = sim;
// //        if($scope.sim.description.search("ACME")!=-1)
// //            $scope.sim.simType = 'ACME_SIM';
//         $scope.simBlocked = false;
//         $scope.blockedByAdmin = false;
//         if(sim.simStatus!="ACTIVE")
//         {
//             $scope.simBlocked = true;
//             if(sim.simStatus == 'BLOCKED_BY_ADMIN')
//                 $scope.blockedByAdmin = true;
//         }
//
//
//         $scope.initDashBoard();
//     });

    function preInitDashBoard(){
        $scope.viewLoading = true;
        $scope.simStatusDeferredInstance = $q.defer();
        $scope.connectedNetwork = null;
        $scope.noSIMService = false;
        $scope.otherActivePlans = [];
        $scope.totalOtherPlans =0;
        $scope.otherCurrentPlansTitle = null;
        $scope.otherPlansIncludeRunningSubscription = false;
        $scope.itemsPerPage = NUMBER_OF_ITEMS_PER_PAGE;
        $scope.currentPage = 1;
        $scope.plansNotSupported = false;
    }

    $scope.unblockSIM = function(){
        var sim = simMgrFactory.getActiveSIM();
        $scope.unBlockInProgress = true;
        $scope.viewLoading = true;
        simMgrFactory.unBlockSIM(sim).success(function(result){
            $scope.simBlocked = false;
            $scope.viewLoading = false;
            $scope.blockedByAdmin = false;
            $scope.unBlockInProgress = false;
        }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
                $scope.unBlockInProgress = false;
        });
    };

    $scope.onBackClick = function(){
        $scope.disableBack = true;
    };

    preInitDashBoard();

    $scope.initDashBoard = function(reload){

        var activeSIM = simMgrFactory.getActiveSIM();

        if(activeSIM)
            $scope.dashboardtitle = activeSIM.description;

            if(!reload && $scope.lastUpdatedTimed)
            {
                var curTime = new Date().getTime();
                //More than a minute since last update
                if(curTime-$scope.lastUpdatedTimed < 60000)
                {
                    return;
                }
            }
            $scope.lastUpdatedTimed= new Date().getTime();
            simMgrFactory.getSIMDetails(activeSIM.simId,true,true).success(function(result){
                // check the active sim's visible status
                // if( result.visibilityStatus !== "visible" ){
                //     simMgrFactory.deInitialize();
                //     simMgrFactory.initialize();
                //     return;
                // }
                var simStatus = result.status;

                // if(!$scope.unBlockInProgress)
                    $scope.viewLoading = false;
                $scope.simStatus = simStatus;
                // if($scope.simStatus.connectionStatus == "CONNECTED")
                // {
                //     $scope.simConnected = true;
                //     for(var i=0;i<$scope.simStatus.list.length;i++)
                //     {
                //         var sub = $scope.simStatus.list[i];
                //         if(sub.subscriptionStatus == "IN-USE")
                //         {
                //             $scope.runningSubscription = sub;
                //             $rootScope.runningSubscription = sub;
                //             $scope.connectedNetwork = $scope.runningSubscription.networkGroupInfo;
                //             $scope.dataRemaining = commonInfoFactory.formatBalanceDataSize($scope.runningSubscription);// + "MB";
                //             $scope.daysRemaining = commonInfoFactory.formatSubRemainingTime($scope.runningSubscription);//.balanceInDays + " days left";
                //             $scope.$broadcast('onRunningSubscriptionDetected',simStatus);
                //             break;
                //         }
                //     }
                // }
                // else
                // {
                //     $scope.simConnected = false;
                //     $scope.runningSubscription = null;
                //     $rootScope.runningSubscription = null;
                //     $scope.dataRemaining = null;
                //     $scope.daysRemaining = null;
                //     $rootScope.runningSubscription =null;
                //     $rootScope.$broadcast('onSIMNotConnected',simStatus);
                // }
                $scope.simStatusDeferredInstance.resolve();

            }).error(function(err){
                $scope.simStatusDeferredInstance.resolve();
                if(!$scope.unBlockInProgress)
                    $scope.viewLoading = false;
            });

//        if(updateOtherActivePlans){
//            $scope.updateOtherActivePlans();
//        }
    };

    $scope.$on('onAvailableNetworkLoaded',function(data,response)
    {
        $scope.viewLoading = false;
        console.log("onAvailableNetworkLoaded Cbk");

        var availableNw = (response && response.networkGroupInfo)?response.networkGroupInfo:response;
        if(($scope.visibleNetwork && availableNw && $scope.visibleNetwork.networkGroupId != availableNw.networkGroupId)
            || (!$scope.visibleNetwork && availableNw) || ($scope.visibleNetwork && !availableNw))
        {
            $scope.updateOtherActivePlans();
        }
        else
        if(!availableNw && !$scope.otherActivePlans.length)
        {
            $scope.updateOtherActivePlans();
        }

        $scope.visibleNetwork = availableNw;
        $scope.plansNotSupported = $scope.visibleNetwork?($scope.visibleNetwork.status?!$scope.visibleNetwork.status.serviceAvailable:false):true; //this variable is not used anywhere. Hence not correcting to handle based on 'serviceDisabledCountries' field
        $scope.noSIMService = $scope.visibleNetwork?false:true;
    });

    $scope.$on('onNOServicesInCurrentLocation',function(){
        $scope.noSIMService = true;
        //$scope.dataRemaining = null;
        //$scope.daysRemaining = null;
        $scope.plansNotSupported = true;
    });

    $scope.updateOtherActivePlans = function()
    {
       // $scope.getOtherActivePlansInPage(0,NUMBER_OF_ITEMS_PER_PAGE);
    };

    $scope.getNextOtherActivePlans = function()
    {
        var totalItem = $scope.otherPlansIncludeRunningSubscription?$scope.totalOtherPlans-1:$scope.totalOtherPlans;
        if(totalItem>0 && totalItem > $scope.otherActivePlans.length)
        {
            var nextIndex = ($scope.otherPlansIncludeRunningSubscription?$scope.otherActivePlans.length+1:$scope.otherActivePlans.length);
            $scope.getOtherActivePlansInPage(nextIndex,NUMBER_OF_ITEMS_PER_PAGE);
        }
    };

    $scope.getOtherActivePlansInPage = function(stIndex,count){

        if(!simMgrFactory.getActiveSIM()){
            return;
        }

        if($scope.busy)
            return;

        $scope.busy = true;
        simMgrFactory.getSubscriptions(simMgrFactory.getActiveSIM().simId,"ACTIVE",stIndex,count).success(function(response){

            if(stIndex == 0)
            {
                $scope.otherActivePlans = [];
            }

            //$scope.otherActivePlans = response.list;
            $scope.busy = false;
            var activePlans = response.list;

            $scope.totalOtherPlans  = response.totalCount;
            var runningSub = $scope.runningSubscription;

            if(activePlans && activePlans.length)
            {
                for(var i= 0,l=activePlans.length;i<l;i++)
                {
                    var sub = activePlans[i];
                    if($scope.visibleNetwork && sub.networkGroupInfo.networkGroupId == $scope.visibleNetwork.networkGroupId)
                    {
                        //Remove this entry from other active plans as it is already shown
                        $scope.otherPlansIncludeRunningSubscription = true;
                        continue;
                    }
                    $scope.otherActivePlans.push(sub);
                }
            }

            if($scope.otherActivePlans && $scope.otherActivePlans.length)
                $scope.otherCurrentPlansTitle = Localize.translate('Other active plans (%{val}):',{val : $scope.otherActivePlans.length});
            else
                $scope.otherCurrentPlansTitle = "";
        }).error(function(er){
                $scope.busy = false;
            });

    };


    $scope.switchPlan = function(subscription){
        simMgrFactory.switchToSubscription(subscription.subscriptionId).success(function(response){
            $scope.initDashBoard();
            //$location.path("/dashboard");
        }).error(function(data,status){

            });
    };
    //simMgrFactory.initialize();

   /* if($scope.timer && angular.isDefined($scope.timer))
    {
        $interval.cancel($scope.timer);
        $scope.timer = undefined;
    }

    $scope.timer = $interval(function(){
        if(!$scope.unBlockInProgress)
            simMgrFactory.initialize();
    },60000,0,true);

    $scope.$on('$destroy', function() {
        // Make sure that the interval nis destroyed too
        if(angular.isDefined($scope.timer))
            $interval.cancel($scope.timer);
        $scope.timer = undefined;
    });*/

    $scope.onTopupSubscription = function(subscription){

        var location = (subscription.lastVisitedCountry && subscription.lastVisitedCountry.length)?subscription.lastVisitedCountry:(subscription.lastPurchaseIntentCountry && subscription.lastPurchaseIntentCountry.length)?subscription.lastPurchaseIntentCountry:subscription.networkGroupInfo.countryCodes;
        
        $location.path("/buy-plan2");
        $location.search({nw:subscription.networkGroupInfo.networkGroupId,si:2,planPurchaseLoc:location.toString()});
        $rootScope.topupNetworkInfo = subscription.networkGroupInfo;
    }

}]);

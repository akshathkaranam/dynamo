'use strict';
angular.module('gigSkyWebApp')
    .directive('myLoadingSpinner', function() {
        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            scope: {
                loading: '=myLoadingSpinner'
            },
            templateUrl: 'spinner.html',
            link: function(scope, element, attrs) {
//                scope.myLoadingSpinner = attrs['myLoadingSpinner'];
                var radius = 14;
                var lines = 13;
                if(attrs['size']=='sm')
                {
                    radius = 7;
                    lines = 8;
                }
                var opts = {
                    lines: lines, // The number of lines to draw
                    length: 4, // The length of each line
                    width: 3, // The line thickness
                    radius: radius, // The radius of the inner circle
                    corners: 1, // Corner roundness (0..1)
                    rotate: 16, // The rotation offset
                    direction: 1, // 1: clockwise, -1: counterclockwise
                    color: '#000', // #rgb or #rrggbb or array of colors
                    speed: 1, // Rounds per second
                    trail: 60, // Afterglow percentage
                    shadow: false, // Whether to render a shadow
                    hwaccel: false, // Whether to use hardware acceleration
                    className: 'spinner', // The CSS class to assign to the spinner
                    zIndex: 2e9, // The z-index (defaults to 2000000000)
                    top: '50%', // Top position relative to parent
                    left: '50%' // Left position relative to parent
                };
                var spinner = new Spinner(opts).spin();
                var loadingContainer = element.find('.my-loading-spinner-container')[0];
                loadingContainer.appendChild(spinner.el);
            }
        };
    });

angular.module('gigSkyWebApp').directive('stepIndicator',['$location','$rootScope',function($location,$rootScope){

    return {
        restrict:'E',
        templateUrl:'stepIndicator.html',
        scope:{maxSteps:'=maxSteps',currentStep:'=current',planPath:'=planPath'},
        link:function(scope,element,attrs){

            scope.lazyLoadURL = $rootScope.lazyLoadURL;
            var title1 = Localize.translate('Select destination');
            var title2= Localize.translate('Select a plan for use in');
            var title3 = Localize.translate('Make payment');
            scope.isDisabled =function(i){
                return (i+1)>=scope.currentStep;
            };
            var s = parseInt(scope.currentStep);
            switch(s)
            {
                case 1:
                    if(scope.maxSteps==3){
                        scope.steps = [{img:'step1SelectedIcon.png',path:null,title:title1},
                            {img:'step2deSelectedIcon.png',path:null,title:title2},
                            {img:'step3deSelectedIcon.png',path:null,title:title3}];}
                    else{
                        scope.steps = [{img:'step1SelectedIcon.png',path:null,title:title2},
                            {img:'step2deSelectedIcon.png',path:null,title:title3}];
                    }
                    break;
                case 2:
                    if(scope.maxSteps==3)
                        scope.steps = [{img:'stepChkIcon.png',path:'#/buy-plan1',title:title1},{img:'step2SelectedIcon.png',path:null,title:title2},{img:'step3deSelectedIcon.png',path:null,title:title3}];
                    else
                        scope.steps = [{img:'stepChkIcon.png',path:scope.planPath,title:title2},{img:'step2SelectedIcon.png',path:null,title:title3}];
                    break;
                case 3:
                    scope.steps = [{img:'stepChkIcon.png',path:'#/buy-plan1',title:title1},{img:'stepChkIcon.png',path:scope.planPath,title:title2},{img:'step3SelectedIcon.png',path:null,title:title3}];
                    break;
            }
        }

    };
}]);
angular.module('gigSkyWebApp').directive('viewTitle',['$location',function($location){

    return {
        restrict:'E',
        templateUrl:'viewTitle.html',
        scope:{viewName:'=viewName',viewTitle:'=?',translateRequired:'=?translateRequired',back:'&onBack',backUrl:'=',disableBack:'=disableBack'},
        link:function(scope,element,attrs){

            scope.$watch('viewName',updateTitle);
            scope.$watch('disableBack',function(){
                scope.isBackPossible = !scope.disableBack;
            });
            function updateTitle(){
                scope.viewTitle = scope.viewName;
                if(scope.translateRequired === undefined){
                    scope.translateRequired = true;
                }
                if(scope.viewName != null && scope.viewName.length > 0 && scope.translateRequired){
                    scope.viewTitle  = Localize.translate(scope.viewName);
                }
            };
            updateTitle();
            scope.isBackPossible = !scope.disableBack;
            scope.onBackClick = function(){

                if(attrs.onBack && scope.back)
                {
                    scope.back();
                }

                if(scope.backUrl)
                {
                    $location.path(scope.backUrl);
                    return;
                }
            }
        }

    };
}]);

angular.module('gigSkyWebApp').controller ('topHeaderController',['$scope','$rootScope','$location',function ($scope,$rootScope,$location){
        $(".navbar-toggle").on('click',function (event){
            $(".navbar-toggle > i").toggleClass("fa-bars");
            $(".navbar-toggle > i").toggleClass("fa-close");
        $(".custPanel").toggle(400,function() {
            $(".custPanel").animate({width:"100%",display:"none",opacity: "1"},100, "linear" );

        });
        event.stopPropagation();
    });

    $scope.navigateToUrl = function(url){
        window.location.href = url;
    }


    /*    $(document).click(function(event) {
            if($('.mobilemenu').is(":visible")){
                if(!$(event.target).closest('.leftNavigation').length) {
                    if($('.leftNavigation').is(":visible")) {
                        event.stopPropagation();
                        event.preventDefault();
                        $(".custPanel").toggle(400,function() {
                            $(".custPanel").animate({width:"100%",display:"none",opacity: "1"},100, "linear" );

                        });
                    }
                }
            }
        });*/
}]);

angular.module('gigSkyWebApp').controller ('desktopMenuCtrl',['$scope','$rootScope','$location',function ($scope,$rootScope,$location){
          $scope.$location = $location;
          $scope.$on('onLogOut',function(){
             //Reset menu state on logout..
             $scope.isCollapsed4 = false;
             $scope.isCollapsed3 = false;
             $scope.isCollapsed2 = false;

          });
		  var activePath = null;
		  $scope.$on('$routeChangeSuccess', function(){
		    activePath = $location.path();

              var referralEnabled = sessionStorage.getItem("isReferralEnabled"); //added to hide or unHide the refer a friend option (both desktop & mobile menu)
              $rootScope.isReferralEnabled = (referralEnabled != null )?referralEnabled:false;

		    //console.log("routeChangeSuccess " + $location.path() );

          $("#loginSeparator").show();
          $("#loginLink").show();
          $("#signUpSeparator").show();
          $("#signUpLink").show();

              if ($scope.isActive("/changePassword") || $scope.isActive("/linked-accounts") || $scope.isActive("/editProfile")) {
                  $scope.isCollapsed4 = true;
              }
              if ($scope.isActive("/paymentMethod") || $scope.isActive("/paymentHistory") || $scope.isActive("/creditHistory") || $scope.isActive("/creditBalance")) {
                  $scope.isCollapsed3 = true;
              }
              if ($scope.isActive("/my-sims") || $scope.isActive("/activesim") || $scope.isActive("/shop")) {
                  $scope.isCollapsed2 = true;
              }

            if($scope.isActive("/signUp"))
            {
                $("#signUpSeparator").hide();
                $("#signUpLink").hide();
            }
            else if($scope.isActive("/login"))
            {
                $("#loginSeparator").hide();
                $("#loginLink").hide();
            }



		   // $(".custPanel").animate({width:"85%",display:"none",opacity: "1"},100, "linear" );


          });
		  $scope.isActive = function( pattern ) {
            if(activePath == null)
            {
                activePath = $location.path();
            }
		    var result = (new RegExp( pattern )).test( activePath );
            //console.log("pattern " + pattern + " activePath " + activePath + " result " + result);

            return result;
		  };

}]);

//toggle
angular.module('gigSkyWebApp').controller ('mobileMenuCtrl',['$scope','$rootScope','$location','$window',function ($scope,$rootScope,$location,$window){
    $scope.isActive = function( pattern ) {
        if(activePath == null)
        {
            activePath = $location.path();
        }
        var result = (new RegExp( pattern )).test( activePath );
      //  console.log("pattern in Mobile menu controll" + pattern + " activePath " + activePath + " result " + result);

        return result;
    };

        $scope.$location = $location;
        $scope.$on('onLogOut',function(){
            //Reset menu state on logout..
            $scope.isCollapsed4 = false;
            $scope.isCollapsed3 = false;
            $scope.isCollapsed2 = false;
        });
          //IE- fix..
//          $(".custPanel").click(function(event){
//              if(event)
//              $(event).preventDefault();
//          });
    if ($scope.isActive("/changePassword") || $scope.isActive("/linked-accounts") || $scope.isActive("/editProfile")) {
        $scope.isCollapsed4 = true;
    }
    if ($scope.isActive("/paymentMethod") || $scope.isActive("/paymentHistory") || $scope.isActive("/creditHistory") || $scope.isActive("/creditBalance") ) {
        $scope.isCollapsed3 = true;
    }
    if ($scope.isActive("/my-sims") || $scope.isActive("/activesim") || $scope.isActive("/shop")) {
        $scope.isCollapsed2 = true;
    }
          var activePath = null;
		  $scope.$on('$routeChangeSuccess', function(){
		    activePath = $location.path();
		   $(".custPanel").css("display","none");
          });
		  $scope.isActive = function( pattern ) {
		    return (new RegExp( pattern )).test( activePath );
		  };
          $scope.closeMenu = function(){
              $(".custPanel").css("display","none");

                  $(".navbar-toggle > i").toggleClass("fa-bars");
                  $(".navbar-toggle > i").toggleClass("fa-close");



          };

          $scope.navigateToUrl = function(url){
            window.location.href = url;
          }

          $('#mobileMenu li a').click(function(event){
                $scope.closeMenu();
                event.stopPropagation();
          });

}]);

angular.module('gigSkyWebApp').controller('languageController',['$scope',function($scope) {
	/*$scope.changeLanguage = function (langKey) {
		console.log(" in changelanguage " + langKey );
		$translate.use(langKey);
	};*/
}]);


angular.module('gigSkyWebApp').controller ('userInfoController',['$scope','$rootScope','commonInfoFactory','authFactory','$location','$analytics',function ($scope,$rootScope,commonInfoFactory,authFactory,$location,$analytics){

	$scope.logout= function (){
        $rootScope.logOutInProgress = true;
        authFactory.logout()
			.then(function () {
                $analytics.eventTrack('logout', { category: 'behavior' });
                $rootScope.logOutInProgress = false;
				$rootScope.logoutLink="";
				$rootScope.imageLink="";
                $rootScope.formErrorMsg=null;
				$location.path( "#/login" );
                authFactory.clearSession();
			},function(){
                $rootScope.logOutInProgress = false;
            });
		};
    $rootScope.welcomeName = sessionStorage.getItem("userEmail");
    $rootScope.welcomeNameShort = sessionStorage.getItem("userEmail");
    if($rootScope.welcomeNameShort != null && $rootScope.welcomeNameShort.length>20)
    {
        $rootScope.welcomeNameShort = $rootScope.welcomeNameShort.substring(0,20) + "...";
    }
}]);

angular.module('gigSkyWebApp').directive('gsFormValidation',function(){
    return{
        controller:['$scope','$filter','commonInfoFactory','$element',function($scope,$filter,commonInfoFactory,$element){

            $scope.isFieldNotValid = function(field,label,errorStr)
            {
                field.errorStr = null;
                if(field.$invalid)
                {
                    var el = $($element).find('input[name="'+field.$name+'"]');
                    var minlengthErrorStr = null;
                    var nameOnCardErrorStr = $(el).attr('nameOnCardErrorStr');
                    if(el && el.length )
                    {
                        minlengthErrorStr = $(el).attr('minlengtherror');
                    }
                    if(field.$error && field.$error.minlength && minlengthErrorStr)
                    {
                        field.errorStr = minlengthErrorStr;
                    }else
                    if(field.$error && field.$error.pattern && nameOnCardErrorStr){
                        field.errorStr = Localize.translate(nameOnCardErrorStr);
                    }else
                    if(errorStr)
                        field.errorStr = errorStr;
                    else
                        field.errorStr = Localize.translate('%{val} invalid',{val : label});
                    $scope.currentInvalidField = field;
                    return true;
                }

                    //Exception case..

                if(field.$name == 'ccv')
                {
                    var el = $($element).find('input[name="ccv"]');

                    var ccn = $($element).find('input[name="CCN"]');
                    var carNum = $(ccn).val();
                    var ccv = $(el).val();
                    if(($.payment.cardType(carNum) != 'amex' && ccv && ccv.length !==3) || ($.payment.cardType(carNum) == 'amex' && ccv && ccv.length !==4)){
                        var label = Localize.translate("Card security code");
                        field.errorStr = Localize.translate('%{val} invalid', {val : label});
//                        $scope.setFormFieldInvalid($scope.ccForm.ccv);
                        $scope.currentInvalidField = field;
                        return true;
                    }
                }

                return false;
            }

            $scope.isFieldNotValidInGroup = function(field,groupField,label,errorStr,form)
            {
                field.errorStr = null;
                groupField.errorStr = null;
                if(field.$invalid)
                {
                    if(errorStr)
                        groupField.errorStr = errorStr;
                    else
                        groupField.errorStr = Localize.translate('%{val} invalid',{val : label});
                    $scope.currentInvalidField = groupField;
                    return true;
                }
                //Exception case..
                if(field.$name == 'expMonth' || field.$name== 'expYear'){

                    var d = new Date();
                    var expY= $($element).find('input[name="expYear"]');
                    expY = $(expY).val();
                    var expYear = "20"+expY;
                    return $scope.isExpDateNotValid(form.expMonth,form.expYear,form.expDate,Localize.translate('Expiration date'),expYear)

                }
                return false;
            }

            $scope.fieldsNotMatching = function(f1,label1,f2,label2,errorStr)
            {
                f1.errorStr = null;
                f2.errorStr = null;
                if(f1.$viewValue != f2.$viewValue)
                {
                    if(errorStr)
                        f2.errorStr = errorStr;
                    else
                        f2.errorStr = Localize.translate('%{val1} and %{val2} do not match',{val1:label1,val2:label2});
                    return true;
                }
                return false;
            }

            $scope.isCCNotValid = function (mf, label) {
                mf.$invalid = false;
                //console.log("CC validator" + mf.$viewValue);
                var fieldVal = mf.$viewValue;
                var errorStr = null;
                var validVISAOrMasterormaex = false;
                var validCC = false;
                if (fieldVal){
                    fieldVal = fieldVal.replace(/[- ]/g,'');
                    if(fieldVal.length > 16) {
                        fieldVal = fieldVal.substring(0, 16);
                        mf.$setViewValue(fieldVal);
                    }
                    if(fieldVal.length<=16)
                    {
                        validVISAOrMasterormaex = commonInfoFactory.validateCreditCardType(fieldVal);
                        if(!validVISAOrMasterormaex)
                        {
                            errorStr = Localize.translate('Only AMEX, VISA or MasterCard is supported.');
                            //errorStr = Localize.translate('Only VISA or MasterCard is supported.');
                        }
                        validCC = commonInfoFactory.validateCreditCard(fieldVal);
                    }
                }

                if (!fieldVal || !validVISAOrMasterormaex || !validCC) {
                    mf.$invalid = true;
                    var el = $($element);
                    var f = $(el).find('input[name="' + mf.$name + '"]');
                    if (f && f.length)
                        $scope.scrollToFieldIfNotInView(f);
                    return $scope.isFieldNotValid(mf, label,errorStr);
                }
                return false;
            };

            $scope.isExpDateNotValid=function(monthField,yearField,maskField,label,yearValue){
                maskField.$invalid = true;
                var invalid = monthField.$invalid || yearField.$invalid;
                var formatDate = $filter('date')(new Date(), 'yyyy-MM');
                var expDate = $filter('date')(yearValue + "-" + monthField.$viewValue, 'yyyy-MM');
                invalid = (invalid) ? invalid : (formatDate > expDate);
                if (invalid) {
                    var el = $($element);
                    var f = $(el).find('input[name="' + yearField.$name + '"]');
                    if (f && f.length)
                        $scope.scrollToFieldIfNotInView(f);
                    return  $scope.isFieldNotValid(maskField, label);
                }
                return false;
            }
            $scope.scrollToFieldIfNotInView = function(f)
            {
                var offset = $(f).offset().top - $(document).scrollTop();
                //console.log("offset is "+offset);
                if(offset>window.innerHeight || offset<0){
                    $(document).scrollTop($(f).offset().top-40);
                }
            }

            $scope.resetselectedState = function( ele ){
                setTimeout( function() {

                    ele.css("background", "#1cbbf0");

                    ele.hover(function () {
                        $(this).css("background", "#129CCA")
                     });
                    ele.mouseout(function () {
                        $(this).css("background", "#1cbbf0")
                    });

                    document.querySelector(".btn-default").addEventListener("touchstart",  function(){   this.style.backgroundColor="#129CCA"; } );

                    document.querySelector(".btn-default").addEventListener( "touchend", function(){ this.style.backgroundColor ="#1cbbf0"; } );
                }, 500 );

            }

        }],

        link:function(scope,element,attrs)
        {
            $(element).find(":input").bind('focus',function(){
                if(scope.currentInvalidField)
                    scope.currentInvalidField.errorStr = null;
            });

            $(element).find(":input").bind('keyup',function(){
                if(scope.currentInvalidField)
                    scope.currentInvalidField.errorStr = null;
            });

            scope.$on('$destroy',function(){
                $(element).find(":input").unbind('focus');
                $(element).find(":input").unbind('keyup');
            });

            function fieldLabel(f)
            {
                var fname = $(f).attr("name");
                var labelTitle = $(f).attr("gslabel");
                if(!labelTitle)
                {
                    var inGroup = $(f).attr('groupfield');
                    if(inGroup)
                    {
                        fname = inGroup;
                    }
                    var  label = $(element).find('label[for="'+fname+'"]');
                    labelTitle = $(label).html();
                }
                else{
                    labelTitle = Localize.translate(labelTitle);
                }
                return labelTitle;
            }


            function isTextInput(node) {
                return ['INPUT', 'TEXTAREA'].indexOf(node.nodeName) !== -1;
            }

//            scope.setFormFieldInvalid = function(field){
//                scope.currentInvalidField = field;
//                var btn = $(element).find(".btn-default");
//                var f  = $(element).find('input[name="'+ field.$name +'"]');
//                scope.scrollToFieldIfNotInView(f );
//                scope.resetselectedState( btn );
//            };

            scope.passwordPolicyMsg = function(event){
                var langCode = sessionStorage.getItem("currentLanguageCode");
                scope.isDisplay = true;

                //if(langCode == 'ja'){
                //    scope.passwordMsg = Localize.translate('The GigSky password should be at least 8 characters.');
                //}else
                {
                    scope.passwordMsg = Localize.translate('Your GigSky password should be at least 8 characters in length and contain at least one uppercase letter and one number.');
                }
                event.stopPropagation();
            }
            scope.clearErrorMessage = function(){
                if(scope.currentInvalidField)
                    scope.currentInvalidField.errorStr = null;
            };

            scope.isFormValid = function(form){
                element = $('form[name="'+form.$name +'"]');
                var btn = $(element).find(".btn-default");


                //to dismiss the keyboard when we click on go button in ios
                var activeElem = document.activeElement;
                if ( activeElem && isTextInput(activeElem) && !($(activeElem).attr('ng-blur'))) {
                    activeElem.blur();
                }

                var allinputs = $(element).find(":input:visible");

                for(var i= 0,c=allinputs.length;i<c;i++)
                {
                    var f = allinputs[i];
//                    if($(f).attr('optional')=="true")
//                        continue;

                    var labelTitle = fieldLabel(f);
                    if(!labelTitle)
                        continue;
                    var inGroup = $(f).attr('groupfield');
                    var fname = $(f).attr('name');
                    var requiredError = $(f).attr('requiredError');
                    requiredError = Localize.translate(requiredError);
                    if(inGroup)
                    {
                        var gfield = form[inGroup];
                        if(!gfield)
                            form[inGroup] = {};

                        if(scope.isFieldNotValidInGroup(form[fname],form[inGroup] ,labelTitle,null,form))
                        {
                            scope.scrollToFieldIfNotInView(f );
                            scope.resetselectedState( btn );
                            return false;
                        }
                    }
                    else
                    if(fname)
                    {
                        if(scope.isFieldNotValid(form[fname],fieldLabel(f),requiredError))
                        {
                           scope.scrollToFieldIfNotInView(f );
                            scope.resetselectedState( btn );
                            return false;
                        }
                    }
                    var confirm = $(f).attr("confirm");
                    if(confirm)
                    {
                       var label2Title = fieldLabel(f);
                       var confirmField = $(element).find('input[name="'+confirm+'"]');
                       var label1Title = fieldLabel(confirmField);
                       if(scope.fieldsNotMatching(form[confirm],label1Title,form[fname],label2Title))
                       {
                           if($(f).attr('type')=='password')
                           {
                               $(f).val('');
                               form[fname].$setViewValue('');
                           }
                           scope.scrollToFieldIfNotInView(f );
                           scope.resetselectedState( btn );
                           return false;
                       }
                    }

                    /**
                     * password policy
                     * this validation applies for only input fields with id containing 'validatePwd'
                     */
                    if( $(f).attr('type')=='password')
                    {
                        var fieldId = $(f).attr('validatePwd');

                        if(fieldId){
                            var val = f.value;

                            //pattern to match non-latin characters
                            var pattern = /[^\u0000-\u007F | \u0080-\u00FF | \u0100-\u017F | \u0180-\u024F | \u1E00-\u1EFF | \u2C60-\u2C7F | \uA720-\uA7FF]+/;
                            if(pattern.test(val)){
                                var field = form[fname];
                                scope.currentInvalidField = field;
                                field.errorStr = Localize.translate('Your password must have only english characters.');
                                return false;
                            }
                            else{
                                if(!(/[A-Z]/.test(val) && /\d/.test(val))){
                                    var field = form[fname];
                                    scope.currentInvalidField = field;
                                    field.errorStr = Localize.translate('Your password must have at least one uppercase letter and one number.');
                                    return false;
                                }
                            }
                        }

                    }

                    var validateReferralCode = $(f).attr('validateRef');

                    /**
                     * validating invite code field in signUp form
                     * - it's an optional field - length can be 0
                     * - if present it should be
                     *  - min length - 4
                     *  - max length - 9
                     *  - should allow only alphanumeric
                     *  - should contain some aplpha & numeric
                     */
                    if(validateReferralCode){
                        var val = f.value;

                        var rcPattern = /^[a-zA-Z0-9]{4,9}$/;
                        if(val.length !=0 && ( (val.length <4) || (val.length>10) || !rcPattern.test(val))){
                            var field = form[fname];
                            scope.currentInvalidField = field;
                            var label = fieldLabel(f);
                            field.errorStr = Localize.translate('%{val} invalid',{val : label});
                            return false;
                        }
                    }

                }

                return true;
            }
        }
    }
});

angular.module('gigSkyWebApp').directive('fieldLimit',function(){
    return {
        link:function(scope,element,attr){
            var limit = attr['fieldLimit'];
            function onKeyup(event){
                var v = $(element).val();
                if($(element).val().length>limit)
                {
                    $(element).val(v.substr(0,2));
                    event.preventDefault();
                }
            }
            $(element).bind('keyup',onKeyup);

            function onKeyDown(event){
                var v = $(element).val();
                var d = event.which;
                if(d == 8 || d==37 || d== 39 || d == 13  || d == 9)
                    return;
                if($(element).val().length == limit)
                {
                    event.preventDefault();
                }
            }
            $(element).bind('keydown',onKeyDown);

           scope.$on('$destroy',function(){
                $(element).unbind('keyup',onKeyup);
                $(element).unbind('keydown',onKeyDown);
            });
        }
    }

});

angular.module('gigSkyWebApp').directive('autoFocusNext',function(){
    return {
        link:function(scope,element,attr){
            $(element).on('keyup',function(e){

                var next = attr['nextField'];
                var maxL = attr['nextOnLength'];
                var maxLC = attr['ccAutoLen'];
                var matchPattern = attr['nextOnPattern'];

                var $target, value;
                $target = $(e.currentTarget);
                value = $target.val();
                var matches = false;
                if(matchPattern)
                {
                    var re = new RegExp(matchPattern);
                    matches = re.test(value);
                    if(matches && value.length <maxL && value < 10 && value > 1)
                    {
                        $target.val('0'+value);
                    }
                }

                //max length calculation for credit card number based on type
                if(maxLC) {
                    maxLC = $.parseJSON(maxLC);
                    for (var i = 0; i < maxLC.condetion.length; i++) {
                        if (maxLC.condetion[i].cctype == $.payment.cardType(maxLC.cardNo)) {
                            maxL = maxLC.condetion[i].autolen;
                            break;
                        }
                    }
                }

                if(value.length==maxL || matches)
                {

                    var isTouchDevice = 'ontouchend' in document;
                    if(!isTouchDevice)
                    {
                        $('input[id="'+next+'"]').focus();
                    }
                    else{
                        $('input[id="'+next+'"]').unbind('touchstart');
                        $('input[id="'+next+'"]').on('touchstart', function () {
                            $(this).focus();
                            $('input[id="'+next+'"]').unbind('touchstart');
                        });
                        $('input[id="'+next+'"]').trigger('touchstart');
                    }
                    //$(this).next('input').trigger('touchstart')
                }
            })
        }
    }
});
angular.module('gigSkyWebApp').controller('supportController',['$scope','$http','$modal','authFactory','commonInfoFactory',function($scope,$http,$modal,authFactory,commonInfoFactory){
    $scope.viewLoading = true;
    authFactory.getAccount().success(function (userInfo) {
        $scope.viewLoading = false;
        $scope.emailId = userInfo.emailId;
        $scope.name = userInfo.firstName ? userInfo.firstName : "";

    }).error(function (data, status) {
        $scope.viewLoading = false;
        $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
    });

    $scope.createSupportTicket= function (supportForm){

        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr = null;

        if(!$scope.isFormValid(supportForm))
            return;
        createSupportRequest();

    };

    function createSupportRequest(){
        $scope.viewLoading = true;
        $scope.formSubmitErrorStr = false;
        var postData = {type:"SupportEmail",category : "CONTACT_HELP",contactEmail:$scope.contact,name:$scope.name,location:$scope.location,subject:$scope.subject,message:$scope.message};
        if($scope.emailId && $scope.emailId.length)
        {
            postData.gigskyEmail = $scope.emailId;
        }

        if($scope.device && $scope.device.length)
        {
            postData.mobileDevice = $scope.device;
        }

        $http.post(GS_NON_ACCOUNT_API_BASE+'supportEmail',postData).success(function(){
            $scope.viewLoading = false;
            $scope.supportMessage = "";
            $scope.contact="";
            $scope.location="";
            $scope.subject="";
            $scope.message="";
            $scope.device="";
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Your request has been submitted to our Help Center. We will get in touch with you soon.'),Localize.translate('OK')).open('sm');
        }).error(function(data,status){
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
        });
    }
}]);

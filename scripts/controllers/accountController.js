'use strict';


angular.module('gigSkyWebApp').controller('signUpController',['$analytics','$scope','$window','$rootScope','$location','authFactory','commonInfoFactory','$http','referralService',function ($analytics,$scope,$window,$rootScope,$location,authFactory,commonInfoFactory,$http,referralService){

    $scope.viewLoading = false;
    $rootScope.fromSignup = false;
    $scope.selectCountryStr=Localize.translate('Select country');
    $scope.minimumLengthPwdError = Localize.translate('Your password must have a minimum of 8 characters.');

    var search = $location.search();

    $scope.widgetId = null;

    $scope.$on('$destroy',function() {
        if (window["grecaptcha"] && $scope.widgetId)
            grecaptcha.reset($scope.widgetId);
    });

    window.captchaLoadCallback = function()
    {
        var event = document.createEvent('Event');
        event.initEvent('CaptchaLoad', true, true);
        document.dispatchEvent(event);
    }

    // Listen for the event.
    document.addEventListener('CaptchaLoad', function (e) {
        $scope.$apply(function(){
            $scope.viewLoading = false;
        });
        $scope.widgetId = grecaptcha.render('captcha-div', {
            'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
        });
    }, false);

    if(!$rootScope.isCaptive && !DISABLE_CAPTCHA)
    {
        if(!$rootScope.loadCaptchaScript)
        {
            $scope.viewLoading = true;
            var scriptElement =document.createElement('script');
            var firstScriptElement=document.getElementsByTagName('script')[0];
            scriptElement.async=1;
            if($rootScope.enableLocalization)
            {
                scriptElement.src="//www.google.com/recaptcha/api.js?onload=captchaLoadCallback&render=explicit" + "&hl=" + $rootScope.currentLangCode;
            }
            else
            {
                scriptElement.src="//www.google.com/recaptcha/api.js?onload=captchaLoadCallback&render=explicit";
            }
            firstScriptElement.parentNode.insertBefore(scriptElement,firstScriptElement);
            $rootScope.loadCaptchaScript = true;
        }
        else
        {
            $scope.widgetId = grecaptcha.render('captcha-div', {
                'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
            });
        }
    }



    $scope.updateCountryList($scope,'formSubmitErrorStr');

    $scope.linkToConur = false;
    if($location.path().indexOf('signUp-concur')!=-1)
    {
        $scope.linkToConur = true;
    }

	$scope.signIn= function (signup_form,event){

        if(event && event.which != 13){
            return;
        }

        console.log("Agree to terms " + $scope.agreeTerms);

        $rootScope.fromSignup = true;
        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr = null;
        signup_form.captchaErrorStr = null;

        if(!$scope.isFormValid(signup_form))
        {
            return;
        }

        if((!$rootScope.isCaptive && !DISABLE_CAPTCHA) && grecaptcha.getResponse($scope.widgetId) == "")
        {
            signup_form.captchaErrorStr = Localize.translate('Please complete CAPTCHA verification.');
            return;
        }
        if($scope.referralCode && $scope.referralCode.length > 3 )
        {
            $scope.viewLoading = true;
            referralService.validateCode($scope.referralCode, 'referral').success(function(){
                $scope.viewLoading = false;
                createAccount();
            }).error(function(data,status){
                $scope.viewLoading = false;
                if(data && (data.errorInt == 12006 || data.errorInt == 12600) ){
                    $scope.formSubmitErrorStr = Localize.translate("Unable to complete request due to an internal error. Please try again later.");
                }
                else{
                    $analytics.eventTrack('invalid referral', { category: 'behavior'});
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                }
            });
        }
        else
            createAccount();
    };


//    function linkToConcurAccount(){
//
//        var s = $location.search();
//        if(s.code){
//            authFactory.linkConcurAccount(s.code).success(function(){
//                $location.path('/concur-connected-on-signup');
//            }).error(function(data,status){
//                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
//                    $scope.viewLoading = false;
//                    //TODO: need an option to retry..linking
//            });
//        }
//        else
//            console.log("Authorization code not present in Query params");
//    }

	function createAccount(){
        $scope.viewLoading = true;
        if(!$rootScope.isCaptive && !DISABLE_CAPTCHA)
        {
            authFactory.signUpWithCaptcha($scope.emailId, $scope.password,$scope.selectCountry,grecaptcha.getResponse($scope.widgetId),$scope.referralCode)
			.success(function (data) {
			        sessionStorage.setItem("newAccount","yes");
                    var signUpType = ($scope.referralCode)?'referral':'nil';
                    $analytics.eventTrack('sign up', {  category: "conversion", label:signUpType });
                    $rootScope.userEmailId = $scope.emailId;
                    $rootScope.userPassword = $scope.password;
                    var options = $scope.referralCode?"referral":"auto";
                    ($scope.linkToConur)?$location.path( "/login-link-concur/"+options ):$location.path("/login/"+options);
				})
			.error(function (data, status) {
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
                grecaptcha.reset($scope.widgetId);
                $(document).scrollTop(0);
				});
        }
        else
        {
            authFactory.signIn($scope.emailId, $scope.password,$scope.selectCountry,$scope.referralCode)
                .success(function (data) {
                    sessionStorage.setItem("newAccount","yes");
                    var signUpType = ($scope.referralCode)?'referral':'nil';
                    $analytics.eventTrack('sign up', {  category: "conversion", label:signUpType });
                    $rootScope.userEmailId = $scope.emailId;
                    $rootScope.userPassword = $scope.password;
                    var options = $scope.referralCode?"referral":"auto";
                    ($scope.linkToConur)?$location.path( "/login-link-concur/"+options ):$location.path("/login/"+options);
                })
                .error(function (data, status) {
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                    $scope.viewLoading = false;
                    $(document).scrollTop(0);
                });
        }
	}

    function updateTerms(){

        var loc = "/";
        if($rootScope.isCaptive)
        {
            loc = "";
        }

        var termsText = Localize.translate('I have read and accepted GigSky\'s <a class="blueText" href="'+loc+'terms-and-conditions.html'+$rootScope.getSessionInfo()+'">Terms of Service</a> and ' +
            '<a class="blueText" href="'+loc+'privacy-policy.html'+ $rootScope.getSessionInfo()+'">Privacy Policy</a>');


        $("#termsLabel").html(termsText);
    }
    updateTerms();

}]);

angular.module('gigSkyWebApp').controller ('ConcurLinkedConfController',['$analytics','$scope','$rootScope', '$location','authFactory','commonInfoFactory','$modal','$routeParams',function ($analytics, $scope,$rootScope, $location,authFactory,commonInfoFactory,$modal,$routeParams){

    var p = $location.path();
    if(p == '/concur-connected')
    {
        $scope.title = Localize.translate('LINK YOUR CONCUR ACCOUNT');
        $scope.p1= Localize.translate('Your GigSky and Concur accounts are now connected!');
    }
    else if(p == '/concur-connected-on-signup')
    {
        $scope.title = Localize.translate('ACCOUNT CREATED AND LINKED');
        $scope.p1= Localize.translate('You still need to add a GigSky SIM card to your GigSky account before you can start tracking data plan purchases in your Concur account.');
        $scope.accCreated = true;
    }

    $scope.addSIMCard = function(){
        var token = sessionStorage.getItem("temptoken");
        sessionStorage.setItem("token",token);
        $location.path( "/login/validated" );
    };
}]);

    angular.module('gigSkyWebApp').controller ('EmailConfirmationController',['$scope','$rootScope','$location','authFactory','$modal','SimManagerFactory','commonInfoFactory',function ($scope,$rootScope,$location,authFactory,$modal,simManagerFactory,commonInfoFactory){
        $scope.showConfirmMsgOnLogin = false;
        $scope.viewLoading = false;
        $scope.emailConfirmMsg = Localize.translate('A confirmation email has been sent to %{val}. You have 30 days to follow the instructions in that email to confirm your ' +
            'account. Note: An unconfirmed account with or without an activated SIM card will be deleted after 30 days, per GigSky\'s Terms of Service.',{val : $rootScope.welcomeName});

        if ($rootScope.fromSignup) {
            $scope.showConfirmMsgOnLogin = false;
        } else if (!$rootScope.fromSignup) {
            $scope.showConfirmMsgOnLogin = true;
        }

        if(isSim2ReplaceScreenShown()){
            $rootScope.showOnlyLogout = true;
        }


        function isSim2ReplaceScreenShown(){
            if ($rootScope.enableSim1ToSim2Transition) {
                var replacementBeginDate = getDateObj(sessionStorage.getItem("replacementBeginDate"));
                var sim1List = JSON.parse(sessionStorage.getItem("sim1ListToReplace")) || 0;
                var today = new Date();

                if((today >= replacementBeginDate) && sim1List && sim1List.length > 0){
                    return true
                }
            }
            return false
        }

        $scope.continue = function () {
            $location.path("/dashboard");
        };

        $scope.sendActivationLinkEmail = function () {
            $scope.viewLoading = true;
            $scope.formSubmitErrorStr = null;

            authFactory.sendActivationLink($rootScope.welcomeName)
                .success(function (response) {
                    $scope.viewLoading = false;
                    $scope.skip();
                    new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("An activation link has been sent to your email account."),Localize.translate("OK")).open('sm');
                })
                .error(function (data, status) {
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                    $scope.viewLoading = false;

                });
        };

        $scope.skip = function () {
            if(isSim2ReplaceScreenShown()){
                $location.path("/simReplace");
            } else {
                $location.path("/dashboard");
            }

        }

    }]);

angular.module('gigSkyWebApp').controller ('LoginController',['$analytics','$scope','$rootScope', '$location','authFactory','commonInfoFactory','$modal','$routeParams','SimManagerFactory','$q','referralService',function ($analytics, $scope,$rootScope, $location,authFactory,commonInfoFactory,$modal,$routeParams,simManagerFactory,$q,referralService){
    $scope.emailId="";
    $scope.password="";
    $scope.viewLoading = false;
    $scope.formSubmitErrorStr = null;
    $scope.minLengthPwdError = Localize.translate('Your password must have a minimum of 4 characters.');
    sessionStorage.removeItem("isReferralEnabled");
    var options = $routeParams.options;

    var search = $location.search();
    var eidPresent = (!search.eid && !authFactory.getDeviceId())?0:1;
    var locPresent = (!search.loc && !authFactory.getCurrentLocation())?0:1;
    if(!options && (!eidPresent || !locPresent))
    {
        $location.path("/moerror");
        $location.search({eidPresent:eidPresent,locPresent:locPresent});
        return;
    }
    saveEIDDetails();
    function saveEIDDetails(){
        if(search.eid) {
            authFactory.clearEIDDetails();
            var eid = search.eid;
            var loc = search.loc;
            var moTrId = search.moTransactionId;

            authFactory.setCurrentLocation(loc);
            authFactory.setDeviceId(eid);
            if (search.iccid)
                authFactory.setICCIDList(search.iccid);
            if (moTrId)
                authFactory.setMOTransactionId(moTrId);
        }
    }

    $scope.sendActivationLink= function (){
        $scope.viewLoading = false;
        $scope.emailId = !$scope.emailId? authFactory.getUserEmailId():$scope.emailId;
        $scope.formSubmitErrorStr = null;
        $scope.formSubmitResponseMsg = null;
        $scope.viewLoading = true;
        authFactory.sendActivationLink($scope.emailId)
            .success(function (response) {
                //$scope.formSubmitResponseMsg=$filter('translate')('lt_activation_link_send');
                $scope.viewLoading = false;
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("An account confirmation email has been sent to you. Use the link in the email to confirm your account."),Localize.translate("OK")).open('sm');
            })
            .error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;

            });
    };

    $scope.title = 'Log in';

    if($rootScope.loginErrOnEdit === true){

        new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('Account is not confirmed'),Localize.translate('Resend confirmation link?'),Localize.translate("OK"),Localize.translate("Cancel"),function(){
            $scope.sendActivationLink();
        }).open('sm');
        $rootScope.loginErrOnEdit = false;
        return;
    }

    if(options)
    {
        if((options == 'auto' || options == 'referral') && $rootScope.userEmailId && $rootScope.userPassword){
            $scope.emailId= $rootScope.userEmailId;
            $scope.password = $rootScope.userPassword;
            $rootScope.userEmailId = $rootScope.userPassword = null;
            loginInternal();
        }else if(options == 'forceChangePassword'){
            onLoginCallback();
        }
    }
    else
    {
        var p = $location.path();
        if(p.indexOf('login-link-concur')!=-1)
            $scope.linkingOnLogin = true;
    }

    $scope.logIn= function (login_form){
        $rootScope.fromSignup = false;
        authFactory.clearSession();
        $scope.formSubmitErrorStr = null;
        $scope.formErrorMsg = null;
        if(!$scope.isFormValid(login_form)){
            return;
        }
        else{
            loginInternal();
        }
    };

    $scope.onSignUpClick = function(){
        $location.path('/signUp-concur');
    }

    function loginInternal(){
        $scope.formSubmitErrorStr = null;
        $scope.simInitError = null;
        $scope.viewLoading  = true;
        var isReferralFlow = (options == 'referral');
        authFactory.login($scope.emailId, $scope.password,isReferralFlow)
            .success(function (response) {
                response.gsCredit = (response.gsCredit)?commonInfoFactory.stripDecimal(response.gsCredit):response.gsCredit;
                authFactory.setUserEmailId($scope.emailId);
                onLoginCallback(response.loginRes, response.refStatus, response.gsCredit);
            })
            .error(function (data,status) {
                var loginType = (options == 'auto') ? 'Auto login' : 'Explicit login';
                $analytics.eventTrack('login failed', { category: 'behavior' , label : 'Explicit login'});
                $scope.password="";
                $scope.viewLoading = false;
                $(document).scrollTop(0);
                if(data && data.errorInt && data.errorInt == 2005)
                {
                    authFactory.setUserEmailId($scope.emailId);
                    new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('Account is not confirmed'),Localize.translate('Resend confirmation link?'),Localize.translate("OK"),Localize.translate("Cancel"),function(){
                        $scope.sendActivationLink();
                    }).open('sm');
                    return;
                }
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);


            });
    }

    if(options && options == 'validated')
        onLoginCallback();

    function onLoginCallback(loginRes,refStatus,gsCredit) {

        $analytics.eventTrack('login', {
            category: 'behavior',
            label: 'Explicit login'
        });
        saveEIDDetails();

        $scope.formSubmitErrorStr = null;
        $scope.viewLoading = true;
        var eid = authFactory.getDeviceId();
        simManagerFactory.getSIMAssociationDetails(eid).success(function (result) {
            if (result.status == "ASSOCIATED_OTHER_ACCOUNT") {
                $scope.simInitError = Localize.translate("The eSIM installed on this device is associated with a different account. To purchase service, please login to the right GigSky account.");
                $scope.viewLoading = false;
                return;
            }
            else if (result.status == "NOT_ASSOCIATED") {
                simManagerFactory.addGSMASIM(eid).success(function(){continueLoginFlow(loginRes,refStatus,gsCredit)}).error(function (data, status) {
                    $scope.simInitError = commonInfoFactory.getLocalizedErrorMsg(data,status);
                    sessionStorage.setItem("NewSIMAdded","yes");
                    $scope.viewLoading = false;
                    return;
                });
            }
            else if (result.status == "ASSOCIATED_CURRENT_ACCOUNT") {

                simManagerFactory.syncAndFetchSIMDetails(eid).success(function(){continueLoginFlow(loginRes,refStatus,gsCredit)}).error(function(data,status){
                    $scope.simInitError = commonInfoFactory.getLocalizedErrorMsg(data,status);
                    $scope.viewLoading = false;
                    return;
                });
            }
        }).error(function (data, status) {
            $scope.simInitError = commonInfoFactory.getLocalizedErrorMsg(data,status);
            $scope.viewLoading = false;
            return;
        });
    }

    function continueLoginFlow(loginRes,refStatus,gsCredit) {

        $scope.viewLoading = false;
        if (loginRes && loginRes.forcePasswordChange) {
            $rootScope.forcePasswordChange = true;

            var token = sessionStorage.getItem("token");
            sessionStorage.removeItem("token");
            sessionStorage.setItem("temptoken", token);
            var loginOptions = $location.path();

            $location.path('/forceChangePassword' + loginOptions);
            return;
        }

        if (loginRes && refStatus) {
            var referralMsg = '';

            if (refStatus == 'SIGNUP_WITH_INVALID_REFERRAL') {
                $analytics.eventTrack('invalid referral', {category: 'behavior'});
                referralMsg = Localize.translate('Sorry. The promo code you entered is not valid.');
            }

            referralMsg = ((refStatus == 'SIGNUP_WITH_REFERRAL' && gsCredit) ? Localize.translate('You have %{val} of GigSky Credit for your next data plan purchase.', {val: gsCredit}) :
                (refStatus == 'CREDIT_ADD_IN_PROCESS') ? Localize.translate('Your sign up credit will be applied within the next 24 hours.') : referralMsg);

            new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, referralMsg, Localize.translate("OK")).open('sm');

        }


        var p = $location.path();
        if (p.indexOf('login-link-concur') != -1) {
            var s = $location.search();
            var linkConcur = function linkConcur() {
                $scope.viewLoading = true;
                authFactory.linkConcurAccount(s.code).success(function () {
                    delete s.code;
                    var token = sessionStorage.getItem("token");
                    sessionStorage.removeItem("token");
                    if (options && options == 'auto') {
                        sessionStorage.setItem("temptoken", token);
                        $location.path('/concur-connected-on-signup');
                    }
                    else {
                        $location.path('/concur-connected');
                    }
                    var userCountryCode = sessionStorage.getItem("userCountryCode");
                    if (!userCountryCode)
                        userCountryCode = "N/A";
                    $analytics.eventTrack('concur link', {category: 'behavior', label: userCountryCode});
                    $location.search(s);

                }).error(function (data, status) {

                    $scope.viewLoading = false;

                    if (data && data.errorInt == 7202) {

                        new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, Localize.translate("A Concur Account is currently linked. Tap Replace to link a new Concur Account."), Localize.translate("Replace"), Localize.translate("Cancel"), function () {
                            $scope.viewLoading = true;
                            authFactory.unLinkConcurAccount().success(function () {
                                linkConcur();
                            }).error(function (data, status) {
                                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                                $scope.viewLoading = false;
                            });
                        }, function (data, status) {
                            $scope.password = '';
                            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                            $scope.viewLoading = false;
                        }).open('sm');
                    }
                    else {
                        $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                    }
                });
            };

            linkConcur();
        }
        else {
            $scope.viewLoading = true;

            $q.all([authFactory.getAccount(), referralService.getReferralServiceStatus()]).then(function (response) {
                var referralEnabled = response[1].data && response[1].data.referralEnable;
                $rootScope.isReferralEnabled = referralEnabled;
                onAccountGetSuccess(response[0].data);
            }, function (error) {
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
                $scope.viewLoading = false;
            });

        }

        function onAccountGetSuccess(userInfo) {
            sessionStorage.setItem("userCountryCode", userInfo.country);
            sessionStorage.setItem("userCurrency", userInfo.preferredCurrency);
            sessionStorage.setItem("userEmail", userInfo.emailId);

            var userType = (userInfo.emailId.indexOf("gigsky.com") != -1) ? 'internal' : 'external';
            $analytics.setUserProperties({dimension5: userType});
            $analytics.setUserProperties({dimension7: userInfo.country});

            $rootScope.welcomeName = userInfo.emailId;
            $rootScope.welcomeNameShort = userInfo.emailId;
            if ($rootScope.welcomeNameShort != null && $rootScope.welcomeNameShort.length > 20) {
                $rootScope.welcomeNameShort = $rootScope.welcomeNameShort.substring(0, 20) + "...";
            }
            $rootScope.accountActivationStatus = userInfo.accountActivationStatus;
            $scope.title = "";
            $rootScope.linkingToConcur = false; //Reset linking to Concur, if app is going to dashboard part.
            var alertShown = sessionStorage.getItem('activationStatusAlert');

            if(userInfo.tncAndPPAcceptanceStatus === 'PENDING'){
                $location.path( "/acceptTerms" );
            }else  if (!alertShown && $rootScope.accountActivationStatus == 'ACTIVE_CONF_PENDING') {
                sessionStorage.setItem('activationStatusAlert', 'shown');
                $location.path("/signUpConfirm");
            } else {
                $location.path("/dashboard");
            }
            if (options && options == 'forceChangePassword') {
                $location.replace();
            }

        }
    }

}]);


angular.module('gigSkyWebApp').controller ('LinkedAccountsController',['$scope','$rootScope', '$location','authFactory','commonInfoFactory','$modal','$analytics',function ($scope,$rootScope, $location,authFactory,commonInfoFactory,$modal,$analytics){

    $scope.$on('onPageRefresh',function(){
        $scope.linkErrorStr = null;
        if($scope.refreshRequired)
        {
            onPageLoad();
        }
    });

    $scope.linkToFB = function()
    {
        $scope.formSubmitErrorStr = null;
        $scope.linkErrorStr = null;
        $scope.viewLoading = true;
        authFactory.linkToFBAccount().then(function(response){
            $scope.viewLoading = false;
            updateAcLinkedSection(fbModel,$scope.unlinkFromFB);
        },function(data,status){
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            $scope.viewLoading = false;
            $scope.refreshRequired = true;
        });
    };

    $scope.unlinkFromFB = function(){

        var unlinkCB = function() {
            $scope.formSubmitErrorStr = null;
            $scope.linkErrorStr = null;
            $scope.viewLoading = true;
            authFactory.unlinkFromFBAccount().success(function (response) {
                $scope.viewLoading = false;
                updateAcNotLinkedSection(fbModel, Localize.translate('Link to Facebook to automatically log in to your GigSky account with your Facebook credentials.'), $scope.linkToFB);
            }).error(function (data, status) {
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                $scope.viewLoading = false;
                $scope.refreshRequired = true;
            });
        };

        new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Do you want to unlink Facebook?'),Localize.translate('OK'),Localize.translate('Cancel'),unlinkCB).open('sm');

    };

    $scope.linkToConcur = function(){
        $scope.linkErrorStr = null;
        var absUrl = $location.absUrl();
        var host = absUrl.substr(0,absUrl.indexOf("#"));
        var redirecturl = host+'linktoconcur.php';
        if(host.indexOf("index.html")!=-1)
        {
             redirecturl = host.replace("index.html","linktoconcur.php");
        }
        window.location =CONCUR_URL+redirecturl;
    };

    $scope.unlinkFromConcur = function(){
        var unlinkCB = function() {
            $scope.linkErrorStr = null;
            $scope.formSubmitErrorStr = null;
            $scope.viewLoading = true;
            authFactory.unLinkConcurAccount().success(function (response) {
                $scope.viewLoading = false;
                var userCountryCode = sessionStorage.getItem("userCountryCode");
                if(!userCountryCode)
                    userCountryCode = "N/A";
                $analytics.eventTrack('concur unlink', {  category: 'behavior',label :userCountryCode});

                updateAcNotLinkedSection(concurModel, Localize.translate('Link to Concur to automatically send GigSky data purchase receipts to your Concur account for expense reporting.'), $scope.linkToConcur);
            }).error(function (data, status) {
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                $scope.viewLoading = false;
                $scope.refreshRequired = true;
            });
        };

        new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Do you want to unlink Concur?'),Localize.translate('OK'),Localize.translate('Cancel'),unlinkCB).open('sm');
    };

    var fbModel = {imgSrc:'styles/images/common/fb-logo2.jpg',name:'Facebook',actionMsg:"EQWRqr"};
    var concurModel = {imgSrc:'styles/images/common/concur-logo.png',name:'Concur'};
    updateAcNotLinkedSection(fbModel,Localize.translate('Link to Facebook to automatically log in to your GigSky account with your Facebook credentials.'),$scope.linkToFB);
    updateAcNotLinkedSection(concurModel,Localize.translate('Link to Concur to automatically send GigSky data purchase receipts to your Concur account for expense reporting.'),$scope.linkToConcur);

    if($rootScope.enableConcur)
    {
        $scope.linkedAccounts = [concurModel];
    }
    else
    {
        $scope.linkedAccounts = [];
    }

    $scope.refreshRequired = false;

    function updateAcLinkedSection(accObj,actionCbk){
        accObj.msg = Localize.translate('Your GigSky account is linked to %{val}.',{val : accObj.name});
        accObj.linked = true;
        accObj.actionMsg = Localize.translate('Unlink %{val}.',{val : accObj.name});
        accObj.action = actionCbk;
    }

    function updateAcNotLinkedSection(accObj,msg,actionCbk){

        accObj.msg = msg;
        accObj.linked = false;
        accObj.actionMsg = Localize.translate('Link %{val}.',{val : accObj.name});
        accObj.action = actionCbk;
    }

    function onPageLoad(){
        $scope.userInfo = null;
        $scope.viewLoading = true;
        $scope.formSubmitErrorStr = null;
        authFactory.getAccount( )
            .success(function (userInfo){
                $scope.viewLoading = false;
                $scope.userInfo = userInfo;
                $scope.acInfoAvailable = true;
                if(userInfo.linkedActList)
                {
                    for(var i=0; i <userInfo.linkedActList.length;i++){

                        var ac = userInfo.linkedActList[i];

                        if(ac.actType=="FACEBOOK")
                        {
                            updateAcLinkedSection(fbModel,$scope.unlinkFromFB);
                        }
                        else if(ac.actType=="CONCUR")
                        {
                            updateAcLinkedSection(concurModel,$scope.unlinkFromConcur);
                        }
                    }
                }
            }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
                $scope.acInfoAvailable = false;
                $scope.refreshRequired = true;
            });
    }

    var s = $location.search();
var samplefunc = function(){
    updateAcLinkedSection(concurModel,$scope.unlinkFromConcur);
    onPageLoad();
    delete s.code;
    $location.search(s);
}
    if(s.code)
    {
        //Link to concur approved.
        $scope.formSubmitErrorStr = null;
        $scope.viewLoading = true;
        authFactory.linkConcurAccount(s.code).success(function(response){
            $scope.viewLoading = false;
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Your GigSky and Concur accounts are now linked. You can now optionally send a copy of the receipt for each data plan purchase to your Concur account.'),Localize.translate('OK'),null,samplefunc).open('sm');
        }).error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
                $scope.refreshRequired = true;
            });
        return;
    }
    else if(s.error && s.error_description)
    {
        var errorDescr = s.error_description;
        //errorDescr = errorDescr.("+", " ");
        errorDescr = errorDescr.replace(/\+/g, ' ');
        $scope.linkErrorStr = errorDescr;
        //$scope.viewLoading = false;
        onPageLoad();
        delete s.error;
        delete s.error_description;
        $location.search(s);

    }
    else
    {
        onPageLoad();
    }

}]);

 
   angular.module('gigSkyWebApp').controller ('editAccountController',
                        ['$scope','$rootScope', '$location','$window','$timeout', 'authFactory','commonInfoFactory','$modal',
                            function ($scope,$rootScope, $location,$window,$timeout, authFactory,commonInfoFactory,$modal){
	   $scope.viewLoading = true;
       $scope.selectCountryStr=Localize.translate('Select country');
       $scope.selectCurrencyStr=Localize.translate('Select currency');

       $scope.navigateBack = function(){
           $location.path("/dashboard");
       };
       //Common control specific apis definied in main.js//main controller.
       $scope.updateCountryList($scope,'formSubmitErrorStr');
       $scope.updateCurrencyList($scope,'formSubmitErrorStr');

	   authFactory.getAccount( )
	   		.success(function (userInfo) {
                $scope.viewLoading = false;
	   			$scope.emailId=userInfo.emailId;
				$scope.firstName=userInfo.firstName?userInfo.firstName:"";
       			$scope.lastName=userInfo.lastName?userInfo.lastName:"";
				$scope.address1=userInfo.address1?userInfo.address1:"";
				$scope.address2=userInfo.address2?userInfo.address2:"";
				$scope.phone=userInfo.phone?userInfo.phone:"";
		        $scope.city=userInfo.city?userInfo.city:"";
		        $scope.selectCountry=userInfo.country;
		        $scope.state=userInfo.state?userInfo.state:"";
		        $scope.zipCode=userInfo.zipCode?userInfo.zipCode:"";
				$scope.preferredCurrency=userInfo.preferredCurrency;
               $scope.currencySubText = Localize.translate('* Your currency is set to %{val}, based on your country of residence.',{val: userInfo.preferredCurrency});
           }).error(function(data,status){
                $scope.viewLoading = false;
	 		});
	  
	
	  $scope.updateAccount= function (){
          $scope.formSubmitResponseMsg = null;
          $scope.formSubmitErrorStr = null;
          if(!$scope.isFormValid($scope.editAcct_form)){
			return;
		  }
		  else{
              $scope.viewLoading = true;
			 authFactory.updateAccount($scope.firstName, $scope.lastName, $scope.selectCountry, $scope.preferredCurrency, $scope.address1, $scope.address2, $scope.zipCode, $scope.phone, $scope.state, $scope.city)
			 .success(function (userInfo) {
                 $(document).scrollTop(0);
				 $rootScope.welcomeName=$scope.emailId;
                 $rootScope.welcomeNameShort = $scope.emailId;
                 if($rootScope.welcomeNameShort != null && $rootScope.welcomeNameShort.length>20 )
                 {
                     $rootScope.welcomeNameShort = $rootScope.welcomeNameShort.substring(0,20) + "...";
                 }
                 $scope.currencySubText = Localize.translate('* Your currency is set to %{val}, based on your country of residence.',{val: userInfo.preferredCurrency});
                 $scope.viewLoading = false;
             })
			 .error(function(data,status){
                     $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                     $(document).scrollTop(0);
                     $scope.viewLoading = false;
		 	});
		 }
	 };
  }]);

angular.module('gigSkyWebApp').controller ('editEmailController',['$scope', '$location','authFactory','commonInfoFactory','$modal',function ($scope, $location,authFactory,commonInfoFactory,$modal){
    $scope.viewLoading = false;
    $scope.minLengthPwdError = Localize.translate('Your password must have a minimum of 4 characters.');
    $scope.changeEmail= function (form){
        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr =null;
        if(!$scope.isFormValid(form))
        {
            return;
        }
        $scope.viewLoading = true;
        authFactory.changeEmail($scope.emailId,$scope.password)
            .success(function () {
                $scope.emailId = "";
                $scope.password = "";
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("A confirmation email has been sent to your account."),Localize.translate("OK")).open('sm');
                $scope.viewLoading = false;
            })
            .error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });


    };
}]);

angular.module('gigSkyWebApp').controller ('changePwdController',['$scope', '$location','authFactory','commonInfoFactory','$modal','$rootScope','$routeParams','$analytics',function ($scope, $location,authFactory,commonInfoFactory,$modal,$rootScope,$routeParams,$analytics){

    if($location.path().indexOf('forceChangePassword')!= -1){
        $rootScope.showOnlyHeader = false;
        $rootScope.forcePasswordChange = true;
        $scope.title = Localize.translate('Please update the password for this account');
    }
    $scope.viewLoading = false;

    $scope.minimumLengthPwdError = Localize.translate('Your password must have a minimum of 8 characters.');
    $scope.minLengthPwdError = Localize.translate('Your password must have a minimum of 4 characters.');

    $('form[name="changePwdForm"] input').on('keyup change',function(){
        $scope.pwdsNotMatching = false;
    });

    $scope.resetPassword = function(){
        $scope.viewLoading = true;
        var emailID = authFactory.getUserEmailId();
        authFactory.forgotPassword(emailID)
            .success(function () {
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Instructions to reset your password have been sent to your registered email account."),Localize.translate("OK"),null,function(){
                    $location.path('/login');
                }).open('sm');
                $scope.viewLoading = false;
            })
            .error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;
            });
    }

    $scope.confirmPwdReset = function()
    {
        new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Do you want to reset your password?"),Localize.translate("OK"),Localize.translate("Cancel"),function(){
        $scope.resetPassword();
        }).open('sm');
    }

    $scope.changePassword= function (form,event){
        if(event && event.which != 13){
            return;
        }

        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr =null;
        var options = $routeParams.options;

        if(!$scope.isFormValid(form))
        {
            return;
        }
        $scope.viewLoading = true;
        authFactory.changePassword($scope.oldPassword,$scope.newPassword)
            .success(function () {
                $scope.oldPassword="";
                $scope.newPassword="";
                $scope.confirmPassword="";
                $(document).scrollTop(0);
                $scope.viewLoading = false;
                var changePwdType = 'Normal';
                if($rootScope.forcePasswordChange){
                    changePwdType = 'Forced';
                    $rootScope.forcePasswordChange = false;
                    $location.path(options+'/forceChangePassword');
                    $location.replace();
                }
                $analytics.eventTrack('change password', { category: 'behavior' , label : changePwdType});
            })
            .error(function(data,status){
                $scope.oldPassword="";
                $scope.newPassword="";
                $scope.confirmPassword="";
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $(document).scrollTop(0);
                $scope.viewLoading = false;
            });
    };

    $scope.cancel = function(){
        if($rootScope.forcePasswordChange){
            var options = $routeParams.options;
            $location.path(options);
        }else{
            $location.path('/dashboard');
        }
    };


}]);


angular.module('gigSkyWebApp').controller ('confirmationCtrl',['$rootScope','$scope', '$location','authFactory','commonInfoFactory','$routeParams',function ($rootScope,$scope, $location,authFactory,commonInfoFactory,$routeParams){
    $scope.viewLoading = true;

    $scope.minimumLengthPwdError = Localize.translate('Your password must have a minimum of 8 characters.');

    var token = $location.search().data;  //$routeParams.token;
    var path = $location.path();
    $scope.showPwdRestForm = false;
    var ACC_ACTIVATION = 1;
    var ACC_UNBLOCK = 2;
    var PWD_RESET_CONFIRM = 3;
    var ACC_EMAIL_CONFIRM = 4;
    var curOperation = 0;
    if(path.indexOf('/confirmemail')!=-1)
    {
        $scope.title= 'Email Change Confirmation';
        curOperation = ACC_EMAIL_CONFIRM;
        activateUser(token);
    }
    if(path.indexOf('/activate')!=-1)
    {
        $scope.title= 'Account Activation';
        curOperation = ACC_ACTIVATION;
        $rootScope.linkingToConcur = true; //Disable header links login and signUp
        activateUser(token);
    }
    if(path.indexOf('/reset')!=-1)
    {
        $scope.title= 'Change Password';
        curOperation = PWD_RESET_CONFIRM;
        isTokenValid(token);
    }
    if(path.indexOf('/unblock')!=-1)
    {
        $scope.title= 'Unblock Account';
        curOperation = ACC_UNBLOCK;
        isTokenValid(token);
    }

     function isTokenValid(token){
        $scope.viewLoading = true;
            authFactory.isTokenValid(token,(curOperation==ACC_UNBLOCK)?'CONFIRM_UNBLOCK_ACCOUNT':'CONFIRM_PASSWORD_RESET').success(function(response){
                $scope.showPwdRestForm = true;
                $scope.viewLoading = false;
                $scope.titleSubText = (curOperation == ACC_UNBLOCK)?Localize.translate('To complete unblocking of your account, please set a new password.'):Localize.translate('Set new password for your account.');
        }).error(function(data,status){
                if(data && data.errorInt && data.errorInt == 2001)
                {
                    $("#actionreqmesssage").html(Localize.translate('Link validity has expired. <a href=\"#/forgotPasswd\">Click here</a> to generate link again.'));
                    //$scope.formSubmitResponseMsg = $filter('translate')('lt_link-validity-expired-message');
                }
                else
                {
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                }
                $scope.viewLoading = false;
            });
    };

    $scope.confirmPwdReset= function (event){

        if(event && event.which != 13){
            return;
        }

        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr =null;
        $scope.pwdsNotMatching = false;
        if(!$scope.isFormValid($scope.changePwdForm))
        {
            return;
        }
        $scope.viewLoading = true;
        authFactory.confirmPwdReset(token,$scope.newPassword)
            .success(function () {
                $scope.titleSubText = null;
                $scope.showPwdRestForm = false;
                if(curOperation == PWD_RESET_CONFIRM)
                {
                    $("#actionreqmesssage").html(Localize.translate('Your account password has been changed. Please <a href=\"#/login\">log in</a> with new credentials.'));
                }
                else
                    $("#actionreqmesssage").html(Localize.translate('Your account is unblocked.<br/><span> Please <a href=\"#/login\">log in</a> with new credentials.</span>'));
                $scope.viewLoading = false;$(document).scrollTop(0);
            })
            .error(function(data,status){
                $scope.newPassword="";
                $scope.confirmPassword="";
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;$(document).scrollTop(0);
            });

    };

     function activateUser(token){
        $scope.formSubmitResponseMsg = null;
        $scope.formSubmitErrorStr =null;
        $scope.viewLoading = true;
        authFactory.activateUserOrConfirmEmail(token,(curOperation==ACC_ACTIVATION)?false:true)
            .success(function (response) {
                $(document).scrollTop();
                if(curOperation == ACC_ACTIVATION){
                    //$location.path('/login/validated');
                    $("#actionreqmesssage").html(Localize.translate('Welcome to GigSky. Your account is now active.'));
                    $scope.viewLoading = false;
                }
                else{
                    $scope.viewLoading = false;
                    $("#actionreqmesssage").html(Localize.translate('Your account email ID has been changed. Please <a href=\"#/login\">log in</a> with new email ID.'));
                }
            })
            .error(function(data,status){
                $scope.viewLoading = false;
                $(document).scrollTop();
                if(data && data.errorInt && data.errorInt == 2001)
                {
                    var failmsg = Localize.translate('Email change confirmation link has expired. Please <a href=\"#/login\">log in</a> to your account and submit new change email request.');
                    if(curOperation==ACC_ACTIVATION)
                        failmsg = Localize.translate('Account activation link validity expired. <a href=\"#/sendactlink\">Click here</a> to resend activation email.');
                    $("#actionreqmesssage").html(failmsg);
                }else
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
    };
}]);

angular.module('gigSkyWebApp').controller ('forgotPasswdController',['$scope', '$rootScope','$location','authFactory','commonInfoFactory','$modal','$analytics',function ($scope, $rootScope,$location,authFactory,commonInfoFactory,$modal,$analytics){

    $scope.$on('$destroy',function() {
        if (window["grecaptcha"] && $scope.widgetId)
            grecaptcha.reset($scope.widgetId);
    });

     $scope.widgetId = null;
     $scope.navigateBack = function()
     {
         var path = $location.path();
         if(path.indexOf('forgotpwd')!=-1)
         {
             $location.path('/changePassword');
         }
         else
         {
             if(path.indexOf('concur')!=-1)
             {
                window.history.back();
             }
             else{
                $location.path('/login');
             }
         }
     }
    //////////////////////adfadfa

    $scope.viewLoading = false;
    $rootScope.fromSignup = false;

    window.captchaLoadCallback = function()
    {
        var event = document.createEvent('Event');
        event.initEvent('CaptchaLoad', true, true);
        document.dispatchEvent(event);
    };

    // Listen for the event.
    document.addEventListener('CaptchaLoad', function (e) {
        $scope.$apply(function(){
            $scope.viewLoading = false;
        });
        $scope.widgetId= grecaptcha.render('captcha-div', {
            'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
        });
    }, false);

    if(!$rootScope.isCaptive && !DISABLE_CAPTCHA)
    {
        if(!$rootScope.loadCaptchaScript)
        {
            $scope.viewLoading = true;
            var scriptElement =document.createElement('script');
            var firstScriptElement=document.getElementsByTagName('script')[0];
            scriptElement.async=1;
            if($rootScope.enableLocalization)
            {
                scriptElement.src="//www.google.com/recaptcha/api.js?onload=captchaLoadCallback&render=explicit" + "&hl=" + $rootScope.currentLangCode;
            }
            else
            {
                scriptElement.src="//www.google.com/recaptcha/api.js?onload=captchaLoadCallback&render=explicit";
            }
            firstScriptElement.parentNode.insertBefore(scriptElement,firstScriptElement);
            $rootScope.loadCaptchaScript = true;
        }
        else
        {
            $scope.widgetId= grecaptcha.render('captcha-div', {
                'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
            });
        }
    }

    ////////////////////adf afd

     $scope.viewLoading = false;
	 $scope.forgotPassword= function (){
         $scope.formSubmitErrorStr = null;
         $scope.formSubmitResponseMsg = null;
         $rootScope.fromSignup = true;
         $scope.forgetPasswd_form.captchaErrorStr = null;

         if(!$scope.isFormValid($scope.forgetPasswd_form))
         {
             return;
         }

         if((!$rootScope.isCaptive && !DISABLE_CAPTCHA) && grecaptcha.getResponse($scope.widgetId) == "")
         {
             $scope.forgetPasswd_form.captchaErrorStr = Localize.translate('Please complete CAPTCHA verification.');
             return;
         }

         $scope.viewLoading = true;

         $scope.viewLoading = true;
         if(!$rootScope.isCaptive && !DISABLE_CAPTCHA)
         {
             authFactory.forgotPasswordWithCaptcha($scope.emailId,grecaptcha.getResponse($scope.widgetId))
             .success(function (data) {
                 //$scope.formSubmitResponseMsg=$filter("translate")("lt_passwdForgot-success");
                 $scope.viewLoading = false;
                 $scope.emailId="";
                 grecaptcha.reset($scope.widgetId);
                 $analytics.eventTrack('forgot password', {  category: 'behavior'});
                 new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Instructions to reset your password have been sent to your registered email account."),Localize.translate("OK")).open('sm');
             })
             .error(function(data,status){
                 $scope.viewLoading = false;
                 grecaptcha.reset($scope.widgetId);
                 $scope.emailId="";
                 if(data && data.errorInt == 2000)
                 {
                     new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Instructions to reset your password have been sent to your registered email account."),Localize.translate("OK")).open('sm');
                 }
                 else {
                     $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                 }
                 $(document).scrollTop(0);
             });
         }
         else
         {

             authFactory.forgotPassword($scope.emailId)
                 .success(function () {
                     //$scope.formSubmitResponseMsg=$filter("translate")("lt_passwdForgot-success");
                     $scope.viewLoading = false;
                     $scope.emailId="";
                     $analytics.eventTrack('forgot password', {  category: 'behavior'});
                     new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Instructions to reset your password have been sent to your registered email account."),Localize.translate("OK")).open('sm');
                 })
                 .error(function(data,status){
                     $scope.viewLoading = false;
                     $scope.emailId="";
                     if(data && data.errorInt == 2000)
                     {
                         new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Instructions to reset your password have been sent to your registered email account."),Localize.translate("OK")).open('sm');
                     }
                     else {
                         $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
                     }
                     $(document).scrollTop(0);
                 });
         }

	 };



 }]);

angular.module('gigSkyWebApp').controller ('ActivationLinkCtrl',['$scope', '$location','authFactory','commonInfoFactory','$modal',function ($scope, $location,authFactory,commonInfoFactory,$modal){
    $scope.viewLoading = false;
    $scope.sendActivationLink= function (){
        $scope.formSubmitErrorStr = null;
        $scope.formSubmitResponseMsg = null;
        if(!$scope.isFormValid($scope.sendActLinkForm))
        {
            return;
        }
        $scope.viewLoading = true;
        authFactory.sendActivationLink($scope.emailId)
            .success(function (response) {
               //$scope.formSubmitResponseMsg=$filter('translate')('lt_activation_link_send');
                $scope.viewLoading = false;
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("An activation link has been sent to your email account."),Localize.translate("OK")).open('sm');
            })
            .error(function(data,status){
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                $scope.viewLoading = false;

            });
    };

}]);

angular.module('gigSkyWebApp').controller ('MODirectErrorCtrl',['$scope', '$location','authFactory',function ($scope, $location,authFactory){
    var s = $location.search();
    $scope.erroMessage = null;

    var eidEerror = (s.eidError == "1");
    if(eidEerror){
        // eUICC incompatible error
        $scope.erroMessage = Localize.translate("GigSky data plans for this device are temporarily unavailable for purchase. Please go to support.gigsky.com for more information.");
        return;
    }

    var noEID = (!s.eidPresent || s.eidPresent == "0")
    var noLoc = (!s.locPresent || s.locPresent == '0')

    if(noEID)
    {
        $scope.erroMessage  = Localize.translate("Your GigSky SIM is not supported with Mobile plans experience. If your GigSky SIM was obtained from a retail store (online or distributor), visit app.gigsky.com to get started. If your GigSky SIM was issued as part of a corporate plan, please contact your IT Administrator for further instructions.");
    }
    else if(noLoc){
        $scope.erroMessage  = Localize.translate("Device information is not available to proceed further.");
    }

    var notification = {success:false,transactionId:authFactory.getMOTransactionId(),
        purchaseResult:{userAccount:'None',purchaseInstrument:'None', line:'None',
            moDirectStatus:'ClientError',planName:'' }};

    authFactory.sendMONotification(notification);


}]);


angular.module('gigSkyWebApp').controller ('referFriendController',['$scope', '$location','authFactory','commonInfoFactory','$modal','referralService','$q','$timeout','$analytics',function ($scope, $location,authFactory,commonInfoFactory,$modal,referralService,$q,$timeout,$analytics){

    /**
     * @func getReferralDetails
     * @desc gets referral details from referral backend
     */
    function getReferralDetails(){

        var countryCode = sessionStorage.getItem("userCountryCode");
        var customerId = sessionStorage.getItem("customerId");
        $scope.viewLoading = true;
        //$scope.showToolTip = false;

        $q.all([referralService.getReferralAccountDetails(customerId) ,referralService.getActiveSchemeByCountryCode(countryCode)]).then(function (response) {
            $scope.viewLoading = false;
            formatReferralDetails(response[0].data, response[1].data);
        }, function (error) {
            $scope.viewLoading = false;
            if(error.data && ( error.data.errorInt == 12006 || error.data.errorInt == 12600))
            {
                $scope.formSubmitErrorStr = Localize.translate("Unable to complete request due to an internal error. Please try again later.")
            }
            else
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
        });

    }

    /**
     * @func formatReferralDetails
     * @desc formats referral details
     * @param accDetails
     * @param schemeDetails
     */
    function formatReferralDetails(accDetails,schemeDetails){

        $scope.referralCode = accDetails.referralCode ;
        $scope.referralUrl = accDetails.referralUrl ;
        $scope.advoacateEmailId = accDetails.emailId;

        $scope.advocateCreditExpiryPeriod = schemeDetails.advocateCreditExpiryPeriod;
        var creditScheme = schemeDetails.creditScheme[0];

        $scope.advocateAmount = commonInfoFactory.formatPrice(creditScheme.advocateAmount,creditScheme.currency);
        $scope.advocateAmount = commonInfoFactory.stripDecimal($scope.advocateAmount);
        $scope.newCustomerAmount = commonInfoFactory.formatPrice(creditScheme.newCustomerAmount,creditScheme.currency);
        $scope.newCustomerAmount = commonInfoFactory.stripDecimal($scope.newCustomerAmount);
        $scope.referralMsg = Localize.translate("Earn GigSky Credit by referring your friends! Get %{advamt} of GigSky Credit for each friend that signs up and uses GigSky's data service.",{advamt:$scope.advocateAmount});
        $scope.referralInviteMsg= Localize.translate("Congratulations! Free Mobile Data! Get %{refamt} towards the purchase of your first GigSky Mobile Data plan. Use code %{refcode} when signing up for a GigSky account to receive your credit. Find out more at %{reflink}",{refamt:$scope.newCustomerAmount,refcode:$scope.referralCode,reflink:'https://goo.gl/lgv4Pa'});
        $scope.toolTipMsg = Localize.translate("Copied to clipboard");
        $scope.showToolTip = false;
        $scope.onSuccess = function(e) {
            $analytics.eventTrack('referral share', { category: 'behavior'});
            $scope.showToolTip = true;
            $scope.toolTipMsg = Localize.translate("Copied to clipboard");
            $timeout(function(){
                $scope.showToolTip = false;
            },1000);
            e.clearSelection();
        };

        $scope.onError = function(e) {
            $scope.showToolTip = true;
            $scope.toolTipMsg = Localize.translate("Unable to copy to clipboard");
            $timeout(function(){
                $scope.showToolTip = false;
            },1000);
        };


        $scope.showMoreDetails = function () {
            new ReferralInfoPopUp($scope,$modal,'ReferralHowItWorks.html',$scope.newCustomerAmount,$scope.advocateAmount).open('sm');
        };

    }

    getReferralDetails();


}]);

angular.module('gigSkyWebApp').controller ('AcceptTermsController',['$scope','$rootScope','$location','authFactory','$modal','SimManagerFactory','commonInfoFactory',function ($scope,$rootScope,$location,authFactory,$modal,simManagerFactory,commonInfoFactory){
    $scope.viewLoading = false;

    $scope.acceptTerms = function (accept_terms_form) {
        $scope.formSubmitErrorStr = null;
        if(!$scope.isFormValid(accept_terms_form))
        {
            return;
        }
        $scope.viewLoading = true;

        authFactory.acceptTerms().success(function () {
            $scope.viewLoading = false;
            $scope.navigate();

        }).error(function (data, status) {
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data, status);
        });

    };


    $scope.navigate = function () {
        var alertShown = sessionStorage.getItem('activationStatusAlert');

        if(!alertShown && $rootScope.accountActivationStatus === 'ACTIVE_CONF_PENDING') {
            sessionStorage.setItem('activationStatusAlert', 'shown');
            $location.path( "/signUpConfirm" );
        }else{
            $location.path("/dashboard");
        }
    };

    function updateTerms(){

        var loc = "/";
        if($rootScope.isCaptive)
        {
            loc = "";
        }

        var termsText = Localize.translate('I have read and accepted GigSky\'s <a class="blueText" href="'+loc+'terms-and-conditions.html'+$rootScope.getSessionInfo()+'">Terms of Service</a> and ' +
            '<a class="blueText" href="'+loc+'privacy-policy.html'+ $rootScope.getSessionInfo()+'">Privacy Policy</a>');


        $("#termsLabel").html(termsText);
    }
    updateTerms();
}]);

angular.module('gigSkyWebApp').controller ('MarketingController',['$scope', '$location',function ( $scope, $location){

    $scope.navigate = function (path) {

        if(path ==='signUp' || path === 'login'){
            $location.path('/'+path);
        }
    }


}]);

angular.module('gigSkyWebApp').controller ('CancelServiceController',['$scope', '$location', 'authFactory', 'SimManagerFactory', '$analytics','commonInfoFactory',function ( $scope, $location, authFactory, simMgr, $analytics, commonInfoFactory){

    $scope.userEmailId = authFactory.getUserEmailId();

    $scope.cancelService = function (){

        var deviceId = authFactory.getDeviceId();
        $scope.viewLoading = true;

        simMgr.deleteSIM(deviceId, true).success(function(){
            $scope.viewLoading = false;
            $analytics.eventTrack('Disassociate CSN');
            authFactory.clearSession();
            sessionStorage.setItem('userEmailMap', $scope.userEmailId);
            $location.path('/cancel-service-confirmation');
        }).error(function(data,status){
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
        });

    };

    var emailId = sessionStorage.getItem('userEmailMap');
    $scope.serviceCancelMsg = Localize.translate("If you want to use our service again on this device, just login with your GigSky account %{emailId}.",{emailId:emailId});

    $scope.closeWebSheet = function(){
        sessionStorage.removeItem('userEmailMap');
        $location.path('/login');
    };


}]);


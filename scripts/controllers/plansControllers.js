/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 06/08/14
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */

angular.module('gigSkyWebApp').controller('plansListController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$modal','$analytics','$q',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$modal,$analytics,$q){

    $rootScope.plansLoading = true;
    var activeSIM = simMgrFactory.getActiveSIM();

    $scope.disableBack = true;

    var s = $location.search();
    $scope.si = 3; //Step indicator max steps.
    $scope.currentstep = 2;
    if(s)
    {
        $scope.si = s.si? s.si:3;
    }
    if($scope.si!=3)
        $scope.currentstep = 1;


    $scope.onBack = function()
    {
        $scope.disableBack = true;
    };

    if(s.loc)
    {
        $analytics.eventTrack('plans viewed', {  category: 'behavior', label:s.loc.toUpperCase()});

    }

    if(s && s.sim)
    {
        if($rootScope.purchaseForSIM)
            $scope.selectPlanTitle = $rootScope.purchaseForSIM.description;
        else
        {
            //Fetch sim details..
            $scope.viewLoading = true;
            simMgrFactory.getSIMDetails(s.sim,true).success(function(result){
                $scope.viewLoading = false;
                $rootScope.purchaseForSIM = result;
                $scope.selectPlanTitle = $rootScope.purchaseForSIM.description;
            }).error(function(data,status,headers,config,statusText){
                    $scope.viewLoading = false;
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                });
        }

    }
    else
    if(activeSIM)
    {
        $scope.selectPlanTitle = activeSIM.description;
    }
    else
    {
        $scope.selectPlanTitle = Localize.translate('Select a plan for use in');
    }

}]);

angular.module('gigSkyWebApp').directive('availableNetworks',function(){

    return {
        transclude:true,
        restrict:'E',
        templateUrl: LAZY_LOAD_BASE+'html/views/available-networks.html',
        link:function(scope,element,attr){

        },
        scope:{
            location:'=location'
        },
        controller:'availableNetworksController'
    };
});

angular.module('gigSkyWebApp').controller('availableNetworksController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$modal','$interval','$analytics',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$modal,$interval,$analytics){

    $scope.availableNetworks = {};
    $scope.referFriendMsg = Localize.translate("Refer A Friend");
    $scope.forbiddenCountriesMsg = Localize.translate("Residents are not permitted to buy plans in your location. You may buy our plans in all the other countries we serve. We apologize for any inconvenience this may cause.");
    $scope.goToReferFriend = function () {
        $analytics.eventTrack('refer a friend clicked', { category: 'behavior'});
        $location.path('/referFriend');
    };
    $scope.requestPending = false;

    function getLocationDetails() {
        if($scope.requestPending)return;
        $scope.requestPending = true;
        carrierMgrFactory.getCustomLocationDetails($scope.location).success(function (res) {
            $scope.isForbidden = res.isForbidden;
            $scope.requestPending = false;
            if($scope.isForbidden){
                clearTimer();
            }
            $scope.availableNetworks = res.availableNetworks;
            $rootScope.$broadcast(($scope.availableNetworks && $scope.availableNetworks.length) ? 'onAvailableNetworkLoaded' : 'onNOServicesInCurrentLocation');

        }).error(function (error) {
            $scope.requestPending = false;
            $rootScope.$broadcast('onNOServicesInCurrentLocation');
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
        });
    }

    $scope.confirmSwitchSubscription = function(e,subscription)
    {
        e.stopPropagation();
        if($scope.isSelectedImsiDifferenFromCurrent(subscription)){
            var profileName = subscription.imsiInfoList.list[0].profileName;
            var alertMessage = Localize.translate("To use this plan, please select \"%{profilename}\" from the Settings menu of your device",{profilename: profileName});
            new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, alertMessage, Localize.translate("OK")).open('sm');
        }else{
            var networkGroupName = subscription.networkGroupInfo.networkGroupName;
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate("Do you want to make this \"%{networkgroupname}\" Plan active?",{networkgroupname: networkGroupName}),Localize.translate("OK"),Localize.translate("Cancel"),function(){
                $scope.switchSubscription(subscription);
            }).open('sm');
        }

    }

    /**
     * This function validates whether selected imsi is same or different from current IN-USE subscription
     */
    $scope.isSelectedImsiDifferenFromCurrent = function (selectedSubscription) {
        var inUseSubscriptionImsi, selectedSubscriptionImsi;
        var isDifferentImsi = false;
        for (var i = 0; i < $scope.availableNetworks.length; i++) {
            var network = $scope.availableNetworks[i];
            var subscription = network.subscription;
            if (subscription && subscription.subscriptionStatus == 'IN-USE') {
                if (subscription.imsiInfoList && subscription.imsiInfoList.list && subscription.imsiInfoList.list.length) {
                    var imsiInfo = subscription.imsiInfoList.list[0];
                    inUseSubscriptionImsi = imsiInfo.imsi;
                    break;
                }
            }

        }

        if (selectedSubscription.imsiInfoList && selectedSubscription.imsiInfoList.list && selectedSubscription.imsiInfoList.list.length) {
            selectedSubscriptionImsi = selectedSubscription.imsiInfoList.list[0].imsi;
            if (inUseSubscriptionImsi != selectedSubscriptionImsi) {
                isDifferentImsi = true;
            }
        }

        return isDifferentImsi;

    }

    $scope.switchSubscription = function (subscription) {
        $scope.activatingSubscriptionId = subscription.subscriptionId;
        $scope.requestPending = true;
        carrierMgrFactory.activateSubscription(subscription.subscriptionId).success(function (res) {

            for (var i = 0; i < $scope.availableNetworks.length; i++) {
                var network = $scope.availableNetworks[i];
                var subscriptionObj = network.subscription;

                if(subscriptionObj && subscriptionObj.subscriptionId == $scope.activatingSubscriptionId){
                    subscriptionObj.subscriptionStatus = "IN-USE";
                }else if(subscriptionObj && subscriptionObj.subscriptionStatus == "IN-USE" && subscriptionObj.subscriptionId != $scope.activatingSubscriptionId){
                    subscriptionObj.subscriptionStatus = "ACTIVE";
                }
            }

            var availableNwIds = carrierMgrFactory.formatAvailableNetworkIds($scope.availableNetworks);
            $scope.availableNetworks = carrierMgrFactory.formatAvailableNetworks($scope.availableNetworks, availableNwIds);

            $scope.activatingSubscriptionId = undefined;
            $scope.requestPending = false;
            var networkGroupName = subscription.networkGroupInfo.networkGroupName;
            var successMessage = Localize.translate('"%{networkgroupname}" Plan is active now',{networkgroupname: networkGroupName});
            new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, successMessage, Localize.translate("OK")).open('sm');
        }).error(function (error) {
            $scope.activatingSubscriptionId = undefined;
            $scope.requestPending = false;
            var networkGroupName = subscription.networkGroupInfo.networkGroupName;
            var failureMessage = Localize.translate('Failed to switch to "%{networkgroupname}" plan',{networkgroupname: networkGroupName});
            new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, failureMessage, Localize.translate("OK")).open('sm');
        });
    }


    function clearTimer() {
        // Make sure that the interval is destroyed too
        if(angular.isDefined($scope.timer))
            $interval.cancel($scope.timer);
        $scope.timer = undefined;
    }

    if($scope.timer)
    {
        clearTimer();
    }

    $scope.timer = $interval(function(){
        if($scope.timer)
            getLocationDetails();
    },60000,0,true);

    $scope.$on('$destroy', function() {
        clearTimer();
    });

    $scope.$watch('location',function(n,o){
        if(n) {
            getLocationDetails();
        }
    });

    $scope.navigateToNetwork = function (nwId, subscription) {
        if (subscription && subscription.subscriptionStatus == 'ACTIVE' && $scope.activatingPlanId != subscription.subscriptionId){
            return;
        }
        if(nwId){
            $location.path('/network/'+nwId);
        }
    }

}]);

angular.module('gigSkyWebApp').controller('networkViewController',['$scope','SimManagerFactory','carrierManagerFactory','$routeParams','commonInfoFactory','$location',function ($scope,simMgrFactory,carrierMgrFactory,commonInfoFactory,$routeParams,$location){

    $scope.networkId = $routeParams.networkId;

    var activeSIM = simMgrFactory.getActiveSIM();
    $scope.title = (activeSIM) ? activeSIM.description : '';

    $scope.onBackClick = function () {
        $location.path('/dashboard');
    }

}]);

angular.module('gigSkyWebApp').directive('networkPlans',function(){

    return {
        transclude:true,
        restrict:'E',
        templateUrl: LAZY_LOAD_BASE+'html/views/network.directive.html',
        link:function(scope,element,attr){

        },
        scope:{
            networkId:'=networkId'
        },
        controller:'networkController'
    };
});

angular.module('gigSkyWebApp').controller('networkController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$modal','paymentFactory','$analytics','$q','$routeParams','$interval',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$modal,paymentFactory,$analytics,$q,$routeParams, $interval){

    $scope.networkId =  $scope.networkId ? $scope.networkId : $routeParams.networkId ;
    $scope.viewLoading = false;
    $scope.nwlogosLoading = false;
    $scope.plansNotSupported = false;
    $scope.plansLoading = false;
    $scope.networkInfo = {};
    $scope.countries=[];
    $scope.carriers=[];
    $scope.countryPlan = false;
    $scope.countryPlanSubText=null;
    $scope.allPlansDurationMsg=null;
    $scope.currentCountryName=null;
    $scope.planGroups=[];
    $scope.isDashBoard = false;
    $scope.referFriendMsg = Localize.translate("Refer A Friend");
    $scope.goToReferFriend = function () {
        $analytics.eventTrack('refer a friend clicked', { category: 'behavior'});
        $location.path('/referFriend');
    };

    $scope.init= function(networkInfo){
        $scope.networkInfo = networkInfo;
    }

    var curPath = $location.path();
    if(curPath.search("dashboard")!=-1)
    {
        $scope.isDashBoard = true;
    }


    if($scope.network && $scope.network.networkGroupInfo)
        $scope.networkInfo = $scope.network.networkGroupInfo;
    else
        $scope.networkInfo = $scope.network;

    $scope.currentCountryName = $scope.country;

    $scope.plans = [];
    $scope.balanceInfo = {};
    $scope.payStatus = {};
    $scope.payMethodInfo = {};
    $scope.plansAvailable = true;
    $scope.requestPending = false;
    function getAdvancedNetworkDetails(showProgressWheel) {
        $scope.plansLoading = showProgressWheel;
        if($scope.requestPending) return;
        $scope.requestPending = true;
        carrierMgrFactory.getAdvancedNetworkDetails($scope.networkId).success(function (res) {
            $scope.plansLoading = false;
            $scope.requestPending = false;
            $scope.networkInfo = res.details ? res.details : {} ;
            $scope.plansNotSupported = $scope.networkInfo.plansNotSupported;
            $scope.subscription = res.subscription ? res.subscription : null ;
            $scope.planGroups = res.plans? res.plans : [] ;
            $scope.buyPlanLinkText = ($scope.subscription && $scope.subscription.subscriptionStatus != 'EXPIRED')?Localize.translate("Top-Up"):Localize.translate("Buy Now");

        }).error(function (error) {
            $scope.plansLoading = false;
            $scope.requestPending = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
        });
    }

    $scope.$watch('networkId',function(n,o){
        if(n) {
            getAdvancedNetworkDetails(true);
        }
    });

    function clearTimer() {
        // Make sure that the interval is destroyed too
        if(angular.isDefined($scope.timer))
            $interval.cancel($scope.timer);
        $scope.timer = undefined;
    }

    if($scope.timer)
    {
        clearTimer();
    }

    $scope.timer = $interval(function(){
        if($scope.timer)
            getAdvancedNetworkDetails(false);
    },60000,0,true);

    $scope.$on('$destroy', function() {
        clearTimer();
    });

    /*$scope.$watch('network',function(n,o){
        //    $scope.networkInfo = $scope.$parent.connectedNetwork;

        //Non acme SIM
        var s = $location.search();
        if(s.st && s.st=='g')
            $scope.isACMESIM = false;
        else{
            var activeSIM = simMgrFactory.getActiveSIM();
            $scope.isACMESIM = (activeSIM) && (activeSIM.simType == 'ACME_SIM');
        }

        if ($scope.network && $scope.network.networkGroupInfo)
            $scope.networkInfo = $scope.network.networkGroupInfo;
        else
            $scope.networkInfo = $scope.network;

        if($scope.networkInfo)
            $scope.countryPlan = ($scope.networkInfo.networkGroupType == "COUNTRY");

        updateCurrentCountryLabel($scope.purchaseIntentCountries);

        if($scope.networkInfo){
            $scope.plansNotSupported = isPlansNotSupported($scope.purchaseIntentCountries, $scope.networkInfo);
        }

        if ($scope.networkInfo && $scope.networkInfo.networkGroupId) {
            $rootScope.plansLoading = true;
            updateNetworkLogo();
            if(!($scope.serviceEndDateCrossed || $scope.planPurchaseEndDateCrossed)) {

                if (!$scope.networkInfo.countryList) {
                    $scope.nwlogosLoading = true;
                    commonInfoFactory.getCountryListByCodes($scope.networkInfo.countryCodes).success(function (response) {
                        $scope.networkInfo.countryList = response.countryList;
                        $scope.nwlogosLoading = false;
                        $scope.countries = $scope.networkInfo.countryList;
                        updateOtherCountriesList();
                    }).error(function (data, status) {
                        $scope.nwlogosLoading = false;
                    });
                }
                else {
                    $scope.countries = $scope.networkInfo.countryList;
                    updateOtherCountriesList();
                }
                if(!$scope.isACMESIM){
                    var userCountryOfResidence = sessionStorage.getItem("userCountryCode");
                    $q.all([carrierMgrFactory.getForbiddenCountries(), commonInfoFactory.getCountryNameByCode(userCountryOfResidence)]).then(function (results) {

                        var forbiddenCountries = results[0].data.list;
                        var currentLocationArr = $scope.location;

                        $scope.forbidden = commonInfoFactory.isCountryForbidden(userCountryOfResidence, currentLocationArr, forbiddenCountries);

                        //$scope.forbidden = commonInfoFactory.isCountryForbidden("IN", ["IN"],[{code:"IN"}]);

                        if ($scope.forbidden) {
                            var forbiddenCountryName = results[1].data.name;
                            $scope.forbiddenCountriesMsg = Localize.translate("Residents are not permitted to buy plans in your location. You may buy our plans in all the other countries we serve. We apologize for any inconvenience this may cause.");
                            $rootScope.plansLoading = false;
                        } else {
                            carrierMgrFactory.getCarrierPlans($scope.networkInfo.networkGroupId, null, s.sim).success(function (data) {
                                $scope.plans = data.list;
                                $scope.plansAvailable = ($scope.plans.length != 0);
                                $rootScope.plansLoading = false;
                                if ($scope.plansAvailable) {
                                    $scope.allPlansHaveValidity = $scope.plans[0].validityPeriodInDays;
                                    $scope.planGroups = [[$scope.plans[0]]];
                                    $scope.plans[0].position = 0;
                                    for (var i = 1; i < $scope.plans.length; i++) {
                                        var plan = $scope.plans[i];
                                        plan.position = i;
                                        var foundInPreviousGroups = false;
                                        for (var j = 0; j < $scope.planGroups.length; j++) {
                                            var planGroupValidity = $scope.planGroups[j][0].validityPeriodInDays;
                                            if (planGroupValidity == plan.validityPeriodInDays) {
                                                $scope.planGroups[j].push(plan);
                                                foundInPreviousGroups = true;
                                                break;
                                            }
                                        }
                                        if (!foundInPreviousGroups) {
                                            $scope.planGroups.push([plan]);
                                        }
                                    }
                                }
                            }).error(function (data, status) {
                                $rootScope.plansLoading = false;
                            });
                        }
                    }, function (error) {
                        $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
                    });
                }

            }else{
                $rootScope.plansLoading = false;
            }

        }

    });*/

    $scope.viewAllCountries = function(nwId){
        $location.path('/plan-coverage/'+nwId);
    };


    $scope.commonInfoFactory = commonInfoFactory;

    function addPlanPurchaseAnalytics(plan)
    {

        var countryOfRes = sessionStorage.getItem("userCountryCode");
        var freePlanAnalytics = "";
        var purchaseEventName = "Plan Purchase";
        if(plan.freePlan)
        {
            freePlanAnalytics = ", Free";
            purchaseEventName = "free plan purchase";
        }

        var loc = ($scope.networkInfo && $scope.networkInfo.countryList && $scope.networkInfo.countryList.length)?$scope.networkInfo.countryList[0].code:null;
        var s = $location.search();
        if(!loc && s.loc)
            loc = s.loc;

        $analytics.setUserProperties({dimension2: (loc && loc == countryOfRes)?'in country':'out of country'});
        $analytics.eventTrack(purchaseEventName, {  category: 'conversion', label:commonInfoFactory.formatPlanDataSize(plan)+', ' + $scope.networkInfo.networkGroupName + ', '+ commonInfoFactory.formatPrice(plan.price,plan.currency)+freePlanAnalytics});
    }

    $scope.purchaseFreePlan = function(plan)
    {
        $rootScope.plansLoading = true;
        var s = $location.search();
        var purchaseIntentCountries=null;
        if(s.loc)
        {
            purchaseIntentCountries = [s.loc];
        }
        else
            purchaseIntentCountries = $scope.purchaseIntentCountries;
        paymentFactory.makePayment(plan,null,null,s.sim,null,purchaseIntentCountries).success(function(response){
            $rootScope.plansLoading = false;
            addPlanPurchaseAnalytics(plan);
            $location.path("/paymentProcessed");
            $location.search({trid:response.transactionId,fr:1});
        }).error(function(data,status){
            $rootScope.plansLoading = false;
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',commonInfoFactory.getLocalizedErrorMsg(data,status),null,Localize.translate("OK")).open('sm');
        });
    };

    $scope.purchasePlan = function(plan)
    {
        sessionStorage.removeItem('billingId');
        if(!canBuyPlan())
            return;
        $rootScope.plan = plan;
        $rootScope.networkInfo = $scope.networkInfo;
        var curPath = $location.path();
        var pp = 'd';
        if(curPath.search("network")!=-1)
        {
            pp = 'a';
        }
        var s = $location.search();
        if(!s)
        {
            s = {};
        }
        s.pd=plan.planId;
        s.pp=pp;
        s.nw=$scope.networkInfo.networkGroupId;
        if(!s.loc && $scope.purchaseIntentCountries)
        {
            s.lu= $scope.purchaseIntentCountries.toString();
        }

        $location.path("/buy-plan3");
        $location.search(s);
    };

    function canBuyPlan(){
        var activeSIM = simMgrFactory.getActiveSIM();
        if(!activeSIM)
        {
            $analytics.eventTrack('purchase attempt', {  category: 'behavior', label : 'no active sim'});
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('No SIM available'),Localize.translate('You need to activate your GigSky SIM card to buy a plan.'),Localize.translate("OK")
            ).open('sm');
            return false;
        }

        var s = $location.search();
        if(s.st && s.st=='g')
        {
            return true;
        }
        else
        if(activeSIM.simType == 'ACME_SIM')
            return false;
        else if(activeSIM && activeSIM.simStatus !="ACTIVE")
        {
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('SIM Blocked'),Localize.translate('Current SIM is blocked. Unblock the SIM to connect to GigSky.'),Localize.translate("OK")
            ).open('sm');
            return false;
        }
        return true;
    }

    $scope.preparePurchasePlan = function(plan){


        sessionStorage.removeItem('appliedPromo');
        var freePlanAnalytics = "";
        if(plan.freePlan)
            freePlanAnalytics = ", Free";

        $analytics.eventTrack('plan selected', {  category: 'behavior', label:commonInfoFactory.formatPlanDataSize(plan)+', ' + $scope.networkInfo.networkGroupName + ', '+ commonInfoFactory.formatPrice(plan.price, plan.currency)+freePlanAnalytics});

        var dataInMB  = plan.dataLimitInKB/1024.0;
        dataInMB = Math.round(dataInMB*100)/100;
        var s = $location.search();
        var countryCode;
        if(s.loc)
            countryCode = s.loc.toUpperCase();
        else if($scope.networkInfo.countryList && $scope.networkInfo.countryList.length)
        {
            countryCode = $scope.networkInfo.countryList[0].code;
        }

        ga('set','&cu',plan.currency);
        ga('ec:addImpression', {
            'id': countryCode+'-'+dataInMB,
            'name': countryCode+'-'+dataInMB,
            'price': (plan.freePlan?0:plan.price),
            'category': countryCode,
            'position': plan.position+1
        });
        ga('send','pageview');

        if(!canBuyPlan())
            return;

        if(plan.freePlan && plan.freePlanStatus)
        {
            //Confirm purchase.
            if(plan.freePlanStatus == "AVAILABLE")
            {
                if(plan.singlePurchaseOnly)
                {
                    var msg = Localize.translate('Confirm using this one time plan for %{country}', {country: $scope.networkInfo.networkGroupName});
                    var title = Localize.translate('Add Free %{plansize} data plan?',{plansize:commonInfoFactory.formatPlanDataSize(plan)});
                }
                else {
                    var msg = Localize.translate('Do you want to activate this plan for use in %{country}?', {country: $scope.networkInfo.networkGroupName});
                    var title = Localize.translate('Add Free data plan');
                }
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',title,msg,Localize.translate("OK"),Localize.translate("Cancel"),function(){$scope.purchaseFreePlan(plan);
                }).open('sm');
            }
            else if(plan.freePlanStatus == "UNAVAILABLE")
            {
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Sorry. Free plan is only available for currently available network.'),Localize.translate("OK")).open('sm');
            }
            else if(plan.freePlanStatus == "USED")
            {
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Sorry. A free plan has already been used in your location.'),Localize.translate("OK")).open('sm');
            }
            return;
        }
        else
        {
            $scope.purchasePlan(plan);
        }
    };

    $scope.getPlanValidityTitle = function(duration,noofPlans){
        if(noofPlans==1)
        {
            return Localize.translate("%{noofdays} Day Plan",{noofdays:duration});
        }
        else
            return Localize.translate("%{noofdays} Day Plans",{noofdays:duration});
    }

}]);

angular.module('gigSkyWebApp').controller('planCoverageController',['$scope','carrierManagerFactory','$routeParams','$window','SimManagerFactory',function ($scope,carrierMgrFactory,$routeParams,$window,simMgrFactory){

    $scope.networkId = $routeParams.networkId;
    var nwInfo= carrierMgrFactory.getNetworkDetailsFromSession($scope.networkId);
    $scope.nwDetails = nwInfo.details ? nwInfo.details : {};
    $scope.coverage = nwInfo.coverage ? nwInfo.coverage : '';
    $scope.planCoverageTitle = Localize.translate("Plan coverage");

    var activeSIM = simMgrFactory.getActiveSIM();
    $scope.title = (activeSIM) ? activeSIM.description : '';

    $scope.onBackClick = function () {
        $window.history.back();
    }

}]);

    angular.module('gigSkyWebApp').controller('deprecated_availableNetworksController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$modal',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$modal){

    var searchObj = $location.search();

    $scope.currentNetwork = null;
    $scope.location=null;

    var requestObj = null;

    if(searchObj && searchObj.nw)
    {
        $scope.availableCarriers = [];
        $scope.otherAvailableNetworks = [];
        if(!$rootScope.topupNetworkInfo || !$rootScope.topupNetworkInfo.countryList)
        {
            requestObj = carrierMgrFactory.getCarrierInfo(searchObj.nw);
            handleRequest(requestObj);
        }
        else {
            $scope.currentNetwork = $rootScope.topupNetworkInfo;
            updateSubscription();
        }
    }
    else
    if(searchObj && searchObj.loc)
    {
        //Get available network in a given location
        $scope.availableCarriers = [];
        $scope.otherAvailableNetworks = [];
        var activeSIM = simMgrFactory.getActiveSIM();
        var simType = (activeSIM)?    activeSIM.simType:null;
        requestObj = carrierMgrFactory.getAvailableCarriersInCountry(searchObj.loc,simType,activeSIM);
        $scope.location=[searchObj.loc];
        handleRequest(requestObj);
    }
    else
    {
        //Get available networks of current sim
        $scope.availableCarriers = [];
        $scope.otherAvailableNetworks = [];

        $scope.$on('onSIMNotConnected',function(data,simStatus){
            if(!simMgrFactory.getActiveSIM())
                return;
            updateAvailableNetworks(simStatus);
        });

        $scope.$on('onRunningSubscriptionDetected',function(data,simStatus){
            if($scope.currentNetwork)
            {
                var nw = ($scope.currentNetwork.networkGroupInfo)?$scope.currentNetwork.networkGroupInfo:$scope.currentNetwork;
                //Need to re order the plans list.
                if(nw.networkGroupId != $rootScope.runningSubscription.networkGroupInfo.networkGroupId)
                if(!orderAvailableNetworks(($rootScope.runningSubscription)?$rootScope.runningSubscription.networkGroupInfo:null))
                {
                    $scope.currentNetwork = $rootScope.runningSubscription.networkGroupInfo;
                    $rootScope.$broadcast('onAvailableNetworkLoaded',$scope.currentNetwork);
                }
                updateSubscription();
            }
            updateAvailableNetworks(simStatus);
        });
    }

    function updateAvailableNetworks(response)
    {
        //Fallback to explicit call for available networkgroups.
//        if(!response.availableNetworkGroupList)
//        {
//            requestObj =  carrierMgrFactory.getAvailableCarriers();
//            handleRequest(requestObj);
//            return;
//        }


        $scope.availableCarriers = response.availableNetworkGroupList;

        if((!response.availableNetworkGroupList || !response.availableNetworkGroupList.length))
            $rootScope.$broadcast('onNOServicesInCurrentLocation');

        orderAvailableNetworks(($rootScope.runningSubscription)?$rootScope.runningSubscription.networkGroupInfo:null);
        updateSubscription();
        if($scope.currentNetwork) {
            $scope.location = response.connectionInfo.countries?response.connectionInfo.countries:$scope.currentNetwork.countryCodes;
        }
        $rootScope.$broadcast('onAvailableNetworkLoaded',$scope.currentNetwork);

    }

    function orderAvailableNetworks(baseNetwork)
    {
        $scope.otherAvailableNetworks.splice(0,$scope.otherAvailableNetworks.length);
        $scope.currentNetwork = null;
        if($scope.availableCarriers && $scope.availableCarriers.length>1)
        {
            if(!baseNetwork)
            {
                $scope.currentNetwork = $scope.availableCarriers[0];
                for(var i=1;i<$scope.availableCarriers.length;i++)
                {
                    $scope.otherAvailableNetworks[i-1] = $scope.availableCarriers[i];
                }
            }
            else
            {
                for(var i= 0,k=0;i<$scope.availableCarriers.length;i++)
                {
                    var nw = ($scope.availableCarriers[i].networkGroupInfo)?$scope.availableCarriers[i].networkGroupInfo:$scope.availableCarriers[i];

                    if(nw.networkGroupId == baseNetwork.networkGroupId)
                    {
                        $scope.currentNetwork = nw;
                    }
                    else
                    {
                        $scope.otherAvailableNetworks[k++] =nw;
                    }
                }

                if(!$scope.currentNetwork)
                {
                    $scope.currentNetwork = $scope.otherAvailableNetworks[0];
                    $scope.otherAvailableNetworks.splice(0,1);
                    return false;
                }
            }
        }
        else if($scope.availableCarriers && $scope.availableCarriers.length==1)
        {
            $scope.currentNetwork = $scope.availableCarriers[0];
            $scope.otherAvailableNetworks = [];
            return false;
        }
        else if(baseNetwork)
        {
            $scope.currentNetwork = baseNetwork;
            $scope.otherAvailableNetworks = [];
            return false;
        }

        return true;

    }

    function updateSubscription(){

        if($scope.currentNetwork)
        {
            if($location.search().planPurchaseLoc)
                $scope.location = $location.search().planPurchaseLoc.split(',');
            //Fetch subscription details
            var nw = ($scope.currentNetwork.networkGroupInfo)?$scope.currentNetwork.networkGroupInfo:$scope.currentNetwork;
            carrierMgrFactory.getSubscriptionOfNetwork(nw.networkGroupId).success(function(subscription){

                    if(searchObj.nw && $scope.currentNetwork.networkGroupType && $scope.currentNetwork.networkGroupType !="COUNTRY")
                    {
                        $scope.location =(subscription.lastVisitedCountry && subscription.lastVisitedCountry.length)?subscription.lastVisitedCountry:(subscription.lastPurchaseIntentCountry && subscription.lastPurchaseIntentCountry.length)?subscription.lastPurchaseIntentCountry:"";
                    }
                    $scope.currentSubscription = subscription;
                    $scope.dataRemaining = commonInfoFactory.formatBalanceDataSize(subscription);// + "MB";
                    $scope.daysRemaining = commonInfoFactory.formatSubRemainingTime(subscription);//.balanceInDays + " days left";

                    if(Localize.translate("Expired")==$scope.daysRemaining)
                    {
                        $scope.dataRemaining = null;
                    }
                    $scope.timeStamp = "";
                    if(subscription.balanceAsOf)
                        $scope.timeStamp = Localize.translate('(Updated: %{val})',{val : commonInfoFactory.setDate(subscription.balanceAsOf,true)});

            }).error(function(data,status){
                    $scope.dataRemaining = null;
                    $scope.daysRemaining = null;
                    $scope.currentSubscription = null;
                });
        }
        else
        {
            $scope.currentSubscription = null;
            $scope.dataRemaining = null;
            $scope.daysRemaining = null;

        }
    }
    function handleRequest(requestObj){

        requestObj.success(function(response){
            $rootScope.plansLoading = false;
            if(response.type == 'NetworkGroupInfo')
            {
                $scope.currentNetwork = response;
                updateSubscription();
                return;
            }

            $scope.availableCarriers = response.list;

            if((!response.list || !response.list.length) && $location.path()=="/dashboard")
                $rootScope.$broadcast('onNOServicesInCurrentLocation');

            orderAvailableNetworks(($rootScope.runningSubscription)?$rootScope.runningSubscription.networkGroupInfo:null);
            updateSubscription();
            $rootScope.$broadcast('onAvailableNetworkLoaded',$scope.currentNetwork);


        }).error(function(er){
                $rootScope.plansLoading = false;
            });
    }

}]);

angular.module('gigSkyWebApp').controller('deprecated_networkController',['$scope','$window','$rootScope','$location','authFactory','$http','SimManagerFactory','carrierManagerFactory','commonInfoFactory','$modal','paymentFactory','$analytics','$q',function ($scope,$window,$rootScope,$location,authFactory,$http,simMgrFactory,carrierMgrFactory,commonInfoFactory,$modal,paymentFactory,$analytics,$q){

    $scope.viewLoading = false;
    $scope.nwlogosLoading = false;
    $scope.plansNotSupported = false;
    $scope.resourcePath = GS_SERVER_BASE;
    $scope.networkInfo = {};
    $scope.countries=[];
    $scope.carriers=[];
    $scope.countryPlan = false;
    $scope.countryPlanSubText=null;
    $scope.allPlansDurationMsg=null;
    $scope.currentCountryName=null;
    $scope.planGroups=[];
    $scope.planCoverageTitle = Localize.translate("Plan coverage");
    $scope.isDashBoard = false;
    $scope.referFriendMsg = Localize.translate("Refer A Friend");
    $scope.goToReferFriend = function () {
        $analytics.eventTrack('refer a friend clicked', { category: 'behavior'});
        $location.path('/referFriend');
    };

    $scope.init= function(networkInfo){
        $scope.networkInfo = networkInfo;
    }

    var curPath = $location.path();
    if(curPath.search("dashboard")!=-1)
    {
        $scope.isDashBoard = true;
    }


    if($scope.network && $scope.network.networkGroupInfo)
        $scope.networkInfo = $scope.network.networkGroupInfo;
    else
        $scope.networkInfo = $scope.network;

    $scope.currentCountryName = $scope.country;

    $scope.plans = [];
    $scope.balanceInfo = {};
    $scope.payStatus = {};
    $scope.payMethodInfo = {};
    $scope.plansAvailable = true;

    // var termsText = Localize.translate('GigSky Service subject to <a class="blueText" href="'+$rootScope.staticbaseurl+'/terms-and-conditions'+$rootScope.getSessionInfo()+'">Terms of Service</a>');
    // $("#disclaimer").html(termsText);

    $scope.$watch('subscription',function(n,o){

        if(n) {
            var subscription = n;
            $scope.dataRemaining = commonInfoFactory.formatBalanceDataSize(subscription);// + "MB";
            $scope.daysRemaining = commonInfoFactory.formatSubRemainingTime(subscription);//.balanceInDays + " days left";
            $scope.subscriptionDetails = $scope.dataRemaining+" | "+$scope.daysRemaining;

            if (Localize.translate("Expired") == $scope.daysRemaining) {
                $scope.dataRemaining = null;
                $scope.subscriptionDetails =$scope.daysRemaining;
            }
            $scope.timeStamp = "";
            if (subscription.balanceAsOf)
                $scope.timeStamp = Localize.translate('(Updated: %{val})', {val: commonInfoFactory.setDate(subscription.balanceAsOf, true)});


        }
        else {
            $scope.dataRemaining = $scope.daysRemaining = null;
            $scope.subscriptionDetails = null;
        }

        $scope.buyPlanLinkText = (subscription && subscription.subscriptionStatus != 'EXPIRED')?Localize.translate("Top-Up"):Localize.translate("Buy Now");
    });

    //$scope.$watch('country',function(n,o){
    //   if(n)
    //   {
    //       $scope.currentCountryName = $scope.country;
    //       updateCurrentCountryLabel();
    //   }
    //});

    $scope.$watch('location',function(n,o){
        if(n) {
            $scope.purchaseIntentCountries = $scope.location;
            updateNetworkLogo();
            updateCurrentCountryLabel($scope.purchaseIntentCountries);
            updateOtherCountriesList();
        }
    })

    //$scope.$watch('purchaseIntentCountries',function(n,o){
    //    $scope.currentCountriesX = $scope.purchaseIntentCountries;
    //});

    function updateCurrentCountryLabel(countries){
        if($scope.networkInfo && countries && countries.length) {
            commonInfoFactory.getCountryNameByCode(countries).success(function (resp) {
                $scope.currentCountryName = resp.name;
            });
            $scope.countryPlanSubText = " + " + Localize.translate("Other Countries");
        }
    }

    function prepareCoverageCountryList() {
        var countryServiceDisabledMap = {};

        var serviceDisabledCountries = $scope.networkInfo.serviceDisabledCountries;

        if(serviceDisabledCountries){
            for(var j=0; j<serviceDisabledCountries.length; j++){
                countryServiceDisabledMap[serviceDisabledCountries[j]] = true;
            }
        }

        $scope.countryList = $scope.networkInfo.countryList[0].name + ((countryServiceDisabledMap[$scope.networkInfo.countryList[0].code]) ? ' ('+ Localize.translate('temporarily unavailable')+')' : '');
        for(var i=1;i<$scope.networkInfo.countryList.length;i++)
        {
            var country = $scope.networkInfo.countryList[i];
            $scope.countryList += " | "+ country.name + ((countryServiceDisabledMap[country.code]) ? ' ('+ Localize.translate('temporarily unavailable')+')' : '');
        }
    }

    function updateOtherCountriesList(){
        if($scope.networkInfo && $scope.networkInfo.countryList && $scope.networkInfo.countryList.length)
        {
            $scope.countries = $scope.networkInfo.countryList;
            $scope.countries.sort(function(a,b){
                var n1 = a.name.toUpperCase();
                var n2 = b.name.toUpperCase();
                return n1.localeCompare(n2);
            });
            prepareCoverageCountryList();
        }
    }

    function updateNetworkLogo(){
        if($scope.networkInfo) {
            if ($scope.networkInfo.networkGroupType == "COUNTRY")
                $scope.imgPath = GS_SERVER_BASE + '/' + $scope.networkInfo.logoUrl;
            else {
                var countryCode = ($scope.purchaseIntentCountries && $scope.purchaseIntentCountries.length)?$scope.purchaseIntentCountries[0]:null;
                if (countryCode) {
                    var imgPath = $scope.networkInfo.logoUrl;
                    imgPath = imgPath.replace(/country_logos_v3_square.*png$/, "country_logos_v3_square/"+countryCode + ".png");
                    $scope.imgPath = GS_SERVER_BASE + '/' + imgPath;
                }
                else
                {
                    $scope.imgPath = null;
                }
            }
        }
    }

    function isPlansNotSupported(location, networkInfo) {
        var loc;
        var notSupported = false;
        if(!location)
            return notSupported;

        location = location.toString();
        if(location.indexOf(",")===-1)
        {
            loc = location;
        }
        else {
            if(location.indexOf("US")!==-1)
                loc = "US";
            else {
                var locations = location.split(",");
                loc = locations[0];
            }
        }

        var serviceDisabledCountries = networkInfo.serviceDisabledCountries;
        if(serviceDisabledCountries && serviceDisabledCountries.length){
            for(var i=0; i<serviceDisabledCountries.length; i++){
                if(loc === serviceDisabledCountries[i]){
                    notSupported = true;
                    break;
                }
            }
        }

        return notSupported;


    }


    $scope.$watch('network',function(n,o){
        //    $scope.networkInfo = $scope.$parent.connectedNetwork;

        //Non acme SIM
        var s = $location.search();
        if(s.st && s.st=='g')
            $scope.isACMESIM = false;
        else{
        var activeSIM = simMgrFactory.getActiveSIM();
        $scope.isACMESIM = (activeSIM) && (activeSIM.simType == 'ACME_SIM');
        }

        if ($scope.network && $scope.network.networkGroupInfo)
            $scope.networkInfo = $scope.network.networkGroupInfo;
        else
            $scope.networkInfo = $scope.network;

        if($scope.networkInfo)
            $scope.countryPlan = ($scope.networkInfo.networkGroupType == "COUNTRY");

        updateCurrentCountryLabel($scope.purchaseIntentCountries);

        if($scope.networkInfo){
            $scope.plansNotSupported = isPlansNotSupported($scope.purchaseIntentCountries, $scope.networkInfo);
        }

        if ($scope.networkInfo && $scope.networkInfo.networkGroupId) {
            $rootScope.plansLoading = true;
            updateNetworkLogo();
            if(!($scope.serviceEndDateCrossed || $scope.planPurchaseEndDateCrossed)) {

                if (!$scope.networkInfo.countryList) {
                    $scope.nwlogosLoading = true;
                    commonInfoFactory.getCountryListByCodes($scope.networkInfo.countryCodes).success(function (response) {
                        $scope.networkInfo.countryList = response.countryList;
                        $scope.nwlogosLoading = false;
                        $scope.countries = $scope.networkInfo.countryList;
                        updateOtherCountriesList();
                    }).error(function (data, status) {
                        $scope.nwlogosLoading = false;
                    });
                }
                else {
                    $scope.countries = $scope.networkInfo.countryList;
                    updateOtherCountriesList();
                }
                if(!$scope.isACMESIM){
                    var userCountryOfResidence = sessionStorage.getItem("userCountryCode");
                    $q.all([carrierMgrFactory.getForbiddenCountries(), commonInfoFactory.getCountryNameByCode(userCountryOfResidence)]).then(function (results) {

                        var forbiddenCountries = results[0].data.list;
                        var currentLocationArr = $scope.location;

                        $scope.forbidden = commonInfoFactory.isCountryForbidden(userCountryOfResidence, currentLocationArr, forbiddenCountries);

                        //$scope.forbidden = commonInfoFactory.isCountryForbidden("IN", ["IN"],[{code:"IN"}]);

                        if ($scope.forbidden) {
                            var forbiddenCountryName = results[1].data.name;
                            $scope.forbiddenCountriesMsg = Localize.translate("Residents are not permitted to buy plans in your location. You may buy our plans in all the other countries we serve. We apologize for any inconvenience this may cause.");
                            $rootScope.plansLoading = false;
                        } else {
                            carrierMgrFactory.getCarrierPlans($scope.networkInfo.networkGroupId, null, s.sim).success(function (data) {
                                $scope.plans = data.list;
                                $scope.plansAvailable = ($scope.plans.length != 0);
                                $rootScope.plansLoading = false;
                                if ($scope.plansAvailable) {
                                    $scope.allPlansHaveValidity = $scope.plans[0].validityPeriodInDays;
                                    $scope.planGroups = [[$scope.plans[0]]];
                                    $scope.plans[0].position = 0;
                                    for (var i = 1; i < $scope.plans.length; i++) {
                                        var plan = $scope.plans[i];
                                        plan.position = i;
                                        var foundInPreviousGroups = false;
                                        for (var j = 0; j < $scope.planGroups.length; j++) {
                                            var planGroupValidity = $scope.planGroups[j][0].validityPeriodInDays;
                                            if (planGroupValidity == plan.validityPeriodInDays) {
                                                $scope.planGroups[j].push(plan);
                                                foundInPreviousGroups = true;
                                                break;
                                            }
                                        }
                                        if (!foundInPreviousGroups) {
                                            $scope.planGroups.push([plan]);
                                        }
                                    }
                                }
                            }).error(function (data, status) {
                                $rootScope.plansLoading = false;
                            });
                        }
                    }, function (error) {
                        $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(error.data, error.status);
                    });
                }

            }else{
                $rootScope.plansLoading = false;
            }

        }

    });
    $scope.$parent.$parent.$watch("disableBack",function(n,o){
            if(n !== undefined)
                $scope.viewCoverage = !n;
    });
    $scope.viewAllCountries = function(){
        $scope.$parent.$parent.disableBack = false;
        $scope.viewCoverage = true;
    };
    $scope.showAccordion = true;
    console.log("path " + $location.path() + " window width " + $(document).width());
    if($location.path() != '/dashboard' && $(document).width() >= 960)
    {
        // by default carrier logos are shown.
        $scope.showAccordion = false;
    }

    if($scope.showAccordion == true)
    {
        $scope.collapsed = true;
        $scope.onViewMoreDetails = function()
        {
            $scope.collapsed = !$scope.collapsed;
            if(!$scope.collapsed)
            {
                //Fetch.. supported carries of a network.
                if($scope.networkInfo && !$scope.carriers.length){
                    $scope.nwlogosLoading = true;
                    carrierMgrFactory.getCarrierInfoDetailed($scope.networkInfo.networkGroupId).success(function(response){
                        $scope.nwlogosLoading = false;
                        $scope.carriers = response.carrierList;
                        $scope.countries = response.countryList;
                    }).error(function(data,status){
                            $scope.nwlogosLoading = false;
                        });
                }
            }
        }
    }
    else
    {
        $scope.collapsed = true;
    }

    $scope.handleAccordion = function()
    {
        if($scope.showAccordion)
        {
            status.open = !status.open;
            $scope.onViewMoreDetails();
        }
        else
        {
            status.open = true;
        }
    }

    $scope.commonInfoFactory = commonInfoFactory;

    function addPlanPurchaseAnalytics(plan)
    {

        var countryOfRes = sessionStorage.getItem("userCountryCode");
        var freePlanAnalytics = "";
        var purchaseEventName = "Plan Purchase";
        if(plan.freePlan)
        {
            freePlanAnalytics = ", Free";
            purchaseEventName = "free plan purchase";
        }

        var loc = ($scope.networkInfo && $scope.networkInfo.countryList && $scope.networkInfo.countryList.length)?$scope.networkInfo.countryList[0].code:null;
        var s = $location.search();
        if(!loc && s.loc)
          loc = s.loc;

        $analytics.setUserProperties({dimension2: (loc && loc == countryOfRes)?'in country':'out of country'});
        $analytics.eventTrack(purchaseEventName, {  category: 'conversion', label:commonInfoFactory.formatPlanDataSize(plan)+', ' + $scope.networkInfo.networkGroupName + ', '+ commonInfoFactory.formatPrice(plan.price,plan.currency)+freePlanAnalytics});
    }

    $scope.purchaseFreePlan = function(plan)
    {
        $rootScope.plansLoading = true;
        var s = $location.search();
        var purchaseIntentCountries=null;
        if(s.loc)
        {
            purchaseIntentCountries = [s.loc];
        }
        else
            purchaseIntentCountries = $scope.purchaseIntentCountries;
        paymentFactory.makePayment(plan,null,null,s.sim,null,purchaseIntentCountries).success(function(response){
            $rootScope.plansLoading = false;
            addPlanPurchaseAnalytics(plan);
            $location.path("/paymentProcessed");
            $location.search({trid:response.transactionId,fr:1});
        }).error(function(data,status){
                $rootScope.plansLoading = false;
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',commonInfoFactory.getLocalizedErrorMsg(data,status),null,Localize.translate("OK")).open('sm');
            });
    };

    $scope.purchasePlan = function(plan)
    {
        sessionStorage.removeItem('billingId');
        if(!canBuyPlan())
            return;
        $rootScope.plan = plan;
        $rootScope.networkInfo = $scope.networkInfo;
        var curPath = $location.path();
        var pp = 'p';
        if(curPath.search("dashboard")!=-1)
        {
            pp = 'd';
        }
        var s = $location.search();
        if(!s)
        {
            s = {};
        }
        s.pd=plan.planId;
        s.pp=pp;
        s.nw=$scope.networkInfo.networkGroupId;
        if(!s.loc && $scope.purchaseIntentCountries)
        {
            s.lu= $scope.purchaseIntentCountries.toString();
        }

        $location.path("/buy-plan3");
        $location.search(s);
    };

    function canBuyPlan(){
        var activeSIM = simMgrFactory.getActiveSIM();
        if(!activeSIM)
        {
            $analytics.eventTrack('purchase attempt', {  category: 'behavior', label : 'no active sim'});
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('No SIM available'),Localize.translate('You need to activate your GigSky SIM card to buy a plan.'),Localize.translate("OK")
            ).open('sm');
            return false;
        }

        var s = $location.search();
        if(s.st && s.st=='g')
        {
            return true;
        }
        else
        if(activeSIM.simType == 'ACME_SIM')
            return false;
        else if(activeSIM && activeSIM.simStatus !="ACTIVE")
        {
            new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('SIM Blocked'),Localize.translate('Current SIM is blocked. Unblock the SIM to connect to GigSky.'),Localize.translate("OK")
            ).open('sm');
            return false;
        }
        return true;
    }

    $scope.preparePurchasePlan = function(plan){

        var freePlanAnalytics = "";
        if(plan.freePlan)
            freePlanAnalytics = ", Free";

        $analytics.eventTrack('plan selected', {  category: 'behavior', label:commonInfoFactory.formatPlanDataSize(plan)+', ' + $scope.networkInfo.networkGroupName + ', '+ commonInfoFactory.formatPrice(plan.price, plan.currency)+freePlanAnalytics});

        var dataInMB  = plan.dataLimitInKB/1024.0;
        dataInMB = Math.round(dataInMB*100)/100;
        var s = $location.search();
        var countryCode;
        if(s.loc)
            countryCode = s.loc.toUpperCase();
        else if($scope.networkInfo.countryList && $scope.networkInfo.countryList.length)
            {
                countryCode = $scope.networkInfo.countryList[0].code;
            }

        ga('set','&cu',plan.currency);
        ga('ec:addImpression', {
            'id': countryCode+'-'+dataInMB,
            'name': countryCode+'-'+dataInMB,
            'price': (plan.freePlan?0:plan.price),
            'category': countryCode,
            'position': plan.position+1
        });
        ga('send','pageview');

        if(!canBuyPlan())
            return;

        if(plan.freePlan && plan.freePlanStatus)
        {
            //Confirm purchase.
            if(plan.freePlanStatus == "AVAILABLE")
            {
                if(plan.singlePurchaseOnly)
                {
                    var msg = Localize.translate('Confirm using this one time plan for %{country}', {country: $scope.networkInfo.networkGroupName});
                    var title = Localize.translate('Add Free %{plansize} data plan?',{plansize:commonInfoFactory.formatPlanDataSize(plan)});
                }
                else {
                    var msg = Localize.translate('Do you want to activate this plan for use in %{country}?', {country: $scope.networkInfo.networkGroupName});
                    var title = Localize.translate('Add Free data plan');
                }
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',title,msg,Localize.translate("OK"),Localize.translate("Cancel"),function(){$scope.purchaseFreePlan(plan);
                }).open('sm');
            }
            else if(plan.freePlanStatus == "UNAVAILABLE")
            {
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Sorry. Free plan is only available for currently available network.'),Localize.translate("OK")).open('sm');
            }
            else if(plan.freePlanStatus == "USED")
            {
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',null,Localize.translate('Sorry. A free plan has already been used in your location.'),Localize.translate("OK")).open('sm');
            }
            return;
        }
        else
        {
            $scope.purchasePlan(plan);
        }
    };

    $scope.getPlanValidityTitle = function(duration,noofPlans){
        if(noofPlans==1)
        {
            return Localize.translate("%{noofdays} Day Plan",{noofdays:duration});
        }
        else
            return Localize.translate("%{noofdays} Day Plans",{noofdays:duration});
    }

}]);

angular.module('gigSkyWebApp').directive('searchField',function(){

    return {

        link:function(scope,element)
        {
            element.bind('keydown',function(e){
                scope.searchFieldContainer = ['searchInputCancel'];
                scope.showCancelButton = true;
            });

        }
    };
});

angular.module('gigSkyWebApp').controller ('paymentProcessedController',['$location','$scope','paymentFactory','$rootScope','authFactory','commonInfoFactory','carrierManagerFactory',function($location,$scope,paymentFactory,$rootScope,authFactory,commonInfoFactory,carrierMgrFactory){

    var s = $location.search();
    $scope.transactionErrorMsg = null;
    $scope.transactionPending = true;
    $scope.notification = {};


    if(s.trid)
        $scope.transactionId = Localize.translate('Transaction ID is %{val}',{val : s.trid});

    if(s.fr && s.fr==1)
    {
        $scope.freePlan = true;
    }
   ///////////////////////
    //
    $scope.viewTitle = Localize.translate('Your purchase is being processed.');


    $scope.iccid = null;

    $scope.activatePlan = function () {
        //$scope.transactionPending = true;
        //$scope.transactionErrorMsg="Trying to send download profile request :  ";
        // try {
        //
        //     if(!window.DataMart)
        //     {
        //         $scope.transactionErrorMsg += "  DataMart object not present in Window Context. "
        //     }
        //     else if(!window.DataMart.notifyPurchaseResult)
        //     {
        //         $scope.transactionErrorMsg += "  notifyPurchaseResult API not present in DataMart Context. "
        //     }
        //
        //
        //     if(!DataMart)
        //     {
        //         $scope.transactionErrorMsg += "  DataMart object not present in Root Context. "
        //     }
        //     else if(!DataMart.notifyPurchaseResult)
        //     {
        //         $scope.transactionErrorMsg += "  notifyPurchaseResult API not present in DataMart Context. "
        //     }
        //
        //
        //     if (window.DataMart && window.DataMart.notifyPurchaseResult)
        //     {
        //         $scope.transactionErrorMsg += " Data Mart Available in Window";
        //         window.DataMart.notifyPurchaseResult(JSON.stringify($scope.notification));
        //     }
        //     else
        //     if (DataMart && DataMart.notifyPurchaseResult) {
        //         $scope.transactionErrorMsg += " Data Mart Available in Root";
        //         DataMart.notifyPurchaseResult(JSON.stringify($scope.notification));
        //     }
        //
        //     $scope.transactionErrorMsg += " Successfully sent notifyPurchase Event";
        // }
        // catch(e){
        //     $scope.transactionErrorMsg += ( "  Data Mart SDK Not available: Error " + e  );
        // }
        response = authFactory.sendMONotification($scope.notification);
        if(response)
        {
            $scope.transactionSuccessMsg = null;
            $scope.transactionErrorMsg = Localize.translate(response);
        }
    };

    $scope.goToDashBoard = function () {
        var params = $location.search();
        delete params.trid;
        $location.search(params);
        $location.path("/dashboard");
    };

    $scope.$on('onTransactionFailed', function (data, trresponse) {
        $scope.transactionPending = false;
        $scope.viewTitle = Localize.translate('Purchase failed');
        if (trresponse)
            $scope.transactionErrorMsg = trresponse.userDisplayErrorStr;
        else
            $scope.transactionErrorMsg = Localize.translate('Failed to complete the transaction.');
    });

    function getSubscriptionAndActivatePlan(trresponse) {

        var planName = trresponse.networkGroupInfo.networkGroupName;
        var nwid = trresponse.networkGroupInfo.networkGroupId;
        var dataDes = commonInfoFactory.formatPlanDataSize(trresponse.networkGroupPlan);
        carrierMgrFactory.getSubscriptionOfNetwork(nwid, authFactory.getDeviceId()).success(function (response) {
            $scope.transactionPending = false;
            if (response.imsiInfoList && response.imsiInfoList.list && response.imsiInfoList.list.length) {
                var imsilist = response.imsiInfoList.list;
                var l = response.imsiInfoList.list.length;
                var i = 0;
                for (i = 0; i < l; i++) {
                    var imsi = imsilist[i];
                    if (imsi.type == "GsmaEsimImsiInfo" && imsi.status == "ACTIVE") {
                        $scope.iccid = imsi.iccid;
                        $scope.transactionSuccessMsg = Localize.translate('%{val1} of data for use in %{val2} has been applied to your GigSky account.', {
                            val1: dataDes,
                            val2: planName
                        });
                        $scope.viewTitle = Localize.translate('Purchase completed');
                        var instrument = sessionStorage.getItem("newAccount")?'New':'Existing';
                        var iccids = authFactory.getICCIDList();

                        var isICCIDNew = 'New';

                        if(iccids && iccids.indexOf($scope.iccid)!=-1)
                        {
                            isICCIDNew = 'Existing';
                        }

                        var purchaseResult = {userAccount:instrument,purchaseInstrument:instrument, line:isICCIDNew,
                            moDirectStatus:'Complete',planName:planName + " "+dataDes+ " Plan" };

                        $scope.notification = {ver:1, success:true,transactionId:authFactory.getMOTransactionId(),
                            purchaseResult:JSON.stringify(purchaseResult)};

                        if(isICCIDNew == "New")
                        {
                            $scope.notification.activationCode = imsi.activationCode;
                        }
                        $scope.notification.iccid = $scope.iccid;
                        //$scope.transactionSuccessMsg += ( "Notification: "+ JSON.stringify($scope.notification));

                        //acmeService.dataPlanAccountUpdatedWithIccid($scope.iccid);
                        break;
                    }
                }
            }

            if (!$scope.transactionSuccessMsg) {
                $scope.transactionPending = false;
                $scope.transactionErrorMsg = Localize.translate('Failed to complete the transaction.');
                $scope.viewTitle = Localize.translate('Purchase failed');
            }
        }).error(function (data, status) {
            $scope.transactionPending = false;
            $scope.transactionErrorMsg = commonInfoFactory.getLocalizedErrorMsg(data, status);
            $scope.viewTitle = Localize.translate('Purchase failed');
            //authFactory.sendMONotification();

        });
    }

    $scope.$on('onTransactionComplete', function (data, trresponse) {
        getSubscriptionAndActivatePlan(trresponse);
    });

    // $scope.$on('onTransactionTimeout', function () {
    //     $rootScope.pageLoadingFailed = true;
    // });
    $scope.$on('onTransactionTimeout', function () {
        $scope.transactionPending = false;
        $scope.viewTitle = Localize.translate('Purchase is pending');
        $scope.transactionPendingMsg = Localize.translate('Your plan purchase is taking longer than expected. Tap Refresh to get the current status of your data plan purchase.');
    });

    var currentSIM = authFactory.getDeviceId();

    $scope.refreshStatus = function () {
        $scope.transactionPending = true;
        $scope.transactionPendingMsg = null;
        paymentFactory.trMonitor.monitor(s.trid, currentSIM);
    };

    paymentFactory.trMonitor.monitor(s.trid, currentSIM);

    sessionStorage.removeItem('billingId');

}]);
angular.module('gigSkyWebApp').controller ('browsePlansController',['$scope','$rootScope','paymentFactory','$location','commonInfoFactory','carrierManagerFactory','SimManagerFactory','$analytics',function ($scope,$rootScope,paymentFactory,$location,commonInfoFactory,carrierManagerFactory,simMgrFactory,$analytics){

    $scope.searchFieldContainer = ['searchInput'];
    $scope.viewLoading = true;
    $scope.searched = false;
    $scope.showCancelButton = false;
    $scope.countriesForSearchKey = [ ];
    $scope.errorResponse = null;
    $scope.isAppleSim = false;
    $scope.searchStr="";

    function prepareCountryName(countries) {
        for(var i=0; i<countries.length; i++){
            var country = countries[i];

            if(country.serviceAvailable === false){
                country.name = country.name +' ('+ Localize.translate('temporarily unavailable')+')';
            }
        }

    }

    var activeSIM = simMgrFactory.getActiveSIM();
    if(activeSIM)
    {
        //$scope.noSIMAvailable = false;
        if(activeSIM.simType === "ACME_SIM")
        {
            $scope.isAppleSim = true;
            $scope.viewLoading = false;

        }else if(activeSIM.simType === "TELNA_SIM"){
            if($rootScope.enableSim1ToSim2Transition) {
                var planPurchaseEndDate = sessionStorage.getItem("planPurchaseEndDate");
                var serviceEndDate = sessionStorage.getItem("serviceEndDate");

                var planEndDate = commonInfoFactory.convertDateFormat(getDateObj(planPurchaseEndDate));
                var serviceDate = commonInfoFactory.convertDateFormat(getDateObj(serviceEndDate));

                var today = new Date();

                if(today > getDateObj(planPurchaseEndDate)){
                    $scope.viewLoading = false;
                    $scope.planPurchaseEndDateCrossed = true;
                    $scope.simReplaceMsg = Localize.translate("Data Plans for this SIM are no longer available for purchase. Select your replacement SIM to continue to use GigSky’s data service. Please contact help@gigsky.com if you have any questions, or need a replacement SIM.");
                }

                if(today > getDateObj(serviceEndDate)){
                    $scope.viewLoading = false;
                    $scope.serviceEndDateCrossed = true;
                    $scope.simReplaceMsg = Localize.translate("This SIM will no longer work on GigSky's mobile data service. Select your replacement SIM to continue to use GigSky’s data service. Please contact help@gigsky.com if you have any questions, or need a replacement SIM.");
                }
            }
        }
        $scope.selectDestTitle = activeSIM.description;
    }
    else
    {
        $scope.selectDestTitle = 'Select destination';
    }
    var simType = (activeSIM)?    activeSIM.simType:null;

    function filterVisitedCountries()
    {
        if($scope.visitedCountries && $scope.visitedCountries.length && $scope.countries && $scope.countries.length)
        {
            for(var i=0;i<$scope.visitedCountries.length;i++)
            {
                var c = $scope.visitedCountries[i];
                $scope.countries = $scope.countries.filter(function(e){
                   if(e.code == c.code)
                       return false;
                    return true;
                });
            }
        }
    }
    if(!$scope.isAppleSim && !($scope.planPurchaseEndDateCrossed || $scope.serviceEndDateCrossed))
    {

        commonInfoFactory.getSupportedCountryList(simType,activeSIM).success(function(response){
            $scope.viewLoading=false;
            $scope.countries = response.list;
            $scope.errorResponse = null;
            prepareCountryName($scope.countries);
            filterVisitedCountries();
        }).error(function(data,status,headers,config){
                $scope.viewLoading=false;
                $scope.countries= [];
                $scope.errorResponse=commonInfoFactory.getLocalizedErrorMsg(data,status);
            });

        $scope.visitedCountries = [];
        if(activeSIM){
            commonInfoFactory.getVisitedCountryList(activeSIM.simId).success(function(response){
                $scope.visitedCountries = response.list;
                prepareCountryName($scope.visitedCountries);
                filterVisitedCountries();
            }).error(function(data,status,headers,config){

                });
        }
    }

    $scope.onSearchCancel = function()
    {
        $scope.countriesForSearchKey = [];
        $scope.showCancelButton=!$scope.showCancelButton;
        $scope.searchStr="i";
        $("#searchCountries").val("");
        $scope.searched=false;
        $scope.searchFieldContainer = ['searchInput'];
    };

    $scope.noResults = false;
    $scope.searchCountries = function(searchKey)
    {
        $scope.searched = true;
        $scope.noResults = false;
        $scope.errorResponse = null;
        if(!searchKey || searchKey.length==0)
        {
            $("#searchCountries").blur();
            $scope.noResults = true;
            return;
        }

        $analytics.eventTrack('plan search', {category: 'behavior' , label : searchKey});

        $("#searchCountries").blur();
        $scope.searchViewLoading = true;
        commonInfoFactory.getCountriesForSearchKey(searchKey,simType,activeSIM).success(function(response){
            $scope.searchViewLoading = false;
            $scope.countriesForSearchKey = (response.list && response.list.length)?response.list:[];
            $scope.noResults = ($scope.countriesForSearchKey.length)?false:true;
        }).error(function(data,status){
                $scope.searchViewLoading = false;
                $scope.countriesForSearchKey = [];
                $scope.errorResponse = commonInfoFactory.getLocalizedErrorMsg(data,status);
            });
    };
}]);

angular.module('gigSkyWebApp').controller ('makePaymentController',['$scope','$rootScope','paymentFactory','$location','commonInfoFactory','carrierManagerFactory','$q','SimManagerFactory','authFactory','$analytics','applePayService','$filter','referralService','$modal',function ($scope,$rootScope,paymentFactory,$location,commonInfoFactory,carrierManagerFactory,$q,simMgr,authFactory,$analytics,applePayService,$filter,referralService,$modal){

    commonInfoFactory.maskTelField();

    $scope.transcationPinLabel = Localize.translate("Transaction PIN");
    $scope.confirmNewTranscationPinLabel = Localize.translate("Confirm New Transaction PIN");
    $scope.paymentMethodLabel = Localize.translate("Payment method:");
    $scope.selectPaymentMethodLabel = Localize.translate("Select payment method:");
    $scope.setApplePayDef = false;

    (function updatePayStepIndicator(){
        var s = $location.search();
        $scope.buyPlan2Path="#/dashboard";

        if(s.pp ==='a'){
            $scope.buyPlan2Path="#/network/"+ s.nw;
        }
        else if(s.loc && s.pp !='d')
            $scope.buyPlan2Path="#/buy-plan2?loc="+ s.loc+"&si=3";
        else if(s.pp!='d')
            $scope.buyPlan2Path="#/buy-plan2?nw="+ s.nw+"&si=2";
        $scope.si = 3; //Step indicator max steps.
        $scope.currentstep = 3;
        if(s)
        {
            $scope.si = s.si? s.si:3;
            if(s.pp=='d' || s.pp=='a')
                $scope.si = 2;
        }
        if($scope.si!=3)
            $scope.currentstep = 2;



        if(s && s.sim)
        {
            $scope.buyPlan2Path=$scope.buyPlan2Path+"&sim="+ s.sim;
            updateSIMDetailOfPlanPurchase(s.sim);
        }
        else{
            var activeSIM = simMgr.getActiveSIM();
            if(activeSIM)
            {
                $scope.simName = $scope.makePaymentTitle = activeSIM.description;
                $rootScope.purchaseForSIM = activeSIM;
            }
            else
            {
                $scope.simName = $scope.makePaymentTitle = 'Make payment';
            }

        }

    })();


    var termsText = Localize.translate('I agree to GigSky\'s <a class="blueText" href="'+$rootScope.staticbaseurl+'/terms-and-conditions'+$rootScope.getSessionInfo()+'">Terms of Service</a>');
    $("#termsLabel").html(termsText);

    $scope.viewLoading = true;
    var simDetailsPromise  = null;
    var nwDetailsPromise = null;
    var planDetailsPromise = null;
    var accountStatusPromise = null;

    function updateSIMDetailOfPlanPurchase(simId){

        if($rootScope.purchaseForSIM){
            $scope.simName = $scope.makePaymentTitle = $rootScope.purchaseForSIM.description;
        }
        else
        {
            //Fetch sim details..
            simDetailsPromise = simMgr.getSIMDetails(simId,true);
            simDetailsPromise.success(function(result){
                $rootScope.purchaseForSIM = result;
                $scope.simName = $scope.makePaymentTitle = $rootScope.purchaseForSIM.description;
            }).error(function(data,status){
                    $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
                });
        }
    }

    $scope.bNeedPayMethod = true;
    $scope.payMethodInfo= {};
    $scope.plan = $rootScope.plan;
    $scope.networkInfo = $rootScope.networkInfo;
    $scope.accountPayStatus = {};
    $scope.commonInfoFactory = commonInfoFactory;
    var obj = $location.search();
    $scope.planId = obj.pd;
    $scope.networkId = obj.nw;
    $scope.planName="";
    var bid = sessionStorage.getItem('billingId');
    $scope.currentBillingId = (bid)?bid:null;
    $scope.accountPayStatusDeferred = $q.defer();
    $scope.existingCCs = 0;
    $scope.existingPPs = 0;


    function showDisclaimer(nwInfo) {
        if(!nwInfo)
            return;
        var ATTsubscription = (nwInfo.imsiProvider == 'AT&T');
        var isCountryCodePresent = (nwInfo.countryCodes && nwInfo.countryCodes.length);
        if(ATTsubscription && isCountryCodePresent) {
            var countryCodes = nwInfo.countryCodes;
            var countryList = $filter('filter')(countryCodes, 'US');
            $scope.showUnusedDataMsg = (countryList.length == 0);
        }
    }

    showDisclaimer($scope.networkInfo);

    $scope.$on('onActiveSIMAvailable',function(){
        var s = $location.search();
        if(s && s.sim)
        {
            updateSIMDetailOfPlanPurchase(s.sim);
        }
        else{
            var activeSIM = simMgr.getActiveSIM();
            $scope.simName = $scope.makePaymentTitle = activeSIM.description;
            $rootScope.purchaseForSIM = activeSIM;
        }
    });
    $scope.payMethodDisplayStr = function(payMethod)
    {
        if(!payMethod)
            return "";

        if(payMethod.paymentMethod == "ApplePay")
        {
            return Localize.translate('Apple Pay');
        }
        else if(payMethod.paymentMethod == "CreditCard")
        {
            return Localize.translate('Credit card ending %{val}',{val: payMethod.displayCC});
        }
        else if(payMethod.paymentMethod == "Paypal")
        {
            return Localize.translate('PayPal account %{val}',{val: payMethod.userEmailId});
        }
    };
    if(!$scope.plan)
    {
        //Fetch plan and network details
        nwDetailsPromise = carrierManagerFactory.getCarrierInfo($scope.networkId);
        nwDetailsPromise.success(function(carrierInfo){
            $scope.networkInfo = carrierInfo;
            showDisclaimer($scope.networkInfo);
        });
        planDetailsPromise = carrierManagerFactory.getCarrierPlans($scope.networkId);
        planDetailsPromise.success(function(carrierPlans){
            for(var i=0; i< carrierPlans.list.length; i++)
            {
                var carrierPlan = carrierPlans.list[i];
                if(carrierPlan.planId == $scope.planId)
                {
                    $scope.plan = carrierPlan;
                    break;
                }
            }
        });
    }


    $scope.canShowBuyPlanOption = function(){

        return $scope.payMethodInfo.billingId || !$scope.bNeedPayMethod;
    };
    
    accountStatusPromise = commonInfoFactory.accountPaymentStatus();

    function calculateOrderDetails(payStatus,applePayStatus){

            $scope.accountPayStatus = payStatus;

            $scope.applePayAvailable = applePayStatus;

            $scope.payMethodInfo.billingId = ($scope.currentBillingId!=null)?$scope.currentBillingId:$scope.accountPayStatus.billingId;


            var promoDetails = sessionStorage.getObj('appliedPromo');

            if (promoDetails){
                var promoCodeAmount = promoDetails.amount;
                $scope.promoCodeAmount = promoCodeAmount;
                $scope.appliedPromocode = promoDetails.code;

                $scope.promoCodeAppliedMessage = Localize.translate('Promo code "%{val}" is applied',{val: $scope.appliedPromocode});
                $scope.promoAmountStr = commonInfoFactory.formatPrice($scope.promoCodeAmount ,$scope.plan.currency,true);

                var balanceAmount = (promoCodeAmount == $scope.plan.price) ? 0 : ($scope.plan.price - promoCodeAmount);
                if (balanceAmount) {
                    $scope.orderTotalAmount = payStatus.gigskyCreditAmt >= balanceAmount ? 0 : (balanceAmount - payStatus.gigskyCreditAmt);
                    $scope.useCreditAmount = $scope.plan.price - ($scope.orderTotalAmount + promoCodeAmount);
                }else{
                    $scope.orderTotalAmount = 0;
                    $scope.useCreditAmount = 0;
                }
                $scope.orderTotalAmount = ($scope.orderTotalAmount)? parseFloat( $scope.orderTotalAmount.toFixed(2)) : 0;
            }else{
                $scope.orderTotalAmount = (payStatus.gigskyCreditAmt>=$scope.plan.price)?0:($scope.plan.price-payStatus.gigskyCreditAmt);
                $scope.orderTotalAmount = ($scope.orderTotalAmount)? parseFloat( $scope.orderTotalAmount.toFixed(2)) : 0;
                $scope.useCreditAmount = $scope.plan.price - $scope.orderTotalAmount;
            }

            $scope.orderTotalStr = commonInfoFactory.formatPrice($scope.orderTotalAmount,$scope.plan.currency);
            $scope.useCreditAmount = (Math.round($scope.useCreditAmount*100))/100;

            if($scope.useCreditAmount)
            {
                $scope.creditAmountStr = commonInfoFactory.formatPrice($scope.useCreditAmount ,$scope.plan.currency,true);
            }
            else
            {
                $scope.creditAmountStr = null;
            }

            $scope.bNeedPayMethod = $scope.orderTotalAmount>0;

            if($scope.bNeedPayMethod){

                if($scope.applePayAvailable && $scope.currentBillingId == null){

                    $scope.payMethodInfo.displayStr = Localize.translate('Apple Pay');
                    $scope.payMethodInfo.billingId = null;
                    $scope.payMethodInfo.paymentMethod = "ApplePay";
                    $scope.currentPayMethod = $scope.payMethodInfo;
                    $scope.setApplePayDef = true;
                }
                else if($scope.currentBillingId==null ||  ($scope.currentBillingId == $scope.payMethodInfo.billingId))
                {
                    $scope.currentBillingId = $scope.payMethodInfo.billingId;
                    //Update search string.
                    if($scope.accountPayStatus.paymentMethod == "CreditCard")
                    {
                        //$scope.payMethodInfo = CREDIT_CARD;
                        $scope.payMethodInfo.displayStr  = Localize.translate('Credit card ending %{val1} will be charged with %{val2}.',{val1: $scope.accountPayStatus.creditCardNumber,val2:commonInfoFactory.planPrice($scope.plan)});
                    }
                    else if($scope.accountPayStatus.paymentMethod == "Paypal")
                    {
                        $scope.payMethodInfo.displayStr  = Localize.translate('Paypal account %{val1} will be charged with %{val2}.',{val1: $scope.accountPayStatus.paypalEmailId,val2: commonInfoFactory.planPrice($scope.plan)});
                    }
                }
            }

            //$scope.accountPayStatusDeferred.resolve();

    }

    $scope.addPayMethod = function(path)
    {
        $location.path(path);
        var s = $location.search();
        if($scope.payMethodInfo.billingId){
            sessionStorage.setItem('billingId',$scope.payMethodInfo.billingId);
           // s.bid =$scope.payMethodInfo.billingId;
        }

        $location.search(s);
        $location.replace();
    }

    function updatePaymentMethodOptions(allPaymentMethods){
        $scope.paymentMethods = allPaymentMethods.list;
            $scope.existingCCs=0;
            $scope.existingPPs=0;
            for(var i=0; i < $scope.paymentMethods.length;i++)
            {
                var pm = $scope.paymentMethods[i];
                if(pm.billingId == $scope.payMethodInfo.billingId)
                {
                    $scope.changePayMethod(pm);
                }
            }

        if(($scope.paymentMethods.length == 0) && $scope.bNeedPayMethod && !$scope.setApplePayDef){
            $analytics.eventTrack('purchase attempt', {  category: 'behavior', label : 'No payment method'});
            $scope.viewLoading = true;
            $scope.addPayMethod('/paymentMethod');
        }
    }

    var supportedPayMethodsPromise = paymentFactory.getPaymentMethods();

    function isApplePayEnabled(result) {
        var enableApplePay = false;

        for (var i=0;i<result.list.length;i++){
            if ((result.list[i].paymentMethod == "ApplePay") && (result.list[i].enabled == true)) {
                enableApplePay = true;
                break;
            }
        }
        return enableApplePay;
    }

    var billingInfoPromise = paymentFactory.getAllBillingInfo();

    var accountInfoPromise = authFactory.getAccount();



//    billingInfoPromise.success(function(allPaymentMethods){
//        $scope.paymentMethods = allPaymentMethods.list;
//        $scope.accountPayStatusDeferred.promise.then(function(){
//            $scope.existingCCs=0;
//            $scope.existingPPs=0;
//            for(var i=0; i < $scope.paymentMethods.length;i++)
//            {
//                var pm = $scope.paymentMethods[i];
//                if(pm.paymentMethod == "CreditCard")
//                {
//                    $scope.existingCCs++;
//                }
//                else if(pm.paymentMethod == "Paypal")
//                {
//                    $scope.existingPPs++;
//                }
//                if(pm.billingId == $scope.payMethodInfo.billingId)
//                {
//                    $scope.changePayMethod(pm);
//                }
//            }
//
//            if($scope.existingCCs > 0 || $scope.existingPPs > 0)
//            {
//                $scope.showAddCC = false;
//                $scope.showAddPaypal = false;
//            }
//            else
//            {
//                $scope.showAddCC = true;
//                $scope.showAddPaypal = true;
//            }
//
//            //$scope.allBillingInfoDeferredInstance.resolve();
//        });
//
//    });

    $scope.concurAcLinked = false;
    $scope.sendConcurReceipt = false;

    $q.all([planDetailsPromise,accountStatusPromise,accountInfoPromise,supportedPayMethodsPromise,billingInfoPromise,simDetailsPromise,nwDetailsPromise]).then(function(results){

        var enableApplePay = isApplePayEnabled(results[3].data);

        if(enableApplePay){
            applePayService.isApplePayAvailable().then(function (applePayStatus) {
                $scope.viewLoading = false;
                calculateOrderDetails(results[1].data,applePayStatus.data);

                var userInfo = results[2].data;
                if(userInfo.linkedActList)
                {
                    for(var i=0 ; i<userInfo.linkedActList.length;i++){
                        var ac = userInfo.linkedActList[i];
                        if(ac.actType=="CONCUR")
                        {
                            $scope.concurAcLinked = true;
                            $scope.sendConcurReceipt = true;
                        }
                    }
                }

                updatePaymentMethodOptions(results[4].data);
            }, function (error) {
                $scope.viewLoading = false;
                $scope.formSubmitErrorStr = "Unable to get order summary details.";
            });
        }else{
            $scope.viewLoading = false;
            calculateOrderDetails(results[1].data,false);

            var userInfo = results[2].data;
            if(userInfo.linkedActList)
            {
                for(var i=0 ; i<userInfo.linkedActList.length;i++){
                    var ac = userInfo.linkedActList[i];
                    if(ac.actType=="CONCUR")
                    {
                        $scope.concurAcLinked = true;
                        $scope.sendConcurReceipt = true;
                    }
                }
            }

            updatePaymentMethodOptions(results[4].data);
        }

    }, function (results) {
        $scope.viewLoading = false;
        $scope.formSubmitErrorStr = "Unable to get order summary details.";
    });

    $scope.changePayMethod = function(payMethod)
    {
        sessionStorage.setItem('billingId',payMethod.billingId);
        updateCurrentPaymethodInfo(payMethod);
        function updateCurrentPaymethodInfo(payMethod)
        {
            $scope.currentPayMethod = payMethod;
            $scope.payMethodInfo.billingId = payMethod.billingId;
            $scope.currentBillingId = $scope.payMethodInfo.billingId;
            $scope.payMethodInfo.forceUserEdit = payMethod.forceUserEdit;

            if(payMethod.paymentMethod == "CreditCard" && $scope.payMethodInfo.forceUserEdit && $scope.bNeedPayMethod)
            {
                $scope.viewLoading = true;
                 var path = "editCC/"+$scope.payMethodInfo.billingId;
                $location.path(path);
                return;
            }

            if(payMethod.paymentMethod == "CreditCard")
            {
                //$scope.payMethodInfo = CREDIT_CARD;
                $scope.payMethodInfo.displayStr  = Localize.translate('Credit card ending %{val1} will be charged with %{val2}.',{val1: payMethod.displayCC,val2: commonInfoFactory.planPrice($scope.plan)});
            }
            else if(payMethod.paymentMethod == "Paypal")
            {
                $scope.payMethodInfo.displayStr  = Localize.translate('Paypal account %{val1} will be charged with %{val2}.',{val1: payMethod.userEmailId,val2: commonInfoFactory.planPrice($scope.plan)});
            }

        }
    };

    function addPlanPurchaseAnalytics(plan,paymentType)
    {
        var countryOfRes = sessionStorage.getItem("userCountryCode");
        var freePlanAnalytics = "";
        var purchaseEventName = (paymentType== 'Apple Pay')?'apple pay purchase':"plan purchase";
        if(plan.freePlan)
        {
            freePlanAnalytics = ", Free";
            purchaseEventName = "Free Plan Purchase";
        }

        var loc = ($scope.networkInfo && $scope.networkInfo.countryList && $scope.networkInfo.countryList.length)?$scope.networkInfo.countryList[0].code:null;
        var s = $location.search();
        if(!loc && s.loc)
            loc = s.loc;

        $analytics.setUserProperties({dimension2: (loc && loc == countryOfRes)?'in country':'out of country'});

        $analytics.eventTrack(purchaseEventName, {  category: 'behavior', label:commonInfoFactory.formatPlanDataSize(plan)+', ' + $scope.networkInfo.networkGroupName + ', '+ commonInfoFactory.formatPrice(plan.price,plan.currency)+freePlanAnalytics});
        var dataInMB  = plan.dataLimitInKB/1024.0;
        dataInMB = Math.round(dataInMB*100)/100;
        var trid = s.trid;

        ga('set','&cu',plan.currency);

        ga('ec:addProduct', {
            'id': loc+'-'+dataInMB,
            'name': loc+'-'+dataInMB, // Data plan amount in megabytes (string).
            'price': plan.price, // Data plan price (currency).
            'category': loc,   // Data plan country code (string).
            'quantity': 1
        });

        ga('ec:setAction','purchase',{
            'id':''+trid,
             'revenue':plan.price
        });
        ga('send', 'pageview');
    }

    $scope.onPlanPurchaseSuccess = function (response,paymentType) {
        $location.path("/paymentProcessed");
        if($scope.sendConcurReceipt){
            var userCountryCode = sessionStorage.getItem("userCountryCode");
            if(!userCountryCode)
                userCountryCode = "N/A";
            $analytics.eventTrack('concur submit receipt', {  category: 'behavior', label: userCountryCode});
        }

        $scope.isPaymentInProgress = false;
        $location.search({trid:response.transactionId});
        addPlanPurchaseAnalytics($scope.plan,paymentType);
    };

    $scope.onPlanPurchaseFailure = function(data,status){
        $scope.viewLoading = false;
        $scope.isPaymentInProgress = false;
        $(document).scrollTop(0);

        if(!$scope.setApplePayDef){
            $scope.accountPayStatus.transactionPinSet = true;
        }

        var errorMsg = commonInfoFactory.getLocalizedErrorMsg(data,status);
        if(errorMsg)
        {
            $scope.formSubmitErrorStr = errorMsg;
            $('[pin]').val('');
        }
        else
        {
            $scope.formSubmitErrorStr = Localize.translate('Unable to complete request due to an internal error. Please try again later.');
        }
    };

    $scope.confirmPurchase = function()
    {
        $scope.formSubmitErrorStr = null;
        if (!$scope.isPaymentInProgress) {
            $scope.isPaymentInProgress = true;
            $scope.viewLoading = true;

            if ($scope.promoCode) {
                if (!$scope.appliedPromocode) {
                    $scope.onApplyPromoCode(true);
                } else {
                    applyPromoAndMakePayment();
                }
            } else {
                makePayment();
            }
        }
    };

    function makePayment() {
        var submitReceipt = [];
        if($scope.sendConcurReceipt)
        {
            submitReceipt = ['CONCUR'];
        }

        var credits = $scope.promoCodeAmount ? $scope.useCreditAmount + $scope.promoCodeAmount : $scope.useCreditAmount;
        paymentFactory.makePayment($scope.plan,$scope.payMethodInfo.billingId,credits,$rootScope.purchaseForSIM.simId,submitReceipt,getPurchaseIntentCountries())
            .success($scope.onPlanPurchaseSuccess)
            .error($scope.onPlanPurchaseFailure);
    }

    function applyPromoAndMakePayment() {
        var customerId = sessionStorage.getItem("customerId");
        var data = preparePromoCodeRequest(false);
        data.validationMode = false;
        referralService.applyPromoCode(customerId, $rootScope.purchaseForSIM.simId, data).success(function(response){
            makePayment();
        }).error(function(data,status){
            $scope.isPaymentInProgress = false;  
            $scope.viewLoading = false;
            $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
        });
    }

    function getPurchaseIntentCountries()
    {
        var purchaseIntentCountries=null;
        var s = $location.search();
        if(s.loc)
        {
            purchaseIntentCountries = [s.loc];
        }
        else if(s.lu)
        {
            purchaseIntentCountries = s.lu.split(",");
        }
        else {
            purchaseIntentCountries = [authFactory.getCurrentLocation()];
        }
        return purchaseIntentCountries;
    }

    $scope.changePaymentMethod = function(){
        $location.path('/paymentMethod');
      //  $location.replace();
    }

    $scope.showPromoInput = false;

    /**
     * This function validates the promocode
     * @param implicitPromoCodeValidation: this is true when promocode entered is not validated on clicking apply button and user has directly clicked on buy plan.
     */
    $scope.onApplyPromoCode = function (implicitPromoCodeValidation) {
        $scope.formSubmitErrorStr = null; 
        $scope.promoCodeErrorStr = '';
        var promoCodePattern = /^[a-zA-Z0-9]{4,9}$/;
        if(!$scope.promoCode || (($scope.promoCode.length < 4) || ($scope.promoCode.length > 10) || !promoCodePattern.test($scope.promoCode))){
            var label = Localize.translate("Promo Code");
            $scope.promoCodeErrorStr = Localize.translate('%{val} invalid',{val : label});
            if(implicitPromoCodeValidation){
                $scope.isPaymentInProgress = false;
                $scope.viewLoading = false;
            }
            return;
        }
        var customerId = sessionStorage.getItem("customerId");
        var data = preparePromoCodeRequest(true);
        $scope.viewLoading = true;
        referralService.applyPromoCode(customerId, $rootScope.purchaseForSIM.simId, data).success(function(response){
            $scope.viewLoading = false;
            $scope.isPaymentInProgress = false;
            $scope.appliedPromocode = $scope.promoCode;
            sessionStorage.setObj('appliedPromo', {code: $scope.appliedPromocode , amount: response.amount});
            calculateOrderDetails($scope.accountPayStatus, $scope.applePayAvailable);

            if(implicitPromoCodeValidation)
            {
                var title = Localize.translate('Promo code %{val} has been applied',{val: $scope.promoCode});
                var credits = response.amount ? (Math.round(response.amount*100))/100 : 0;
                var creditsString =   commonInfoFactory.formatPrice(credits,$scope.plan.currency);
                var msg = Localize.translate('You will receive %{credits} promo credits. Your updated total order amount is %{orderamount}.',{credits: creditsString, orderamount: $scope.orderTotalStr});
                new confirmAlert($scope,$modal,'myModalConfirmAlert.html',title,msg,Localize.translate("Buy"),Localize.translate("Cancel"),function(){
                    applyPromoAndMakePayment();
                }).open('sm');

            }
        }).error(function(data,status){
            $scope.viewLoading = false;
            if(implicitPromoCodeValidation){
                $scope.isPaymentInProgress = false;
                $scope.formSubmitErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            }else{
                $scope.promoCodeErrorStr = commonInfoFactory.getLocalizedErrorMsg(data,status);
            }

        });
    }

    function preparePromoCodeRequest(validationMode){
        $scope.promoCode = $scope.promoCode.toUpperCase();
        return {
            "type": "ApplyCode",
            "code": $scope.promoCode,
            "currency": $scope.plan.currency,
            "cartAmount": $scope.plan.price,
            "validationMode": validationMode

        };
    }

    $scope.removePromoCode = function(){
        $scope.appliedPromocode = undefined;
        $scope.promoCode = '';
        $scope.showPromoInput = true;
        sessionStorage.removeItem('appliedPromo');
        $scope.clearErrorMsg();
        calculateOrderDetails($scope.accountPayStatus, $scope.applePayAvailable);
    }

    $scope.clearErrorMsg = function () {
        $scope.promoCodeErrorStr = '';
        $scope.formSubmitErrorStr = '';
    }

}]);

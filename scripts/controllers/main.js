'use strict';

angular.module('gigSkyWebApp').controller('MainCtrl', ['$scope','authFactory','$modal','commonInfoFactory','$rootScope','$window','SimManagerFactory','$location','$analytics','$route','referralService',function ($scope,authFactory,$modal,commonInfoFactory,$rootScope,$window,simMgrFactory,$location,$analytics,$route,referralService) {

    $scope.replaceSimLabel = Localize.translate("REPLACE SIM");
    $scope.activateSimLabel = Localize.translate("Activate SIM");
    $scope.activeSIM = null;


    $rootScope.pageRefresh = function(page)
    {
        var path = $location.path();
        if(path.indexOf(page)!=-1)
        {
            $rootScope.$broadcast('onPageRefresh');
        }
    }

    $scope.retryPageLoad = function () {
        $rootScope.pageLoadingFailed = false;
        $rootScope.servicesDown = false;
        $location.path($location.path());
        $route.reload();
    };

    function isTextInput(node) {
        return ['INPUT', 'TEXTAREA'].indexOf(node.nodeName) !== -1;
    }

    //to dismiss the keyboard when we click outside incase of ios
     document.addEventListener('touchstart', function(e) {
       if (!isTextInput(e.target) && e.target.nodeName != "BUTTON" && isTextInput(document.activeElement)) {
           //alert( "target::" + e.target.nodeName + "activeElement::" + document.activeElement );
           document.activeElement.blur();
        }
    }, false);


    $(".navbar-toggle").on('click',function (event){
        $(".navbar-toggle > i").toggleClass("fa-bars");
        $(".navbar-toggle > i").toggleClass("fa-close");
        $(".custPanel").toggle(400,function() {
            $(".custPanel").animate({width:"100%",display:"none",opacity: "1"},100, "linear" );

        });
        event.stopPropagation();
    });

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.isCollapsed = false;
//    $scope.countryListObject = [];
//    $scope.countryList = sessionStorage.getObj("countryList") ;
//    if ($scope.countryList == null ){
//        authFactory.getCountryList()
//            .success(function (result) {
//                $scope.countryList = result.list;
//                //console.log("length is " +$scope.countryList.length);
//                for (var i=0;i<result.list.length;i++){
//                    $scope.countryListObject.push({
//                        "name":result.list[i].name,
//                        "code":result.list[i].code
//                    });
//                }
//                sessionStorage.setObj("countryList",$scope.countryListObject) ;
//            })
//            .error(function () {
//                console.log("error occured in country List");
//            });
//    }


    // $scope.$on('onTransactionComplete',function(data,trresponse){
    //     var planName = trresponse.networkGroupInfo.networkGroupName;
    //     var dataDes = commonInfoFactory.formatPlanDataSize(trresponse.networkGroupPlan);
    //     new confirmAlert($scope,$modal,'myModalConfirmAlert.html',Localize.translate('Payment Successful'),Localize.translate('%{val1} of data for use in %{val2} has been applied to your GigSky account.',{val1 : dataDes,val2 : planName}),Localize.translate("OK")).open('sm');
    // });

    $rootScope.openPage = function(url){
        $window.location = url;
    };

    $rootScope.openShopSIM = function(url){
        $analytics.eventTrack('buy SIM Step1',{category: 'behavior' , label : $rootScope.currentLangCode});
        $window.location = url;

    };

    $rootScope.openPageInNewTab = function(url){
        $window.open(url,'_blank');
    };

    $rootScope.openGSView = function(url){
        $location.path(url);
    };
    $scope.isCollapsed4 = false;
    $scope.isCollapsed3 = false;
    $scope.isCollapsed2 = false;
    $scope.$on('onLogOut',function(){
        //Reset menu state on logout..
        $scope.isCollapsed4 = false;
        $scope.isCollapsed3 = false;
        $scope.isCollapsed2 = false;
    });

    $scope.isActiveSIM = function(sim)
    {
        var activeSIM = simMgrFactory.getActiveSIM();

        //console.log( "sim::");
        //console.log( sim.simId  + " active SIM::" + activeSIM.simId );

        if( activeSIM && sim.simId == activeSIM.simId) {

            return true;
        }
        return false;
    };

    $scope.updateCountryList = function(scope,errorField)
    {
        var countryList = commonInfoFactory.getStoredCountryList();
        if(countryList instanceof Array)
        {
            $scope.countryList = countryList;
        }
        else
        {
            countryList.success(function(){
                $scope.countryList = commonInfoFactory.countryList;
            }).error(function(data,status){
                    scope[errorField] = commonInfoFactory.getLocalizedErrorMsg(data,status);
                });
        }
    }
    $scope.updateCurrencyList = function(scope,errorField)
    {
        commonInfoFactory.getCurrencyList().then(function(result){
            var currencyList = result.data.list;
            if(currencyList instanceof Array)
            {
                $scope.currencyList = currencyList;
            }
        },function(data, status){
            scope[errorField] = commonInfoFactory.getLocalizedErrorMsg(data,status);
        });
    }
    $scope.onChangeSIM = function(sim ){

        var activeSIM = simMgrFactory.getActiveSIM();
        var redirectToDashboard = true;

        if(!sim) //No sims are available.. hence simply change to dashboard..
        {
            if($location.path() != "/dashboard")
            {
                $location.path("/dashboard");
                $location.search({});
            }
            return;
        }

        //simMgrFactory.getSIMDetails( sim.simId).success( function( response ){
            if( sim.visibilityStatus === "hidden" ) {

                $rootScope.editSIM = sim;


                var msg = 'Unhide the SIM from the SIM Details screen to make it visible in "My SIMs" list.';
                //if ($(window).width() >= 960) {
                //    msg = "Unhide the SIM to make it visible in "My SIMs" list";
                //}


                new confirmAlert($scope, $modal, 'myModalConfirmAlert.html', null, Localize.translate(msg), Localize.translate('OK'), null, function () {
                    //if ($(window).width() < 960)
                    {
                        //redirect to sim details page
                        // show unhide button
                        $location.path('/editSIM');
                        $location.search({id: sim.simId});
                        redirectToDashboard = false;
                        return false;
                    }
                }).open('sm');

            }
            else {

                if( activeSIM && activeSIM.simId == sim.simId ) {
                    if ($location.path() != "/dashboard") {
                        $location.path("/dashboard");
                        $location.search({});
                    }
                    return;
                }
                else {

                    simMgrFactory.setActiveSIM(sim);
                    $location.path("/dashboard");
                    $location.search({});
                }

            }

       // });


    };
    $rootScope.$on("$locationChangeSuccess", function(event, next, current) {
        $(document).scrollTop(0);
        var p = $location.path();
        $rootScope.isBorderDisabled = false;
        if(!isSessionAuthRequired())
        {
            $rootScope.isBorderDisabled = true;
        }
    });
//
//    $scope.$on('$routeChangeSuccess', function(){
//        $('.viewContainer').show();
//    });
//
//    $scope.$on('$routeChangeStart', function(){
//        $('.viewContainer').hide();
//    });

    function initNonAccountPages()
    {
        $rootScope.showNav=false;
        $rootScope.showOnlyHeader=true;
        $rootScope.showHeaderNav=false;
        $rootScope.showOnlyLogout=false;
        $rootScope.hideHeader = false;
    }

    function initAccountPages()
    {
        $rootScope.showNav=true;
        $rootScope.showOnlyHeader=false;
        $rootScope.showHeaderNav=true;
        $rootScope.showOnlyLogout=false;
        $rootScope.hideHeader = false;
    }

   /*$scope.$watch(function(){return Localize.translate('GigSky Account Login'); },function(newval){
        if(newval !='lt_gigsky_app_title')
            $("#titleofpage").html(newval);
    })*/

    $rootScope.getSessionInfo = function(){
        var qp = "?sl="+$rootScope.currentLangCode+"&wv=1";
        if(isSessionAuthRequired() && authFactory.isSessionValid())
        {
            qp += "&ls=true";
        }
        else
            qp += "&ls=false";

        return qp;
    }

    function isSessionAuthRequired(){
        var path = $location.path();
        if (path=="" || (path.indexOf("/login")!=-1) ||(path.indexOf("/signUp")!=-1) || (path.indexOf("/forgotPasswd")!=-1) || (path == "/sendactlink") ||
            (path.indexOf("/activate")!=-1 ) ||
            (path.indexOf("/reset")!=-1 ) ||
            (path.indexOf("/confirmemail")!=-1 ) ||
            (path.indexOf("/unblock")!=-1 ) ||  (path.indexOf("/concur-connected")!=-1 ) || (path.indexOf("/acceptTerms")!=-1) ||
            (path.indexOf("/simReplace")!=-1 ) || (path.indexOf("/confirmDoNotReplace")!=-1) || (path.indexOf("/forceChangePassword")!=-1) || (path.indexOf("/simReplaceAddress")!=-1)
            || (path.indexOf("/marketing")!=-1) || (path.indexOf("/moerror")!=-1) || (path.indexOf("/cancel-service-confirmation")!=-1)
            )
        {
            return false;
        }
        return true;
    }


    $rootScope.$on("$locationChangeStart", function(event, next, current) {

        $rootScope.pageLoadingFailed = false;
        $rootScope.servicesDown = false;
        if (!isSessionAuthRequired()){
            initNonAccountPages();
            var path = $location.path();
            if (path == "/signUpConfirm") {
                $rootScope.showOnlyHeader = false;
            }else if((path == "/simReplace") || (path == "/confirmDoNotReplace") || (path == "/simReplaceAddress") || path == "/acceptTerms"){
                $rootScope.showOnlyHeader = false;
                $rootScope.showOnlyLogout = true;
            }else if(path == '/forceChangePassword'){
                $rootScope.showOnlyHeader = false;
            }else if(path === '/marketing'){
                $rootScope.hideHeader = true;
            }

        }
        else{
            if(!authFactory.isSessionValid())
            {
                initNonAccountPages();
                $location.path('/login');
                $location.replace();
                return;
            }
            initAccountPages();
            if ($location.path() == "/dashboard")
            {
                simMgrFactory.initialize();
            }
        }
    });

    $scope.$on('onActiveSIMAvailable',function(){
        $scope.simAvailable = true;
        $scope.activeSIM = simMgrFactory.getActiveSIM();
    });

    $scope.$on('onActiveSIMChanged',function(){
       $scope.simAvailable = true;
       $scope.activeSIM = simMgrFactory.getActiveSIM();
    });

    $scope.$on('onNoSIMAvailable',function(){
        $scope.simAvailable = false;
        $scope.activeSIM = null;

    });
    simMgrFactory.initialize();


  }]);
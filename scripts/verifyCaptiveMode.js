/**
 * Created by jagadishdande on 12/19/16.
 */
/**
 * Created by jagadish on 17/02/16.
 */

(function checkCaptiveMode()
{
    var request = new XMLHttpRequest();
    if(window.location.hostname.indexOf("app.gigsky.com")!=-1) {
        request.open("HEAD", "https://cdn-prod.gigsky.com/captivemode.png", true);
        request.send();
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 0)
                {
                    var curURL = window.location.href;
                    var pos = curURL.indexOf(".com");
                    var path = curURL.substring(pos+4);
                    if(path && path.length>4)
                        window.location.href = "https://captive.gigsky.com"+path;
                    else
                        window.location.href = "https://captive.gigsky.com";
                }
            }
        }
    }
})();
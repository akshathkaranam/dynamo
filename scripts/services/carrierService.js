/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 04/07/14
 * Time: 3:48 PM
 * To change this template use File | Settings | File Templates.
 */
'use strict';

angular.module('gigSkyWebApp').factory('carrierManagerFactory',['$rootScope','$http','SimManagerFactory','commonInfoFactory','$q','authFactory',function($rootScope,$http,simMgrFactory,commonInfoFactory,$q, authFactory){

    var carrierMgrFactory = {};

    var gsmaImsiStatusMap = {INACTIVE: 'Inactive', ACTIVE: 'Active' };

    carrierMgrFactory.getCarrierInfo = function(carrierId){
        var def = $q.defer();
        var defPromise = commonInfoFactory.extendHttpPromise(null,null,def);

        $http.get(GS_NON_ACCOUNT_API_BASE+"networkGroups/"+carrierId).success(function(nwGroupInfo){
            if(!nwGroupInfo.countryList && nwGroupInfo.countryCodes)
            {
                commonInfoFactory.getCountryListByCodes(nwGroupInfo.countryCodes).success(function(response){
                     nwGroupInfo.countryList = response.countryList;
                     def.resolve({data:nwGroupInfo});
                }).error(function(data,status){
                     def.reject({data:data,status:status});
                });
            }
        }).error(function(data,status){
            def.reject({data:data,status:status});
        });

        return def.promise;

    };

    carrierMgrFactory.getCarrierInfoDetailed = function(carrierId)
    {
        return $http.get(GS_NON_ACCOUNT_API_BASE+"networkGroups/"+carrierId+"/details");
    };

    carrierMgrFactory.getCarrierPlans = function(carrierId,currency,simid){
        var activeSIM = simMgrFactory.getActiveSIM();
        var url = GS_NON_ACCOUNT_API_BASE+"networkGroups/"+carrierId+"/plans?includeFreePlan=true";

        if(!simid && activeSIM)
            simid = activeSIM.simId;

        if(simid)
            url += "&simId="+simid;

        if(currency)
            url += "&currency="+currency;
        return $http.get(url);
    };

    carrierMgrFactory.getForbiddenCountries = function(){
        var activeSIM = simMgrFactory.getActiveSIM();
        var simType = (activeSIM)?activeSIM.simType:false;
        if(simType)
        {
            var url= GS_NON_ACCOUNT_API_BASE+"countries/forbidden/buySameCountry?startIndex=0&count=10000&type=" + simType;
            var countries = JSON.parse(sessionStorage.getItem(simType+"-forbiddencountries"));
            if(countries)
            {
                return commonInfoFactory.extendHttpPromise(countries);
            }
            return $http.get(url).success(function(countries){
                sessionStorage.setItem(simType+"-forbiddencountries",JSON.stringify(countries));
            });
        }else{
            return commonInfoFactory.extendHttpPromise({"type":"ForbiddenCountryList","startIndex":0,"count":0,"totalCount":0,"simType":"GIGSKY_SIM","list":[]});
        }

    };

    carrierMgrFactory.getAvailableCarriers = function(){
        var activeSIM = simMgrFactory.getActiveSIM();
        var url = GS_API_BASE+"SIM/"+activeSIM.simId+"/availableNetworkGroups";
        return $http.get(url);
    };

    carrierMgrFactory.getSubscriptionOfNetwork = function(networkId) {
        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        var activeSIM = simMgrFactory.getActiveSIM();
        if(activeSIM && activeSIM.simId){
            var url = GS_API_BASE+"SIM/"+activeSIM.simId+"/networkgroup/"+networkId+"/subscription?detail=location";
            $http.get(url).success(function (res) {
                updateSubInSessionNetworkDetails(networkId, res);
                def.resolve({data: res});
            }).error(function (err, status) {
                if(err && err.errorInt === 4001){
                    def.resolve({data: null});
                }else{
                    def.reject({data:err,status:status});
                }
            })

        }
        return promise;

    };
    carrierMgrFactory.getAvailableCarriersInCountry = function(countryCode,simType,activeSIM){
        var url = GS_NON_ACCOUNT_API_BASE+"countries/"+countryCode+"?startIndex=0&count=32656";
        if(simType)
            url = url + "&simType="+simType;
        if(activeSIM && activeSIM.simId)
            url = url + "&simId="+activeSIM.simId;

        return $http.get(url);
    };

    carrierMgrFactory.getSupportedCarriersOfNetworkGroup = function(carrierId){

        var url= GS_NON_ACCOUNT_API_BASE+"networkGroups/"+carrierId+"/carriers";
        return $http.get(url);
    };


    carrierMgrFactory.getSupportedCountriesOfNetworkGroup = function(carrierId){

        var url= GS_NON_ACCOUNT_API_BASE+"networkGroups/"+carrierId+"/countries";
        return $http.get(url);
    };

    function updateSubInSessionNetworkDetails(nwId, subscription) {
        var resObj = sessionStorage.getObj(('Network_'+nwId));
        var nwInfo = (resObj && resObj.info) ? resObj.info : {};
        nwInfo.subscription = formatSubscription(subscription);
        sessionStorage.setObj(('Network_'+nwId), {responseTime: (resObj && resObj.responseTime) ? resObj.responseTime : 0, info: nwInfo});
    }

    function savePlanCoverage(nwId, coverage) {
        var resObj = sessionStorage.getObj(('Network_'+nwId));
        var nwInfo = (resObj && resObj.info) ? resObj.info : {};
        nwInfo.coverage = coverage;
        sessionStorage.setObj(('Network_'+nwId), {responseTime: (resObj && resObj.responseTime) ? resObj.responseTime : 0, info: nwInfo});
    }

    carrierMgrFactory.getNetworkDetailsFromSession = function (nwId) {
        var resObj = sessionStorage.getObj(('Network_'+nwId));
        return  (resObj && resObj.info) ? resObj.info : '';
    };

    carrierMgrFactory.getCustomNetworkDetails = function (nwId) {
        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        var reqTime = (new Date()).getTime();
        var resObj = sessionStorage.getObj(('Network_'+nwId));
        if(resObj && ((reqTime - resObj.responseTime)<60000)){
            def.resolve({data:resObj.info});
        }else{
            $q.all([carrierMgrFactory.getCarrierInfo(nwId), carrierMgrFactory.getSubscriptionOfNetwork(nwId), commonInfoFactory.getCountryList()]).then(
                function (res) {
                    var gsCountries = res[2].data;
                    var networkInfo =  {
                        details: formatNetworkDetails(res[0].data, null, gsCountries),
                        subscription: formatSubscription(res[1].data),
                        coverage: prepareCoverageCountryList(res[0].data, gsCountries)
                    };
                    sessionStorage.setObj(('Network_'+nwId), {responseTime: reqTime, info: networkInfo});
                    def.resolve({data: networkInfo});
                },
                function (err, status) {
                    def.reject({data:err,status:status});
                }
            );
        }

        return promise;

    };

    carrierMgrFactory.getCustomPlans = function (nwId, simId) {
        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);
        var bChangeInCurrency = false;
        var userCurrency = sessionStorage.getItem("userCurrency");

        var reqTime = (new Date()).getTime();
        var resObj = sessionStorage.getObj(('Plans_'+nwId));
        var bChangeInCurrency = (resObj && resObj.plans && resObj.plans.length && userCurrency)?(resObj.plans[0].currency!=userCurrency):false;
        if(resObj && ((reqTime - resObj.responseTime)<60000) && !bChangeInCurrency){
            def.resolve({data:resObj.plans});
        }else{

            carrierMgrFactory.getCarrierPlans(nwId, null, simId).success(function (res) {
                var plans = res.list;
                sessionStorage.setObj(('Plans_'+nwId), {responseTime: reqTime, plans: plans});
                def.resolve({data: plans});
            }).error(function (err, status) {
                def.reject({data:err,status:status});
            });
        }

        return promise;

    };

    carrierMgrFactory.getAdvancedNetworkDetails = function (nwId) {
        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        var activeSIM = simMgrFactory.getActiveSIM();
        var simId = activeSIM.simId;

        $q.all([carrierMgrFactory.getCustomNetworkDetails(nwId), carrierMgrFactory.getCustomPlans(nwId, simId)]).then(
            function (res) {
                var nwInfo = res[0].data ? res[0].data : {};
                nwInfo.plans = formatPlans(res[1].data);
                if(!nwInfo.plansNotSupported)
                {
                    nwInfo.plansNotSupported = !nwInfo.plans || nwInfo.plans.length ==0
                }
                def.resolve({data: nwInfo});
            }, function (err, status) {
                def.reject({data:err,status:status});
            }
        );

        return promise;
    };

    carrierMgrFactory.getSubscriptionInLocation = function(loc) {
        var activeSIM = simMgrFactory.getActiveSIM();
        if(activeSIM && activeSIM.simId){
            var url = GS_API_BASE+"SIM/"+activeSIM.simId+"/countries/"+loc+"/subscriptions?excludeInvalid=true";
            return $http.get(url);
        }
        else
        {
            return commonInfoFactory.extendHttpPromise({},true);
        }

    };

    carrierMgrFactory.activateSubscription = function (subscriptionId,simId) {
        var activeSIM = simMgrFactory.getActiveSIM();

        if((!activeSIM || !activeSIM.simId) && simId)
        {
            activeSIM = {simId:simId};
        }

        if (activeSIM) {
            var url = GS_API_BASE + "SIM/" + activeSIM.simId +
                "/subscription/" + subscriptionId + "/activate";
            return $http.put(url);
        }
        else {
            console.log("Active SIM is null");
            return null;
        }
    };
    
    function formatCustomLocationDetails(availableNw) {
        var availableNetworks = [];

        for(var i=0; i<availableNw.availableNwIds.length; i++){
            var nwId = availableNw.availableNwIds[i];
            var network = sessionStorage.getObj(('Network_'+nwId));
            if(!network || (network && !network.info)){
                return null;
            }
            availableNetworks.push(network.info);
        }

        return {isForbidden: availableNw.isForbidden, availableNetworks: availableNetworks};
    }

    function getCustomLocationDetailsFromSession(loc) {
        var resObj = sessionStorage.getObj(('AvailNw_'+loc));
        if(resObj){
            var reqTime = (new Date()).getTime();
            var resTime = resObj.responseTime;

            return ((reqTime - resTime)<60000) ? formatCustomLocationDetails(resObj) : null;
        }
        return resObj;
    }

    function setCustomLocationDetailsInSession(loc, isForbidden, networks, availableNwIds) {
        var resTime = new Date().getTime();
        var details = {responseTime: resTime, isForbidden: isForbidden, availableNwIds: availableNwIds};

        sessionStorage.setObj(('AvailNw_'+loc), details);

        for(var i=0; i<availableNwIds.length; i++){
            sessionStorage.setObj(('Network_'+availableNwIds[i]), {responseTime: resTime, info: networks[availableNwIds[i]]});
        }
    }

    /**
     * add
     * format network
     * format subscription
     * plan coverage
     * monitor
     *
     */
    carrierMgrFactory.getCustomLocationDetails = function (loc) {

        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        var sessionResponse = getCustomLocationDetailsFromSession(loc);
        if(sessionResponse){
            def.resolve({data:sessionResponse});
            return promise
        }

        var userCountryOfResidence = sessionStorage.getItem("userCountryCode");

        var activeSIM = simMgrFactory.getActiveSIM();
        var simType = (activeSIM)? activeSIM.simType : null;

        $q.all([carrierMgrFactory.getForbiddenCountries(), carrierMgrFactory.getAvailableCarriersInCountry(loc,simType,activeSIM),carrierMgrFactory.getSubscriptionInLocation(loc), commonInfoFactory.getCountryList()]).then(
            function (res) {

                var gsCountries = res[3].data;
                var forbiddenCountries = res[0].data.list;
                var isForbidden = commonInfoFactory.isCountryForbidden(userCountryOfResidence, [loc], forbiddenCountries);

                var networks = {};

                var availableNwRes = res[1].data.list;
                for(var i=0; i<availableNwRes.length; i++){
                    var network = availableNwRes[i];
                    var nwId = network.networkGroupId;

                    networks[nwId]  = networks[nwId] ? networks[nwId] : {};
                    networks[nwId].details = formatNetworkDetails(network, loc, gsCountries);
                    networks[nwId].coverage = prepareCoverageCountryList(network, gsCountries);
                }

                var subscriptionRes = res[2].data.list;
                for(var j=0; j<subscriptionRes.length; j++){
                    var subscription = subscriptionRes[j];
                    var nw = subscription.networkGroupInfo ? subscription.networkGroupInfo.networkGroupId : null;
                    if(!nw){
                        continue;
                    }

                    networks[nw]  = networks[nw] ? networks[nw] : {};
                    networks[nw].subscription = formatSubscription(subscription);
                }

                var availableNwIds = carrierMgrFactory.formatAvailableNetworkIds(networks);
                var availableNetworks = carrierMgrFactory.formatAvailableNetworks(networks, availableNwIds);

                setCustomLocationDetailsInSession(loc, isForbidden, networks, availableNwIds);

                def.resolve({data: {isForbidden: isForbidden, availableNetworks: availableNetworks}});
            },
            function (err, status) {
                def.reject({data:err,status:status});
            }
        );

        return promise;
    };

    function updateNetworkLogo(nwInfo, loc){
        if(!nwInfo){
            return;
        }

        nwInfo.imgPath = null;

        if (nwInfo.networkGroupType === "COUNTRY")
            nwInfo.imgPath = GS_SERVER_BASE + '/' + nwInfo.logoUrl;
        else if (loc) {
            var imgPath = nwInfo.logoUrl;
            imgPath = imgPath.replace(/country_logos_v3_square.*png$/, "country_logos_v3_square/"+loc + ".png");
            nwInfo.imgPath = GS_SERVER_BASE + '/' + imgPath;
        }
    }

    function updateCurrentCountryLabel(nwInfo, loc, gsCountries){
        if(nwInfo && loc && loc.length) {
            nwInfo.currentCountryName = commonInfoFactory.getCountryByName(loc, gsCountries);
            nwInfo.countryPlanSubText = " + " + Localize.translate("Other Countries");
        }
    }

    function prepareCoverageCountryList(networkInfo, gsCountries) {
        if(!networkInfo.countryCodes){
            return '';
        }

        var countryList = commonInfoFactory.getCountryNamesFromCodes(networkInfo.countryCodes, gsCountries);

        var countryServiceDisabledMap = {};

        var serviceDisabledCountries = networkInfo.serviceDisabledCountries;

        if(serviceDisabledCountries){
            for(var j=0; j<serviceDisabledCountries.length; j++){
                countryServiceDisabledMap[serviceDisabledCountries[j]] = true;
            }
        }

        countryList.sort(function(a,b){
            var n1 = a.name.toUpperCase();
            var n2 = b.name.toUpperCase();
            return n1.localeCompare(n2);
        });

        var coverage = countryList[0].name + ((countryServiceDisabledMap[countryList[0].code]) ? ' ('+ Localize.translate('temporarily unavailable')+')' : '');
        for(var i=1;i<countryList.length;i++)
        {
            var country = countryList[i];
            coverage += " | "+ country.name + ((countryServiceDisabledMap[country.code]) ? ' ('+ Localize.translate('temporarily unavailable')+')' : '');
        }

        return coverage;
    }

    function formatNetworkDetails(nwInfo, loc, gsCountries) {
        if(!nwInfo){
            return {};
        }
        loc = loc ? loc : authFactory.getCurrentLocation(); //todo: handle properly for other countries

        updateCurrentCountryLabel(nwInfo, [loc], gsCountries);
        updateNetworkLogo(nwInfo, loc);
        nwInfo.plansNotSupported = isPlansNotSupported(loc,nwInfo);
        nwInfo.isCountryPlan = (nwInfo.networkGroupType === "COUNTRY");

        return nwInfo;
    }

    function isPlansNotSupported(location, networkInfo) {
        var loc;
        var notSupported = false;
        if(!location)
            return notSupported;

        location = location.toString();
        if(location.indexOf(",")===-1)
        {
            loc = location;
        }
        else {
            if(location.indexOf("US")!==-1)
                loc = "US";
            else {
                var locations = location.split(",");
                loc = locations[0];
            }
        }

        var serviceDisabledCountries = networkInfo.serviceDisabledCountries;
        if(serviceDisabledCountries && serviceDisabledCountries.length){
            for(var i=0; i<serviceDisabledCountries.length; i++){
                if(loc === serviceDisabledCountries[i]){
                    notSupported = true;
                    break;
                }
            }
        }
        return notSupported;
    }


    function formatSubscription(subscription) {
        if(!subscription){
            return null;
        }

        var dataRemaining = commonInfoFactory.formatBalanceDataSize(subscription);// + "MB";
        var daysRemaining = commonInfoFactory.formatSubRemainingTime(subscription);//.balanceInDays + " days left";
        subscription.displayableInfo = dataRemaining+" | "+daysRemaining;

        if (Localize.translate("Expired") === daysRemaining) {
            subscription.displayableInfo = dataRemaining + " | " + daysRemaining;
            dataRemaining = null;
        }
        subscription.timeStamp = "";
        if (subscription.balanceAsOf)
            subscription.timeStamp = Localize.translate('(Updated: %{val})', {val: commonInfoFactory.setDate(subscription.balanceAsOf, true)});

        subscription.displayableProfile = null;
        if(subscription.imsiInfoList && subscription.imsiInfoList.list && subscription.imsiInfoList.list.length && dataRemaining){
            var imsiInfo = subscription.imsiInfoList.list[0];
            var imsiStatus = (imsiInfo.gsmaImsiStatus)? imsiInfo.gsmaImsiStatus : null;
            var displayableImsiStatus = (imsiStatus && gsmaImsiStatusMap[imsiStatus])? Localize.translate(gsmaImsiStatusMap[imsiStatus]) : '';
            subscription.displayableProfile = Localize.translate('Profile: %{val}', {val: (imsiInfo.profileName)});
        }



        return subscription;
    }

    function formatPlans(plans) {
        var planGroups = [];

        if (plans && plans.length !== 0) {
            planGroups = [[plans[0]]];
            plans[0].position = 0;
            for (var i = 1; i < plans.length; i++) {
                var plan = plans[i];
                plan.position = i;
                var foundInPreviousGroups = false;
                for (var j = 0; j < planGroups.length; j++) {
                    var planGroupValidity = planGroups[j][0].validityPeriodInDays;
                    if (planGroupValidity == plan.validityPeriodInDays) {
                        planGroups[j].push(plan);
                        foundInPreviousGroups = true;
                        break;
                    }
                }
                if (!foundInPreviousGroups) {
                   planGroups.push([plan]);
                }
            }
        }

        return planGroups;
    }

    function compareNetworkType(nw1, nw2) {
        return (nw1 && nw1.networkGroupType === 'COUNTRY') ? -1 : ((nw2 && nw2.networkGroupType === 'COUNTRY')? 1 : ((nw1 && nw1.networkGroupType === 'REGIONAL') ? -1 : ((nw2 && nw2.networkGroupType === 'REGIONAL')? 1 : -1)));
    }

    carrierMgrFactory.formatAvailableNetworkIds = function(networks) {
        return Object.keys(networks).sort(function (n1, n2) {
            var nw1 = networks[n1];
            var nw2 = networks[n2];

            return (nw1.subscription && nw1.subscription.subscriptionStatus == 'IN-USE') ? -1 : (nw2.subscription && nw2.subscription.subscriptionStatus == 'IN-USE') ? 1 : compareNetworkType(nw1.details, nw2.details);
        });
    }

    carrierMgrFactory.formatAvailableNetworks = function(networks, availableNwIds) {

        var availableNetworks = [];

        for(var i=0; i<availableNwIds.length; i++){
            availableNetworks.push(networks[availableNwIds[i]])
        }

        return availableNetworks;

    }

    return carrierMgrFactory;


}]);

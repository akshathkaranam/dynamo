'use strict';

//var GS_API_BASE= "https://staging.gigsky.com/api/v3/";
//var GS_API_BASE = "http://54.241.30.200/api/v3/";	 
angular.module('gigSkyWebApp').factory('authFactory',['$location','$http','$rootScope','$q','commonInfoFactory','referralService',function ($location,$http,$rootScope,$q,commonInfoFactory,referralService){
		var factory ={};
		//console.log("config is " +  config);

        factory.clearSession = function()
        {
            /*sessionStorage.removeItem('customerId');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('loginMode');
            sessionStorage.removeItem('userEmail');
            sessionStorage.removeItem('userCountryCode');
            sessionStorage.removeItem('userCurrency');
            sessionStorage.removeItem('tPinStatus');
            sessionStorage.removeItem('activationStatusAlert');
            sessionStorage.removeItem("replacementBeginDate");
            sessionStorage.removeItem("planPurchaseEndDate");
            sessionStorage.removeItem("serviceEndDate");
            sessionStorage.removeItem("sim1ListToReplace");
            sessionStorage.removeItem('billingId');
            sessionStorage.removeItem('billingId');*/
            sessionStorage.clear();

        }

        factory.clearEIDDetails =function(){
            localStorage.clear();
        }

        factory.sendMONotification = function(payload){
            try {
                if (DataMart && DataMart.notifyPurchaseResult) {
                    DataMart.notifyPurchaseResult(JSON.stringify(payload));
                }
            }
            catch(e) {
                console.error("Error While Sending MO Notification "+e);
                return "Unable to initiate profile download, Please do contact customer support team for further assistance.";
            }

            return null;

        }


        factory.setICCIDList = function(iccidList){
            localStorage.setItem("iccids",iccidList);
        }
        factory.getICCIDList = function(){
            return localStorage.getItem("iccids");
        }
        factory.setCurrentLocation = function(loc){
            if(loc)
            {
                loc = loc.toUpperCase();
            }
            localStorage.setItem("currentLoc",loc);
        }
        factory.getCurrentLocation = function(){
            return localStorage.getItem("currentLoc");
        }

        factory.setDeviceId = function(deviceId){
            localStorage.setItem("deviceId",deviceId);
        }
        factory.setMOTransactionId = function(trId){
            localStorage.setItem("moTrId",trId);
        }
        factory.getMOTransactionId = function(){
            return localStorage.getItem("moTrId");
        }
        factory.getDeviceId = function(){
            return localStorage.getItem("deviceId");
        }
        factory.isSessionValid = function()
        {
            if(sessionStorage.getItem("token"))
                return true;
            else
                return false;
        }
		factory.getCountryList= function (){
			return $http.get(GS_NON_ACCOUNT_API_BASE+'countries/all?startIndex=0&count=32768');
		};

        factory.getUserEmailId = function(){
            return sessionStorage.getItem('userEmail');
        }

        factory.setUserEmailId = function(emailId){
            return sessionStorage.setItem('userEmail',emailId);
        }

        factory.getUserId = function()
        {
            return sessionStorage.getItem('customerId');
        }
        factory.isTokenValid = function(token,tokenType){
            return $http.get(GS_NON_ACCOUNT_API_BASE+"account/confirmToken/status?token="+token+"&tokenType="+tokenType);
        }
        factory.activateUserOrConfirmEmail = function(token,isConfirmEmail){
            this.clearSession();
            var successFn = function(response){
                factory.setLoginMode('gigsky');
                if(response){
                    $http.defaults.headers.common.Authorization = "Basic "+response.data.token;
                    sessionStorage.setItem("token",response.data.token);
                    sessionStorage.setItem("customerId",response.data.customerId);
                    GS_API_BASE = GS_SERVER_BASE + GS_API_VERSION + "account/"+response.data.customerId+"/";
                    $rootScope.$broadcast('onLogIn');
                }
                else{
                    $http.defaults.headers.common.Authorization = null;
                    sessionStorage.removeItem("token");
                }
            };
            var errorFn = function(response){
                $http.defaults.headers.common.Authorization = null;
                sessionStorage.removeItem("token");
            }

            var req = $http.put(GS_NON_ACCOUNT_API_BASE+"account/confirm/"+(isConfirmEmail?'emailId':'account')+"?token="+token);
            req.then(successFn,errorFn);
            return req;
        }

        factory.confirmPwdReset= function (token,newPassword){
            var putReq = $http.put(GS_NON_ACCOUNT_API_BASE+'account/confirm/password?token='+token,{newPassword:newPassword});
            return putReq;
        };


		factory.signIn= function (emailId ,password,country,referralCode){
            var signInData={type:"AccountDetails", actCreationType: "GIGSKY",emailId:emailId,password:password,country:country, tncAndPPAcceptanceStatus: 'AGREE'};
            if(referralCode && referralCode.length>4){
                signInData.referralCode=referralCode;
            }
			return $http.post(GS_NON_ACCOUNT_API_BASE+'account',signInData);
		};

        factory.signUpWithCaptcha= function (emailId ,password,country,code,referralCode){

            var signInData={type:"AccountDetails", actCreationType: "GIGSKY",emailId:emailId,password:password,country:country,code:code, tncAndPPAcceptanceStatus: 'AGREE'};
            if(referralCode && referralCode.length>4){
                signInData.referralCode=referralCode;
            }
            var destination_url = "createaccount.php?des=" + encodeURI(GS_NON_ACCOUNT_API_BASE);
            return $http.post(destination_url,signInData);
        };


    /*factory.login= function (emailId ,password){
            factory.clearSession();
			var loginData= {type:"AccountLogin",loginType: "NORMAL",emailId:emailId,password:password};

          // var loginReq = $http.post(GS_NON_ACCOUNT_API_BASE+'account/login', loginData);
		    var loginReq =commonInfoFactory.retryRequest(GS_NON_ACCOUNT_API_BASE+'account/login','POST', loginData);
            var successFn = function(response){
                factory.setLoginMode('gigsky');
                if(response){
                    $http.defaults.headers.common.Authorization = "Basic "+response.token;
                    sessionStorage.setItem("token",response.token);
                    sessionStorage.setItem("customerId",response.customerId);
                    GS_API_BASE = GS_SERVER_BASE + GS_API_VERSION + "account/"+response.customerId+"/";
                    $rootScope.$broadcast('onLogIn');
                }
                else{
                    $http.defaults.headers.common.Authorization = null;
                    sessionStorage.removeItem("token");
                }
            };
            var errorFn = function(response){
                $http.defaults.headers.common.Authorization = null;
                sessionStorage.removeItem("token");
            };
            loginReq.success(successFn).error(errorFn);
            return loginReq;
		};*/

    factory.login = function (emailId, password, isReferralFlow) {
        factory.clearSession();
        var loginData= {type:"AccountLogin",loginType: "NORMAL",emailId:emailId,password:password};

        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        var loginReq =commonInfoFactory.retryRequest(GS_NON_ACCOUNT_API_BASE+'account/login','POST', loginData);
        var successFn = function(loginResponse){
            factory.setLoginMode('gigsky');
            if(loginResponse){
                $http.defaults.headers.common.Authorization = "Basic "+loginResponse.token;
                sessionStorage.setItem("token",loginResponse.token);
                sessionStorage.setItem("customerId",loginResponse.customerId);
                GS_API_BASE = GS_SERVER_BASE + GS_API_VERSION + "account/"+loginResponse.customerId+"/";
                $rootScope.$broadcast('onLogIn');

                (isReferralFlow)?prepareMonitorReferralStatus(loginResponse.customerId,loginResponse) : def.resolve({data:{loginRes:loginResponse}});
            }
            else{
                $http.defaults.headers.common.Authorization = null;
                sessionStorage.removeItem("token");
                def.resolve({data:{loginRes:loginResponse}})
            }
        };
        var errorFn = function(loginErrResponse,status){
            $http.defaults.headers.common.Authorization = null;
            sessionStorage.removeItem("token");
            def.reject({data:loginErrResponse,status:status});
        };


        function prepareMonitorReferralStatus(customerId,loginResponse){
            referralService.getReferralServiceStatus().success(function (response) {
                var referralEnabled = response.referralEnable;
                (referralEnabled)?monitorReferralStatus(customerId,loginResponse): def.resolve({data:{loginRes:loginResponse}});
            }).error(function (error) {
                console.log(error);
                def.resolve({data:{loginRes:loginResponse}});
            });
        }

        function monitorReferralStatus(customerId,loginResponse){


            var onSignUpCreditSuccessListener = $rootScope.$on('onSignUpCreditSuccess',function(data,accResponse){

                commonInfoFactory.retryRequest(GS_API_BASE+'credit/gigskyCredit','GET', null).success(function(creditRes){
                    var totalCreditBalance = creditRes.gigskyBalance;
                    var creditCurrency = creditRes.currency;
                    var totalCreditBal = commonInfoFactory.formatPrice(totalCreditBalance,creditCurrency); //during sign up this totalCredit= sign up credit
                    unRegisterAllEvents();
                    def.resolve({data:{loginRes:loginResponse,refStatus:'SIGNUP_WITH_REFERRAL',gsCredit:totalCreditBal}})
                }).error(function(data,status){
                    unRegisterAllEvents();
                    $rootScope.pageLoadingFailed = true;
                });
            });

            var onInvalidReferralSignUpListener =  $rootScope.$on('onInvalidReferralSignUp',function(data,accResponse){
                unRegisterAllEvents();
                def.resolve({data:{loginRes:loginResponse,refStatus:'SIGNUP_WITH_INVALID_REFERRAL'}});
            });

            var onSignUpCreditFailureListener = $rootScope.$on('onSignUpCreditFailure',function(data,accResponse){
                unRegisterAllEvents();
                def.resolve({data:{loginRes:loginResponse,refStatus:'CREDIT_ADD_IN_PROCESS'}});
            });

            var onReferralSignUpTimeoutListener = $rootScope.$on('onReferralSignUpTimeout',function(data,accResponse){
                unRegisterAllEvents();
                def.resolve({data:{loginRes:loginResponse,refStatus:'CREDIT_ADD_IN_PROCESS'}});
            });

            function unRegisterAllEvents(){
                onSignUpCreditSuccessListener();
                onInvalidReferralSignUpListener();
                onSignUpCreditFailureListener();
                onReferralSignUpTimeoutListener();
            }

            referralService.rsMonitor.monitor(customerId);

        }

        loginReq.success(successFn).error(errorFn);

        return promise;
    };


        factory.linkConcurAccount = function(code){
            var linkData={type:'LinkAccount',authKey:code};
            return $http.post(GS_API_BASE+'thirdparty/concur', linkData);
            //return commonInfoFactory.extendHttpPromise(' ');

        };

        factory.unLinkConcurAccount = function(){

            return ($http['delete'])(GS_API_BASE+'thirdparty/concur');

        };

        factory.getLoginMode  = function(){
            return sessionStorage.getItem('loginMode');
        }

        factory.setLoginMode = function(mode)
        {
            sessionStorage.setItem('loginMode',mode);
        }

		factory.logout= function (){

            var deferredInstance = $q.defer();

            if(factory.getLoginMode() &&  factory.getLoginMode()== 'facebook')
            {
                try
                {
                    FB.getLoginStatus(function(response) {
                        if (response && response.status === 'connected') {
                            FB.logout(function(response) {
                                logoutInternal();
                            });
                        }
                        else
                        {
                            logoutInternal();
                        }
                    },true);
                }
                catch(e)
                {
                    logoutInternal();
                }
            }
            else
            {
                return logoutInternal();
            }

            function logoutInternal(){

                // var instrument = sessionStorage.getItem("newAccount")?'New':'Existing';
                //
                // var notification = {success:false,transactionId:sessionStorage.getItem("mmoTrId"),
                //     purchaseResult:{userAccount:instrument,purchaseInstrument:instrument, line:'None',
                //         moDirectStatus:'ClientError',planName:'' }};
                //
                // authFactory.sendMONotification(notification);


                var logoutReq = $http.post(GS_NON_ACCOUNT_API_BASE+'account/logout');
                var successFn = function(response){
                    $http.defaults.headers.common.Authorization = null;
                    $rootScope.$broadcast('onLogOut');
                    deferredInstance.resolve(response);
                };
                var errorFn = function(response){
                    $http.defaults.headers.common.Authorization = null;
                    $rootScope.$broadcast('onLogOut');
                    deferredInstance.reject(response);
                }
                logoutReq.then(successFn,errorFn);
                return logoutReq;
            }

            return deferredInstance.promise;

		};

        factory.loginUsingFBAuth = function(userid,token)
        {
            factory.clearSession();
            var loginData='{"type":"AccountLogin", "loginType": "USING_FACEBOOK","facebookId":"'+userid+'","facebookToken":"'+token+'"}';
            var loginReq = $http.post(GS_NON_ACCOUNT_API_BASE+'account/login', loginData);
            var successFn = function(response){
                if(response){
                    factory.setLoginMode('facebook');
                    $http.defaults.headers.common.Authorization = "Basic "+response.data.token;
                    sessionStorage.setItem("token",response.data.token);
                    sessionStorage.setItem("customerId",response.data.customerId);
                    GS_API_BASE = GS_SERVER_BASE + GS_API_VERSION + "account/"+response.data.customerId+"/";
                    $rootScope.$broadcast('onLogIn');
                }
                else{
                    $http.defaults.headers.common.Authorization = null;
                    sessionStorage.removeItem("token");
                }
            };
            var errorFn = function(response){
                $http.defaults.headers.common.Authorization = null;
                sessionStorage.removeItem("token");

            }
            loginReq.then(successFn,errorFn);
            return loginReq;
        }

        factory.sendActivationLink = function(emailId)
        {
            var data = '{"type":"AccountActivation","emailId":"'+emailId+'"}';
            return $http.post(GS_NON_ACCOUNT_API_BASE+'account/activation', data);
        }
        factory.logInWithFB = function()
        {
            var deferInstance = $q.defer();
            FB.login(function(response) {
                if(response.status == "connected")
                {
                    // link to GigSky account...
                    factory.userId = response.authResponse.userID;
                    factory.token = response.authResponse.accessToken;

                    factory.loginUsingFBAuth(factory.userId,factory.token).success(function(response){
                            deferInstance.resolve(response);
                    }).error(function(data,status){
                            deferInstance.reject(data,status);
                    });
                }
                else if(response.status == "not_authorized")
                {
                    deferInstance.reject({errorInt:1,userDisplayErrorStr:Localize.translate('Please authorize GigSky application to access Facebook account.')});
                }
                else
                {
                    deferInstance.reject({errorInt:1,userDisplayErrorStr:Localize.translate('Please authorize GigSky application to access Facebook account.')});
                }
            }, {scope: 'basic_info'});
            return deferInstance.promise;
        }

        factory.linkToFBAccount = function()
        {
            var deferInstance = $q.defer();
            FB.login(function(response) {
                if(response.status == "connected")
                {
                    // link to GigSky account...
                    factory.userId = response.authResponse.userID;
                    factory.token = response.authResponse.accessToken;

                    linkToFBAccountInternal(factory.userId,factory.token).success(function(response){
                        deferInstance.resolve(response);
                    }).error(function(data,status){
                            deferInstance.reject(data,status);
                    });
                }
                else if(response.status == "not_authorized")
                {
                    deferInstance.reject({errorInt:1,userDisplayErrorStr:Localize.translate('Please authorize GigSky application to access Facebook account.')});
                }
                else
                {
                    deferInstance.reject({errorInt:1,userDisplayErrorStr:Localize.translate('Please authorize GigSky application to access Facebook account.')});
                }
            }, {scope: 'basic_info'});
            return deferInstance.promise;

            function linkToFBAccountInternal(userId,token){
                var postData='{"type":"FacebookAccountInfo", "userId": "'+userId+'","accessToken":"'+token+'"}';
                return $http.post(GS_API_BASE+'linkedAccounts/facebook',postData);
            }

        }

        factory.unlinkFromFBAccount = function()
        {
            //Hack to clear session token.. Not sure how this is working.
            try
            {
                FB.logout();
                FB.getLoginStatus(function(response) {
                },true);
            }
            catch(e)
            {
            }

            return ($http['delete'])(GS_API_BASE+'linkedAccounts/facebook');
        };

        factory.getAccount= function (){
            return $http.get(GS_API_BASE);
        };
		
		factory.updateAccount= function (firstName,lastName,country,currency,address1,address2,zipCode,phone,state,city){
			var userData={"type":"AccountDetails", "firstName": firstName,"lastName": lastName,"country": country,"address1": address1,"address2":address2,"zipCode":zipCode,"phone": phone,"state":state,"city":city};
			if(ENABLE_CURRENCY_EDIT){
                userData.preferredCurrency = currency;
            }
			return $http.put(GS_API_BASE,userData).success(function(userInfo){
                sessionStorage.setItem("userCountryCode" ,userInfo.country);
                sessionStorage.setItem("userCurrency" ,userInfo.preferredCurrency);
                sessionStorage.setItem("userEmail" ,userInfo.emailId);
            });
		};


		factory.changePassword= function (oldPassword,newPassword){
            var successFn = function(response){
                if(response){
                    $http.defaults.headers.common.Authorization = "Basic "+response.data.token;
                    sessionStorage.setItem("token",response.data.token);
                }
                else{
                    $http.defaults.common.Authorization = null;
                    sessionStorage.removeItem("token");
                }
            };

			var passwordData={type:"AccountPassword",oldPassword:oldPassword,newPassword:newPassword};
            var temptoken = sessionStorage.getItem("temptoken");

            if(!$http.defaults.headers.common.Authorization && temptoken)
                $http.defaults.headers.common.Authorization  = temptoken;

			var putReq = $http.put(GS_API_BASE+'password',passwordData);
            putReq.then(successFn);
            return putReq;
		};
		
        factory.forgotPassword= function (emailId){
			var forgotPasswordData='{"type":"ResetPassword", "emailId": "'+emailId+'"}';
			return $http.post(GS_NON_ACCOUNT_API_BASE+'account/password',forgotPasswordData);
		};

        factory.forgotPasswordWithCaptcha= function (emailId,code){

            var forgotPasswordData='{"type":"ResetPassword", "emailId": "'+emailId+'", "code":"'+code+'"}';
            var destination_url = "forgotpwd.php?des=" + encodeURI(GS_NON_ACCOUNT_API_BASE);
            return $http.post(destination_url,forgotPasswordData);
        };

        factory.changeEmail= function (emailId,password){
            var forgotPasswordData={type:"ChangeEmail", emailId:emailId,password:password};
            return $http.put(GS_API_BASE+'emailId',forgotPasswordData);
        };

        factory.acceptTerms = function () {
            var reqData = {type: 'TnCAndPrivacyPolicy', acceptanceStatus: 'AGREE'};
            return $http.post(GS_API_BASE+'tnc',reqData);
        };

        $rootScope.$on('onLogOut',function(){
            factory.clearSession();
        });

		return factory;
}]);
	
	
	




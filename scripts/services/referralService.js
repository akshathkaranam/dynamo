'use strict';

angular.module('gigSkyWebApp').factory('referralService',['$location','$http','$q','commonInfoFactory','$rootScope','$interval',function ($location,$http,$q,commonInfoFactory,$rootScope,$interval) {

    var referralServiceFr = {
        getReferralServiceStatus:getReferralServiceStatus,
        getReferralAccountDetails:getReferralAccountDetails,
        getActiveSchemeByCountryCode:getActiveSchemeByCountryCode,
        getActiveSchemeByReferralCode:getActiveSchemeByReferralCode,
        validateCode:validateCode,
        applyPromoCode:applyPromoCode
    };

    function getReferralServiceStatus(){
        var defer = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,defer);

        var referralEnabled = JSON.parse(sessionStorage.getItem("isReferralEnabled"));

        if(referralEnabled == null || referralEnabled == undefined){
            $http.get(GS_REFERRAL_API_BASE+'configs').then(function (response) {
                var isReferralEnabled = (response.data && response.data.referralEnable);
                sessionStorage.setItem("isReferralEnabled",isReferralEnabled);
                defer.resolve({data:{referralEnable:isReferralEnabled}});
            }, function (error) {
                console.log(error.data);
                defer.resolve({data:{referralEnable:false}});
            });
        }else{
            defer.resolve({data:{referralEnable:referralEnabled}});
        }

        return promise;
    }

    function getReferralAccountDetails (customerId,cancelPromise) {
        if(cancelPromise){
            return $http.get(GS_REFERRAL_API_BASE+'account/'+customerId,{timeout:cancelPromise});
        }else
            return $http.get(GS_REFERRAL_API_BASE+'account/'+customerId);
    }

    function getActiveSchemeByCountryCode(countryCode){
        return $http.get(GS_REFERRAL_API_BASE+'activeSchemes?country_code='+countryCode);
    }

    function getActiveSchemeByReferralCode(referralCode){

        return $http.get(GS_REFERRAL_API_BASE+'referralCode/'+referralCode);
    }

    /**
     * This API validates the promo code or the referral code passed as input.
     * @param code
     * @param codeType
     * @returns {HttpPromise}
     */
    function validateCode(code, codeType){
        return $http.get(GS_REFERRAL_API_BASE+'validateCode/'+code+'?codeType='+codeType);
    }

    function applyPromoCode(customerId, simId, data){
        return $http.post(GS_REFERRAL_API_BASE+'account/'+ customerId + '/SIM/' + simId + '/applyCode',data);
    }

    referralServiceFr.rsMonitor = {
        monitorInterval:3000,
        maxTries:60,
        pendingQ:[],
        monitor:function(custId){
            var pendingRsItem  = {custId:custId,request:null,nooftries:0,startTime:new Date()};
            var custIdExists = false;
            for(var i=0;i<this.pendingQ.length;i++)
            {
                var pt = this.pendingQ[i];
                if(pt.custId==custId)
                {
                    custIdExists = true;
                    break;
                }
            }
            if(!custIdExists)
                this.pendingQ.push(pendingRsItem);
            this.start();
        },
        quit:function(){
            this.stop();
            this.pendingQ = [];
        },
        remove:function(custId){

            for(var i= 0,c=this.pendingQ.length;i<c;i++)
            {
                var rs = this.pendingQ[i];
                if(custId == rs.custId)
                {
                    this.pendingQ.splice(i,1);
                    if(!this.pendingQ.length)
                    {
                        this.stop();
                    }
                    break;
                }
            }
        },
        start:function(){
            if(this.timer && angular.isDefined(this.timer))
            {
                return;
            }
            var myMonitor = this;
            console.log("Starting monitor of referral status +++");
            onTimerCbk();
            function onTimerCbk(){
                console.log("On Timer Monitor of pending transaction +++ <Pending Queue Size> "+myMonitor.pendingQ.length);

                if(myMonitor.pendingQ.length)
                {
                    if(!myMonitor.httpReqPromise)
                    {
                        myMonitor.httpReqPromise = $q.defer();
                    }

                    for(var i=0;i<myMonitor.pendingQ.length;i++)
                    {
                        var pt = myMonitor.pendingQ[i];
                        console.log("Processing Pending Referral status of custId: "+pt.custId +" Retry Count: "+pt.nooftries + " At: "+new Date());
                        //Request is not in progress..
                        if(!pt.request && pt.nooftries<myMonitor.maxTries){
                            pt.nooftries++;
                            console.log("Initiating Fetch AcDetails of custId: "+pt.custId +" Retry Count: "+pt.nooftries + " At: "+new Date());
                            pt.request = referralServiceFr.getReferralAccountDetails(pt.custId,myMonitor.httpReqPromise.promise).success((function(res){

                                return function(response){

                                    if(response.referralStatus == "SIGNUP_WITH_REFERRAL" || response.referralStatus == "SIGNUP_WITH_INVALID_REFERRAL" || response.referralStatus == "CREDIT_ADD_FAILED")
                                    {
                                        myMonitor.remove(res.custId);
                                        if(response.referralStatus == "SIGNUP_WITH_REFERRAL") {
                                            console.log("Triggering on successful referral sign up AcDetails custId: "+res.custId +" Status: "+response.referralStatus + " At: "+new Date());
                                            $rootScope.$broadcast('onSignUpCreditSuccess', response);
                                        }

                                        if(response.referralStatus == "SIGNUP_WITH_INVALID_REFERRAL") {
                                            console.log("Triggering on sign up with invalid referral code AcDetails custId: "+res.custId +" Status: "+response.referralStatus + " At: "+new Date());
                                            $rootScope.$broadcast('onInvalidReferralSignUp', response);
                                        }

                                        if(response.referralStatus == "CREDIT_ADD_FAILED") {
                                            console.log("Triggering on sign up credit add failure AcDetails custId: "+res.custId +" Status: "+response.referralStatus + " At: "+new Date());
                                            $rootScope.$broadcast('onSignUpCreditFailure', response);
                                        }
                                    }
                                    else
                                    {
                                        console.log("Pending Status received on TrDetails custId: "+res.custId +" Status: "+response.referralStatus + " At: "+new Date());
                                        //todo ask do we need to have any onSignUpCreditPending event
                                        cancelReqOnTimeOut(res,response);
                                    }
                                }})(pt)).error((function(err){return function(){
                                console.log("Error on Get TrDetails custId: "+err.custId +" At: "+new Date());
                                cancelReqOnTimeOut(err);
                            }})(pt));
                        }
                        else if(!pt.request && pt.nooftries>=myMonitor.maxTries)
                        {
                            console.log("Exceeding max tries on Get TrDetails custId: "+pt.custId +" No of Tries: "+ pt.nooftries + " At: "+new Date());
                            myMonitor.remove(pt.custId);
                            $rootScope.$broadcast('onReferralSignUpTimeout',null);
                        }
                    }
                }

                function cancelReqOnTimeOut(tr,response){
                    tr.request = null;
                    var curTime = new Date() - tr.startTime;
                    if(curTime>=myMonitor.monitorInterval*myMonitor.maxTries)
                    {
                        console.log("Took more than 3 mins to Get TrDetails custId: "+tr.custId + " SIMID: "+tr.simId + " No of Tries: "+ tr.nooftries + " At: "+new Date());

                        myMonitor.remove(tr.custId);
                        $rootScope.$broadcast('onReferralSignUpTimeout',response);

                    }
                }
            }
            this.timer = $interval(onTimerCbk,this.monitorInterval,0,false);
        },

        stop:function(){
            if(this.timer && angular.isDefined(this.timer))
            {
                $interval.cancel(this.timer);
                this.timer = undefined;
            }
            if(this.httpReqPromise)
            {
                this.httpReqPromise.resolve();
                this.httpReqPromise = $q.defer();
            }
            for(var i= 0,c=this.pendingQ.length;i<c;i++)
            {
                var rs = this.pendingQ[i];
                rs.request = null;
            }
        }
    };

    $rootScope.$on('onLogOut',function(){
        referralServiceFr.rsMonitor.quit();
    });

    return referralServiceFr;

}]);

'use strict';

//var GS_API_BASE= "https://staging.gigsky.com/api/v3/";
 // var GS_API_BASE = "http://54.241.30.200/api/v3/";
angular.module('gigSkyWebApp').factory('paymentFactory',['$location','$http','SimManagerFactory','$interval','$rootScope','$q','authFactory',function ($location,$http,simMgrFactory,$interval,$rootScope,$q,authFactory){
		var factory ={};

        $rootScope.$on('onLogIn',function(){
            factory.payHistoryOfSIM = null;
        });

        factory.setPayHistoryOfSIM = function(sim)
        {
            factory.payHistoryOfSIM = sim;
        }

        factory.pendingPurchases = [];
		
		factory.getPaymentMethods= function (){
				return $http.get(GS_NON_ACCOUNT_API_BASE+'account/billing/paymentMethods');
		};

        factory.deletePayMethod = function(billingId){
            return ($http['delete'])(GS_API_BASE+'billing/'+billingId);
        }
		factory.getAllBillingInfo= function (){
			return $http.get(GS_API_BASE+'billing');
		};
					
		factory.genPPKey= function (successURL,cancelURL){
			//var data='{"type":"PaypalPreapproval","userEmailId" : "'+ userEmail +'", "preApprovalCancelURL": "http://localhost:8080/JavaPlugInGigSky/app/index.html#/paymentMethod","preApprovalReturnURL":"http://www.gigsky.com/preapproval/success"}';
			//var data='{"type":"PaypalPreapproval","preApprovalCancelURL": "'+ cancelURL +'","preApprovalReturnURL":"'+ successURL+'"}';
			var data='{"type":"PaypalPreapproval","preApprovalCancelURL": "'+ cancelURL +'","preApprovalReturnURL":"'+ successURL+'"}';
			return $http.post(GS_API_BASE+'billing/paypalGenKey',data);
		};
		
		 factory.addPP= function (PPkey){
			 var userData='{"type":"PaypalBillingInfo","paymentMethod":"Paypal", "preApprovalKey": "'+PPkey+'"}';
				return $http.post(GS_API_BASE+'billing',userData);
		 };	
		 
		 factory.updatePP= function (PPkey,billingId){
			 var userData='{"type":"PaypalBillingInfo","paymentMethod":"Paypal", "preApprovalKey": "'+PPkey+'"}';
				return $http.put(GS_API_BASE+'billing/'+billingId,userData);
		 };

        factory.getTransactionStatus = function(trId,simId,cancelPromise)
        {
            if(cancelPromise)
            {
                return $http.get(GS_API_BASE+"SIM/"+simId+"/transactions/"+trId,{timeout:cancelPromise});
            }
            else
            return $http.get(GS_API_BASE+"SIM/"+simId+"/transactions/"+trId);
        };

		factory.makePayment = function(planObj,billingId,gscreditAmount,simid,submitReceipt,purchaseIntentCountries)
        {
            var postData={type:"TransactionRequest",planId:planObj.planId};
            if(billingId)
            postData.billingID = billingId;
            if(gscreditAmount)
            postData.gigskyCreditAmt = gscreditAmount;
            if(purchaseIntentCountries)
            postData.purchaseIntentCountries = purchaseIntentCountries;


            var iccids = authFactory.getICCIDList();
            if(iccids)
            {
                postData.iccIds = iccids.split(",");
            }

            if(!simid)
                simid = simMgrFactory.getActiveSIM().simId;

            if(submitReceipt && submitReceipt.length)
            {
                postData.thirdpartyAppsToSubmitReceipt = submitReceipt;
            }
            var makePayReq = $http.post(GS_API_BASE+"SIM/"+simid+"/transactions",postData);

            makePayReq.then(function(trdata){
                factory.pendingPurchases.push(trdata);
                factory.trMonitor.monitor(trdata.data.transactionId,simid);
            });

            return makePayReq;
        };
		
		factory.getPaymethodDetails= function (billingId){
			return $http.get(GS_API_BASE+'billing/'+billingId);
		};
		
		factory.deleteCC= function (billingId){
			return ($http['delete'])(GS_API_BASE+'billing/'+billingId);
		};

        factory.addCC= function (name,country,address1,address2,zipCode,phone,state,city,ccNo,ccv,expMonth,expYear){
            var ccData=formCCPostData(name,country,address1,address2,zipCode,phone,state,city,ccNo,ccv,expMonth,expYear);
            //var userData='{"type":"CreditCardBillingInfo","paymentMethod":"CreditCard", "nameOnCard": "'+name+'","country":"'+country+'","address1": "'+address1+'","address2":"'+(address2?address2:"")+'","zipCode":"'+zipCode+'","phone": "'+phone+'","state":"'+state+'","city":"'+city+'","ccNumber":"'+ccNo+'","ccv":"'+ccv+'","expMonth":"'+expMonth+'","expYear":"'+expYear+'"}';
            return $http.post(GS_API_BASE+'billing',ccData);
        };

        function formCCPostData(name,country,address1,address2,zipCode,phone,state,city,ccNo,ccv,expMonth,expYear,setDefault)
        {
            var ccData={type:"CreditCardBillingInfo",paymentMethod:"CreditCard",nameOnCard:name,country:country,address1:address1,city:city,ccNumber:ccNo,ccv:ccv,expMonth:expMonth,expYear:expYear};
            if(zipCode!=null)
            {
                ccData.zipCode = zipCode;
            }
            if(phone!=null)
            {
                ccData.phone = phone;
            }
            if(state!=null)
            {
                ccData.state = state;
            }
            if(address2!=null)
            {
                ccData.address2 = address2;
            }

            if(setDefault)
            {
                ccData.defaultBilling = true;
            }
            return ccData;
        }
        factory.updateCC= function (billingId,name,country,address1,address2,zipCode,phone,state,city,ccNo,ccv,expMonth,expYear,setDefault){

            var ccData=formCCPostData(name,country,address1,address2,zipCode,phone,state,city,ccNo,ccv,expMonth,expYear,setDefault);
            //var userData='{"type":"CreditCardBillingInfo","paymentMethod":"CreditCard", "nameOnCard": "'+name+'","country":"'+country+'","address1": "'+address1+'","address2":"'+(address2?address2:"")+'","zipCode":"'+zipCode+'","phone": "'+phone+'","state":"'+state+'","city":"'+city+'","ccNumber":"'+ccNo+'","ccv":"'+ccv+'","expMonth":"'+expMonth+'","expYear":"'+expYear+'"}';
            return $http.put(GS_API_BASE+'billing/'+billingId,ccData);
        };
		

		
		factory.setDef = function (billingId){
			var userData='{"type":"DefaultBillingInfo","billingId":"'+ billingId +'"}';
			return $http.put(GS_API_BASE+'billing/defaultBilling',userData);
		};
		
		factory.getPaymentHistory= function (){
			return $http.get(GS_API_BASE+'SIM/transactions');
		};
		
		factory.getPaymentHistoryForSim= function (selectedSim,startIndex,count){
            if(selectedSim == null)
            {
                return $http.get(GS_API_BASE+'SIM/transactions?startIndex='+startIndex+"&count="+count);
            }
			return $http.get(GS_API_BASE+'SIM/'+selectedSim+'/transactions?startIndex='+startIndex+"&count="+count);
		};
		
		factory.getGigSkyCreditBal= function (){
            return $http.get(GS_API_BASE+'credit/gigskyCredit');
		};
		
		factory.getGigSkyCreditTrans= function (startIndex,count){
			return $http.get(GS_API_BASE+'credit/gigskyCredit/transactions?startIndex='+startIndex+"&count="+count);
		};

    factory.getGigskyCreditGroups = function(){
        return $http.get(GS_API_BASE+'credit/gigskyCreditsGroups?groupBy=EXPIRY_BY_DATE&order=ASC');
    };

    factory.getConsolidatedTransactionHistory = function (startIndex, count) {
        //TODO: Adding transaction type query param to exclude signup credits. It needs need to be removed once promo credit issue is fixed.
        return $http.get(GS_API_BASE+'SIM/transactions/consolidated?startIndex='+startIndex+"&count="+count+"&transactionType=PLAN_PURCHASE,REFUND,CREDIT_EXPIRY,ADVOCATE_CREDIT,GENERAL_CREDIT,CURRENCY_CONVERSION,CREDIT_DEDUCT");
    };

    factory.trMonitor = {
        monitorInterval:3000,
        maxTries:60,
        pendingQ:[],
        monitor:function(trId,simid){
            var pendingTrItem  = {trid:trId,request:null,nooftries:0,simId:simid,startTime:new Date()};
            var trIdExists = false;
            for(var i=0;i<this.pendingQ.length;i++)
            {
                var pt = this.pendingQ[i];
                if(pt.trId==trId)
                {
                    trIdExists = true;
                    break;
                }
            }
            if(!trIdExists)
                this.pendingQ.push(pendingTrItem);
            this.start();
        },
        quit:function(){
            this.stop();
            this.pendingQ = [];
        },
        remove:function(trId){

            for(var i= 0,c=this.pendingQ.length;i<c;i++)
            {
                var tr = this.pendingQ[i];
                if(trId == tr.trid)
                {
                    this.pendingQ.splice(i,1);
                    if(!this.pendingQ.length)
                    {
                        this.stop();
                    }
                    break;
                }
            }
        },
        start:function(){
            if(this.timer && angular.isDefined(this.timer))
            {
                return;
            }
            var myMonitor = this;
            console.log("Starting monitor of pending transaction +++");
            onTimerCbk();
            function onTimerCbk(){
                console.log("On Timer Monitor of pending transaction +++ <Pending Queue Size> "+myMonitor.pendingQ.length);

                if(myMonitor.pendingQ.length)
                {
                    if(!myMonitor.httpReqPromise)
                    {
                        myMonitor.httpReqPromise = $q.defer();
                    }

                    for(var i=0;i<myMonitor.pendingQ.length;i++)
                    {
                        var pt = myMonitor.pendingQ[i];
                        console.log("Processing Pending TrID: "+pt.trid + " SIMID: "+pt.simId + " Retry Count: "+pt.nooftries + " At: "+new Date());
                        //Request is not in progress..
                        if(!pt.request && pt.nooftries<myMonitor.maxTries){
                            pt.nooftries++;
                            console.log("Initiating Fetch TrDetails TrID: "+pt.trid + " SIMID: "+pt.simId + " Retry Count: "+pt.nooftries + " At: "+new Date());
                            pt.request = factory.getTransactionStatus(pt.trid,pt.simId,myMonitor.httpReqPromise.promise).success((function(tr){

                                return function(response){


                                    if(response.status == "COMPLETE" || response.status == "FAILED" || response.status == "PENDING-NETWORKUPDATE")
                                    {
                                        myMonitor.remove(tr.trid);
                                        if(response.status == "COMPLETE" || response.status == "PENDING-NETWORKUPDATE") {
                                            console.log("Triggering on Transaction Complete TrDetails TrID: "+tr.trid + " SIMID: "+tr.simId + " Status: "+response.status + " At: "+new Date());
                                            $rootScope.$broadcast('onTransactionComplete', response);
                                        }
                                        if(response.status == "FAILED") {
                                            console.log("Triggering on Transaction Failure TrDetails TrID: "+tr.trid + " SIMID: "+tr.simId + " Status: "+response.status + " At: "+new Date());
                                            $rootScope.$broadcast('onTransactionFailed', response);
                                        }
                                    }
                                    else
                                    {
                                        console.log("Pending Status received on TrDetails TrID: "+tr.trid + " SIMID: "+tr.simId + " Status: "+response.status + " At: "+new Date());
                                        cancelReqOnTimeOut(tr,response);
                                    }
                                }})(pt)).error((function(tr){return function(){
                                console.log("Error on Get TrDetails TrID: "+tr.trid + " SIMID: "+tr.simId + " At: "+new Date());
                                cancelReqOnTimeOut(tr);
                            }})(pt));
                        }
                        else if(!pt.request && pt.nooftries>=myMonitor.maxTries)
                        {
                            console.log("Exceeding max tries on Get TrDetails TrID: "+pt.trid + " SIMID: "+pt.simId + " No of Tries: "+ pt.nooftries + " At: "+new Date());
                            myMonitor.remove(pt.trid);
                            $rootScope.$broadcast('onTransactionTimeout',null);
                        }
                    }
                }

                function cancelReqOnTimeOut(tr,response){
                    tr.request = null;
                    var curTime = new Date() - tr.startTime;
                    if(curTime>=myMonitor.monitorInterval*myMonitor.maxTries)
                    {
                        console.log("Took more than 3 mins to Get TrDetails TrID: "+tr.trid + " SIMID: "+tr.simId + " No of Tries: "+ tr.nooftries + " At: "+new Date());

                        myMonitor.remove(tr.trid);
                        $rootScope.$broadcast('onTransactionTimeout',null);

                        // if(response && response.status != "PENDING-NETWORKUPDATE" && response.status.search('PENDING')!=-1)
                        // {
                        //     console.log("Triggering transaction pending time out");
                        //     $rootScope.$broadcast('onTransactionTimeout',null);
                        // }
                        // else {
                        //     console.log("Triggering transaction time out");
                        //     $rootScope.$broadcast('onTransactionTimeout', null);
                        // }
                    }
                }
            }
            this.timer = $interval(onTimerCbk,this.monitorInterval,0,false);
        },

        stop:function(){
            if(this.timer && angular.isDefined(this.timer))
            {
                $interval.cancel(this.timer);
                this.timer = undefined;
            }
            if(this.httpReqPromise)
            {
                this.httpReqPromise.resolve();
                this.httpReqPromise = $q.defer();
            }
            for(var i= 0,c=this.pendingQ.length;i<c;i++)
            {
                var tr = this.pendingQ[i];
                tr.request = null;
            }
        }
    };

        $rootScope.$on('onLogOut',function(){
            factory.trMonitor.quit();
        });
        return factory;
}]);


/*angular.module('gigSkyWebApp').factory('friendsFactory', function($resource) {
	  return $resource('friends.json');
	});
	*/

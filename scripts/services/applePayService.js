'use strict';

angular.module('gigSkyWebApp').factory('applePayService',['$location','$http','$rootScope','$q','commonInfoFactory','SimManagerFactory','paymentFactory'
    ,function ($location,$http,$rootScope,$q,commonInfoFactory,simMgrFactory,paymentFactory) {

        var applePayAvailableObj = window.ApplePaySession;
        var merchantId = "merchant.com.gigsky.live.app";
        var applePaySession;
        var isSandbox = APPLE_PAY_SANDBOX;
        if (isSandbox) {
            merchantId = "merchant.com.cert.PAGB90";
        }


        return {
            isApplePayAvailable: isApplePayAvailable,
            authorizePayment: authorizePayment
        };

        function isApplePayAvailable(){
            var defer = $q.defer();
            var promise = commonInfoFactory.extendHttpPromise(null,null,defer);

            var secured = false;
            var protocol = $location.protocol();
            if(protocol == 'https'){
                secured = true;
            }

            if(applePayAvailableObj && secured){

                ApplePaySession.canMakePaymentsWithActiveCard(merchantId).then(function (canMakePayments) {
                    defer.resolve({data:canMakePayments});
                },function(error,status){
                    defer.reject({data:error,status:status});
                });

            }else{
                defer.resolve({data:false});
            }
            return promise;
        }

        function authorizePayment(currencyCode,amount,label,planId,gscreditAmount,submitReceipt,purchaseIntentCountries){

            var def = $q.defer();
            commonInfoFactory.extendHttpPromise(null,null,def);

            var request = {
                countryCode: 'US',
                currencyCode: currencyCode,
                supportedNetworks: ['visa','masterCard','amex','interac','discover'],
                merchantCapabilities: ['supports3DS'],
                total: {label: label, amount: amount+''}
            };

//                if(!applePayAvailable)
//                {
//                    def.reject({data:{userDisplayErrorStr:"Apple Pay Support is not available on this device."},status:status});
//                    return def.promise;
//                }

            try {
                applePaySession = new ApplePaySession(1, request);
            }
            catch(e)
            {
                def.reject({data:{errorInt:100,userDisplayErrorStr:Localize.translate('Unable to initialize an Apple Pay Session')},status:0});
                return def.promise;
            }

            applePaySession.oncancel = function(data,status){
                def.reject({data:{errorInt:100,userDisplayErrorStr:Localize.translate('Apple Pay has been cancelled.')},status:status});
            };
            applePaySession.onvalidatemerchant = function (event) {
                var validationURL = event.validationURL;

                if(isSandbox)
                {
                    validationURL = "https://apple-pay-gateway-cert.apple.com/paymentservices/startSession";
                }
                var domainName = $location.host();

                var postData = {type:'sessionToken',targetURL:validationURL,domainName:domainName,displayName:Localize.translate('GigSky World Mobile Data')};

                $http.post(GS_NON_ACCOUNT_API_BASE+'account/billing/applePay/merchantSession',postData).success(function (data) {
                    if(data && data.sessionToken){
                        applePaySession.completeMerchantValidation(data.sessionToken);
                    }else{
                        applePaySession.abort();
                        def.reject({data:{errorInt:100,userDisplayErrorStr:Localize.translate('Unable to initialize an Apple Pay Session')}});
                    }
                }).error(function (data,status) {
                    applePaySession.abort();
                    def.reject({data:data,status:status});
                });

            };
            function prepareTransactionData(data,amount,currency){
                var date = new Date();
                var dateStr  = date.getFullYear() +"-"+ (date.getMonth()+1) + "-" + (date.getDate() + 1) + " "+date.getHours() + ":" +date.getMinutes() + ":"+ date.getSeconds();

                return {
                    date:dateStr,
                    transactionId:""+data.transaction_id,
                    amount:amount,
                    currency:currency,
                    transactionStatus:data.transaction_status,
                    transactionTag:""+data.transaction_tag,
                    bankCode:data.bank_resp_code,
                    bankMsg:data.bank_message,
                    gatewayCode:data.gateway_resp_code,
                    gatewayMsg:data.gateway_message
                }
            }
            applePaySession.onpaymentauthorized = function(authorized){

                var postData = {type: "TransactionRequest",planId :planId, paymentMethod:"ApplePay"};

                if(gscreditAmount)
                    postData.gigskyCreditAmt = gscreditAmount;

                if(submitReceipt && submitReceipt.length)
                {
                    postData.thirdpartyAppsToSubmitReceipt = submitReceipt;
                }

                if(purchaseIntentCountries)
                    postData.purchaseIntentCountries = purchaseIntentCountries;

                var paymentData = authorized.payment.token.paymentData;

                postData.applePayPaymentInfo = paymentData;
                var simId = simMgrFactory.getActiveSIM().simId;
                $http.post(GS_API_BASE+"SIM/"+simId+"/oneTimeTransactions",postData).success(function (data) {

                    var onTransactionCompleteListener = $rootScope.$on('onTransactionComplete',function(){
                        applePaySession.completePayment(ApplePaySession.STATUS_SUCCESS);
                        unRegisterAllEvents();
                        def.resolve({data:data});
                    });
                    var onTransactionFailureListener = $rootScope.$on('onTransactionFailed',function(d,trresponse){
                        applePaySession.completePayment(ApplePaySession.STATUS_FAILURE);
                        unRegisterAllEvents();
                        def.reject({data:{userDisplayErrorStr:trresponse.userDisplayErrorStr,errorInt:trresponse.userDisplayErrorCode}});
                    });
                    var onTransactionTimeOutListener = $rootScope.$on('onTransactionTimeout',function(){
                        applePaySession.completePayment(ApplePaySession.STATUS_SUCCESS);
                        unRegisterAllEvents();
                        def.resolve({data:data});
                    });

                    function unRegisterAllEvents(){
                        onTransactionTimeOutListener();
                        onTransactionFailureListener();
                        onTransactionCompleteListener();
                    }
                    paymentFactory.trMonitor.monitor(data.transactionId,simId);

                }).error(function (data,status) {
                    applePaySession.completePayment(ApplePaySession.STATUS_FAILURE);
                    def.reject({data:data,status:status});
                });

            };
            applePaySession.onpaymentmethodselected = function(){

                applePaySession.completePaymentMethodSelection({label:"GigSky","amount":amount},[{"label":label,"amount":amount},{"label":"tax","amount":"0.00"}]);
            };

            applePaySession.begin();

            return def.promise;
        }
    }]);

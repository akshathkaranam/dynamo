'use strict';

//var GS_API_BASE= "https://staging.gigsky.com/api/v3/";
//var GS_API_BASE = "http://54.241.30.200/api/v3/";

angular.module('gigSkyWebApp').factory('GSHttpInterceptors',['$location','$q','$rootScope',function($location,$q,$rootScope){

    var msie = parseInt(((/msie (\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1]),10);
    if (isNaN(msie)) {
        msie = parseInt(((/trident\/.*; rv:(\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1]),10);
    }
    return {

        request:function(config){
            config.timeout = 120000;
            $rootScope.servicesDown = false;
            $rootScope.pageLoadingFailed = false;
            //Fix for IE caching issue.. This can be resolved on server end by disabling caching for specific set of queries.
            if(true) //msie)
            {
                if(config.url.indexOf('api/')!=-1 && config.method == "GET")
                {
                    var separator = config.url.indexOf('?') === -1 ? '?' : '&';
                    config.url = config.url+separator+'noCache=' + new Date().getTime();
                }
            }
            if(config.url.indexOf('api/')!=-1)
            {
                var headers = config.headers;
                angular.extend(headers,{"Accept-Language":sessionStorage.getItem("currentLanguageCode")})
            }
            return config;
        },
        responseError:function(resp){
            var retryCount = 0;
            var maxAttempts = 0;
            if(resp.config.config)
            {
                retryCount = resp.config.config.retryCount;
                maxAttempts =resp.config.config.maxRequests;
            }

            if(resp.status==0 && resp.config.method != "GET")
            {
                resp.data = {errorInt:1,userDisplayErrorStr:Localize.translate('Unable to connect to GigSky\'s system.')};
                return $q.reject(resp);
            }else if(resp.status == 0 && (resp.config.method == "GET") && resp.config.url.indexOf("gsre/api/v1/config")==-1)
            {
                if(retryCount >= maxAttempts)
                    $rootScope.pageLoadingFailed = true;

                resp.data = {errorInt:1,userDisplayErrorStr:Localize.translate('Unable to connect to GigSky\'s system.')};
                return $q.reject(resp);
            }
            if(resp.data && resp.data.errorInt && resp.data.errorInt == 8000)
            {
                //GS Backend services are down for maintenance.
                $rootScope.servicesDown = true;
                return $q.reject(resp);
            }

            var deviceId = sessionStorage.getItem("deviceId");
            var loc=sessionStorage.getItem("currentLoc");

            //Add Token expiry should not be handled in case of account activation confirmation,email confirmation,
            //unblock user, reset pwd confirmation
            if(($location.path().indexOf("/activate")!=-1 ) ||
                ($location.path().indexOf("/reset")!=-1 ) ||
                ($location.path().indexOf("/confirmemail")!=-1 ) ||
            ($location.path().indexOf("/unblock")!=-1 ))
            {
                return $q.reject(resp);
            }

            if(resp.status==401 && resp.data && (resp.data.errorInt == 2001 || resp.data.errorInt == 12301))
            {
                $rootScope.$broadcast('onLogOut');
                if($location.path()!="/login")
                {

                    $location.path("/login");
                    $location.replace();
                }
                $rootScope.formErrorMsg = resp.data.userDisplayErrorStr?resp.data.userDisplayErrorStr:resp.data.errorStr;
            }else if(resp.status==401 && resp.data && resp.data.errorInt == 10000)
            {
                $rootScope.loginErrOnEdit = true;
                if($location.path()!="/login")
                {
                    $location.path("/login");
                    $location.replace();
                }

            }

            return $q.reject(resp);

        }

    };
}]);

angular.module('gigSkyWebApp').factory('commonInfoFactory',['$location', '$window', '$http','$filter','$rootScope','$q',function ($location, $window, $http,$filter,$rootScope,$q){

        var factory ={};

        factory.extendHttpPromise = function(val,reject,defer)
        {
            var def = (!defer)?$q.defer():defer;

            if (reject && val)
                def.reject({data:val,status:0});
            else if(val)
                def.resolve({data:val});

            def.promise.success = function(fn) {
                def.promise.then(function(v){
                    fn(v.data);
                }, null);
                return def.promise;
            };
            def.promise.error = function(fn) {
                def.promise.then(null, function(v){
                    fn(v.data, v.status);
                });
                return def.promise;
            };
            return def.promise;
        };

        factory.retryRequest = function (url, method, data) {
            var maxRequests = 3;
            var counter = 1;
            var def = $q.defer();

            var defPromise = factory.extendHttpPromise(null,null,def);
            
            var request = function () {
                $http({method:method, url:url,data:data,config:{retryCount:counter,maxRequests:maxRequests}})
                    .success(function (response) {
                        def.resolve({data:response});
                    })
                    .error(function (response,status) {
                        if (counter < maxRequests && status == 0) {
                            counter++;
                            request();
                        } else {
                            def.reject({data:response,status:status});
                        }
                    });
            };
            request();

            return def.promise;
        };

    factory.isCountryForbidden = function(countryOfResidence,currentLocationArr,forbiddenCountries){

        var isCountryOfResidenceCurrentLoc = false;
        if(countryOfResidence && currentLocationArr &&forbiddenCountries){
            for(var i=0;i<currentLocationArr.length;i++){
                if(currentLocationArr[i].toLowerCase() == countryOfResidence.toLowerCase()){
                    isCountryOfResidenceCurrentLoc = true;
                    break;
                }
            }
            if(isCountryOfResidenceCurrentLoc){
                for(var j=0;j<forbiddenCountries.length;j++){
                    var c = forbiddenCountries[j];
                    if(c.code == countryOfResidence)
                        return true;
                }
            }
        }
        return false;
    };

		factory.getCurrencyList= function (){

            var curList = sessionStorage.getObj("currencyList");
            if(curList)
            {
                return factory.extendHttpPromise(curList);
            }

			return $http.get(GS_NON_ACCOUNT_API_BASE+'supportedCurrencies').success(function(data){
                sessionStorage.setObj("currencyList",data);
            });
	    };

        factory.maskTelField = function() {

            var telFields  = document.querySelectorAll("input[type='tel']:not(.tel-exclude)" );
            var telLen = telFields.length;
            for( var i =0; i < telLen; i++ ) {
                var style = $window.getComputedStyle(telFields[i] );
              //  console.log(style);
                if ( !style.webkitTextSecurity) {
                    telFields[i].setAttribute("type", "password");
                }
            }


        };

        factory.capitalize = function(description){
            return description.replace(/\w\S*/g, function(capitalizedDescription){return capitalizedDescription.charAt(0).toUpperCase() + capitalizedDescription.substr(1);});
        };


    factory.setCurrencySymbol = function(currency) {
	    	var currencySymbol;
			factory.getCurrencyList()
				.success(function (currencyResult) {
					for(var i = 0; i< currencyResult.list.length; i++){
						if (currencyResult.list[i].code == currency ){
							currencySymbol=currencyResult.list[i].currencySymbol;
						}
					}
				})
				.error(function () {
					currencySymbol="";
			});
			return currencySymbol;
		};

        factory.getStoredCountryList = function(){
            var countries= sessionStorage.getObj("countries");
            if(countries && countries.lang == $rootScope.currentLangCode)
            {
                factory.countryList = countries.list;
                return countries.list;
            }
            var promise = factory.getCountryList();
            promise.then(function (result) {
                    storeCountryList(result.data);
                });
            return promise;
        };

        function storeCountryList(result){
            var countries = {lang:$rootScope.currentLangCode,list:[]};
            countries.list = [];
            //Order countries..
            var us;
            var uk;
            var ja;

            for(var i= 0,c=result.list.length;i<c;i++)
            {
                var country = result.list[i];
                if(country.code == 'US' || country.code == 'GB' || country.code == 'JP')
                {
                    us = (country.code == 'US')?country:us;
                    uk = (country.code == 'GB')?country:uk;
                    ja = (country.code == 'JP')?country:ja;
                }
                else
                countries.list.push(country);
            }
            countries.list.splice(0,0,ja);
            countries.list.splice(0,0,uk);
            countries.list.splice(0,0,us);
            factory.countryList = countries.list;
            sessionStorage.setObj("countries",countries);
        }

		factory.getCountryList= function (freshCopy){

            if(!freshCopy){
                var countries= sessionStorage.getObj("countries");
                if(countries && countries.lang == $rootScope.currentLangCode)
                {
                    factory.countryList = countries.list;
                    var def = $q.defer();
                    var defPromise = factory.extendHttpPromise(null,null,def);
                    def.resolve({data:{"type":"CountryList",list:countries.list}});
                    return def.promise;
                }
            }
			return $http.get(GS_NON_ACCOUNT_API_BASE+'countries/all?startIndex=0&count=32768').success(function(result){
                storeCountryList(result);
            });
		};

        factory.getCountryNameByCode = function(codes)
        {
            var def = $q.defer();
            var defPromise = factory.extendHttpPromise(null,null,def);
            if(!codes)
            {
                def.reject({data:{},status:0});
                return def.promise;
            }
            factory.getCountryList().success(function(countryList){
                var name = factory.getCountryByName(codes,countryList);
                def.resolve({data:{name:name}});
            }).error(function(data,status){
                def.reject({data:data,status:status});
            });
            return def.promise;
        };

        factory.getCountryListByCodes = function(codes)
        {
            var def = $q.defer();
            var defPromise = factory.extendHttpPromise(null,null,def);
            if(!codes)
            {
                def.reject({data:{},status:0});
                return def.promise;
            }
            factory.getCountryList().success(function(countryList){
                var list = factory.getCountryNamesFromCodes(codes,countryList);
                def.resolve({data:{countryList:list}});
            }).error(function(data,status){
                def.reject({data:data,status:status});
            });
            return def.promise;
        };


        factory.getCountryNamesFromCodes=function(codes,countryList){

            var countries=[];
            for(var i=0;i<countryList.list.length;i++){
                for(var j=0;j<codes.length;j++) {
                    var code = codes[j];
                    if (code == countryList.list[i].code) {
                        countries.push(countryList.list[i]);
                    }
                }
            }
            return countries;
        };



    factory.getCountryByName=function(codes,countryList){
            if(!codes)
                return "";
            codes = codes.toString();
            var code;
            if(codes.indexOf(",")==-1)
            {
                code = codes;
            }
            else {
                if(codes.indexOf("US")!=-1)
                    code = "US";
                else {
                    codes = codes.split(",");
                    code = codes[0];
                }
            }

            for(var i=0;i<countryList.list.length;i++){
                if(code == countryList.list[i].code){
                    return countryList.list[i].name;
                }
            }
        };

        factory.getVisitedCountryList = function(simId){
                return $http.get(GS_API_BASE+'SIM/'+simId+'/visitedcountries?type=DATA_PURCHASED&startIndex=0&count=3');
        };

        factory.getCountriesForSearchKey = function(searchKey,simType,activeSIM){
            var url = GS_NON_ACCOUNT_API_BASE+'planSearch/countries?KEY='+searchKey+"&startIndex=0&count=32758"
            if(simType)
                url = url + "&simType="+simType;
            if(activeSIM && activeSIM.simId)
                url = url + "&simId="+activeSIM.simId;

            return $http.get(url);
        };
        factory.getSupportedCountryList= function (simType,activeSIM){
            var url = GS_NON_ACCOUNT_API_BASE+'countries?startIndex=0&count=32758';
            if(simType)
                url = url + "&simType="+simType;
            if(activeSIM && activeSIM.simId)
                url = url + "&simId="+activeSIM.simId;

            var supportedCountriesReq =  $http.get(url);

            return supportedCountriesReq;
        };

        /**
         * @func formatExpiryDate
         * @desc formats expiry date to display in MMM dd,yyyy format
         * @param date (dd-mm-yyyy format)
         * @returns {string} date in MMM dd,yyyy format
         */
        factory.formatExpiryDate = function(d){
                if(!d) return null;
                var dateTime = d+' 00:00:00';
                var expiryDateTime = factory.setDate(dateTime);
                var expiryDate = expiryDateTime.split(' ');
                return (expiryDate && expiryDate.length)?expiryDate[0]+' '+expiryDate[1]+' '+expiryDate[2] : d;
        };
	    
		factory.getSimList= function (){
			return $http.get(GS_API_BASE+'SIM');
		};

		factory.setDate=function (dateTime,inLocalTimeZone){
			//console.log("dateTime is " + dateTime);
			  var splitDateTime=dateTime.split(" ");
			  var date=splitDateTime[0];
			  var time=splitDateTime[1];
			  //console.log("date is " + date + "time is " + time);
			  var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			    var dayComps = date.split("-");
			    var timeComps = time.split(":");
			    var month = parseInt(dayComps[1])-1;
                if(inLocalTimeZone)
                {
                    var d = new Date(Date.UTC(dayComps[0],month,dayComps[2],timeComps[0],timeComps[1],timeComps[2], 0));
                }
                else
                {
                    var d = new Date(dayComps[0],month,dayComps[2],timeComps[0],timeComps[1],timeComps[2], 0);
                }

			    var hours = d.getHours();

			    var val = months[d.getMonth()] + " " + (d.getDate()) + ", " +d.getFullYear() + " " +
			        padNumber(hours) + ":" + padNumber(d.getMinutes());

			    return val; 
		}
		
		
		function padNumber(n){
		    return n > 9 ? "" + n: "0" + n;
		}




    factory.formatPlanDataSize = function(planObject)
    {
        if(!planObject)
            return;
        if(planObject.dataUnlimited)
        {
            return "Unlimited";
        }
        else
        {
            var size = planSizeInternal(planObject.dataLimitInKB);
            return size.data + " "+size.measure;
        }
    };

    factory.formatBalanceDataSize = function(subObject)
    {
        var size = planSizeInternal(subObject.balanceDataInKB);
        return size.data + " "+size.measure;
    };

    factory.formatSubRemainingTime = function (subscription)
    {

        var timeRemaining = "";

        if(subscription && subscription.subscriptionStatus == 'EXPIRED')
            return Localize.translate("Expired");

        if(subscription)
        {
            if(subscription.balanceTimeInDay){
                var timeSplit = subscription.balanceTimeInDay.split(":");
                var hr = parseInt(timeSplit[0]);
                var min = parseInt(timeSplit[1]);
                var sec = parseInt(timeSplit[2]);

                var hrsStr = Localize.translate("hours");
                var hrStr = Localize.translate("1 hour");
                if(subscription.balanceInDays >= 1)
                {
                    hrsStr = Localize.translate("hrs");
                    hrStr = Localize.translate("1 hr");
                }

                timeRemaining = (hr) ? ((hr > 1) ? (hr+" "+hrsStr) :hrStr) : ((min) ? ((min > 1) ? (min+" "+ Localize.translate("minutes")) : (Localize.translate("1 minute"))) : (sec > 1) ? (sec + " " +Localize.translate("seconds")) : (Localize.translate("Expired")));

                if(subscription.balanceInDays >= 1 && hr < 1)
                {
                    timeRemaining = '';
                }
            }
            else
            {
                timeRemaining = Localize.translate("Expired");
            }
        }

        if (subscription.balanceInDays >= 1) {

            var daysStr = Localize.translate("days");
            if(subscription.balanceInDays == 1)
                daysStr = Localize.translate("day");


            var bal = subscription.balanceInDays + " "+daysStr +((timeRemaining!=Localize.translate("Expired"))?(" "+timeRemaining):'')+" " +Localize.translate('remaining');;
            return bal;
        }
        else {
            if((timeRemaining!=Localize.translate("Expired")))
                return timeRemaining+" "+Localize.translate('remaining');
        }
        return Localize.translate("Expired");
    };

    factory.formatSubRemainingTimeShort = function (subscription)
    {
        var timeRemaining = "";
        if(subscription && subscription.subscriptionStatus == 'EXPIRED')
            return Localize.translate("Expired");

        if(subscription)
        {
            if(subscription.balanceTimeInDay){
                var timeSplit = subscription.balanceTimeInDay.split(":");
                var hr = parseInt(timeSplit[0]);
                var min = parseInt(timeSplit[1]);
                var sec = parseInt(timeSplit[2]);

                var hrsStr = Localize.translate("hours");
                var hrStr = Localize.translate("1 hour");
                if(subscription.balanceInDays >= 1)
                {
                    hrsStr = Localize.translate("hrs");
                    hrStr = Localize.translate("1 hr");
                }

                timeRemaining = (hr) ? ((hr > 1) ? (hr+" "+hrsStr) :hrStr) : ((min) ? ((min > 1) ? (min+" "+ Localize.translate("minutes")) : (Localize.translate("1 minute"))) : (sec > 1) ? (sec + " " +Localize.translate("seconds")) : (Localize.translate("Expired")));

                if(subscription.balanceInDays >= 1 && hr < 1)
                {
                    timeRemaining = '';
                }
            }
            else
            {
                timeRemaining = Localize.translate("Expired");
            }
        }

        if (subscription.balanceInDays >= 1) {
            var daysStr = Localize.translate("days");
            if(subscription.balanceInDays == 1)
                daysStr = Localize.translate("day");
            var bal = subscription.balanceInDays + " "+daysStr +((timeRemaining!=Localize.translate("Expired"))?(" "+timeRemaining):'');
            return bal;
        }
        else {
            if((timeRemaining!=Localize.translate("Expired")))
               return timeRemaining;
        }
        return Localize.translate("Expired");
    };


    factory.planSize = function(planObj)
    {
        return planSizeInternal(planObj.dataLimitInKB).data;
    };
    //Returns object having data and measure properties.
    factory.subscriptionDataBalance = function(subscription)
    {
        return planSizeInternal(subscription.balanceDataInKB);
    };

    factory.subscriptionTimeBalance = function(subscription)
    {
        return planSizeInternal(subscription);
    }

    factory.planSizeGranularity = function(planObj)
    {
        return planSizeInternal(planObj.dataLimitInKB).measure;
    };

    factory.planPrice = function(planObj,excludeCustomSymbol)
    {
        if(!planObj)
            return;
        if(planObj.freePlan)
          return Localize.translate('Free');

        var curSymbol = getLocalCurrencySign(planObj.currency);

        if(!excludeCustomSymbol && curSymbol=="$")
        {
            curSymbol = "US$ "
        }

        var price =  $filter('currency')(planObj.price,curSymbol);
        if(planObj.currency == "JPY")
        {
            return factory.stripDecimal(price);
        }
        return price;
    };

    factory.planPriceWithOutDecimals = function(planObj)
    {
        if(!planObj)
            return;
        if(planObj.freePlan)
            return Localize.translate('Free');

        var curSymbol = getLocalCurrencySign(planObj.currency);
        if(curSymbol=="$")
        {
            curSymbol = "US$ "
        }

        var p = $filter('currency')(planObj.price,curSymbol);
        return factory.stripDecimal(p);

    };

    factory.formatPrice = function(cost,currency,negative){

        var curSymbol = getLocalCurrencySign(currency);
        if(curSymbol=="$")
        {
            curSymbol = "US$ "
        }
        var price =  $filter('currency')(cost,curSymbol);
        if(negative)
        {
            price = "-"+price;
        }

        if(currency == "JPY")
        {
            return factory.stripDecimal(price);
        }

        return price;
    };

    factory.stripDecimal=function(price)
    {
        if(price.indexOf(".00")!=-1)
        {
            var i = price.indexOf(".00");
            price = price.substr(0,i);
        }
        return price;
    }

    factory.formatPlanPrice = function(cost, currency,negative)
    {
        var curSymbol = getLocalCurrencySign(currency);
        if(curSymbol=="$")
        {
            curSymbol = "US$ "
        }

        var price =  $filter('currency')(cost,curSymbol);
        if(negative)
        {
           price = "-"+price;
        }

        if(currency == "JPY")
        {
            return factory.stripDecimal(price);
        }

        return price;
    }

    factory.planDurationGranularity = function(planObj)
    {
        if (planObj.validityPeriodInDays > 1) {
            return Localize.translate("days");
        }
        else
        if (planObj.validityPeriodInDays == 1) {
            return Localize.translate("day");
        }

    };


    factory.formatPlanDuration = function(planObj)
    {
        if(planObj)
            return Localize.translate("%{noofdays} Day plan",{noofdays:planObj.validityPeriodInDays});
        return '';
    };

    factory.accountPaymentStatus = function()
    {
        return $http.get(GS_API_BASE+"paymentStatus");
    };

    function planSizeInternal(dataLimitInKB)
    {
        var measure = Localize.translate('KB');
        var data = dataLimitInKB;

        if((dataLimitInKB/(1024.0*1024.0)) >= 1.0)
        {
            measure = Localize.translate('GB');
            data = dataLimitInKB / ((1024.0) * (1024.0));
        }
        else if(dataLimitInKB/1024.0 >= 1.0)
        {
            measure = Localize.translate('MB');
            data = dataLimitInKB / 1024.0;
        }

        data = Math.round(data*100)/100;
        return {data:data,measure:measure};
    }

    factory.getLocalizedErrorMsg = function(data,status,statusTextMsg)
    {
        if (data && data.errorInt) {
            var errorStr =  data.userDisplayErrorStr ? data.userDisplayErrorStr : data.errorStr;
            if(!errorStr)
            {
                errorStr = "Internal server error ( "+data.errorInt+" - "+ status + ")"
            }
            return errorStr;
        }
        else if (status && status == 404) {
            return Localize.translate('Resource not found.');
        }
        if(statusTextMsg)
            return statusTextMsg;
        else
            return "Unknown error";
    }
//    factory.formatDuration = function(duration)
//    {
//        if(duration == 1)
//        {
//            $filter('translate')('lt_welcome')
//            return getLocalizedValue("1-day");
//        }
//        else
//        {
//            return sprintf(getLocalizedValue("x-days"),duration);// + " " + getLocalizedValue("Days"));
//        }
//    };

    factory.validateCreditCardType = function(value,ccType){

        var creditCardValidator = {};
        creditCardValidator.cards = {
            'mc':'5[1-5][0-9]+',
//            'ec':'5[1-5][0-9]{14}',
            'vi':'4(?:[0-9]{12}|[0-9]+)',
            'ax':'3[47][0-9]+'
//            ,'dc':'3(?:0[0-5][0-9]{11}|[68][0-9]{12})',
//            'bl':'3(?:0[0-5][0-9]{11}|[68][0-9]{12})',
//            'di':'6011[0-9]{12}',
//            'jcb':'(?:3[0-9]{15}|(2131|1800)[0-9]{11})',
//            'er':'2(?:014|149)[0-9]{11}'
        };

        creditCardValidator.validate = function(value,ccType) {
            value = String(value).replace(/[- ]/g,''); //ignore dashes and whitespaces

            var cardinfo = creditCardValidator.cards, results = [];
            if(ccType){
                var expr = '^' + cardinfo[ccType.toLowerCase()] + '$';
                return expr ? !!value.match(expr) : false; // boolean
            }

            for(var p in cardinfo){
                if(value.match('^' + cardinfo[p] + '$')){
                    results.push(p);
                }
            }
            return results.length ? results[0] : false; // String | boolean
        };
        return creditCardValidator.validate(value,ccType);
    };

    factory.validateCreditCard = function(value,ccType){

        var creditCardValidator = {};
        creditCardValidator.cards = {
            'mc':'5[1-5][0-9]{14}',
//            'ec':'5[1-5][0-9]{14}',
            'vi':'4(?:[0-9]{12}|[0-9]{15})',
            'ax':'3[47][0-9]{13}'
//            ,'dc':'3(?:0[0-5][0-9]{11}|[68][0-9]{12})',
//            'bl':'3(?:0[0-5][0-9]{11}|[68][0-9]{12})',
//            'di':'6011[0-9]{12}',
//            'jcb':'(?:3[0-9]{15}|(2131|1800)[0-9]{11})',
//            'er':'2(?:014|149)[0-9]{11}'
        };

        creditCardValidator.validate = function(value,ccType) {
            value = String(value).replace(/[- ]/g,''); //ignore dashes and whitespaces

            var cardinfo = creditCardValidator.cards, results = [];
            if(ccType){
                var expr = '^' + cardinfo[ccType.toLowerCase()] + '$';
                return expr ? !!value.match(expr) : false; // boolean
            }

            for(var p in cardinfo){
                if(value.match('^' + cardinfo[p] + '$')){
                    results.push(p);
                }
            }
            return results.length ? results.join('|') : false; // String | boolean
        };
        return creditCardValidator.validate(value,ccType);
    };

    //converting date format to MMMM DD, YYYY
    factory.convertDateFormat = function(dateObj){

        if(dateObj){
            var month =  dateObj.getMonth();
            var day = dateObj.getDate();
            var year = dateObj.getFullYear();
            var monthStr;

            var months = ['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December'];
            for(var j=0;j<months.length;j++){
                if(month == j){
                    monthStr = months[j];
                    break;
                }
            }

            return monthStr+ " "+ day + ", " + year;
        }
        else {
            return dateObj;
        }
    }

    return factory;
}]);

angular.module('gigSkyWebApp').factory('GSSessionStorage',['$location','$http','$rootScope',function ($location,$http,$rootScope){

    var storage = {};
    storage.setItem = function(key,data)
    {
        document.cookie=key + "=" + data + "; path=/";
    }
    storage.getItem = function(key)
    {
        var i,x,y,ARRcookies=document.cookie.split(";");
        for (i=0;i<ARRcookies.length;i++){
            x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x=x.replace(/^\s+|\s+$/g,"");
            if (x==key)
            {
                return unescape(y);
            }
        }
        return null;
    }

    storage.setObj = function(key,data)
    {
        if(key=='currentUser')
        {
            this.setItem(key, JSON.stringify(data))
        }
    }

    storage.getObj = function(key)
    {
        if(key=='currentUser')
        {
            return JSON.parse(this.getItem(key))
        }
        return null;
    }

    storage.removeItem = function(key){
        var exdate=new Date();
        exdate.setDate(exdate.getDate() - 1);
        var data="; expires="+exdate.toUTCString();
        document.cookie=key + "=" + data + "; path=/";
    }

    storage.overrideStorageInPrivateBrowse = function(){

        try {
            localStorage.setItem(mod, mod);
            localStorage.removeItem(mod);
            return true;
        } catch (exception) {
            Storage.prototype.setItem = this.setItem;
            Storage.prototype.getItem = this.getItem;
            Storage.prototype.setObj = this.setObj;
            Storage.prototype.getObj = this.getObj;
            Storage.prototype.removeItem = this.removeItem;
        }
    }
    return storage;

}]);

	
	
	




'use strict';

angular.module('gigSkyWebApp').factory('SimManagerFactory',['$rootScope','$http','authFactory','commonInfoFactory','$analytics','$q',function($rootScope,$http,authFactory,commonInfoFactory,$analytics,$q){

    var simManager = {  };
    simManager.initialized = false;
    simManager.initInProgress = false;
    simManager.currentUser = {};

    simManager.deInitialize = function(){
        simManager.initialized = false;
        simManager.initInProgress = false;
        simManager.currentUser.activeSIM = null;
        sessionStorage.setObj('currentUser',simManager.currentUser);
        console.log("SIM DEINIT");
    };
    $rootScope.$on('onLogIn',simManager.deInitialize);
    simManager.addSIMInternal = function(sim)
    {
        simManager.currentUser.totalSIMs++;
    };

    simManager.updateSIMInternal = function(sim,simId)
    {
        if(!simId)
            simId = sim.simId;

        var activeSIM = simManager.getActiveSIM();
        if(sim.simId == activeSIM.simId)
        {
            simManager.updateActiveSIM(sim);
        }

    };

    function updateSIMListFromServer(start,count){

        // get only visible sims
        simManager.getSIMList(start,count,false, false).success(function(response){
            if(response.totalCount == 0 || !response.list.length)
            {
                $analytics.setUserProperties({dimension1: 'no'});
                simManager.initialized = true;
                simManager.initInProgress = false;
                $rootScope.$broadcast('onNoSIMAvailable');
                return;
            }
            var simList = response.list;

            $analytics.setUserProperties({dimension1: response.totalCount > 1?'multiple':'yes'});

            var simTypeMap={};
            for(var i=0; i<simList.length; i++){
                var simType = simList[i].simType;
                if(!simTypeMap[simType])
                {
                    simTypeMap[simType] = [simList[i]];
                }
                else {
                    simTypeMap[simType].push(simList[i]);
                }
            }

            var orderOfActiveSIM = ['GS_SIM_50','GIGSKY_SIM','TELNA_SIM','ACME_SIM'];
            for(var i=0; i< orderOfActiveSIM.length; i++){
                var list = simTypeMap[orderOfActiveSIM[i]];
                var activeSIM = findActiveSIM(list);
                if(activeSIM)
                   break;
            }

            if(!activeSIM)
            {
                for(var i=0; i< orderOfActiveSIM.length; i++){
                    var list = simTypeMap[orderOfActiveSIM[i]];
                    if(list)
                    {
                        activeSIM = list[0];
                        break;
                    }
                }
                if(!activeSIM)
                {
                    activeSIM = response.list[0];
                }
            }

            function findActiveSIM(list){

                if(!list)
                    return null;
                for(var i=0;i<list.length;i++)
                {
                    var sim = list[i];
                    if(sim.simStatus == 'ACTIVE')
                    {
                        return sim;
                    }
                }
                return null;
            }

            if( !simManager.currentUser.activeSIM || simManager.currentUser.activeSIM.visibilityStatus == "hidden")
                simManager.currentUser.activeSIM = activeSIM;

            simManager.initialized = true;
            simManager.initInProgress = false;
            simManager.currentUser.totalSIMs = response.totalCount;
            sessionStorage.setObj('currentUser',simManager.currentUser);
            $rootScope.$broadcast('onActiveSIMAvailable');


        }).error(function(response,status){
                $rootScope.$broadcast('onSIMInitFailed');
                simManager.initialized = false;
                simManager.initInProgress = false;
            });
    }

    simManager.initialize = function(){

      if(!authFactory.isSessionValid())
            return;

      if(!simManager.initInProgress && !simManager.initialized)
      {
          var currentAuthUserId = authFactory.getUserId();
          simManager.currentUser = sessionStorage.getObj('currentUser');
          console.log("SIM iNit with active sim"+simManager.currentUser.activeSIM);
          if(!simManager.currentUser)
              simManager.currentUser = {userid:currentAuthUserId,totalSIMs:0,activeSIM:null};

          simManager.initInProgress = true;
          if(simManager.currentUser && simManager.currentUser.activeSIM)
          {
              var activeSIM =  simManager.currentUser.activeSIM;//sessionStorage.getObj('activeSIM');
              if(activeSIM)
              {
                  simManager.initialized = true;
                  simManager.initInProgress = false;
                  $rootScope.$broadcast('onActiveSIMAvailable');
              }
          }
          else {
              console.log("Setting Current User with Active SIM Null");
              simManager.currentUser = {userid: currentAuthUserId, totalSIMs: 0, activeSIM: null};

          }

          //updateSIMListFromServer(0,500);
          simManager.getSIMDetails(authFactory.getDeviceId());
      }
      else if(!simManager.initInProgress)
      {
          if(simManager.currentUser.activeSIM)
            $rootScope.$broadcast('onActiveSIMAvailable');
          else
              $rootScope.$broadcast('onNoSIMAvailable');
      }
    };
    simManager.getActiveSIM = function(){
        console.log(" Current Active SIM "+ simManager.currentUser.activeSIM);
        return simManager.currentUser.activeSIM;
    };


    simManager.updateActiveSIM = function(sim){

        var activeSIM = this.getActiveSIM();
        if(activeSIM.simId == sim.simId )
        {
            console.log("Updating Active SIM with "+sim);
            simManager.currentUser.activeSIM = sim;
            sessionStorage.setObj('currentUser',simManager.currentUser);
            $rootScope.$broadcast('onActiveSIMChanged');
        }
    };

    simManager.setActiveSIM = function(sim, hidden ){


        console.log("Setting Active SIM "+sim);
        //var activeSIM = this.getActiveSIM();
        //if(!activeSIM || activeSIM.simId != sim.simId)
        {
            simManager.currentUser.activeSIM = sim;
            var currentAuthUserId = authFactory.getUserId();
            if(simManager.currentUser.userid == undefined)
                simManager.currentUser.userid = currentAuthUserId;
            sessionStorage.setObj('currentUser',simManager.currentUser);
            $rootScope.$broadcast('onActiveSIMChanged');
        }
    };

    simManager.editSIM = function(sim){
        var putData = {description:sim.description};
        var sucFn = function(result){
            var activeSIM = simManager.getActiveSIM();
            if(sim.simId == activeSIM.simId)
            {
                simManager.setActiveSIM(result.data);
            }
            simManager.updateSIMInternal(result.data);
        };
        var editSIMReq =  $http.put(GS_API_BASE+'SIM/'+sim.simId,putData);
        editSIMReq.then(sucFn);
        return editSIMReq;
    };

    simManager.blockSIM = function(sim){
        var putData = {type:"EditSIM",status:"BLOCK"};
        var successfn = function(result){
            simManager.updateSIMInternal(result.data);
        };
        var blockSIMReq =$http.put(GS_API_BASE+'SIM/'+sim.simId+"/simStatus",putData);
        blockSIMReq.then(successfn);
        return blockSIMReq;
    };

    simManager.unBlockSIM = function(sim){
        var putData = {type:"EditSIM",status:"UNBLOCK"};
        var sucFn = function(result){
            simManager.updateSIMInternal(result.data);
        };
        var unBlockSIMReq = $http.put(GS_API_BASE+'SIM/'+sim.simId+"/simStatus",putData);
        unBlockSIMReq.then(sucFn);
        return unBlockSIMReq;
    };

    simManager.hideSIM = function( sim ){
        var putData = {type:"EditSIM",visibilityStatus:"hidden"};

        return  $http.put(GS_API_BASE+'SIM/'+sim.simId+"/simStatus",putData).success( function( response ) {

             var activeSIM = simManager.getActiveSIM();
             if(sim.simId == activeSIM.simId){
                 simManager.deInitialize();
                 simManager.initialize();

             }
         });

    };


    simManager.unhideSIM = function(sim){
        var putData = {type:"EditSIM",visibilityStatus:"visible"};
        var sucFn = function(result){
            simManager.updateSIMInternal(result.data);
        };
        var unBlockSIMReq = $http.put(GS_API_BASE+'SIM/'+sim.simId+"/simStatus",putData);
        unBlockSIMReq.then(sucFn);
        return unBlockSIMReq;
    };

    simManager.getSIMList = function(stIndex,count,incSIMStatus, includeHidden ){
        var url = GS_API_BASE+'SIM?startIndex='+stIndex+'&count='+count;

        if(incSIMStatus ){
            url += "&detail=simStatus";
        }

        // show only the visible sims
        if( includeHidden === false  ) {
            url += "&excludeHidden=true";
        }
        return $http.get( url );
    };

    simManager.syncICCIDs = function(simId,iccidList){

        return $http.post(GS_API_BASE+'SIM/'+simId+"/sync",{"iccIds":iccidList});
    }
    
    simManager.syncAndFetchSIMDetails = function (simId,incSIMStatus,includeAvailableNetworkGroups) {

        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);
        var requests = [];
        var iccidList = authFactory.getICCIDList();
        if(iccidList && iccidList.length){
            iccidList = iccidList.split(",");
        }
        else
        {
            iccidList = [];
        }
        requests.push(simManager.syncICCIDs(simId,iccidList));
        requests.push(simManager.getSIMDetails(simId,incSIMStatus,includeAvailableNetworkGroups));
        $q.all(requests).then(function(results){
            def.resolve({data:results[1].data});
        },function(error){
            def.reject({data:error.data,status:error.status});
        });
        return promise;
    }

    simManager.getSIMDetails = function(simId,incSIMStatus,includeAvailableNetworkGroups){

        var sucFn = function(result){
                simManager.setActiveSIM(result.data);
        };
        var url = GS_API_BASE+'SIM/'+simId;
        if(incSIMStatus) {
            url += "?detail=simStatus";
        }
        if(includeAvailableNetworkGroups)
        {
            if(url.indexOf("?")!=-1)
                url +="&simStatusDetail=availableNetworkGroupInfo";
            else
                url +="?simStatusDetail=availableNetworkGroupInfo";
        }

        var simDetailReq =  $http.get(url);
        simDetailReq.then(sucFn);
        return simDetailReq;
    };

    simManager.getSIMStatus = function(simId,incAvailableNws){
        if(!incAvailableNws)
            return $http.get(GS_API_BASE+'SIM/'+simId+"/simStatus");
        else
            return $http.get(GS_API_BASE+'SIM/'+simId+"/simStatus?detail=availableNetworkGroupInfo");
    };

    simManager.getSubscriptions = function(simId,type,stIndex,count){
        return $http.get(GS_API_BASE+'SIM/'+simId+"/subscriptions?type="+type+"&startIndex="+stIndex+"&count="+count+"&detail=location");
    };

    simManager.getSIMAssociationDetails = function(simId){
        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);

        //def.resolve({data:{status:"ASSOCIATED_CURRENT_ACCOUNT"}});

        return $http.get(GS_API_BASE+'SIM/'+simId+'/associationStatus');

        //return promise;
    }

    simManager.addGSMASIM = function (deviceId) {

        var def = $q.defer();
        var promise = commonInfoFactory.extendHttpPromise(null,null,def);
        var simDescription = "Windows SIM - " + deviceId.substr(deviceId.length-4, deviceId.length);

        var postData = {type:"AddSIM",simType:"MSFT_GSMA_ESIM",deviceId:deviceId,description:simDescription};
        var sucFn = function (result) {
            simManager.setActiveSIM(result);
            def.resolve({data:result});
        }

        //Testing
        //simManager.getSIMDetails(deviceId).success(sucFn).error(function(data,status){
          //  def.reject({data:data,status:status});
        //});

        return $http.post(GS_API_BASE+'SIM',postData).success(sucFn);

        //return promise;
    }
    simManager.addSIM = function(activationCode,name)
    {
        var postData = {type:'AddSIM',activationCode:activationCode,description:name};
        var sucFn = function(result){
            var activeSIM = simManager.getActiveSIM();
            if(!activeSIM)
            {
                $analytics.setUserProperties({dimension1: 'yes'});
                simManager.setActiveSIM(result.data);
                $rootScope.$broadcast('onActiveSIMAvailable');
            }
            else{
                $analytics.setUserProperties({dimension1: 'multiple'});
            }
        };
        var postReq =  $http.post(GS_API_BASE+'SIM',postData);
        postReq.then(sucFn);
        return postReq;
    };

    simManager.replaceSIM = function(simId,activationCode,name)
    {
        var sucFn = function(result){
            simManager.updateSIMInternal(result.data,simId);
        };
        var putData = {type:'replaceSIM',activationCode:activationCode,description:name};
        var replaceSIMReq =  $http.put(GS_API_BASE+'SIM/'+simId,putData);
        replaceSIMReq.then(sucFn);
        return replaceSIMReq;
    };

    simManager.switchToSubscription = function(subId){
        var activeSIM = simManager.getActiveSIM();
        return $http.put(GS_API_BASE+'SIM/'+activeSIM.simId+"/connectUsingSubscription/"+subId);
    };

    simManager.subscriptionBalance = function(sim){
        var s = simManager.getConnectedSubscription(sim);
        if(s)
        {
            return commonInfoFactory.formatBalanceDataSize(s);
        }
        else
        {
            return '';
        }
    };
    simManager.getConnectedSubscription = function(sim){
        if(sim.status && sim.status.connectionStatus == 'CONNECTED' && sim.status.list)
        {
            for(var i= 0,k= sim.status.list.length;i<k;i++)
            {
                var s = sim.status.list[i];
                if(s.subscriptionStatus == 'IN-USE')
                {
                    return s;
                }
            }

            return null;
        }
        else
            return null;
    };

    simManager.connectedNetworkGroupName = function(sim){
        var s = simManager.getConnectedSubscription(sim);
        if(s){
            return s.networkGroupInfo.networkGroupName;
        }
        return '';
    };
    simManager.subscriptionRemainingTime = function(sim){
        var s = simManager.getConnectedSubscription(sim);
        if(s)
        {
            return commonInfoFactory.formatSubRemainingTimeShort(s);
        }
        else
        {
            return '';
        }
    };

    simManager.getSim1TransitionDetails = function(){
        return $http.get(GS_API_BASE+'sim1Replacement/');
    };

    simManager.requestForSim2 = function(data){
        return $http.post(GS_API_BASE+'sim1Replacement/', data);
    };
    simManager.setDefaultSIM = function(data){
        return $http.put(GS_SERVER_BASE+'/gsngw/api/v1/defaultSim/',data);
    };

    simManager.deleteSIM = function(deviceId, sendEmail)
    {
        var url = GS_API_BASE+'SIM/'+deviceId;
        if(sendEmail)
        {
            url = url +"?emailConfirmation=true";
        }
        return $http.delete(url);

    };

    return simManager;

}]);
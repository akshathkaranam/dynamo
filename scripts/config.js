'use strict';

var GS_PROD_ENV = 'p';
var GS_LOCAL_ENV = 'l';
var GS_LOCAL_SSL_ENV = 'ls';
var GS_STAGING_ENV = 's';
var GS_LOCAL_TEST_ENV = 'lt';

var GS_ENVIRONMENT = "p";
var IS_CAPTIVE_MODE = true;
var ENABLE_CONCUR = false;
var ENABLE_LOCALIZATION = true;
var ENABLE_AUTODETECT_LANGUAGE = true;
var ENABLE_SIM1_TO_SIM2_TRANSITION = false;
var LOCALIZE_BASE_PATH = "/app/";
var APPLE_PAY_MODE = 'TEST';
var APPLE_PAY_SANDBOX = APPLE_PAY_MODE !== 'LIVE';
var APP_VERSION_NO = '4.0';
var ENABLE_CURRENCY_EDIT = true;

if(window.location.hostname.indexOf("msapp-stage.gigsky.com")!=-1)
{
    GS_ENVIRONMENT = 'p';
}


// if(!APPLE_PAY_SANDBOX)
// {
//     GS_ENVIRONMENT = "cs";
// }

var DISABLE_CAPTCHA = true;
var SUPPORTED_LANGUAGES = ['ja','en','zh-HK','zh-TW','zh','de','it','fr','es'];
var LANG_SUPPORTED = {'ja':"日本語",'en':"English",'de':"Deutsch",'es':"español",'it':"Italiano",'fr':"français",'zh-HK':"Chinese Hong Kong", 'zh':"Chinese Simplified" , 'zh-TW':"Chinese Traditional"};

var LOCALIZE_BASE_PATH = "/";

var STATIC_BASE_URL;
var CURRENT_LANG = 'en';
var remainingUrl;
var isExplicitLangSelected = false;
var explicitLanguage;


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var currentEnv = getParameterByName("env");
if(currentEnv)
{
    GS_ENVIRONMENT = currentEnv;
}


(function updateStaticBase()
{
    var absUrl = window.location.href;
    var host = window.location.host;
    var protocol = window.location.protocol;
    STATIC_BASE_URL = protocol + "//" + host;

    var i = STATIC_BASE_URL.length;
    remainingUrl = absUrl.substr(i+1,absUrl.length);
    for(var i=0;i<SUPPORTED_LANGUAGES.length;i++){
        var langCode = SUPPORTED_LANGUAGES[i];
        langCode = '/' + langCode;

        var j = absUrl.indexOf(langCode);
        if(j!=-1){
            isExplicitLangSelected = true;
            explicitLanguage = SUPPORTED_LANGUAGES[i];
            STATIC_BASE_URL = STATIC_BASE_URL.substr(0,j);
            break;
        }
    }
})();

/**
 * Redirecting to specific language path based on browser's language
 */
if(ENABLE_AUTODETECT_LANGUAGE) {
    var browserLanguages = window.navigator.languages;
    if(!browserLanguages){
        browserLanguages = (window.navigator.language) ? [window.navigator.language] : [window.navigator.userLanguage];
    }
    var url = window.location.pathname;

    var isLanguageUrlUpdated = false;
    if(isExplicitLangSelected){
        CURRENT_LANG = explicitLanguage;
        LOCALIZE_BASE_PATH = '/' + explicitLanguage + '/';
    }else{
        for(var k=0;k<browserLanguages.length;k++) {
            var browserLang = browserLanguages[k];

            for (var i = 0; i < SUPPORTED_LANGUAGES.length; i++) {
                var langCode = SUPPORTED_LANGUAGES[i];
                var upperCase = langCode.toUpperCase();
                var upperCaseBrowserLang = browserLang.toUpperCase();
                if(upperCaseBrowserLang.indexOf(upperCase) != -1){
                    var langCodeUrl = '/' + langCode + '/';
                    var j = url.indexOf(langCodeUrl);
                    CURRENT_LANG = langCode;
                    if(j == -1 && langCode != 'en'){

                        var finalUrl = STATIC_BASE_URL + langCodeUrl + remainingUrl;
                        window.location.replace(finalUrl);
                    }
                    if(langCode != 'en'){
                        LOCALIZE_BASE_PATH = langCodeUrl;
                    }
                    isLanguageUrlUpdated = true;
                    break;

                }
            }

            if(isLanguageUrlUpdated){
                break;
            }
        }
    }

}else{
    for(var i=0;i<SUPPORTED_LANGUAGES.length;i++){
        var langCode = SUPPORTED_LANGUAGES[i];
        var langCodeUrl = '/' + langCode + '/';

        var j = window.location.pathname.indexOf(langCodeUrl);
        if(j!=-1){
            LOCALIZE_BASE_PATH = langCodeUrl;
            break;
        }
    }
}

var lh = window.location.hostname;

var   APPLE_SIM_URL = '/gigsky-apple-sim/';
var   GIGSKY_SIM_URL = '/gigsky-data-sim';
var   BUSINESS_URL = '/business';
var   WEBAPP_URL = "/";
var   GIGSKY_STATIC_URL = '/';


if (lh.indexOf('app.gigsky.com') != -1) {
    APPLE_SIM_URL = '//gigsky.com/index.php' + APPLE_SIM_URL;
    GIGSKY_SIM_URL = '//gigsky.com/index.php' + GIGSKY_SIM_URL;
    GIGSKY_STATIC_URL = '//gigsky.com'
}else if(lh.indexOf('app-stage.gigsky.com') != -1){
    APPLE_SIM_URL = '//www-stage.gigsky.com/index.php' + APPLE_SIM_URL;
    GIGSKY_SIM_URL = '//www-stage.gigsky.com/index.php' + GIGSKY_SIM_URL;
    GIGSKY_STATIC_URL = '//www-stage.gigsky.com'
}




(function initEnvironment(){
    var envOptions = {pserver:"https://services.gigsky.com",pfbid:"491890567488668",pPPUrl:"https://www.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",pConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",pConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3",preserver:"https://services.gigsky.com",
        csserver:"https://cy-services-stage.gigsky.com",csfbid:"491890567488668",csPPUrl:"https://www.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",csConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",csConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3",csreserver:"https://cy-services-stage.gigsky.com",
        lserver:"http://localhost:8080",lfbid:"421025887947166",lPPUrl:"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",lConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",lConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3",lreserver:"http://localhost:8080",
        ltserver:"http://webclienttestnode:8080",ltfbid:"421025887947166",ltPPUrl:"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",ltConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",ltConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3", ltreserver:"http://webclienttestnode:8080",
        lsserver:"https://localhost",lsfbid:"1379123719081974",lsPPUrl:"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",lsConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",lsConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3",lsreserver:"https://localhost",
        sserver:"https://staging.gigsky.com",sfbid:"353068714791824",sPPUrl:"https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-preapproval&preapprovalkey=",sConcurUrl:"https://www.concursolutions.com/net2/oauth2/Login.aspx",sConcurClientKey:"EBN6pcaTg5MjiTPHkBFJb3",sreserver:"https://staging.gigsky.com"};

    window.GS_SERVER_BASE = envOptions[GS_ENVIRONMENT+'server'];
    window.GS_REFERRAL_SERVER_BASE = envOptions[GS_ENVIRONMENT+'reserver'];
    window.GS_FB_BASE = window.GS_SERVER_BASE+'/gscanvas/';
    window.GS_APP_ID = envOptions[GS_ENVIRONMENT+'fbid'];
    window.PayPAL_URL = envOptions[GS_ENVIRONMENT+'PPUrl'];
    window.CONCUR_URL = envOptions[GS_ENVIRONMENT+'ConcurUrl']+"?client_id="+envOptions[GS_ENVIRONMENT+'ConcurClientKey']+"&scope=ERECPT&redirect_uri="//http://localhost";//+envOptions[GS_ENVIRONMENT+'server'];
})();

var serviceURL = getParameterByName("serviceURL");
if(serviceURL)
{
    window.GS_SERVER_BASE = serviceURL;
    APPLE_PAY_SANDBOX = true;
}

var GS_API_VERSION = "/api/v4/";
var GS_SERVER_BASE_URL = 'http://localhost:8080';
var GS_NON_ACCOUNT_API_BASE = GS_SERVER_BASE_URL + GS_API_VERSION;
//The following varible is changed dynamically after successful login.
var GS_API_BASE= GS_SERVER_BASE_URL + GS_API_VERSION + "account/{customerid}/";
var NUMBER_OF_ITEMS_PER_PAGE = 15;

var GS_REFERRAL_API_VERSION ="/gsre/api/v1/";
var GS_REFERRAL_SERVER_BASE_URL = 'http://localhost:8080';
var GS_REFERRAL_API_BASE = GS_REFERRAL_SERVER_BASE_URL + GS_REFERRAL_API_VERSION ;

var LAZY_LOAD_BASE = "";

languages=(ja fr it zh zh-HK zh-TW de es)
 for i in ${languages[@]}; do
 echo $i;
 `wget "http://cdn.localizejs.com/api/lib/PeDs8xcEXPPwd/g?l=$i" -O translations-$i.txt`
echo "<script type=\"text/javascript\">Localize.bootstrap(" > t-$i.la
 cat translations-$i.txt >> t-$i.la
 echo ");</script>" >> t-$i.la
 mv t-$i.la $i.js
 done
 rm t-*
 rm translations-*
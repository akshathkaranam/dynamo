rm -fr temptemplatefiles.html
touch temptemplatefiles.html
while read file;
do
if [[ -f "$file" ]];then
    echo "File exists $file"
    echo "<script type=\"text/ng-template\" id=\"$file\">" >> temptemplatefiles.html
    java -jar ./buildscripts/htmlcompressor-1.5.3.jar $file > temp.html
   cat temp.html >> temptemplatefiles.html
   echo '</script>' >> temptemplatefiles.html
fi
done < $1
echo '</body></html>' >> temptemplatefiles.html
sed -i .bk 's/<\/body>//g' index.html
sed -i .bk 's/<\/html>//g' index.html
cat temptemplatefiles.html >> index.html
rm -fr temptemplatefiles.html
rm -fr index.html.bk
rm temp.html
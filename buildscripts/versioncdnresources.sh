#!/bin/bash
#Directory where all versioned CDN Assets are copied, retaining folder structure.
cdnassetsdir="cdnassets"

if [[ $# != 2 ]]; then
 echo "Invalid arguments !!!!!!!"
 echo "Host name should be in the form <scheme>://<host>:<port>"
 echo "Execute as "
 echo "versionassets.sh <cdnassetslist> <cdnhostname>"
 exit 1
fi

#List of assets needed visioning and hosting on CDN
cdnconfigfile=$1
cdnhost=$2

[ -d $cdnassetsdir ] || mkdir $cdnassetsdir

function relativePath {
assetpath=(${1//\//" "});
cssorhtmlpath=(${2//\//" "});
i=0
while [[ $i < ${#cssorhtmlpath[*]} && ${assetpath[$i]} == ${cssorhtmlpath[$i]} ]]; do
  i=$(( $i + 1));
done
j=$i;
relativepath="."
while [[ $i < $((${#cssorhtmlpath[*]} - 1)) ]]; do
  i=$(( $i + 1));
  relativepath=$relativepath"/..";
done

while [[ $j < ${#assetpath[*]} ]]; do
  relativepath=$relativepath/${assetpath[$j]}
  j=$(( $j + 1));
done

echo $relativepath

}

function updateHtmlContent
{
     result=1
     result2=1
     resource=$1
     `cat $resource | grep "</html>" > /dev/null` #If non template html file apply relative
      if [ $? -eq 0 ] ; then
          resource=${resource/\.\//}
          relativepath=`relativePath $file $resource`;
          echo "Relative Path is $relativepath and resource is $resource"
          relativeescpath=${relativepath//\//\\\/} #Add escape chars
          sed -i .bkup "s/\([\"\']\)\{0,1\}[\t ]*$relativeescpath[\t ]*\([\"\']\)\{0,1\}/\1$esccdnvfile\2/g" $resource #Search and replace all the references of file with versioned one.
          #Remove begining ./ which can also lead to valid relative path.
          relativepath=${relativepath/\.\//}
          echo "Relative Path2 is $relativepath and resource is $resource"
          #Escape relative path
          relativeescpath=${relativepath//\//\\\/}
 #         sed -i .bkup "s/\"[\t ]*$relativeescpath[\t ]*\"/\"$esccdnvfile\"/g" $resource #Search and replace all the references of file with versioned one.
          sed -i .bkup "s/\([\"\']\)\{0,1\}[\t ]*$relativeescpath[\t ]*\([\"\']\)\{0,1\}/\1$esccdnvfile\2/g" $resource #Search and replace all the references of file with versioned one.

      else
          sed -i .bkup "s/\([\"\']\)\{0,1\}[\t ]*$escfile[\t ]*\([\"\']\)\{0,1\}/\1$esccdnvfile\2/g" $resource #Search and replace all the references of file with versioned one.
      fi
      rm -fr $resource.bkup #Remove backup file created in above step

     return 0
}

function updateCSSContent
{
    resource=$1
    resource=${resource/\.\//}
    #Find the relative path of asset from the resource location.
    #Eg: If asset is /styles/images/xy.img adn resource is /styles/css/app/app.css then relative path should be ../../images

    relativepath=`relativePath $file $resource`;

    relativeescpath=${relativepath//\//\\\/} #Add escape chars
    echo "Relative Path is $relativepath and resource is $resource"
    sed -i .bkup "s/([\t ]*\([\"\']\)\{0,1\}[\t ]*$relativeescpath[\t ]*\(\?v=.*\)*\([\"\']\)\{0,1\}[\t ]*)/(\1$esccdnvfile\2\3)/g" $resource
    #sed -i .bkup "s/([\t ]*[\"\'][\t ]*$relativeescpath[\t ]*\(\?v=.*\)?[\"\'][\t ]*)/(\"$esccdnvfile\1\")/g" $resource #Search and replace all the references of file with versioned one.
    #result=$?
    #Remove begining ./ which can also lead to valid relative path.
    relativepath=${relativepath/\.\//}
    #Escape relative path
    relativeescpath=${relativepath//\//\\\/}
    echo "Relative Path2 is $relativepath and resource is $resource"
    sed -i .bkup "s/([\t ]*\([\"\']\)\{0,1\}[\t ]*$relativeescpath[\t ]*\(\?v=.*\)*\([\"\']\)\{0,1\}[\t ]*)/(\1$esccdnvfile\2\3)/g" $resource

    #sed -i .bkup "s/([\t ]*[\"\'][\t ]*$relativeescpath[\t ]*\(\?v=.*\)?[\"\'][\t ]*)/(\"$esccdnvfile\1\")/g" $resource #Search and replace all the references of file with versioned one.

    #sed -i .bkup "s/([\t ]*[\"\'][\t ]*$relativeescpath[\t ]*[\"\'][\t ]*)/(\"$esccdnvfile\")/g" $resource #Search and replace all the references of file with versioned one.
    #sed -i .bkup "s/url([\t ]*$relativeescpath[\t ]*)/url(\"$esccdnvfile\")/g" $resource #Search and replace all the references of file with versioned one.

    rm -fr $resource.bkup #Remove backup file created in above step
    return 0
}


function copyToCDNAssets
{
    echo "Copying file  $1 to $2 And this is enabled to compress $3"
    path=$1
    vpath=$2
    compressfile=$3
    filebasepath=${path%/*}
    filename=${path##.*/}
    cdnfilepath="$cdnassetsdir/$filebasepath"
    echo "$cdnfilepath"
    [ -d $cdnfilepath ] || mkdir -p $cdnfilepath
    cp $path $cdnassetsdir/$vpath
    if [[ $compressfile -eq 1 ]]
    then
        gzip -9k $cdnassetsdir/$vpath
    fi
}

function versionOtherAssets
{
      result=1
      resourcelist=`find . \( -name "*.css" -o -name "*.html" \) -maxdepth 3`
      for resource in $resourcelist
      do
          resourceExt=`expr "$resource" : '.*\(\..*\)'`
          resourceType=${resourceExt/\./}
          if [[ $resourceType == "html" ]]; then
              updateHtmlContent $resource
          else
              updateCSSContent $resource
          fi
      done

      copyToCDNAssets $file $vfile 0 #Copy the file to CDN Assets folder maintaining folder structure.

}


function versionJSCSS
{
  echo "File is JS file"
  htmllist=`find . -name \*.html`
  for resource in $htmllist
  do
    updateHtmlContent $1 $2 $resource
  done
  copyToCDNAssets $file $vfile 1 #Copy the file to CDN Assets folder maintaining folder structure.
}


echo >> $1
#Reads all the asset files from cdnconfigfile and appends svn version in all the occurences of such file.
#

while read file;
do
if [[ -f "$file" ]];then
    #Finds latest svn revision of the file
    svnfile=$file
    if [[ $svnfile =~ .*@.* ]];then
        svnfile=$svnfile@
    fi
    revision=`svn info $svnfile | grep "Last Changed Rev:" | awk  -F': '  '{print $2}'`
    fileType="other"
    #Finds type of file
    fileExt=`expr "$file" : '.*\(\..*\)'`
    fileType=${fileExt/\./}
    vfile=`echo "$file" | sed "s/\.$fileType$/\.$revision\.$fileType/g"`
    escfile=${file//\//\\\/}    #Add escape chars to path seperators.
    cdnvfile="//$cdnhost/$vfile" #Absolute file path including version in CDN Assets folder

    echo "FILE IS "$file
    esccdnvfile=${cdnvfile//\//\\\/} #Add escape chars
    if [[ "$fileType" == "js" || "$fileType" == "css" ]]; then

        if [[ $file =~ .*\.min\.$fileType ]];then
            echo "File $file Is already minified."
        else
            java -jar ./buildscripts/yuicompressor-2.4.8.jar --line-break 0 $file -o '.$fileType$:.$fileType'
        fi
        echo "Versioning js/css file $vfile"
        versionJSCSS
    else if [[ "$fileType" == "map" ]]; then
       copyToCDNAssets $file $file 1
    else
       versionOtherAssets
    fi
    fi
else
if [[ "$file" != "" ]];then
    if [[ $file =~ .*/\*\.png || $file =~ .*/\*\.jpg || $file =~ .*/\*\.jpeg || $file =~ .*/\*\.\* ]];then
      escfile=${file//\//\\\/}    #Add escape chars to path seperators.
      imgresources=`find . -path "*$file" -maxdepth 4`
      for file in $imgresources
      do
          file=${file/\.\//}
          echo "Found other asset $file"
          svnfile=$file
          echo "SVN FILE is" $svnfile
          if [[ $svnfile =~ .*@.* ]];then
              svnfile=$svnfile@
          fi
          revision=`svn info $svnfile | grep "Last Changed Rev:" | awk  -F': '  '{print $2}'`
          fileType="other"
          #Finds type of file
          fileExt=`expr "$file" : '.*\(\..*\)'`
          fileType=${fileExt/\./}
          vfile=`echo "$file" | sed "s/\.$fileType$/\.$revision\.$fileType/g"`
          escfile=${file//\//\\\/}    #Add escape chars to path seperators.
          cdnvfile="//$cdnhost/$vfile" #Absolute file path including version in CDN Assets folder
          esccdnvfile=${cdnvfile//\//\\\/} #Add escape chars

          versionOtherAssets
      done
    fi
else
echo "CDN asset: $file is missing in repository"
#exit 1
fi
fi
done < $1
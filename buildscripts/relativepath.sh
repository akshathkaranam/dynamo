function relativePath {
assetpath=(${1//\//" "});
cssorhtmlpath=(${2//\//" "});
i=0
while [[ $i < ${#cssorhtmlpath[*]} && ${assetpath[$i]} == ${cssorhtmlpath[$i]} ]]; do
  i=$(( $i + 1));
done
j=$i;
relativepath="."
while [[ $i < $((${#cssorhtmlpath[*]} - 1)) ]]; do
  i=$(( $i + 1));
  relativepath=$relativepath"/..";
done

while [[ $j < ${#assetpath[*]} ]]; do
  relativepath=$relativepath/${assetpath[$j]}
  j=$(( $j + 1));
done

echo $relativepath

}
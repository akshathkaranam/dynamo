#!/bin/bash
#Directory where all versioned CDN Assets are copied, retaining folder structure.
cdnassetsdir="cdnassets"

if [[ $# != 2 ]]; then
 echo "Invalid arguments !!!!!!!"
 echo "Host name should be in the form <scheme>://<host>:<port>"
 echo "Execute as "
 echo "versionassets.sh <cdnassetslist> <cdnhostname>"
 exit 1
fi

#List of assets needed visioning and hosting on CDN
cdnconfigfile=$1
cdnhost=$2
esccdnpath=${cdnhost//\//\\\/} #Add escape chars

[ -d $cdnassetsdir ] || mkdir $cdnassetsdir

function updateHtmlContent
{
     resource=$1
     sed -i .bkup "s/\([\"\']\)\{1\}[\t ]*\($imgstarts\/.*.[png|jpg|jpeg|JPEG|gif]\)[\t ]*\([\"\']\)\{1\}/\1\/\/$esccdnpath\/\2\3/g" $resource #Search and replace all the references of file with versioned one.
     rm -fr $resource.bkup #Remove backup file created in above step
}

function versionOtherAssets
{
      result=1
      resourcelist=`find . \( -name "*.html" \) -maxdepth 3`
      for resource in $resourcelist
      do
          resourceExt=`expr "$resource" : '.*\(\..*\)'`
          resourceType=${resourceExt/\./}
          updateHtmlContent $resource
      done
}

echo >> $1
#Reads all the asset files from cdnconfigfile and appends svn version in all the occurences of such file.
#
while read imgstarts;
do
if [[ "$imgstarts" != "" ]];then
versionOtherAssets
fi
done < $1
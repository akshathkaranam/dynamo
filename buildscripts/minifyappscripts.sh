cd=`pwd`
file=$0
cdnhost=$1
appversion=$2
if [[ $# != 2 ]] ; then
echo "Invalid arguments"
echo "Give cdn host name and version number of app js as input argument"
echo "minifyappscripts.sh <cdnhost> <versionno> "
exit 1
fi
basepath=${file%/*}
scriptspath=$cd/$basepath
cd $scriptspath
cat ../scripts/config.js ../scripts/app.js ../scripts/controllers/*.js ../scripts/services/*.js > apptemp.js
sed -i bkup 's/
//g' apptemp.js
java -jar yuicompressor-2.4.8.jar --line-break 0 apptemp.js -o '.js$:.min.js'
sed -E -i bkup '/.*<script.*scripts\/(controllers|services|config\.js).*/d' ../index.html
[ -d "../cdnassets/scripts" ] || mkdir -p ../cdnassets/scripts
mv apptemp.min.js ../cdnassets/scripts/app.min.$appversion.js
gzip -9k ../cdnassets/scripts/app.min.$appversion.js
vfile=scripts/app.min.$appversion.js
cdnvfile="//$cdnhost/$vfile" #Absolute file path including version in CDN Assets folder
esccdnvfile=${cdnvfile//\//\\\/} #Add escape chars
echo "CDN FILE"$esccdnvfile
file=scripts/app.js
escfile=${file//\//\\\/}    #Add escape chars to path seperators.
echo "s/$escfile/$esccdnvfile/g"
`sed -i bkup "s/$escfile/$esccdnvfile/g" ../index.html`
rm apptemp.js
java -jar htmlcompressor-1.5.3.jar --compress-js ../index.html > index.min.html
mv index.min.html ../index.html
#java -jar htmlcompressor-1.5.3.jar --compress-js ../setup-account.php > setup-account.min.php
#mv setup-account.min.php ../setup-account.php
#java -jar htmlcompressor-1.5.3.jar --compress-js ../manage-account.php > manage-account.min.php
#mv manage-account.min.php ../manage-account.php

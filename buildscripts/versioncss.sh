#!/bin/bash
#Directory where all versioned CDN Assets are copied, retaining folder structure.
cdnassetsdir="cdnassets"

if [[ $# != 2 ]]; then
 echo "Invalid arguments !!!!!!!"
 echo "Host name should be in the form <scheme>://<host>:<port>"
 echo "Execute as "
 echo "versionassets.sh <cdnassetslist> <cdnhostname>"
 exit 1
fi

#List of assets needed visioning and hosting on CDN
cdnconfigfile=$1
cdnhost=$2

[ -d $cdnassetsdir ] || mkdir $cdnassetsdir

function updateHtmlContent
{
     resource=$1
     sed -i .bkup "s/\([\"\']\)\{1\}[\t ]*\($escfile\)[\t ]*\([\"\']\)\{1\}/\1$esccdnvfile\3/g" $resource #Search and replace all the references of file with versioned one.
     rm -fr $resource.bkup #Remove backup file created in above step
}

function versionOtherAssets
{
      result=1
      resourcelist=`find . \( -name "*.html" \) -maxdepth 3`
      for resource in $resourcelist
      do
          resourceExt=`expr "$resource" : '.*\(\..*\)'`
          resourceType=${resourceExt/\./}
          updateHtmlContent $resource
      done
}

echo >> $1

while read file;
do
if [[ "$file" != "" ]];then
          file=${file/\.\//}
          echo "Found other asset $file"
          escfile=${file//\//\\\/}    #Add escape chars to path seperators.
          cdnvfile="//$cdnhost/$file" #Absolute file path including version in CDN Assets folder
          esccdnvfile=${cdnvfile//\//\\\/} #Add escape chars
          if [[ $file =~ .*\.min\.css ]];then
               echo "File $file Is already minified."
          else
               java -jar ./buildscripts/yuicompressor-2.4.8.jar --line-break 0 $file -o '.css$:.css'
          fi
          versionOtherAssets
fi
done < $1

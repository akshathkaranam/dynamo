#!/bin/bash
#Directory where all versioned CDN Assets are copied, retaining folder structure.
cdnassetsdir="cdnassets"

if [[ $# != 2 ]]; then
 echo "Invalid arguments !!!!!!!"
 echo "Host name should be in the form <scheme>://<host>:<port>"
 echo "Execute as "
 echo "versionassets.sh <cdnhostname> <buildnumber>"
 exit 1
fi



#List of assets needed visioning and hosting on CDN
cdnhost=$1
buildno=$2

[ -d $cdnassetsdir ] || mkdir $cdnassetsdir

function copyToCDNAssets
{
    echo "Copying file  $1 to $2 And this is enabled to compress $3"
    path=$1
    vpath=$2
    compressfile=$3
    filebasepath=${path%/*}
    filename=${path##.*/}
    cdnfilepath="$cdnassetsdir/$filebasepath"
    echo "$cdnfilepath"
    [ -d $cdnfilepath ] || mkdir -p $cdnfilepath
    cp $path $cdnassetsdir/$vpath
    if [[ $compressfile -eq 1 ]]
    then
        gzip -9k $cdnassetsdir/$vpath
    fi
}

function replaceSourceEnFile
{
    file=$1
    vfile=$2
    resource=$3
    escfile=${file//\//\\\/}    #Add escape chars to path seperators.
    cdnvfile="//$cdnhost/$vfile" #Absolute file path including version in CDN Assets folder
    esccdnvfile=${cdnvfile//\//\\\/}
    sed -i .bkup "s/src=\"$escfile\"/src=\"$esccdnvfile\"/g" $resource
}


copyToCDNAssets scripts/data/locale-en.json scripts/data/locale-$buildno-en.json
copyToCDNAssets scripts/data/locale-ja.json scripts/data/locale-$buildno-ja.json
copyToCDNAssets scripts/data/locale-zh.json scripts/data/locale-$buildno-zh.json
copyToCDNAssets scripts/data/static/support-en.json scripts/data/static/support-$buildno-en.json
copyToCDNAssets scripts/data/static/support-ja.json scripts/data/static/support-$buildno-ja.json
copyToCDNAssets scripts/data/static/termsandconditions-en.json scripts/data/static/termsandconditions-$buildno-en.json
copyToCDNAssets scripts/data/static/termsandconditions-ja.json scripts/data/static/termsandconditions-$buildno-ja.json
copyToCDNAssets scripts/data/static/privacypolicy-en.json scripts/data/static/privacypolicy-$buildno-en.json
copyToCDNAssets scripts/data/static/privacypolicy-ja.json scripts/data/static/privacypolicy-$buildno-ja.json


replaceSourceEnFile  "scripts/data/static/support-en.json" "scripts/data/static/support-$buildno-en.json" "support.html"
replaceSourceEnFile  scripts/data/static/termsandconditions-en.json scripts/data/static/termsandconditions-$buildno-en.json terms-and-conditions.html
replaceSourceEnFile  scripts/data/static/privacypolicy-en.json scripts/data/static/privacypolicy-$buildno-en.json privacy-policy.html
replaceSourceEnFile  scripts/data/locale-en.json scripts/data/locale-$buildno-en.json index.html
tempcdnhost="//$cdnhost"
esccdnhost=${tempcdnhost//\//\\\/}
sed -i .bkup "s/scripts\/data\/locale-/$esccdnhost\/scripts\/data\/locale-$buildno-/g" scripts/app.js
sed -i .bkup "s/'scripts\/data\/static\/'\+i18Substr/'$esccdnhost\/scripts\/data\/static\/'\+i18Substr\+'$buildno-'/g" scripts/staticapp.js


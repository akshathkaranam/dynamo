#!/bin/bash

#development
#base = "https://localhost:8443/webclient/";
#appid = "495138660529683";
#QA
#base = "https://localhost:8443/gscanvas/";
#appid = "353068714791824";
if [ $# -ne 1 ]
then
echo "usage is : configureweb.sh <option>"
echo "Options available are: "
echo "p   - Configures production environment"
echo "q   - Configures QA environment"
echo "l   - Configures Local environment"
echo "ls   - Configures Local Secure environment"
echo "t   - Configures test automation environment"
echo "lt  - Configure local test automation environment"
exit 1
fi

machine="mac"
if [ "$(uname)" == "Darwin" ]; then
machine="mac"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
machine="linux"
fi

envtype=$1

echo "configuring dynamo client to $envtype environment"


buildno=`cat versiondetails.json | sed -n 's/.*buildno":"\([0-9]*\)\".*$/\1/p'`


if [[ "$envtype" == "l" || "$envtype" == "la" ]]; then
    sed -i.bkup 's/var[ \t]*GS_ENVIRONMENT[ \t]*=[ \t]*.*;/var GS_ENVIRONMENT = "'l'";/g' appassets/$buildno/scripts/config.js
    if [[ $envtype == "la" ]]; then
    sed -i.bkup 's/var[ \t]*DISABLE_CAPTCHA[ \t]*=[ \t]*.*;/var DISABLE_CAPTCHA = true;/g' appassets/$buildno/scripts/config.js
    fi
    sed -i.bkup 's/var[ \t]*LAZY_LOAD_BASE[ \t]*=[ \t]*.*;/var LAZY_LOAD_BASE = "'"\/\/localhost\/appassets\/$buildno\/"'";/g' appassets/$buildno/scripts/config.js
    sed -i.bkup 's/var[ \t]*APPLE_PAY_SANDBOX[ \t]*=[ \t]*.*;/var APPLE_PAY_SANDBOX = true;/g' appassets/$buildno/scripts/config.js

    if [[ $machine == "mac" ]]; then
        find . \( -name "*.css" -o -name "*.html" \) -maxdepth 10 -exec sed -i "" 's/cdn.gigsky.com\/webapp/localhost\/appassets/g' {} + ;
        else
        find . \( -name "*.css" -o -name "*.html" \) -exec sed -i 's/cdn.gigsky.com\/webapp/localhost\/appassets/g' {} +;
    fi

     if [[ $machine == "mac" ]]; then
                find . \( -name "*.css" -o -name "index.html" \) -maxdepth 10 -exec sed -i "" 's/config-\([a-z]\)\{1,2\}.js/config.js/g' {} + ;
                else
                find . \( -name "*.css" -o -name "index.html" \) -exec sed -i 's/config-\([a-z]\)\{1,2\}.js/config.js/g' {} +;
     fi

else
            sed -i.bkup 's/var[ \t]*GS_ENVIRONMENT[ \t]*=[ \t]*.*;/var GS_ENVIRONMENT = "'$envtype'";/g' scripts/config.js

            if [[ $machine == "mac" ]]; then
            find . \( -name "captive.html" \) -maxdepth 10 -exec sed -i "" 's/config\([-]\)*\([a-z]\)\{0,2\}.js/config-ca.js/g' {} + ;
            find . \( -name "index.html" \) -maxdepth 10 -exec sed -i "" 's/config\([-]\)*\([a-z]\)\{0,2\}.js/config-'$envtype'.js/g' {} + ;
            else
            find . \( -name "captive.html" \) -exec sed -i 's/config\([-]\)*\([a-z]\)\{0,2\}.js/config-ca.js/g' {} +;
            find . \( -name "index.html" \) -exec sed -i 's/config\([-]\)*\([a-z]\)\{0,2\}.js/config-'$envtype'.js/g' {} +;
            fi

fi



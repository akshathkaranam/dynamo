FROM alpine:latest
RUN \
    apk add --no-cache \
    apache2-proxy \
    apache2-ssl \
    apache2-utils \
    curl \
    logrotate \
    openssl
RUN apk update && apk upgrade && apk add apache2 && mkdir -p /run/apache2 && \
    apk add php7-common php7-session php7-iconv php7-json php7-gd php7-curl php7-simplexml php7-xml php7-mysqli php7-imap php7-cgi fcgi php7-pdo php7-pdo_mysql php7-soap php7-xmlrpc php7-posix php7-mcrypt php7-gettext php7-ldap php7-ctype php7-dom && \
    apk update && apk upgrade && apk add bash apache2 php7-apache2 php7 php7-phar php7-json php7-iconv php7-openssl
COPY webapp /var/www/localhost/htdocs/
RUN chmod 755 /var/www/localhost/htdocs/prepare.sh
RUN chown -R apache:apache /var/www/localhost/htdocs/
EXPOSE 80
WORKDIR /var/www/localhost/htdocs
#CMD ["/usr/sbin/httpd","-k","start","-D","FOREGROUND"]
ENTRYPOINT ["/bin/sh","./prepare.sh"]
CMD ["app.properties"]




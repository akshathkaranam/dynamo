caymancdn=$1
buildno=$2
#./buildscripts/versioncdnresources.sh ./buildscripts/cdnassets-other.conf $caymancdn
#./buildscripts/versioncdnresources.sh ./buildscripts/cdnassets-css.conf $caymancdn
#./buildscripts/mergethirdpartyjs.sh ./buildscripts/thirdparty-js.conf $caymancdn $buildno thirdparty index.html
#./buildscripts/mergethirdpartyjs.sh ./buildscripts/application-js.conf $caymancdn $buildno application index.html

./buildscripts/versionimages.sh ./buildscripts/cdnassets-other.conf $caymancdn
./buildscripts/versioncss.sh ./buildscripts/cdnassets-css.conf $caymancdn

cp -rf static cdnassets/
cp -rf styles cdnassets/
cp -rf bower_components cdnassets/

./buildscripts/mergethirdpartyjs.sh ./buildscripts/thirdparty-js.conf $caymancdn $buildno thirdparty index.html
./buildscripts/mergethirdpartyjs.sh ./buildscripts/application-js.conf $caymancdn $buildno application index.html
